<?php if(!isset($_SESSION)) { session_start(); } ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="google-signin-scope" content="profile email">
<meta name="google-signin-client_id" content="292751117875-7hc3ra4u3lha2fkmv44fpns2v2ko65mm.apps.googleusercontent.com">
<link href="css/css.css" rel="stylesheet" type="text/css" />
<title>.: Sistema general de estad&iacute;stica :.</title>
<script language="javascript" src="script/datosxml.js"></script>
<script src="https://apis.google.com/js/platform.js" async defer></script>
<script language="javascript">
function verif() {
document.getElementById('datos').style.display='none';
}
</script>
</head>
<body>
<div id="datos" align="center">
<h4>Esperando datos de usuario ...<br />
<img src="img/loader.gif" width="16" height="16" /></h4>
</div>
<div style="clear:both; margin-top:10px;" align="center" id="users"></div>
<div style="clear:both;" align="center" id="sesion">
<p>&nbsp;</p>
<p>&nbsp;</p>
<h3 align="center">Utilice sus datos de correo electrónico para iniciar sesi&oacute;n en la herramienta</h3>
    <div class="g-signin2" data-onsuccess="onSignIn" data-theme="dark" data-title="BibloRed" data-width="200" data-longtitle="true"></div>
    <p>&nbsp;</p>
    <div align="center">Haga clic en el bot&oacute;n de google para acceder al servicio de autenticaci&oacute;n con su cuenta de correo institucional</div>
    <script>
      var profile;
	  function onSignIn(googleUser) {
        profile = googleUser.getBasicProfile(); 
		var dominio = profile.getEmail();
		dominio = dominio.split("@");
		if(dominio[1] != "biblored.gov.co")
		{
			alert("La cuenta de correo no es válida. @"+dominio[1]+"\n Cierre la sesión y vuelva a ingresar,\n con una cuenta @biblored.gov.co");
			document.getElementById("log_out").style.display = "";
		}
		else
		{
			
			var id_token = googleUser.getAuthResponse().id_token;
        	console.log("ID Token: " + id_token);
			document.getElementById("users").innerHTML = "<h4>Nombre de usuario: " + profile.getName() + "</h4>";
			document.getElementById("users").innerHTML += "<h4>Cuenta de usuario: " + profile.getEmail() + "</h4>";
			http.open("GET", "script/authxml.php?tab="+profile.getEmail()+"&camp="+profile.getName(), false); //False para que no continue el flujo hasta compeltar la petición ajax
			http.onreadystatechange = useHttpResp;
			return http.send(null);
			
			// Useful data for your client-side scripts:
			/*
			console.log("ID: " + profile.getId()); // Don't send this directly to your server!
			console.log('Full Name: ' + profile.getName()); //Nombre completo
			console.log('Given Name: ' + profile.getGivenName()); //Nombre (s)
			console.log('Family Name: ' + profile.getFamilyName()); //Apellido (s)
			console.log("Image URL: " + profile.getImageUrl());
			console.log("Email: " + profile.getEmail()); */
		}
		document.getElementById("sesion").style.display = "none";
        // The ID token you need to pass to your backend:
        
		//alert(id_token)
      };
	  function useHttpResp() {
		if(http.readyState == 4)
		{
			if(http.status == 200)
			{
				var timeValue = http.responseXML.getElementsByTagName("timenow")[0];
	    		if(timeValue.childNodes.length > 0)
				{
					document.getElementById('sesion').style.display = "none";
					verif();
					var regs = timeValue.childNodes[0].nodeValue;
					regs = regs.split(";");
					if(parseInt(regs[0]) > 0)
					{
						//window.parent.des_menu();
						window.parent.location = "index.php";
						//document.getElementById("users").innerHTML += "<h4>Biblioteca de usuario: " + regs[1] + "</h4>";
					}
					else
					{
						alert("No se ha podido autenticar la cuenta, \n informe al director de nodo o CST \n para verificar los permisos de acceso.")
						document.getElementById("users").innerHTML += "<h4>No se ha podido autenticar la cuenta, informe al director de nodo o CST para verificar los permisos de acceso.</h4>";
						document.getElementById('log_out').style.display = "";
					}
				}
			}
		} 
		else
		{
			document.getElementById('datos').style.display = "";
		}
		}
    </script>
</div>
<div id="log_out" style="display:none; clear:both;" align="center">
<p>&nbsp;</p>
<h3>Cierre la sesión y vuelva a empezar.</h3>
<h3>
<a href="https://accounts.google.com/Logout" onclick="signOut();" target="_blank">Cerrar sesión</a>
</h3>
<script>
  function signOut() {
	var auth2 = gapi.auth2.getAuthInstance();
	auth2.disconnect();
    auth2.signOut().then(function () {
      //console.log('User signed out.');
    });
	document.location = "log_out.php";
	parent.contenido.location = "fill.php";
  }
</script>
</div>
</body>
</html>
