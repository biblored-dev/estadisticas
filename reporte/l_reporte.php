<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/contenedor.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php
include("../script/scripts/session.php");
//include("../script/breadcrumbs.php");
?>
<link href="../css/css.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../script/scripts/jquery.min.js"></script>
<script type="text/javascript" src="../script/menu_ocultar.js"></script>
<script type="text/javascript">
function verif() {
document.getElementById('datos').style.display='none';
}
function foco_in(z) {
document.getElementById(z).focus();
}
</script>
<title>.: Sistema general de estad&iacute;stica :.</title>
<link rel="shortcut icon" href="../img/favicon.ico" />
<!-- InstanceBeginEditable name="doctitle" -->
<link href="../menu/css/css.css" rel="stylesheet" type="text/css" />
<script src="../script/c_color.js"></script>
<script language="javascript">
function serv_mensual(w,x) {
var z = window.open(w, 'Servicio mensual', 'resizable=yes,menubar=no,scrollbars=yes,width=820,height=720');
z.focus();
}
function soport_mensual(w) {
var zx = window.open(w, 'Soportes mensuales', 'resizable=yes,menubar=no,scrollbars=yes,width=660,height=400');
zx.focus();
}
</script>
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" --><!-- InstanceEndEditable -->
</head>
<body>
<div id="datos" align="center">
 <h4>Consolidando datos...<br />
 <img src="../img/loader.gif" width="16" height="16" /></h4>
 </div>
<div align="left"><?php //echo breadcrumbs(); ?></div>
<div align="center" class="popup">
<!-- InstanceBeginEditable name="contenidos" -->
<?php
require_once("../Connections/conect.inc.php");
$elimina = $modifica = "";
if($_GET["modifica"] == "_talleres.php")
{
	$sql = "select estadistica.Id, estadistica.Fecha, estadistica.Hora, estadistica.Participantes, estadistica.Responsable, estadistica.Tipo, estadistica.Actividad, estadistica.Descripcion, programacion.Sesiones, programacion.Observacion from estadistica, programacion where estadistica.Biblioteca = ".$_GET["bib"]." and estadistica.Id_Report = ".$_GET["id_rep"]." and estadistica.Id_Report = programacion.Id";
	$dato = "Tiempo / Cantidad";
}
elseif($_GET["modifica"] == "_ponderado.php")
{
	$sql = "select ponderado.Id, ponderado.Fecha, ponderado.Ponderado as Participantes, ponderado.Responsable, ponderado.Tipo, ponderado.Actividad, ponderado.Descripcion, programacion.Sesiones, programacion.Observacion from ponderado, programacion where ponderado.Biblioteca = ".$_GET["bib"]." and ponderado.Id_Report = ".$_GET["id_rep"]." and ponderado.Id_Report = programacion.Id";
	$dato = "Ponderado";
}
elseif($_GET["modifica"] == "_consolidado.php" || $_GET["modifica"] == "_p_bibliotecologo.php")
{
	$sql = "select consolidado.Id, consolidado.Fecha, consolidado.Cantidad as Participantes, consolidado.Responsable, consolidado.Tipo, consolidado.Actividad, consolidado.Descripcion, programacion.Sesiones, programacion.Observacion from consolidado, programacion where consolidado.Biblioteca = ".$_GET["bib"]." and consolidado.Id_Report = ".$_GET["id_rep"]." and consolidado.Id_Report = programacion.Id";
	$dato = "Consolidado";
	if($_GET["modifica"] == "_p_bibliotecologo.php")
	{
		$dato = "Preguntas resueltas";
		$_GET["modifica"] = "_consolidado.php";
	}
}
elseif($_GET["modifica"] == "_afiliado.php")
{
	$sql = "select afiliaciones.Id, afiliaciones.Fecha_R as Fecha, (afiliaciones.Afi_F + afiliaciones.Afi_M + afiliaciones.Afi_NI) as Participantes, afiliaciones.Responsable, '1' as Tipo, '1' as Actividad, afiliaciones.Descripcion, programacion.Sesiones, programacion.Observacion from afiliaciones, programacion where afiliaciones.Biblioteca = ".$_GET["bib"]." and afiliaciones.Id_Report = ".$_GET["id_rep"]." and afiliaciones.Id_Report = programacion.Id";
	$dato = "Afiliaciones";
}
elseif($_GET["modifica"] == "_visitas.php")
{
	$sql = "select visitas.Id, visitas.Fecha_R as Fecha, sum(visitas.Visitantes) as Participantes, visitas.Responsable, '1' as Tipo, '1' as Actividad, visitas.Descripcion, programacion.Sesiones, programacion.Observacion from visitas, programacion where visitas.Biblioteca = ".$_GET["bib"]." and visitas.Id_Report = ".$_GET["id_rep"]." and visitas.Id_Report = programacion.Id group by visitas.Id_Report";
	$dato = "Total Visitas";
}
elseif($_GET["modifica"] == "_prestamo.php")
{
	$sql = "select prestamos.Id, prestamos.Fecha_R as Fecha, sum(prestamos.Prestamos) as Participantes, prestamos.Responsable, '1' as Tipo, '1' as Actividad, prestamos.Descripcion, programacion.Sesiones, programacion.Observacion from prestamos, programacion where prestamos.Biblioteca = ".$_GET["bib"]." and prestamos.Id_Report = ".$_GET["id_rep"]." and prestamos.Id_Report = programacion.Id group by prestamos.Id_Report";
	$dato = "Total Prestamos";
}
elseif($_GET["modifica"] == "_otras_actividades.php")
{
	$sql = "select otros.Id, otros.Fecha, otros.Hora, otros.Participantes, otros.Responsable, otros.Actividad, otros.Descripcion, '0' as Sesiones, programacion.Id as Reporte, programacion.Observacion from otros, programacion where otros.Biblioteca = ".$_GET["bib"]." and otros.Area = ".$_GET["id_report"]." and otros.Mes = '".$_GET["fech_rep"]."' and programacion.Reporte = ".$_GET["id_report"]." and programacion.Biblioteca = ".$_GET["bib"]." and programacion.Fecha = '".$_GET["fech_rep"]."' order by otros.Fecha";
	$dato = "Tiempo / Participantes";
}
elseif($_GET["modifica"] == "_consultas.php")
{
	$sql = "select consultas.Id, consultas.Fecha, sum(consultas.Consultas) as Participantes, consultas.Responsable, consultas.Descripcion, '1' as Sesiones, '1' as Actividad, programacion.Id as Reporte, programacion.Observacion from consultas, programacion where consultas.Biblioteca = ".$_GET["bib"]." and consultas.Id_Report = ".$_GET["id_rep"]." and programacion.Reporte = ".$_GET["id_report"]." and programacion.Biblioteca = ".$_GET["bib"]." and programacion.Fecha = '".$_GET["fech_rep"]."' group by consultas.Id_Report";
	$dato = "Total consultas en sala";
}
elseif($_GET["modifica"] == "_i_biblio.php")
{
	$sql = "select info_men_bib.Id, info_men_bib.Fecha, sum(info_men_bib.Coleccion_G + info_men_bib.Coleccion_I + info_men_bib.Coleccion_S + info_men_bib.Coleccion_D + info_men_bib.Coleccion_V + info_men_bib.Coleccion_M + info_men_bib.Coleccion_B + info_men_bib.Coleccion_H + info_men_bib.Coleccion_L + info_men_bib.Coleccion_LV + info_men_bib.Coleccion_E + info_men_bib.Coleccion_P + info_men_bib.Coleccion_R + info_men_bib.Coleccion_RV) as Participantes, info_men_bib.Observacion as Descripcion, '1' as Sesiones, '1' as Actividad, '' as Responsable, programacion.Id as Reporte, programacion.Observacion from info_men_bib, programacion where info_men_bib.Biblioteca = ".$_GET["bib"]." and programacion.Id = ".$_GET["id_rep"]." and programacion.Reporte = ".$_GET["id_report"]." and programacion.Biblioteca = ".$_GET["bib"]." and programacion.Fecha = '".$_GET["fech_rep"]."' and info_men_bib.Fecha = '".$_GET["fech_rep"]."'";
	$dato = "Total colecci&oacute;n";
}

if(file_exists("m".$_GET["modifica"]))
	$modifica = "m".$_GET["modifica"];
else
	$modifica = "../acceso.php";
if(file_exists("e".$_GET["modifica"]))
	$elimina = "e".$_GET["modifica"];
else	
	$elimina = "../acceso.php";
//Adicionar cada nuevo reporte especial que se cree
//echo $sql;
$excs = mysqli_query($conect, $sql);
if(mysqli_num_rows($excs) > 0)
{ 
	$f_r = explode("/",$_GET["fech_rep"]);
	$f1 = strtotime(date("Y")."-".date("m")."-".date("d"));
	$f2 = strtotime(date($f_r[1]."-".$f_r[0]."-".date('t', mktime(0, 0, 0, $f_r[0], 1, $f_r[1]))));
	$f3 = $f1 - $f2;
?>
<div align="center" style="width:99%; text-align:center;">
<h4 align="left"><strong>Reporte estad&iacute;stico creados para:</strong> <?php echo $_GET["Reporte"]; ?></h4>
<table width="100%" border="1" cellspacing="0" cellpadding="0">
  <tr>
    <td width="3%"><div align="center"><strong>N&deg;</strong></div></td>
    <td width="7%"><div align="center"><strong>Fecha</strong></div></td>
    <td width="17%"><div align="center"><strong>Responsable de la actividad</strong></div></td>
    <td width="13%"><div align="center"><strong><?php echo $dato; ?></strong></div></td>
    <td width="41%" align="center"><div align="center"><strong>Descripci&oacute;n</strong></div></td>
    <td width="19%"><div align="center"><strong>Opciones</strong></div></td>
  </tr>
  <?php
  for($i=0; $i<mysqli_num_rows($excs); $i++)
  { 
	  $rows = mysqli_fetch_array($excs);
	  if($_GET["modifica"] == "_otras_actividades.php")
	  {
		  $_GET["id_rep"] = $rows["Reporte"];
	  }
  ?>	  
  <tr>
    <td style="background-color:#<?php if($rows["Tipo"] == 0) echo "408080"; else echo "FF8040"; ?>"><?php echo ($i+1); ?></td>
    <td><?php echo $rows["Fecha"]; ?></td>
    <td><span style="font-size:85%;"><?php echo $rows["Responsable"]; ?></span></td>
    <td><div  align="center"><?php if(isset($rows["Hora"])) { echo $rows["Hora"]." / ";} echo $rows["Participantes"]; ?></div></td>
    <td><span class="cssToolTip"><?php echo substr($rows["Descripcion"],0,70); ?> ...
        <span><?php echo $rows["Descripcion"]; ?></span></span></td>
    <td><div id="left_men"> 
    <?php
	if(($f3 / 86400) < 5)
	{ ?>
        <ul>
            <li><a href="javascript:void(0);">Opciones</a>
            <ul>
			<li><a href="javascript:void(0);" onclick="serv_mensual('<?php echo $modifica."?id_report=".$rows['Id']."&bib=".$_GET["bib"]."&nom=".$_GET["nom"]."&report=".$_GET["Reporte"]."&alias=".$_GET["alias"]."&campo=".$_GET["campo"]."&tip_repor=".$rows["Actividad"]."&id_rep=".$_GET["id_rep"]."&fech_rep=".$_GET["fech_rep"]; ?>');">Modificar</a></li>
            <?php
            if($_SESSION['MM_Usr_Pri'] <= 4)
			{ ?>
			<li><a href="javascript:void(0);" onclick="serv_mensual('<?php echo $elimina."?id_report=".$rows['Id']."&bib=".$_GET["bib"]."&nom=".$_GET["nom"]."&report=".$_GET["Reporte"]."&alias=".$_GET["alias"]."&campo=".$_GET["campo"]; ?>');">Eliminar</a></li>
            <?php } ?>
            <li><a href="javascript:void(0);" onclick="soport_mensual('<?php echo "soporte.php?id_report=".$_GET["id_rep"]."&fech_rep=".$_GET["fech_rep"]; ?>');">Soportes</a></li>
            <li><a href="javascript:void(0);" onclick="soport_mensual('<?php echo "observacion.php?id_report=".$_GET["id_rep"]."&fech_rep=".$_GET["fech_rep"]; ?>');">Observación</a></li>
            </ul></li>
        </ul>  
    <?php 
	}
	else
	{
		echo "<span class='infos'>Han transcurrido ".($f3 / 86400)." días del cierre del mes</span>";	
	}
	?>      
    </div></td>
  </tr>
  <?php } ?>
</table>
</div>
<div class="inform" style="overflow:hidden; text-align:justify; padding:5px;">
<?php echo "<strong>Observaciones del mes:</strong><br />".$rows["Observacion"]; ?>
</div>
<?php
mysqli_free_result($excs);
unset($sql,$i,$excs,$rows,$dato,$modifica,$elimina);
unset($f_r, $f1, $f2, $f3);
mysqli_close($conect);
}
else
{
	echo "<h4 align='center'><strong>No se encontraron reportes relacionados.</strong></h4>";
}
?>
<div align="justify" id="db_guardar">&nbsp; <!--Insertar mensaje de ayuda para la página -->
<div class="div_menu" id="aa_1"><a href="javascript:void(0);" onclick="menu('a_b_','1'); mostrar('bb','1');" title="Ayuda">? +</a></div>
<div class="div_menu" style="display:none;" id="bb_1"><a href="javascript:void(0);" onclick="menu('a_b_','2'); mostrar('aa','1');" title="Ayuda">? -</a></div>
<div class="div_ayuda" id="a_b_" style="display:none;"><!-- Quitar comentarios e insertar el texto de ayuda. Aparecerá flotando en el pie de la página.--></div></div>
<!-- InstanceEndEditable -->
<form name="chang_elemt" id="chang_elemt" target="contenido" method="post">
<!-- InstanceBeginEditable name="campos" -->

<!-- InstanceEndEditable -->
</form>
</div></body>

<script language="javascript">
verif();
</script>

<!-- InstanceEnd --></html>