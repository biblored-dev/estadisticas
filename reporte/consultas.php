<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/contenedor.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php
include("../script/scripts/session.php");
//include("../script/breadcrumbs.php");
?>
<link href="../css/css.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../script/scripts/jquery.min.js"></script>
<script type="text/javascript" src="../script/menu_ocultar.js"></script>
<script type="text/javascript">
function verif() {
document.getElementById('datos').style.display='none';
}
function foco_in(z) {
document.getElementById(z).focus();
}
</script>
<title>.: Sistema general de estad&iacute;stica :.</title>
<link rel="shortcut icon" href="../img/favicon.ico" />
<!-- InstanceBeginEditable name="doctitle" -->
<script language="javascript">
function get_fech(w, x) {
w = w.split('/');
var ret = w[1];
if(w[0] < 10)
	ret += "0"+w[0];
else
	ret += w[0];
if(x == 'i')
{
	ret += "01";
}
if(x == 'f')
{
	ret += new Date(w[1] || new Date().getFullYear(), w[0], 0).getDate();
}
return ret;
}
function datosserver(w) {
var id = document.getElementById("id_esp").value.split(",");
for(i=0;i<(id.length - 1);i++)
{
	document.getElementById("bib_"+id[i]).value = 0;
}
var fi = get_fech(document.getElementById('fecha').value, 'i');
var ff = get_fech(document.getElementById('fecha').value, 'f');
document.getElementById('datos').style.display = "";
var sql = "select count(*) as total, z35_material from bib50.z35 where Z35_sub_library = '"+w+"' and z35_event_date between '"+fi+"' and '"+ff+"' and z35_event_type = 80 group by z35_material";
  http.open("GET", "../script/alephxml.php?d1=0&d2=1&dat="+sql, true); //False para que no continue el flujo hasta compeltar la petición ajax
  http.onreadystatechange = datosHttpResp;
  return http.send(null);
}

function datosHttpResp() {
if(http.readyState == 4)
{
	if(http.status == 200)
	{
		var salida = "";
		var total_prest = 0;
		var timeValue = http.responseXML.getElementsByTagName("timenow")[0];
	    if(timeValue.childNodes.length > 0)
		{
			document.getElementById("total_prest").innerHTML = "";
			var regs = timeValue.childNodes[0].nodeValue;
			regs = regs.split(";");
			for(i = 0; i < (regs.length) - 1; i ++)
			{
			   salida = regs[i].split(",,");
			   total_prest += parseInt(salida[0]);
			   if(document.getElementById("bib_"+salida[1]))
			   {
					document.getElementById("bib_"+salida[1]).value = parseInt(salida[0]);
			   }
			}
			document.getElementById("total_prest").innerHTML = "Total consultas de material: "+total_prest;
		}
		verif();
	}
} 
else
{
	document.getElementById('datos').style.display = "";
}
}
function busqueda() {
var val, err, ids;
err = ids = "";
var id = document.getElementById("id_esp").value.split(",");
for(i=0;i<(id.length - 1);i++)
{
	val = document.getElementById("bib_"+id[i]).value;
	if(!isNaN(val) && val > 0 )
		ids += id[i] + ",";
}
val = document.getElementById("funcionario").value;
if(val == null || !isNaN(val) || val.length < 3 || /^\s+$/.test(val))
	err += "Se requiere el nombre del responsable. \n";
val = document.getElementById("descrp").value;
if(val == null || !isNaN(val) || val.length < 20 || /^\s+$/.test(val))
	err += "Se requiere la descripción. \n";
if(ids.length == 0)
	err += "No se cuenta con material consultado,\n haga clic en el enlace \“Actualizar información desde Aleph\”. \n";
if(err.length > 0)
{
	alert("Verifique los siguientes errores: \n\n"+err+"\n");
	return false;
}
else
{
	document.getElementById("id_esp").value = ids;
	return true;
}
}
</script>
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" --><!-- InstanceEndEditable -->
</head>
<body>
<div id="datos" align="center">
 <h4>Consolidando datos...<br />
 <img src="../img/loader.gif" width="16" height="16" /></h4>
 </div>
<div align="left"><?php //echo breadcrumbs(); ?></div>
<div align="center" class="popup">
<!-- InstanceBeginEditable name="contenidos" -->
<?php
include("../Connections/conect.inc.php");
if(isset($_POST["button"], $_POST["id_esp"], $_POST["id_rep"], $_POST["bib"]))
{
include("../script/cant.php");
$ids = explode(",",$_POST["id_esp"]);
for($i=0; $i<(sizeof($ids) - 1); $i++)
{ 
	$claus = $ids[$i]."' and Id_Report = ".$_POST["id_rep"]." and Biblioteca = '".$_POST["bib"];
	if(cant("consultas","Material",$claus) == 0) //tabla, campo, dato
	{
		$sql = "insert into consultas (Id, Fecha, Biblioteca, Material, Id_Report, Consultas, Responsable, Descripcion) VALUES ('', '".$_POST["fecha"]."', '".$_POST["bib"]."', '".$ids[$i]."', '".$_POST["id_rep"]."', '".$_POST["bib_".$ids[$i]]."', '".$_POST["funcionario"]."', '".addslashes($_POST["descrp"])."')";
	}
	else
	{
		$sql = "update consultas set Fecha = '".$_POST["fecha"]."', Consultas = '".$_POST["bib_".$ids[$i]]."', Responsable = '".$_POST["funcionario"]."', Descripcion = '".addslashes($_POST["descrp"])."' where Id_Report = '".$_POST["id_rep"]."' and Biblioteca = '".$_POST["bib"]."' and Material = '".$ids[$i]."'";
	}
	//echo $sql;
	$exc = mysqli_query($conect, $sql);
}

if($exc)
{
    echo "<h3 align='center'>Registro actualizado</h3>";
    ?><script language="javascript">
    alert("Registro actualizado");
    window.close(); 
    </script><?php
}
else
{
    echo "<h3 align='center'>Error al actualizar el registro</h3>";
    exit;
}
unset($exc, $sql, $i, $row, $ids);
mysqli_close($conect);
}
?>
<div align="left">
<h3 align="center">Reporte estad&iacute;stico para <?php echo $_GET["Reporte"] ?></h3>
<div align="center"><strong>Biblioteca: <?php echo $_GET["nom"]; ?></strong>, mes <?php echo $_GET["fech_rep"]; ?></div></div>
<?php
if(isset($_GET["bib"], $_GET["nom"]))
{ 
include("../script/z36h.php");
$ids = "";
?>
<div align="center">
<form name ="formiden" method ="POST" action ="consultas.php?bib=<?php echo $_GET["bib"]; ?>&nom=<?php echo $_GET["nom"]; ?>" onsubmit="return busqueda();" >
<input name="id_rep" id="id_rep" type="hidden" value="<?php echo $_GET["id_rep"]; ?>" />
<input name="bib" id="bib" type="hidden" value="<?php echo $_GET["bib"]; ?>" />
<input name="nombre" id="nombre" type="hidden" value="<?php echo $_GET["nom"]; ?>" />
<input name="cantidad" id="cantidad" type="hidden" value="<?php echo sizeof($z36h); ?>" />
<input name="fecha" id="fecha" type="hidden" value="<?php echo $_GET["fech_rep"]; ?>" />
<?php
$rows["Responsable"] = NULL;
$total_prest = 0;
for($i=0; $i<sizeof($z36h); $i++)
{ 		
	$sql = "select Fecha, Consultas, Responsable from consultas where Material = '".$z36h[$i][0]."' and Biblioteca = ".$_GET["bib"]." and Id_Report = ".$_GET["id_rep"];
	//echo $sql;
	$excs = mysqli_query($conect, $sql);
	if($excs)
		$rows = mysqli_fetch_array($excs);
	else
		$rows["Consultas"] = 0;
	?>
    <div class="list_js" align="left"><strong><?php echo ($i + 1).". ".$z36h[$i][1]; ?></strong>
    <input name="bib_<?php echo $z36h[$i][0]; ?>" id="bib_<?php echo $z36h[$i][0]; ?>" type="text" value="<?php echo $rows["Consultas"]; ?>" readonly="readonly" />
    </div>
<?php
$total_prest += $rows["Consultas"];
$ids .= $z36h[$i][0].","; //exit;
}
$sql = "select Abrev from biblioteca where Id = ".$_GET["bib"];
$excs = mysqli_query($conect, $sql); 
$rows = mysqli_fetch_array($excs);
?>
<div align="right" style="clear:both;"><strong><a href="javascript:void(0);" onclick="datosserver('<?php echo $rows["Abrev"]; ?>');">Actualizar informaci&oacute;n desde Aleph</a></strong></div>
<strong>
<div id="total_prest" align="left" style="clear:both; background-color:#EBEEE5; padding:5px; width:48%; border-radius:3px;">Total consultas de material: <?php echo $total_prest; ?></div></strong>
<div align="left" style="clear:both;"><strong>Nombre del responsable:</strong></div>
<div align="left"><?php include("../script/funcionarios.php"); ?></div>
<div align="left"><strong>Descripci&oacute;n de la actividad realizada:</strong></div>
<div align="left"><textarea name="descrp" id="descrp" rows="3">Verificación de la cantidad de material consultado por los visitantes en cada uno de los espacios de la biblioteca</textarea>
<div class="infos">M&aacute;ximo 500 caracteres. Describa la información que está reportando, si durante el mes se presentaron inconvenientes o acciones que afecten el dato reportado.</div></div>
<div align="center">
<input type="submit" name="button" id="button" value="Actualizar" />
</div>
<input name="id_esp" id="id_esp" type="hidden" value="<?php echo $ids; ?>" />
</form>
</div>
<?php include("../script/observacion.php"); ?>
<?php 
unset($excs, $sql, $i, $rows, $ids, $z36h);
}
?>
<!-- InstanceEndEditable -->
<form name="chang_elemt" id="chang_elemt" target="contenido" method="post">
<!-- InstanceBeginEditable name="campos" -->

<!-- InstanceEndEditable -->
</form>
</div></body>

<script language="javascript">
verif();
</script>

<!-- InstanceEnd --></html>