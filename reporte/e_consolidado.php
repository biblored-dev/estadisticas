<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/e_consolidado.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php
include("../script/scripts/session.php");
//include("../script/breadcrumbs.php");
?>
<link href="../css/css.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
function sel_resp(w) {
for(i = 1; i < document.getElementById("funcionario").length - 1; i++)
{
	if(document.getElementById("funcionario")[i].value.toUpperCase() == w.toUpperCase())
		document.getElementById("funcionario").selectedIndex = i;
}
}
function serv_mensual(w) {
var zz = window.open(w, 'Servicio mensual', 'resizable=yes,menubar=no,scrollbars=yes,width=660,height=600');
zz.focus();
}
function verif() {
document.getElementById('datos').style.display='none';
}
function foco_in(z) {
document.getElementById(z).focus();
}
function busqueda() {
var val, op;
err = "";
val = document.getElementById("fecha").value;
if(val == null || val.length < 5 || /^\s+$/.test(val))
	err += "Se requiere la fecha de la actividad. \n";
val = document.getElementById("participantes").value;
if(isNaN(val) || val == 0 || /^\s+$/.test(val))
	err += "Se requiere el número de participantes. \n";
val = document.getElementById("funcionario")[document.getElementById("funcionario").selectedIndex].value;
if(val == null || val.length < 5 || /^\s+$/.test(val) || val == "Adicionar funcionario")
	err += "Se requiere el nombre del responsable. \n";
val = document.getElementById("descrp").value;
if(val == null || val.length < 20 || /^\s+$/.test(val))
	err += "Se requiere la descripción de la actividad. \n";
if(err.length > 0)
{
	alert("Verifique los siguientes errores: \n\n"+err+"\n");
	return false;
}
else
	return true;
}
</script>
<title>.: Sistema general de estad&iacute;stica :.</title>
<link href="../menu/css/css.css" rel="stylesheet" type="text/css" />
<link rel="shortcut icon" href="../img/favicon.ico" />
<!-- InstanceBeginEditable name="doctitle" -->

<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" --><!-- InstanceEndEditable -->
</head>
<body>
<div id="datos" align="center">
 <h4>Consolidando datos...<br />
 <img src="../img/loader.gif" width="16" height="16" /></h4>
 </div>
<div align="left"><?php //echo breadcrumbs(); ?></div>
<div align="center" class="popup">
<!-- InstanceBeginEditable name="contenidos" -->
<!-- InstanceEndEditable -->
<?php
include("../Connections/conect.inc.php");
if(isset($_POST["fecha"], $_POST["funcionario"], $_POST["participantes"], $_POST["id_rep"], $_POST["bib"]))
{
$sql = "delete from consolidado where Id = ".$_POST["id_rep"];
//echo $sql;
$exc = mysqli_query($conect, $sql);
if($exc)
{
    echo "<h3 align='center'>Registro actualizado</h3>";
    unset($exc, $sql);
	mysqli_close($conect);
    ?><script language="javascript">
    alert("Registro eliminado");
	opener.location.reload();
    window.close();
    </script><?php
}
else
{
    echo "<h3 align='center'>Error al actualizar el registro</h3>";
    exit;
}
}
$sql = "select Fecha, Cantidad, Responsable, Tipo, Descripcion from consolidado where Id = ".$_GET["id_report"];
$excs = mysqli_query($conect, $sql);
$rows = mysqli_fetch_array($excs);
?>
<form name ="formulario" method ="POST" action ="e_consolidado.php" onSubmit="return busqueda()">
<input name="id_rep" id="id_rep" type="hidden" value="<?php echo $_GET["id_report"]; ?>" />
<input name="bib" id="bib" type="hidden" value="<?php echo $_GET["bib"]; ?>" />
<input name="nombre" id="nombre" type="hidden" value="<?php echo $_GET["nom"]; ?>" />
<input name="fecha" id="fecha" type="hidden" value="<?php echo date("Y/n/j"); ?>" />
<table width="99%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="15%"><h3><a href="javascript:void(0);" onclick="javascript:window.history.back(1);" title="Regresar"> << </a></h3></td>
    <td width="70%"><div align="center"><h3>Reporte estad&iacute;stico para <?php echo $_GET["report"] ?></h3>
    (<strong>&Aacute;lias:&nbsp;<?php echo $_GET["alias"] ?></strong>)<br />
    <strong>Biblioteca: <?php echo $_GET["nom"] ?></strong></div>
    </td>
    <td width="15%">&nbsp;</td>
  </tr>
  <tr>
    <td rowspan="8">&nbsp;</td>
    <td><div align="center" style="background-color:#<?php if($rows["Tipo"] == 0) echo "408080"; else echo "FF8040"; ?>">
    <?php 
    if($rows["Tipo"] == 0) echo "Actividad programada"; else echo "Actividad adicional";
    ?>
    </div></td>
    <td rowspan="8">&nbsp;</td>
  </tr>
  <tr>
    <td><div align="left"><strong><?php
    if(strlen($_GET["campo"]) > 0)
    	echo $_GET["campo"]; 
    else
    	echo "N&uacute;mero de usuarios o cantidad a reportar";
    ?>:</strong></div>
    <div align="left" style="width:50%;"><input name="participantes" id="participantes" type="text" size="50" value="<?php echo $rows["Cantidad"]; ?>" /></div></td>
    </tr>
  <tr>
    <td><div align="left"><strong>Responsable de la actividad:</strong></div>
    <div align="left"><input name="funcionario" id="funcionario" type="text" size="50" value="<?php echo $rows["Responsable"]; ?>" readonly="readonly" />
    </div></td>
    </tr>
  <tr>
    <td><div align="left"><strong>Descripci&oacute;n de la actividad realizada:</strong></div>
    <div align="left"><textarea name="descrp" id="descrp" rows="3"><?php echo $rows["Descripcion"]; ?></textarea>
    <div class="infos">M&aacute;ximo 500 caracteres. Describa la información que está reportando, si durante el mes se presentaron inconvenientes o acciones que afecten el dato reportado.</div></div>
    </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
   </tr>
   <tr>
    <td><div align="center">
    <input name="guardar" id="guardar" type="submit" value="Eliminar" />
    </div></td>
  </tr>
</table>
</form>
<?php unset($sql,$excs,$rows); ?>
<form name="chang_elemt" id="chang_elemt" target="contenido" method="post"></form>
</div></body>

<script language="javascript">
verif();
</script>

<!-- InstanceEnd --></html>