<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/e_reporte.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php
include("../script/scripts/session.php");
//include("../script/breadcrumbs.php");
?>
<link href="../css/css.css" rel="stylesheet" type="text/css" />
<script src="../script/calendario.js"></script>
<script src="../script/horario_2.js"></script>
<script type="text/javascript">
function sel_franjas(w) {
w = w.split(",");
for(i = 0; i < w.length - 1; i++)
{
	document.getElementById("franja_pob")[w[i]].selected = true;
}
document.getElementById("franja_pob").readOnly = true;
}
function serv_mensual(w) {
var zz = window.open(w, 'Servicio mensual', 'resizable=yes,menubar=no,scrollbars=yes,width=660,height=600');
zz.focus();
}
function verif() {
document.getElementById('datos').style.display='none';
}
function foco_in(z) {
document.getElementById(z).focus();
}
function busqueda() {
var val, op;
err = "";
document.getElementById("f_pob").value = "";
for(i = 0; i < 5; i++)
{
	if(document.getElementById("franja_pob")[i].selected == true)
       	document.getElementById("f_pob").value += document.getElementById("franja_pob")[i].value+",";
}
val = document.getElementById("fecha").value;
if(val == null || val.length < 5 || /^\s+$/.test(val))
	err += "Se requiere la fecha de la actividad. \n";
val = document.getElementById("hora").value;
if(val == null || val.length < 3 || /^\s+$/.test(val))
	err += "Se requiere el tiempo empleado en la actividad. \n";
val = document.getElementById("participantes").value;
if(isNaN(val) || val == 0 || /^\s+$/.test(val))
	err += "Se requiere el número de participantes. \n";
val = document.getElementById("funcionario")[document.getElementById("funcionario").selectedIndex].value;
if(val == null || val.length < 5 || /^\s+$/.test(val) || val == "Adicionar funcionario")
	err += "Se requiere el nombre del responsable. \n";
val = document.getElementById("f_pob").value;
if(val == null || val.length < 2 || /^\s+$/.test(val))
	err += "Se requiere la franja poblacional. \n";
val = document.getElementById("descrp").value;
if(val == null || val.length < 20 || /^\s+$/.test(val))
	err += "Se requiere la descripción de la actividad. \n";
if(err.length > 0)
{
	alert("Verifique los siguientes errores: \n\n"+err+"\n");
	return false;
}
else
	return true;
}
</script>
<title>.: Sistema general de estad&iacute;stica :.</title>
<link href="../menu/css/css.css" rel="stylesheet" type="text/css" />
<link rel="shortcut icon" href="../img/favicon.ico" />
<!-- InstanceBeginEditable name="doctitle" -->

<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" --><!-- InstanceEndEditable -->
</head>
<body>
<div id="datos" align="center">
 <h4>Consolidando datos...<br />
 <img src="../img/loader.gif" width="16" height="16" /></h4>
 </div>
<div align="left"><?php //echo breadcrumbs(); ?></div>
<div align="center" class="popup">
<!-- InstanceBeginEditable name="contenidos" -->
<!-- InstanceEndEditable -->
<?php
include("../Connections/conect.inc.php");
if(isset($_POST["fecha"], $_POST["hora"], $_POST["participantes"], $_POST["id_rep"], $_POST["bib"]))
{
$sql = "delete from estadistica where Id = ".$_POST["id_rep"];
$exc = mysqli_query($conect, $sql);
if($exc)
{
    echo "<h3 align='center'>Registro actualizado</h3>";
    ?><script language="javascript">
    alert("Registro eliminado");
	opener.location.reload();
    window.close();
    </script><?php
}
else
{
    echo "<h3 align='center'>Error al actualizar el registro</h3>";
    exit;
}
}
$sql = "select Fecha, Hora, Participantes, Responsable, Franja, Tipo, Descripcion from estadistica where Id = ".$_GET["id_report"];
$excs = mysqli_query($conect, $sql);
$rows = mysqli_fetch_array($excs);
?>
<form name ="formulario" method ="POST" action ="e_talleres.php" onSubmit="return busqueda()">
<input name="id_rep" id="id_rep" type="hidden" value="<?php echo $_GET["id_report"]; ?>" />
<input name="bib" id="bib" type="hidden" value="<?php echo $_GET["bib"]; ?>" />
<input name="nombre" id="nombre" type="hidden" value="<?php echo $_GET["nom"]; ?>" />
<input name="f_pob" id="f_pob" type="hidden" value="" />
<table width="99%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="15%">&nbsp;</td>
    <td width="70%"><div align="center"><h3>Reporte estad&iacute;stico para <?php echo $_GET["report"]; ?></h3>
      (<strong>&Aacute;lias:&nbsp;<?php echo $_GET["alias"] ?></strong>)<br />
      <strong>Biblioteca: <?php echo $_GET["nom"] ?></strong></div>
      </td>
    <td width="15%">&nbsp;</td>
  </tr>
  <tr>
    <td rowspan="9">&nbsp;</td>
    <td><div align="center" style="background-color:#<?php if($rows["Tipo"] == 0) echo "408080"; else echo "FF8040"; ?>">
    <?php 
    if($rows["Tipo"] == 0) echo "Actividad programada"; else echo "Actividad adicional";
    ?>
    </div></td>
    <td rowspan="9">&nbsp;</td>
  </tr>
  <tr>
    <td align="center"><div align="left"><strong>Fecha de la actividad:</strong></div>
      <div align="left" style="width:55%;"><span class="cssToolTip"><input name="fecha" id="fecha" type="text" size="50" style="width:85%;" readonly="readonly" value="<?php echo $rows["Fecha"]; ?>" />
        <img src="../icon/calendar_.gif" alt="seleccione" width="24" height="12" />
      </div></td>
    </tr>
  <tr>
    <td><div align="left"><strong>Tiempo utilizado en la actividad:</strong></div>
    <div align="left" style="width:55%;"><span class="cssToolTip"><input name="hora" id="hora" type="text" size="50" style="width:85%;" readonly="readonly" value="<?php echo $rows["Hora"]; ?>" />
    <img src="../icon/wait.png" alt="seleccione" width="12" height="16" />
    </div></td>
  </tr>
  <tr>
    <td><div align="left"><strong>N&uacute;mero de participantes:</strong></div>
    <div align="left" style="width:50%;"><input name="participantes" id="participantes" type="text" size="50" value="<?php echo $rows["Participantes"]; ?>" readonly="readonly" /></div></td>
  </tr>
  <tr>
    <td><div align="left"><strong>Responsable de la actividad:</strong></div>
    <div align="left"><input name="funcionario" id="funcionario" type="text" size="50" value="<?php echo $rows["Responsable"]; ?>" readonly="readonly" />
    </div></td>
  </tr>
  <tr>
    <td><div align="left"><strong>Seleccione la franja poblacional:</strong></div>
    <div align="left"><?php include("../script/franja_p.php"); ?>
    <div class="infos">Para seleccionar más de una opción mantenga la tecla "Ctrl" oprimida.</div>
    <script language="javascript">
	sel_franjas('<?php echo $rows["Franja"]; ?>');
    document.getElementById("franja_pob").disabled = true;
    </script>
    </div>
    </td>
  </tr>
  <tr>
    <td><div align="left"><strong>Descripci&oacute;n de la actividad realizada:</strong></div>
    <div align="left"><textarea name="descrp" id="descrp" rows="3" readonly="readonly"><?php echo $rows["Descripcion"]; ?></textarea>
    <div class="infos">M&aacute;ximo 500 caracteres. Describa la información que está reportando, si durante el mes se presentaron inconvenientes o acciones que afecten el dato reportado.</div></div>
    </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
   </tr>
   <tr>
    <td><div align="center"><input name="guardar" id="guardar" type="submit" value="Eliminar" /></div></td>
  </tr>
</table>
</form>
<?php unset($sql,$excs,$rows); ?>
<form name="chang_elemt" id="chang_elemt" target="contenido" method="post"></form>
</div></body>

<script language="javascript">
verif();
</script>

<!-- InstanceEnd --></html>