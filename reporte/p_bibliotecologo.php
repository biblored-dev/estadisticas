<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php
include("../script/scripts/session.php");
//include("../script/breadcrumbs.php");
function get_fech($w, $x) {
$w = explode("/", $w);
$ret = $w[1]."-";
if($w[0] < 10)
	$ret .= "0".$w[0]."-";
else
	$ret .= $w[0]."-";
if($x == 'i')
{
	$ret .= "01";
}
if($x == 'f')
{
	$ret .= "31";
}
return $ret;
}
?>
<link href="../css/css.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../script/scripts/jquery.min.js"></script>
<script type="text/javascript" src="../script/menu_ocultar.js"></script>
<script type="text/javascript">
function datosserver(w,x,y) {

  var sql = "Select count(*) as Respuestas from pregunta where Fecha between '"+w+"' and '"+x+"' and Respondio = '"+y+"'";
  http.open("GET", "../script/dat_portxml.php?d1=Respuestas&d2=1&dat="+sql+"&db=biblored_pbibliotecologo", true); //False para que no continue el flujo hasta compeltar la petición ajax
  http.onreadystatechange = datosHttpResp;
  return http.send(null);
}

function datosHttpResp() {
if(http.readyState == 4)
{
	if(http.status == 200)
	{
		if(http.responseXML)
		{
			var timeValue = http.responseXML.getElementsByTagName("timenow")[0];
			if(timeValue.childNodes.length > 0)
			{
				var regs = timeValue.childNodes[0].nodeValue;
				regs = regs.split(";");
				if(regs.length > 1)
				{
				   document.getElementById("participantes").value = parseInt(regs[0]);
				}
				else
				{
					alert("Error de conexión, intente nuevamente más tarde o informe a CST");	
				}
			}
		}
		else
		{
			alert("Error de conexión, intente nuevamente más tarde o informe a CST");	
		}
		verif();
	}
} 
else
{
	document.getElementById('datos').style.display = "";
}
}

function serv_mensual(w) {
var zz = window.open(w, 'Servicio mensual', 'resizable=yes,menubar=no,scrollbars=yes,width=660,height=600');
zz.focus();
}
function verif() {
document.getElementById('datos').style.display='none';
}
function foco_in(z) {
document.getElementById(z).focus();
}
function busqueda() {
var val, op;
err = "";
val = document.getElementById("fecha").value;
if(val == null || val.length < 5 || /^\s+$/.test(val))
	err += "Se requiere la fecha de la actividad. \n";
val = document.getElementById("participantes").value;
if(isNaN(val) || val == 0 || /^\s+$/.test(val))
	err += "Se requiere la cantidad a reportar. \n";
val = document.getElementById("funcionario")[document.getElementById("funcionario").selectedIndex].value;
if(val == null || val.length < 5 || /^\s+$/.test(val) || val == "Adicionar funcionario")
	err += "Se requiere el nombre del responsable. \n";
val = document.getElementById("descrp").value;
if(val == null || val.length < 20 || /^\s+$/.test(val))
	err += "Se requiere la descripción de la actividad. \n";
op = document.getElementById("tip_r").selectedIndex;
val = document.getElementById("tip_r")[op].value;
if(val == null || val.length == 0 || /^\s+$/.test(val))
	err += "Se requiere el tipo de reporte. \n";
if(err.length > 0)
{
	alert("Verifique los siguientes errores: \n\n"+err+"\n");
	return false;
}
else
	return true;
}
</script>
<title>.: Sistema general de estad&iacute;stica :.</title>
<link href="../menu/css/css.css" rel="stylesheet" type="text/css" />
<link rel="shortcut icon" href="../img/favicon.ico" />

</head>
<body>
<div id="datos" align="center">
 <h4>Consolidando datos...<br />
 <img src="../img/loader.gif" width="16" height="16" /></h4>
 </div>
<div align="left"><?php //echo breadcrumbs(); ?></div>
<div align="center" class="popup">
<?php
include("../Connections/conect.inc.php");
if(isset($_POST["fecha"], $_POST["funcionario"], $_POST["participantes"], $_POST["id_rep"], $_POST["bib"]))
{
	if(isset($_POST["guardar"]))
		$sql = "insert into consolidado (Id, Fecha, Biblioteca, Id_Report, Cantidad, Responsable, Tipo, Actividad, Descripcion) VALUES ('', '".$_POST["fecha"]."', '".$_POST["bib"]."', '".$_POST["id_rep"]."', '".$_POST["participantes"]."', '".strtoupper($_POST["funcionario"])."', '".$_POST["tipo_act"]."', '".$_POST["tip_r"]."', '".addslashes($_POST["descrp"])."')";
	else
		$sql = "update consolidado set Fecha = '".$_POST["fecha"]."', Cantidad = '".$_POST["participantes"]."', Responsable = '".strtoupper($_POST["funcionario"])."', Tipo = '".$_POST["tipo_act"]."', Descripcion = '".addslashes($_POST["descrp"])."' where Id = ".$_POST["id_report"];
//echo $sql;
$exc = mysqli_query($conect, $sql);
if($exc)
{
    echo "<h3 align='center'>Registro actualizado</h3>";
    ?><script language="javascript">
    alert("Registro actualizado");
    window.close(); 
    </script><?php
}
else
{
    echo "<h3 align='center'>Error al actualizar el registro</h3>";
    exit;
}
}
?>
<form name ="formulario" method ="POST" action ="p_bibliotecologo.php" onSubmit="return busqueda()">
<input name="id_rep" id="id_rep" type="hidden" value="<?php echo $_GET["id_rep"]; ?>" />
<input name="bib" id="bib" type="hidden" value="<?php echo $_GET["bib"]; ?>" />
<input name="nombre" id="nombre" type="hidden" value="<?php echo $_GET["nom"]; ?>" />
<input name="fecha" id="fecha" type="hidden" value="<?php echo date("Y/n/j"); ?>" />
<table width="99%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="10%">&nbsp;</td>
    <td width="80%"><div align="center"><h3>Reporte estad&iacute;stico para <?php echo $_GET["Reporte"] ?></h3>
    (<strong>&Aacute;lias:&nbsp;<?php echo $_GET["alias"] ?></strong>)<br />
    <strong>Biblioteca: <?php echo $_GET["nom"] ?></strong></div>
    </td>
    <td width="10%">&nbsp;</td>
  </tr>
  <tr>
    <td rowspan="9">&nbsp;</td>
    <td><div id="left_men">
    <ul>
        <?php
		$tecs = array(10=>"jmsd_referencista",11=>"tnt_referencista",3=>"bvb_referencista",10=>"tnl_referencista",8=>"sb_referencista",16=>"rt_referencista");
		if(in_array($tecs[$_GET["bib"]], $tecs))
		{
			$sql = "select consolidado.Id, consolidado.Fecha, consolidado.Cantidad, programacion.Sesiones from consolidado, programacion where consolidado.Biblioteca = ".$_GET["bib"]." and consolidado.Id_Report = ".$_GET["id_rep"]." and consolidado.Id_Report = programacion.Id and programacion.Fecha = '".$_GET["fech_rep"]."'";
			//echo $sql;
			$excs = mysqli_query($conect, $sql);
			if(mysqli_num_rows($excs) > 0)
			{ ?>
			<li style="width:160px;"><a href="javascript:void(0);">Reportes creados <?php echo mysqli_num_rows($excs); ?> de <span id="num_progr">&nbsp;</span></a>
			<ul style="z-index:1000;">
			<?php
			for($i=0; $i<mysqli_num_rows($excs); $i++)
			{ 
				$rows = mysqli_fetch_array($excs);
			?>
				<li><a href="javascript:void(0);" onclick="serv_mensual('<?php echo "m_consolidado.php?id_report=".$rows['Id']."&bib=".$_GET["bib"]."&nom=".$_GET["nom"]."&report=".$_GET["Reporte"]."&alias=".$_GET["alias"]."&campo=".$_GET["campo"]; ?>');"><?php echo $rows['Fecha']; ?></a></li>
			 <?php } ?>   
			 </ul>
			 <script language="javascript">
				document.getElementById("num_progr").innerHTML = "<?php echo $rows['Sesiones']; ?>";
			 </script>
			 <?php } ?> 
         <?php } ?>     
        </li>
    </ul>
    </div>
    </td>
    <td rowspan="9">&nbsp;</td>
  </tr>
  <tr>
    <td><div align="left"><strong>Tipo de reporte:</strong></div>
    <div align="left" style="padding-left:10px;">
    <?php include("../script/tipo_report.php"); ?>
    </div>
    </td>
  </tr>
  <tr>
    <td><div align="left"><strong><?php
    if(strlen($_GET["campo"]) > 0)
    	echo $_GET["campo"]; 
    else
    	echo "N&uacute;mero de usuarios o cantidad a reportar";
    ?>:</strong></div>
    <?php
	if(in_array($tecs[$_GET["bib"]], $tecs))
	{
		?>
    	<div align="left" style="width:50%;"><input name="participantes" id="participantes" type="text" size="50" value="<?php echo isset($rows["Cantidad"]) ? $rows["Cantidad"] : '' ?>" /></div>
        <div class="infos">Valor tomado directamente desde la herramienta de “Preg&uacute;ntele al bibliotec&oacute;logo” al momento de cargar este reporte.</div>
        <div align="right" style="clear:both;"><strong><a href="javascript:void(0);" onclick="datosserver('<?php echo get_fech($_GET["fech_rep"], 'i'); ?>', '<?php echo get_fech($_GET["fech_rep"], 'f', ''); ?>', '<?php echo $tecs[$_GET["bib"]]; ?>');">Actualizar informacion desde el portal web</a></strong></div>
	<?php
	}
	?>
	</td>
  </tr>
  <tr>
    <td><div align="left"><strong>Responsable de la actividad:</strong></div>
    <div align="left"><?php include("../script/funcionarios.php"); ?>
    </div></td>
  </tr>
  <tr>
    <td><div align="left"><strong>Descripci&oacute;n de la actividad realizada:</strong></div>
    <?php include("../script/caracteristicas.php"); ?>
    </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
   </tr>
   <tr>
    <td><div align="center">
    <?php
    if(mysqli_num_rows($excs) > 0 && mysqli_num_rows($excs) >= $rows['Sesiones'])
    {  ?>
    	<h3>Se han completado las sesiones programadas</h4>
        <input name="actualizar" id="actualizar" type="submit" value="Actualizar" />
        <input name="tipo_act" id="tipo_act" type="hidden" value="1" />
		<input name="id_report" id="id_report" type="hidden" value="<?php echo $rows['Id']; ?>" />
    <?php }
    else
    { ?>
    	<input name="guardar" id="guardar" type="submit" value="Guardar registro" />
        <input name="tipo_act" id="tipo_act" type="hidden" value="0" />
    <?php 
    }
    unset($sql,$excs,$rows,$i,$tecs);
    ?>    
    </div></td>
  </tr>
  <tr>
    <td>
    <?php include("../script/observacion.php"); ?>
    </td>
  </tr>
</table>
</form>

<form name="chang_elemt" id="chang_elemt" target="contenido" method="post"></form>
</div></body>

<script language="javascript">
verif();
</script>

</html>