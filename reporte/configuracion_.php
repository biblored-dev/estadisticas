﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/contenedor.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php
include("../script/scripts/session.php");
//include("../script/breadcrumbs.php");
?>
<link href="../css/css.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../script/scripts/jquery.min.js"></script>
<script type="text/javascript" src="../script/menu_ocultar.js"></script>
<script type="text/javascript">
function verif() {
document.getElementById('datos').style.display='none';
}
function foco_in(z) {
document.getElementById(z).focus();
}
</script>
<title>.: Sistema general de estad&iacute;stica :.</title>
<link rel="shortcut icon" href="../img/favicon.ico" />
<!-- InstanceBeginEditable name="doctitle" -->
<link href="../menu/css/css.css" rel="stylesheet" type="text/css" />
<script src="../script/c_color.js"></script>
<script language="javascript" src="../script/datosxml.js"></script>
<script type="text/javascript" src="../script/menu_ocultar.js"></script>
<script type="text/javascript" src="../script/mes.js"></script>
<script language="javascript">
var err = "";
var cant = 0;
var sent_control = false;
function cancelar_rep(w,x) {
if(confirm("El reporte '"+x+"'.\nSe eliminará de la lista."))
{
var prog = document.getElementById("conter_"+w);
prog.parentNode.removeChild(prog);
var ids = document.getElementById("id_e").value.split(",");
document.getElementById("id_e").value = "";
for(i=0; i<ids.length; i++)
{
	if(ids[i].length > 0 && ids[i] != w)
	{
		document.getElementById("id_e").value += ids[i]+",";
	}
}
}
}

function mostrar_todo() {
reporteserver(document.getElementById("area_r")[document.getElementById("area_r").selectedIndex].value, document.getElementById("biblo")[document.getElementById("biblo").selectedIndex].value, document.getElementById("fecha").value);
document.getElementById("tod_estad").style.display = "none";
}

function borrar() {
document.getElementById("guardar").disabled = false;
document.getElementById('estadisticas').innerHTML = "";	
document.getElementById('db_guardar').style.display = "none";
document.getElementById('tod_estad').style.display = "none";
document.getElementById('id_e').value = "";	
document.getElementById('num_b').value = "";	
document.getElementById('id_b').value = document.getElementById("biblo")[document.getElementById("biblo").selectedIndex].value;	
parent.contenido.location = "../fill.php";
}

function pordemanda(w) {
document.getElementById("bib_"+w).value = "S_P_D";
document.getElementById("bib_"+w).readOnly = true;
document.getElementById("demanda_"+w).disabled = true;
}

function edit_mensual(w) {
alert("Acción no permitida, han transcurrido \n"+w+" días del cierre del mes.");
}

function asignar_publico(w,x,y) {
var	alias = prompt("¿"+y+" \n al final de todas las sesiones?:", x);
if(alias != null && alias >= 0 && alias != x)
{
	document.getElementById("publico_"+w).innerHTML = alias;
}
}

function crear_alias(w,x,z) {
var	alias = prompt("Crear un alias para el reporte y biblioteca actual:\n"+z, x);
if(alias != null && alias != x && alias != z)
{
	nuevoalias(w, document.getElementById("biblo")[document.getElementById("biblo").selectedIndex].value, document.getElementById("fecha").value, alias)
	document.getElementById("alias_"+w).innerHTML = "("+alias+")";
}
else
	alert("Nombre no válido.");
}

function nuevoalias(w,x,y,z) {
  var sql = "insert into prog_alias (Id, Reporte, Biblioteca, Fecha, Alias) values ";
  http.open("GET", "../script/insertalias.php?dat="+sql+"&rep="+w+"&bib="+x+"&fec="+y+"&ali="+z, false); //False para que no continue el flujo hasta compeltar la petición ajax
  http.onreadystatechange = aliasHttpResp;
  return http.send(null);
}

function aliasHttpResp() {
if(http.readyState == 4)
{
	if(http.status == 200)
	{
		var timeValue = http.responseXML.getElementsByTagName("timenow")[0];
	    if(timeValue.childNodes.length > 0)
		{
			var regs = timeValue.childNodes[0].nodeValue;
			if(regs == "OK")
			{
				alert("Alias creado satisfactoriamente");
			}
			else
			{
				alert("Error al crear el alias");
			}
		}
		else
		{
			alert("Error al crear el alias");
		}
		verif();
	}
} 
else
{
	document.getElementById('datos').style.display = "";
}
}

function reporteserver(x,w,y) { //Todos los reportes
  //select reporte.Id, reporte.Nombre, reporte.Campo from reporte where reporte.Estado = 1 and reporte.Area = "+x+" order by reporte.Nombre asc
  sent_control = true;
  var sql = "select distinct(t1.Id), t1.Nombre, t1.Campo, t1.Continuo, t1.Sesiones, t1.Publico, t1.Sub, t1.Reporte from (select Rep.Id, Rep.Nombre, Rep.Campo, Rep.Continuo, Rebi.Sesiones, Prog.Publico, Prog.Sub, Prog.Id as Reporte from reporte Rep, report_bib Rebi, programacion Prog where Rep.Estado = 1 and Rep.Area = "+x+" and Rep.Id > 17 and Rep.Id = Prog.Reporte and Rebi.Reporte = Rep.Id and Prog.Fecha = '"+y+"' and Prog.Sub in(0,1) and Prog.Biblioteca = "+w+" and Rebi.Biblioteca = "+w+" and Rebi.Sesiones > 0 order by Rep.Nombre asc) t1 union select * from (select reporte.Id, reporte.Nombre, reporte.Campo, reporte.Continuo, report_bib.Sesiones, '0' as Publico, '0' as Sub, '0' as Reporte from reporte, report_bib where reporte.Estado = 1 and reporte.Area = "+x+" and report_bib.Reporte = reporte.Id and report_bib.Fecha = '"+y+"' and reporte.Id > 17 and report_bib.Biblioteca = "+w+" and report_bib.Sesiones > 0 order by reporte.Nombre asc) t2";
  http.open("GET", "../script/datosxml_ll.php?d1=Sesiones&d2="+w+"&dat="+sql+"&fecha_s="+y, true); //False para que no continue el flujo hasta compeltar la petición ajax
  http.onreadystatechange = datosHttpResp;
  return http.send(null);
}

function datosserver(w,x,y) { //Reportes preconfigurados
  sent_control = false;
  document.getElementById("tod_estad").style.display = "";
  var sql = "select reporte.Id, reporte.Nombre, reporte.Campo, reporte.Continuo, report_bib.Sesiones, programacion.Publico, programacion.Sub, programacion.Id as Reporte from reporte, report_bib, programacion where reporte.Estado = 1 and reporte.Area = "+x+" and report_bib.Reporte = reporte.Id and reporte.Id > 17 and programacion.Fecha = '"+y+"' and programacion.Sub in(0,1) and reporte.Id = programacion.Reporte and programacion.Biblioteca = "+w+" and report_bib.Biblioteca = "+w+" and report_bib.Sesiones > 0 and report_bib.Fecha = '"+y+"' order by reporte.Nombre asc";
  http.open("GET", "../script/datosxml_ll.php?d1=Nombre&d2="+w+"&dat="+sql+"&fecha_s="+y, true); //False para que no continue el flujo hasta compeltar la petición ajax
  http.onreadystatechange = datosHttpResp;
  return http.send(null);
}

function datosHttpResp() {
if(http.readyState == 4)
{
	if(http.status == 200)
	{
		var salida = "";
		var campo = "";
		var activo = "";
		var desact = "";
		var eliminar = "";
		clonar = "";
	    borrar();
		document.getElementById('tod_estad').style.display = "";
		var timeValue = http.responseXML.getElementsByTagName("timenow")[0];
	    if(timeValue.childNodes.length > 0)
		{
			var regs = timeValue.childNodes[0].nodeValue;
			regs = regs.split(";");
			var listado = document.getElementById('estadisticas');
			listado.length = regs.length;
			listado.innerHTML += "<p align='center'><strong>N&uacute;mero de sesiones o mediciones a realizar para el mes "+document.getElementById("fecha").value+", seg&uacute;n el plan de acci&oacute;n:</strong></p>";
			for(i = 0; i < (regs.length) - 1; i ++)
			{
			   salida = regs[i].split(",,");
			   campo = "Dato a reportar ";
			   if(parseInt(salida[0]) < 5 || document.getElementById('area_r').value == 6)
			   {
				   if(salida[3].length > 0)
				   {
						campo += "(<a href='javascript:void(0);' onclick=\"asignar_publico("+salida[1]+",'"+salida[6]+"','"+salida[3]+"');\" class=\"cssToolTip\"><span>Haga clic para asignar una \""+salida[3]+"\" esperados (as) al final de todas las sesiones programadas al mes, opcional</span>"+salida[3]+": </a><span id='publico_"+salida[1]+"'>"+salida[6]+"</span>)";
						campo_inf = salida[3]+" esperado";
				   }
				   else
				   {
						campo += "(<a href='javascript:void(0);' onclick=\"asignar_publico("+salida[1]+",'"+salida[6]+"','Público esperado');\" class=\"cssToolTip\"><span>Haga clic para asignar una \""+salida[3]+"\" esperados (as) al final de todas las sesiones programadas al mes, opcional</span>Público esperado: </a><span id='publico_"+salida[1]+"'>"+salida[6]+"</span>)";
						campo_inf = "Público esperado";	
				   }
			   }
			   else
			   {
				   campo += "(<a href='javascript:void(0);' onclick=\"edit_mensual('"+salida[0]+"');\" class=\"cssToolTip\"><span>Haga clic para asignar una \""+salida[3]+"\" esperados (as) al final de todas las sesiones programadas al mes, opcional</span>"+salida[3]+": </a><span id='publico_"+salida[1]+"'>"+salida[6]+"</span>)";
						campo_inf = salida[3]+" esperado";
			   }
			   if(!document.getElementById("conter_"+salida[1]))
			   {
				   activo = "";
				   //activo = "readOnly='readOnly'"; //inactivo por defecto
				   desact = "";
				   
				   /*if(salida[7] == 0)
				   {
				   		if(parseInt(salida[0]) < 5 || document.getElementById('area_r').value == 6)
						{
							clonar = "<div class='list_cln' id='clon_"+salida[1]+"'><a href='javascript:void(0);' onclick=\"instancias('../reporte/clonar.php','"+salida[1]+"','"+campo_inf+"','"+salida[2]+"');\" class=\"cssToolTip\"><span class='span'>Clonar este reporte, adicionar hasta 3 instancias para diligenciar de forma independiente</span>Adicionar reporte</a></div>";
						}
						else
						{
							clonar = "<div class='list_cln' id='clon_"+salida[1]+"'><a href='javascript:void(0);' onclick=\"edit_mensual('"+salida[0]+"');\" class=\"cssToolTip\"><span class='span'>Clonar este reporte, adicionar hasta 3 instancias para diligenciar de forma independiente</span>Adicionar reporte</a></div>";
						}
				   }
				   else
				   {
				   		clonar = "<div class='list_cln' id='clon_"+salida[1]+"'><a href='javascript:void(0);' onclick=\"c_instancias('../reporte/c_clonar.php','"+salida[1]+"','"+campo_inf+"','"+salida[2]+"','"+salida[8]+"');\" class=\"cssToolTip\"><span class='span'>Consultar reportes clonados, reporte dividido en instancias</span>Consultar reportes</a></div>";
						//activo = "readOnly='readOnly'";
						desact = "disabled='disabled'";
						campo = campo_inf+": <span id='publico_"+salida[1]+"'>"+salida[6]+"</span>";
				   } */
				   if(salida[4] == "1")
				   {
				   		salida[5] = salida[4];
						activo = "readOnly='readOnly'";
						desact = "disabled='disabled'";
						//clonar = "<div class='list_cln'><a href='javascript:void(0);' class=\"cssToolTip\"><span class='span'>Reporte único a diligenciar al mes, corresponde a información que se extrae de otros servicios</span>Reporte &uacute;nico&nbsp;</a></div>";
				   }
				   else if(salida[5] == "255")
				   {
				   		salida[5] = "S_P_D";
				   }
				   if(parseInt(salida[0]) >= 5 && document.getElementById('area_r').value != 6)
				   {
					   //activo = "readOnly='readOnly'";
					   desact = "disabled='disabled'";
				   }
				   if(sent_control == true)
				   {
						eliminar = "<div class='list_cln' id='elimin_"+salida[1]+"'><a href='javascript:void(0);' onclick=\"cancelar_rep('"+salida[1]+"','"+salida[2]+"')\" class=\"cssToolTip\"><span class='span'>Cancelar el reporte de las estadísticas de la biblioteca</span>&nbsp; Suprimir reporte &nbsp;</a></div>";
				   }

				   programas = "<div align='left' class='list_legen'><div style='float:left; width:8%;'><input name='bib_"+salida[1]+"' id='bib_"+salida[1]+"' type=\"text\" class=\"infos\" value='"+salida[5]+"' "+activo+" maxlength='3' /></div><div class='list_extn'> &nbsp; <strong>"+salida[2]+"</strong>"+eliminar+"</div></div>";
				   
				   if(parseInt(salida[0]) < 5 || document.getElementById('area_r').value == 6)
				   {
					   if(salida[8] > 0)
					   {
							alias = "<div class='list_prgm' id='alias_"+salida[8]+"'>(<a href='javascript:void(0);' onclick=\"crear_alias("+salida[8]+",'"+salida[9]+"','"+salida[2]+"');\" class=\"cssToolTip\"><span>Haga clic para crearle un alias al programa, solo aplica para la biblioteca seleccionada</span>"+salida[9]+"</a>)</div>";
					   }
					   else
					   {
						   alias = "<div class='list_prgm' id='alias_"+salida[8]+"'>(<a href='javascript:void(0);' onclick=\"alert('Primero guarde los cambios');\" class=\"cssToolTip\"><span>Haga clic para crearle un alias al programa, solo aplica para la biblioteca seleccionada</span>"+salida[9]+"</a>)</div>";
					   }
				   }
				   else
				   {
					   alias = "<div class='list_prgm' id='alias_"+salida[8]+"'>(<a href='javascript:void(0);' onclick=\"edit_mensual('"+salida[0]+"');\" class=\"cssToolTip\"><span>Haga clic para crearle un alias al programa, solo aplica para la biblioteca seleccionada</span>"+salida[9]+"</a>)</div>";
				   }
				   usuarios = "<div class='list_prgm' id='asignado_"+salida[1]+"'>"+campo+"</div>";
				   boton = "<div class='list_float'><input name='demanda_"+salida[1]+"' id='demanda_"+salida[1]+"' type='button' value='Sesión por demanda' onclick='pordemanda("+salida[1]+");'"+desact+" /></div>";
				   
				   //listado.innerHTML += "<div class=\"list_js\" id='conter_"+salida[1]+"'>"+programas+alias+clonar+"<div align='left' class='list_legen' style='clear:both;'>"+usuarios+boton+"</div></div>";
				   listado.innerHTML += "<div class=\"list_js\" id='conter_"+salida[1]+"'>"+programas+alias+clonar+"<div align='left' class='list_legen' style='clear:both;'>"+usuarios+"</div></div>"; //sin el botón de sesiones por demanda
				   document.getElementById('id_e').value += salida[1]+",";
				   document.getElementById('num_b').value += salida[6]+",";
			   }
			}
			listado.innerHTML += "<div style='clear:both;' align='center'>. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .</div>";
			document.getElementById('db_guardar').style.display = "";
			document.getElementById('div_unico').style.display = "";
		}
		else //if(cant == 0)
		{
			if(confirm("No se encontraron reportes para los datos ingresados.\n¿Desea crearlos de acuerdo a los datos ingresados?"))
				reporteserver(document.getElementById("area_r")[document.getElementById("area_r").selectedIndex].value, document.getElementById("biblo")[document.getElementById("biblo").selectedIndex].value, document.getElementById("fecha").value);
			document.getElementById('div_unico').style.display = "none";
			cant ++;
		}
		verif();
	}
} 
else
{
	document.getElementById('datos').style.display = "";
}
}
function instancias(w,x,y,z) {
parent.contenido.location = w+"?Rep="+x+"&Tex=bib_"+x+"&Bot=demanda_"+x+"&Pub=publico_"+x+"&Dat="+y+"&Nom="+z+"&Fec="+document.getElementById("fecha_s").value+"&Bib="+document.getElementById("id_b").value;
document.getElementById("clon_"+x).style.backgroundColor = "#00CC99";
}
function c_instancias(w,x,y,z,wx) {
parent.contenido.location = w+"?Rep="+x+"&Tex=bib_"+x+"&Bot=demanda_"+x+"&Pub=publico_"+x+"&Dat="+y+"&Nom="+z+"&Con="+wx+"&Fec="+document.getElementById("fecha_s").value+"&Bib="+document.getElementById("id_b").value;
document.getElementById("clon_"+x).style.backgroundColor = "#00CC99";
}
function clon_creado(w,x,y,z,wx,wy) {
if(z == '0')	
	document.getElementById("clon_"+x).innerHTML = "<a href='javascript:void(0);' onclick=\"c_instancias('../reporte/c_clonar.php','"+x+"','"+y+"','"+wy+"','"+wx+"');\" title='Consultar reportes clonados'>Consultar reportes</a>";
document.getElementById("asignado_"+x).innerHTML = y+": "+w+")";
}
function configuracion() {
err = document.getElementById("num_b").value = "";	
var id_e = document.getElementById('id_e').value.split(",");
//document.getElementById("id_e").value = "";
var val;
for(i=0; i<(id_e.length - 1); i++)
{
	error_color(1,"conter_"+id_e[i]);
	val = document.getElementById("bib_"+id_e[i]).value;
	//alert(document.getElementById("bib_"+id_e[i]).value);
	if(isNaN(val) || parseInt(val) < 0 || /^\s+$/.test(val) || val.length == 0 || val > 120)
	{
		error_color(0,"conter_"+id_e[i]);
		err += "El número de sesiones no es válido. \n";
	}
	else //if(val == "S_P_D" || parseInt(val) > 0)
	{
		document.getElementById("num_b").value += document.getElementById("publico_"+id_e[i]).innerHTML + ",";
		//document.getElementById('id_e').value += id_e[i] + ",";
	}
}
if(document.getElementById('id_e').value.length == 0 && err.length == 0)
{
	err += "No se ha indicado número de sesiones. \n";
}
if(err.length > 0)
{
	alert("Verifique los siguientes errores: \n\n"+err+"\n");
	//document.getElementById('id_e').value = id_e.join(",");
	return false;
}
else
{
	document.getElementById("guardar").disabled = true;
	return true;
}	
}
function busqueda() {
var op, bib;
err = "";
bib = document.getElementById("area_r").selectedIndex;
if(bib == null || isNaN(bib) || bib == 0 || /^\s+$/.test(bib))
	err += "Se requiere el área responsable. \n";
op = document.getElementById("nodo").selectedIndex;
if(op == null || isNaN(op) || op == 0 || /^\s+$/.test(op))
	err += "Se requiere el nombre del nodo. \n";
op = document.getElementById("biblo").selectedIndex;
if(op == null || isNaN(op) || op == 0 || /^\s+$/.test(op))
	err += "Se requiere el nombre de la biblioteca. \n";
if(err.length > 0)
{
	alert("Verifique los siguientes errores: \n\n"+err+"\n");
	return false;
}
else
{
	//document.getElementById("unico").checked = false;
	document.getElementById("fecha_s").value = document.getElementById("fecha").value;
	datosserver(document.getElementById("biblo")[op].value, document.getElementById("area_r")[bib].value, document.getElementById("fecha").value);
	return false;
}
}
function serv_mensual(w) {
var z = window.open(w, 'Servicio mensual', 'resizable=yes,menubar=no,scrollbars=yes,width=660,height=530');
z.focus();
}
function marcar_todo() {
var id_e = document.getElementById('id_e').value.split(",");
if(document.getElementById("unico_valor").value >= 0)
{
	for(i=0; i<(id_e.length - 1); i++)
	{
		if(document.getElementById("bib_"+id_e[i]).readOnly == false)
		{
			document.getElementById("bib_"+id_e[i]).value = document.getElementById("unico_valor").value;
		}
	}
}
}
window.onload = function() {
document.getElementById("area_r").onchange = function(){borrar()};
}
</script>
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" --><!-- InstanceEndEditable -->
</head>
<body>
<div id="datos" align="center">
 <h4>Consolidando datos...<br />
 <img src="../img/loader.gif" width="16" height="16" /></h4>
 </div>
<div align="left"><?php //echo breadcrumbs(); ?></div>
<div align="center" class="popup">
<!-- InstanceBeginEditable name="contenidos" -->
<h4 align="left">&nbsp; Configuraci&oacute;n mensual de reportes</h4>
<?php 
//echo "<h2 align='center'>Registro de actividades cerrado</h2>";
//exit;
$envio = "#"; //caso contrario #
if($_SESSION['MM_Usr_Pri'] <= 4)
	include("../script/filtro_a_b.php");
else
	echo "<strong>No posee privilegios para esta p&aacute;gina</strong>";	
?>
<form name ="formconf" method ="POST" action ="g_configuracion.php" onsubmit="return configuracion();">
<input name="id_b" id="id_b" type="hidden" value="<?php echo $_GET["bib"]; ?>" />
<input name="fecha_s" id="fecha_s" type="hidden" value="" />
<input name="num_b" id="num_b" type="hidden" value="" /> <!-- Cantidad de asistenites esperados en el mes para el evento -->
<input name="id_e" id="id_e" type="hidden" value="" /> <!-- lis ids de cada uno de los eventos cargados -->
<div style="width:100%; height:auto; margin:0px auto; z-index:0;" id="estadisticas"></div><!-- listado de reportes -->
<div align="left" id="div_unico" style="display:none; background-color:#EEE; border-radius:5px; padding-left:10px; z-index:0;">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="33%">&nbsp;</td>
    <td width="34%">
    <div align="center" id="db_guardar"><input name="guardar" id="guardar" type="submit" value="Guardar" /></div>
    </td>
    <td width="33%">
    <div style="display:;" align="right" id="tod_estad"><a href="#" onclick="mostrar_todo();">Mostrar todos los reportes</a></div>
    </td>
  </tr>
</table>
</div>
<div class="div_menu" id="aa_1" style="clear:both;"><a href="javascript:void(0);" onclick="menu('a_b_','1'); mostrar('bb','1');" title="Ayuda">? +</a></div>
<div class="div_menu" style="display:none;" id="bb_1"><a href="javascript:void(0);" onclick="menu('a_b_','2'); mostrar('aa','1');" title="Ayuda">? -</a></div>
<div style="clear:both;">
<div class="div_ayuda" id="a_b_" style="display:none;">Los reportes y sus sesiones para esta concesión, están prestablecidos de acuerdo a los lineamientos, no pueden ser modificados. Bajo cada nombre de reporte se encuentra el alias con que cada biblioteca puede identificar cada programa, puede ser modificado dando clic en el enlace y digitando el nombre. El público esperado es opcional, para ingresar una cantidad, de clic en el enlace e ingrese la cifra. Para bloquear un reporte para toda la Red, ingrese por “Configuración / Reporte de estadísticas”, seleccione la línea responsable y del listado en el menú de opciones seleccione “Suprimir/Activar”. Para modificar la cantidad de sesiones, se debe repetir el paso anterior, pero seleccionar “Programación” y en la biblioteca correspondiente indicar “0” cero.</div>
</div>
</form>

<!-- InstanceEndEditable -->
<form name="chang_elemt" id="chang_elemt" target="contenido" method="post"></form>
</div></body>

<script language="javascript">
verif();
</script>

<!-- InstanceEnd --></html>