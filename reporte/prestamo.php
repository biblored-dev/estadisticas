<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/contenedor.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php
include("../script/scripts/session.php");
//include("../script/breadcrumbs.php");
?>
<link href="../css/css.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../script/scripts/jquery.min.js"></script>
<script type="text/javascript" src="../script/menu_ocultar.js"></script>
<script type="text/javascript">
function verif() {
document.getElementById('datos').style.display='none';
}
function foco_in(z) {
document.getElementById(z).focus();
}
</script>
<title>.: Sistema general de estad&iacute;stica :.</title>
<link rel="shortcut icon" href="../img/favicon.ico" />
<!-- InstanceBeginEditable name="doctitle" -->
<script language="javascript" src="../script/datosxml.js"></script>
<script language="javascript" src="../script/coleccion.js.php"></script>
<script language="javascript">
//var campo = new Array();
function get_fech(w, x) {
w = w.split('/');
var ret = w[1];
if(w[0] < 10)
	ret += "0"+w[0];
else
	ret += w[0];
if(x == 'i')
{
	ret += "01";
}
if(x == 'f')
{
	ret += new Date(w[1] || new Date().getFullYear(), w[0], 0).getDate();
}
return ret;
}
function datosserver(w) {
/*campo.length = coleccion.length;*/
for(var dato in coleccion){
//campo[dato] = 0;
if(document.getElementById("bib_"+dato))
{
	document.getElementById("bib_"+dato).value = 0;
}
}
  var fi = get_fech(document.getElementById('fecha').value, 'i');
  var ff = get_fech(document.getElementById('fecha').value, 'f');
  document.getElementById('datos').style.display = "";
  //var sql = "select count(z36_ID) as total, z30_collection as coleccion from BIB50.Z36, bib50.z30 where z30_sub_library = '"+w+"' and z36_sub_library = '"+w+"' and z30_collection in('GEN', 'INF', 'DISGR', 'SONOT', 'VIDET', 'MULT', 'BEBET', 'HEMER', 'LIBVI', 'LUDOT', 'PCPOR', 'INTER') and z30_REC_KEY = z36_REC_KEY and Z36_LOAN_DATE BETWEEN '"+fi+"' and '"+ff+"' group by z30_collection union select count(z36H_ID) as total, z30_collection as coleccion from BIB50.Z36H, bib50.z30 where z30_sub_library = '"+w+"' and z36H_sub_library = '"+w+"' and z30_collection in('GEN', 'INF', 'DISGR', 'SONOT', 'VIDET', 'MULT', 'BEBET', 'HEMER', 'LIBVI', 'LUDOT', 'PCPOR', 'INTER') and z30_REC_KEY = z36H_REC_KEY and Z36H_LOAN_DATE BETWEEN '"+fi+"' and '"+ff+"' group by z30_collection";
  var sql = "select count(*) as total, Z30_collection as coleccion from BIB50.Z36, bib50.Z30 where Z36_sub_library = '"+w+"' and Z30_sub_library = '"+w+"' and z30_item_status != 3 and Z36_REC_KEY = Z30_REC_KEY and Z36_LOAN_DATE BETWEEN '"+fi+"' and '"+ff+"' group by Z30_collection union select count(*) as total, Z30_collection as coleccion from BIB50.Z36H, bib50.Z30 where Z36H_sub_library = '"+w+"' and Z30_sub_library = '"+w+"' and z30_item_status != 3 and Z36H_REC_KEY = Z30_REC_KEY and Z36H_LOAN_DATE BETWEEN '"+fi+"' and '"+ff+"' group by Z30_collection";
  http.open("GET", "../script/alephxml.php?d1=0&d2=1&dat="+sql, true); //False para que no continue el flujo hasta compeltar la petición ajax
  http.onreadystatechange = datosHttpResp;
  return http.send(null);
}

function datosHttpResp() {
if(http.readyState == 4)
{
	if(http.status == 200)
	{
		var salida = "";
		var total_prest = 0;
		var timeValue = http.responseXML.getElementsByTagName("timenow")[0];
	    if(timeValue.childNodes.length > 0)
		{
			document.getElementById("otros_pres").innerHTML = "";
			var regs = timeValue.childNodes[0].nodeValue;
			regs = regs.split(";");
			for(i = 0; i < (regs.length) - 1; i ++)
			{
			   salida = regs[i].split(",,");
			   total_prest += parseInt(salida[0]);
			   if(salida[1] == "SONOT" || salida[1] == "VIDET")
			   {
				   document.getElementById("bib_SONOT").value = parseInt(document.getElementById("bib_SONOT").value) + parseInt(salida[0]);
			   }
			   else
			   {
				   if(document.getElementById("bib_"+salida[1]))
				   {
						document.getElementById("bib_"+salida[1]).value = parseInt(document.getElementById("bib_"+salida[1]).value) + parseInt(salida[0]);
				   }
				   else
				   {
					    document.getElementById("bib_GEN").value = parseInt(document.getElementById("bib_GEN").value) + parseInt(salida[0]);
					    if(coleccion[salida[1]])
					   		document.getElementById("otros_pres").innerHTML += coleccion[salida[1]]+":  "+parseInt(salida[0])+"<br />";
						else
							document.getElementById("otros_pres").innerHTML += salida[1].charAt(0).toUpperCase() + salida[1].slice(1)+":  "+parseInt(salida[0])+"<br />";
				   }
			   }
			}
			document.getElementById("total_pres").innerHTML = "Total prestamos: "+total_prest;
		}
		verif();
	}
} 
else
{
	document.getElementById('datos').style.display = "";
}
}

function sel_resp(w) {
for(i = 1; i < document.getElementById("funcionario").length - 1; i++)
{
	if(document.getElementById("funcionario")[i].value.toUpperCase() == w.toUpperCase())
		document.getElementById("funcionario").selectedIndex = i;
}
}
function busqueda() {
var val, err;
err = "";
var id = document.getElementById("id_esp").value.split(",");
for(i=0;i<(id.length - 1);i++)
{
	val = document.getElementById("bib_"+id[i]).value;
	if(val == null || isNaN(val) || /^\s+$/.test(val) || val.length < 1 )
		err += "Se requiere el valor del reporte "+(i + 1)+". \n";
}
val = document.getElementById("funcionario")[document.getElementById("funcionario").selectedIndex].value;
if(val == null || val.length < 5 || /^\s+$/.test(val) || val == "Adicionar funcionario")
	err += "Se requiere el nombre del responsable. \n";
val = document.getElementById("descrp").value;
if(val == null || !isNaN(val) || val.length < 20 || /^\s+$/.test(val))
	err += "Se requiere la descripción. \n";
if(err.length > 0)
{
	alert("Verifique los siguientes errores: \n\n"+err+"\n");
	return false;
}
else
	return true;	
}
</script>
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" --><!-- InstanceEndEditable -->
</head>
<body>
<div id="datos" align="center">
 <h4>Consolidando datos...<br />
 <img src="../img/loader.gif" width="16" height="16" /></h4>
 </div>
<div align="left"><?php //echo breadcrumbs(); ?></div>
<div align="center" class="popup">
<!-- InstanceBeginEditable name="contenidos" -->
<?php
include("../Connections/conect.inc.php");
if(isset($_POST["button"], $_POST["id_esp"], $_POST["id_rep"], $_POST["bib"]))
{
include("../script/cant.php");
$ids = explode(",",$_POST["id_esp"]);
$idsal = explode(",",$_POST["id_sal"]);
for($i=0; $i<$_POST["cantidad"]; $i++)
{ 
	$claus = $_POST["fecha"]."' and Sala = '".$idsal[$i];
	if(cant("prestamos","Fecha",$claus) == 0) //tabla, campo, dato
	{
		$sql = "insert into prestamos (Id, Fecha, Fecha_R, Biblioteca, Sala, Id_Report, Prestamos, Responsable, Descripcion) VALUES ('', '".$_POST["fecha"]."', '".date("Y-n-j")."', '".$_POST["bib"]."', '".$idsal[$i]."', '".$_POST["id_rep"]."', '".$_POST["bib_".$ids[$i]]."', '".$_POST["funcionario"]."', '".addslashes($_POST["descrp"])."')";
	}
	else
	{
		$sql = "update prestamos set Prestamos = '".$_POST["bib_".$ids[$i]]."', Fecha_R = '".date("Y-n-j")."' where Sala = '".$idsal[$i]."' and Fecha = '".$_POST["fecha"]."'";
	}
	//echo $sql;
	$exc = mysqli_query($conect, $sql);
}
//$sql = "update programacion set Sesiones = ".$_POST["cantidad"]." where Id = ".$_POST["id_rep"];
//$sql = "update programacion set Sesiones = '1' where Id = ".$_POST["id_rep"];
//$exc = mysqli_query($conect, $sql);
if($exc)
{
    echo "<h3 align='center'>Registro actualizado</h3>";
    ?><script language="javascript">
    alert("Registro actualizado");
    window.close(); 
    </script><?php
}
else
{
    echo "<h3 align='center'>Error al actualizar el registro</h3>";
    exit;
}
unset($exc, $sql, $i, $row, $ids, $idsal);
mysqli_close($conect);
}
?>
<div align="left">
<img src="../icon/visitas.png" width="40" height="35" alt="icono" /> &nbsp; 
<h3 align="center">Reporte estad&iacute;stico para <?php echo $_GET["Reporte"] ?></h3>
<div align="center">(<strong>&Aacute;lias:&nbsp;<?php echo $_GET["alias"] ?></strong>)<br />
<strong>Biblioteca: <?php echo $_GET["nom"] ?></strong>, mes <?php echo $_GET["fech_rep"]; ?></div></div><br />
<?php
if(isset($_GET["bib"], $_GET["nom"]))
{ 
$sql = "select salas.Id, salas.Nombre, salas.Coleccion, biblioteca.Abrev from salas, biblioteca where salas.Biblioteca = ".$_GET["bib"]." and biblioteca.Id = ".$_GET["bib"]." and salas.Consulta = 1 union select o_prestamos.Id, o_prestamos.Nombre, o_prestamos.Coleccion, biblioteca.Abrev from o_prestamos, biblioteca where o_prestamos.Biblioteca = ".$_GET["bib"]." and biblioteca.Id = ".$_GET["bib"];
//echo $sql;
$excs = mysqli_query($conect, $sql);
$total_prest = 0;
$ids_v = "";
$ids_s = "";
$fecha_r = "0000-00-00";
?>
<div align="center">
<form name ="formiden" method ="POST" action ="prestamo.php?bib=<?php echo $_GET["bib"]; ?>&nom=<?php echo $_GET["nom"]; ?>" onsubmit="return busqueda();">
<input name="id_rep" id="id_rep" type="hidden" value="<?php echo $_GET["id_rep"]; ?>" />
<input name="bib" id="bib" type="hidden" value="<?php echo $_GET["bib"]; ?>" />
<input name="nombre" id="nombre" type="hidden" value="<?php echo $_GET["nom"]; ?>" />
<input name="cantidad" id="cantidad" type="hidden" value="<?php echo mysqli_num_rows($excs); ?>" />
<input name="fecha" id="fecha" type="hidden" value="<?php echo $_GET["fech_rep"]; ?>" />
<?php
for($i=0; $i<mysqli_num_rows($excs); $i++)
{
	$rows = mysqli_fetch_array($excs);
	$sql = "select Fecha_R, Prestamos, Responsable from prestamos where prestamos.Sala = ".$rows["Id"]." and Fecha = '".$_GET["fech_rep"]."'";
	$excss = mysqli_query($conect, $sql);
	if(mysqli_num_rows($excss))
	{
		$rowss = mysqli_fetch_array($excss);
		$fecha_r = $rowss["Fecha_R"];
		$total_prest += $rowss["Prestamos"];
	}
	else
		$rowss["Prestamos"] = "";
	?>
    <div class="list_js" align="left"><strong><?php echo ($i + 1).". ".$rows["Nombre"]; ?></strong>
    <input name="bib_<?php echo $rows["Coleccion"]; ?>" id="bib_<?php echo $rows["Coleccion"]; ?>" type="text" value="<?php echo $rowss["Prestamos"]; ?>" placeholder="000" />
    </div>
<?php
$ids_v .= $rows["Coleccion"].",";
$ids_s .= $rows["Id"].",";
}
?>
<div align="right" style="clear:both;"><strong><a href="javascript:void(0);" onclick="datosserver('<?php echo $rows["Abrev"]; ?>');">Actualizar informacion desde Aleph</a></strong></div>
<div align="left">&Uacute;ltima fecha de actualizaci&oacute;n de registros: <?php echo $fecha_r; ?></div>
<strong>
<div align="left" style="clear:both; background-color:#EBEEE5; padding:5px; width:90%; border-radius:3px;">Otros pr&eacute;stamos sumados a sala general:
<div align="left" id="otros_pres"></div>
<div align="left" id="total_pres">Total pr&eacute;stamos: <?php echo $total_prest; ?></div></div></strong>
<div align="left" style="clear:both;"><strong>Nombre del responsable:</strong></div>
<div align="left" style="width:50%; float:left;"><?php include("../script/funcionarios.php"); ?></div>
<?php
if(isset($rowss["Responsable"]))
{ ?>
<script language="javascript">sel_resp('<?php echo $rowss["Responsable"]; ?>');</script>
<?php } ?>
<div align="left" style="clear:both;"><strong>Descripci&oacute;n de la actividad realizada:</strong></div>
<?php include("../script/caracteristicas.php"); ?>
<div align="center">
<input type="submit" name="button" id="button" value="Guardar"/>
</div>
<input name="id_esp" id="id_esp" type="hidden" value="<?php echo $ids_v; ?>" />
<input name="id_sal" id="id_sal" type="hidden" value="<?php echo $ids_s; ?>" />
</form>
<?php include("../script/observacion.php"); ?>
<div class="infos" align="left" style="padding:10px; background-color:#EBEEE5;">El proceso puede tardar entre dos (2) y tres (3) minutos, <strong>por favor no cierre la ventana hasta que termine</strong>. La informaci&oacute;n es consolidada a la fecha y hora de la consulta, se debe guardar la informaci&oacute;n para que sea almacenada.</div>
</div>
<?php 
mysqli_free_result($excs);
@ mysqli_free_result($excss);
unset($excs, $excss, $sql, $i, $rows, $rowss, $ids_v, $ids_s, $total_prest);
}
?>
<!-- InstanceEndEditable -->
<form name="chang_elemt" id="chang_elemt" target="contenido" method="post">
<!-- InstanceBeginEditable name="campos" -->

<!-- InstanceEndEditable -->
</form>
</div></body>

<script language="javascript">
verif();
</script>

<!-- InstanceEnd --></html>