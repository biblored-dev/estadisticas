<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/reporte.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php
include("../script/scripts/session.php");
//include("../script/breadcrumbs.php");
?>
<link href="../css/css.css" rel="stylesheet" type="text/css" />
<script src="../script/calendario.js"></script>
<script src="../script/horario_2.js"></script>
<script type="text/javascript">
function serv_mensual(w) {
var zz = window.open(w, 'Servicio mensual', 'resizable=yes,menubar=no,scrollbars=yes,width=660,height=600');
zz.focus();
}
function verif() {
document.getElementById('datos').style.display='none';
}
function foco_in(z) {
document.getElementById(z).focus();
}
function busqueda() {
var val, op;
err = "";
document.getElementById("f_pob").value = "";
for(i = 0; i < 4; i++)
{
	if(document.getElementById("franja_pob")[i].selected == true)
       	document.getElementById("f_pob").value += document.getElementById("franja_pob")[i].value+",";
}
val = document.getElementById("fecha").value;
if(val == null || val.length < 5 || /^\s+$/.test(val))
	err += "Se requiere la fecha de la actividad. \n";
val = document.getElementById("hora").value;
if(val == null || val.length < 3 || /^\s+$/.test(val))
	err += "Se requiere el tiempo empleado en la actividad. \n";
val = document.getElementById("participantes").value;
if(isNaN(val) || val == 0 || /^\s+$/.test(val))
	err += "Se requiere el número de participantes. \n";
val = document.getElementById("funcionario")[document.getElementById("funcionario").selectedIndex].value;
if(val == null || val.length < 5 || /^\s+$/.test(val) || val == "Adicionar funcionario")
	err += "Se requiere el nombre del responsable de reportar la actividad. \n";
val = document.getElementById("f_pob").value;
if(val == null || val.length < 2 || /^\s+$/.test(val))
	err += "Se requiere la franja poblacional. \n";
val = document.getElementById("descrp").value;
if(val == null || val.length < 20 || /^\s+$/.test(val))
	err += "Se requiere la descripción de la actividad. \n";
op = document.getElementById("tip_r").selectedIndex;
val = document.getElementById("tip_r")[op].value;
if(val == null || val.length == 0 || /^\s+$/.test(val))
	err += "Se requiere el alcance del reporte. \n";
op = document.getElementById("t_resp").selectedIndex;
val = document.getElementById("t_resp")[op].value;
if(val == null || val.length == 0 || /^\s+$/.test(val))
	err += "Se requiere el tipo de responsable de realizar la actividad. \n";
if(err.length > 0)
{
	alert("Verifique los siguientes errores: \n\n"+err+"\n");
	return false;
}
else
	return true;
}
</script>
<title>.: Sistema general de estad&iacute;stica :.</title>
<link href="../menu/css/css.css" rel="stylesheet" type="text/css" />
<link rel="shortcut icon" href="../img/favicon.ico" />
<!-- InstanceBeginEditable name="doctitle" -->

<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" --><!-- InstanceEndEditable -->
</head>
<body>
<noscript>
<h3 align="center" class="contend">Estimado usuario, esta p&aacute;gina requiere para su funcionamiento el uso de JavaScript. Si lo ha deshabilitado intencionadamente, por favor vuelva a activarlo durante el proceso de registro de informaci&oacute;n o solicite la ayuda a CST.</h3>
</noscript>
<div id="datos" align="center">
 <h4>Consolidando datos...<br />
 <img src="../img/loader.gif" width="16" height="16" /></h4>
 </div>
<div align="left"><?php //echo breadcrumbs(); ?></div>
<div align="center" class="popup">
<!-- InstanceBeginEditable name="contenidos" -->
<!-- InstanceEndEditable -->
<?php
include("../Connections/conect.inc.php");
if(isset($_POST["fecha"], $_POST["hora"], $_POST["participantes"], $_POST["id_rep"], $_POST["bib"]))
{
$sql = "insert into estadistica (Id, Fecha, Biblioteca, Hora, Id_Report, Participantes, Responsable, Franja, Tipo, Actividad, Alcance, Descripcion, Fecha_R) VALUES ('', '".$_POST["fecha"]."', '".$_POST["bib"]."', '".$_POST["hora"]."', '".$_POST["id_rep"]."', '".$_POST["participantes"]."', '".strtoupper($_POST["funcionario"])."', '".$_POST["f_pob"]."', '".$_POST["tipo_act"]."', '".$_POST["tip_r"]."', '".$_POST["t_resp"]."', '".addslashes($_POST["descrp"])."', '".date("Y-n-j")."')";
//echo $sql;
$exc = mysqli_query($conect, $sql);
if($exc) 
{
    echo "<h3 align='center'>Registro actualizado</h3>";
    unset($sql,$exc);
	mysqli_close($conect);
    ?><script language="javascript">
    alert("Registro actualizado");
    window.close(); 
    </script><?php
}
else
{
    echo "<h3 align='center'>Error al actualizar el registro</h3>";
    exit;
}
}
?>
<form name ="formulario" method ="POST" action ="talleres.php" onsubmit="return busqueda();">
<input name="id_rep" id="id_rep" type="hidden" value="<?php echo $_GET["id_rep"]; ?>" />
<input name="bib" id="bib" type="hidden" value="<?php echo $_GET["bib"]; ?>" />
<input name="nombre" id="nombre" type="hidden" value="<?php echo $_GET["nom"]; ?>" />
<input name="f_pob" id="f_pob" type="hidden" value="" />
<table width="99%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="10%">&nbsp;</td>
    <td width="80%"><div align="center"><h3>Reporte estad&iacute;stico para <?php echo $_GET["Reporte"]; ?></h3>
    (<strong>&Aacute;lias:&nbsp;<?php echo $_GET["alias"] ?></strong>)<br />
    <strong>Biblioteca: <?php echo $_GET["nom"] ?></strong></div>
    </td>
    <td width="10%">&nbsp;</td>
  </tr>
  <tr>
    <td rowspan="10">&nbsp;</td>
    <td><div id="left_men">
      <ul>
        <?php
            $sql = "select estadistica.Id, estadistica.Fecha, estadistica.Franja, estadistica.Actividad, programacion.Sesiones from estadistica, programacion where estadistica.Biblioteca = ".$_GET["bib"]." and estadistica.Id_Report = ".$_GET["id_rep"]." and programacion.Fecha = '".$_GET["fech_rep"]."' and estadistica.Id_Report = programacion.Id";
            //echo $sql;
            $excs = mysqli_query($conect, $sql);
			if(mysqli_num_rows($excs) > 0)
            { ?>
        <li style="width:160px;"><a href="javascript:void(0);">Reportes creados <?php echo mysqli_num_rows($excs); ?> de <span id="num_progr">&nbsp;</span></a>
          <ul style="z-index:1000;">
            <?php
            for($i=0; $i<mysqli_num_rows($excs); $i++)
			{ 
            	$rows = mysqli_fetch_array($excs);
            ?>
            <li><a href="javascript:void(0);" onclick="serv_mensual('<?php echo "m_talleres.php?id_report=".$rows['Id']."&bib=".$_GET["bib"]."&nom=".$_GET["nom"]."&report=".$_GET["Reporte"]."&alias=".$_GET["alias"]."&campo=".$_GET["campo"]."&tip_repor=".$rows["Actividad"]."&id_rep=".$_GET["id_rep"]."&fech_rep=".$_GET["fech_rep"]."&franja_pob=".$rows["Franja"]; ?>');"><?php echo $rows['Fecha']; ?></a></li>
            <?php } ?>   
            </ul>
          <script language="javascript">
				document.getElementById("num_progr").innerHTML = "<?php if($rows['Sesiones'] == 255) echo 'N'; else echo $rows['Sesiones']; ?>";
			 </script>
          <?php } ?> 
          </li>
        </ul>
      </div>
    </td>
    <td rowspan="10">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><div style="float:left; width:49%;"><div align="left"><strong>Fecha de la actividad:</strong></div>
      <div align="left" style="width:99%;"><span class="cssToolTip"><input name="fecha" id="fecha" type="text" size="50" style="width:85%;" onFocus="doShow('date_p','formulario','fecha');" readonly="readonly" placeholder="aaaa-m-d" />
        <img src="../icon/calendar_.gif" alt="seleccione" width="24" height="12" onClick="doShow('date_p','formulario','fecha')" /><span>Haga clic para abrir el calendario y seleccionar la fecha</span></span><br />
        <div class="date_p" id="date_p" align="left" style="display:none;">&nbsp;</div>
      </div></div>
      <div style="float:right; width:49%;">
      <div align="left"><strong> &nbsp; &nbsp;Tipo de actividad:</strong></div>
      <div align="left" style="padding-left:10px;">
      <?php include("../script/tipo_report.php"); ?>
      </div>
      </div>
    </td>
  </tr>
  <tr>
    <td><div style="float:left; width:49%"><div align="left"><strong>Tiempo utilizado en la actividad:</strong></div>
    <div align="left" style="width:99%;"><span class="cssToolTip"><input name="hora" id="hora" type="text" size="50" style="width:85%;" onFocus="doHour('asin_hor','formulario','hora');" placeholder="1:00" />
   <img src="../icon/wait.png" alt="seleccione" width="12" height="16" onClick="doHour('asin_hor','formulario','hora');" /><span>Haga clic para abrir el horario. Ajuste el tiempo requerido y luego haga clic en ‘Seleccionar’</span></span><br />
    <div class="date_p" id="asin_hor" align="left" style="display:none;">&nbsp;</div>
    </div></div>
    <div style="float:right; width:49%">
    <div align="left" style="padding-left:10px;">
    <?php include("../script/tipo_respon.php"); ?>
    </div>
    </div>
    </td>
  </tr>
  <tr>
    <td><div align="left"><strong><?php
    if(strlen($_GET["campo"]) > 0)
    	echo $_GET["campo"];
    else
    	echo "N&uacute;mero de usuarios o cantidad a reportar";
    ?>:</strong></div>
    <div align="left" style="width:46%;"><input name="participantes" id="participantes" type="text" size="50" placeholder="000" /></div></td>
  </tr>
  <tr>
    <td><div align="left"><strong>Responsable de reportar la actividad:</strong></div>
    <div align="left"><?php include("../script/funcionarios.php"); ?></div>
    </td>
  </tr>
  <tr>
    <td><div align="left"><strong>Seleccione la franja poblacional:</strong></div>
    <div align="left"><?php include("../script/franja_p.php"); ?></div>
    </td>
  </tr>
  <tr>
    <td><div align="left"><strong>Descripci&oacute;n de la actividad realizada:</strong></div>
    <?php include("../script/caracteristicas.php"); ?>
    </td>
  </tr>
  <tr>
    <td><div align="center" id="activ_add">
    <?php
    if(mysqli_num_rows($excs) > 0 && mysqli_num_rows($excs) >= $rows['Sesiones'])
    { ?>
    	<h3>Se han completado las sesiones programadas</h3>
        <div id="activ_adic"></div>
        <input name="tipo_act" id="tipo_act" type="hidden" value="1" />
    <?php 
    }
    elseif(mysqli_num_rows($excs) > 0 && $_GET["Continuo"] == 1)
    { ?>
    	<h3>Este reporte es &uacute;nico al mes y ya fue creado,<br />para cambiar la informaci&oacute;n modifique el actual</h3>
    <?php }
    else
    { ?>
    	<div id="activ_norm"></div>
        <input name="tipo_act" id="tipo_act" type="hidden" value="0" />
    <?php
    }
    unset($sql,$excs,$rows,$i);
    ?>
    </div></td>
  </tr>
  <tr>
    <td>
    <?php include("../script/observacion.php"); ?>
    </td>
  </tr>
</table>
</form>
<form name="chang_elemt" id="chang_elemt" target="contenido" method="post"></form>
</div></body>
<script language="javascript">
verif();
if(document.getElementById("activ_adic"))
{
	document.getElementById("activ_adic").innerHTML = "<input name='guardar' id='guardar' type='submit' value='Guardar como actividad adicional' />";
}
else if(document.getElementById("activ_norm"))
{
	document.getElementById("activ_norm").innerHTML = "<input name='guardar' id='guardar' type='submit' value='Guardar actividad' />";
}
</script>
<!-- InstanceEnd --></html>