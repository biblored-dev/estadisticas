﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/contenedor.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php
include("../script/scripts/session.php");
//include("../script/breadcrumbs.php");
?>
<link href="../css/css.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../script/scripts/jquery.min.js"></script>
<script type="text/javascript" src="../script/menu_ocultar.js"></script>
<script type="text/javascript">
function verif() {
document.getElementById('datos').style.display='none';
}
function foco_in(z) {
document.getElementById(z).focus();
}
</script>
<title>.: Sistema general de estad&iacute;stica :.</title>
<link rel="shortcut icon" href="../img/favicon.ico" />
<!-- InstanceBeginEditable name="doctitle" -->

<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" --><!-- InstanceEndEditable -->
</head>
<body>
<div id="datos" align="center">
 <h4>Consolidando datos...<br />
 <img src="../img/loader.gif" width="16" height="16" /></h4>
 </div>
<div align="left"><?php //echo breadcrumbs(); ?></div>
<div align="center" class="popup">
<!-- InstanceBeginEditable name="contenidos" -->
<?php
if(isset($_POST["id_b"], $_POST["id_e"], $_POST["guardar"]))
{
	$f_r = explode("/",$_POST["fecha_s"]);
	$f1 = strtotime(date("Y")."-".date("m")."-".date("d"));
	$f2 = strtotime(date($f_r[1]."-".$f_r[0]."-".date('t', mktime(0, 0, 0, $f_r[0], 1, $f_r[1]))));
	$f3 = $f1 - $f2;
	//echo ($f3 / 86400)." Días";
	if(($f3 / 86400) < 5)
	{
		require_once("../Connections/conect.inc.php");
		$id_e = explode(",",$_POST["id_e"]); //Id de cada programa
		//echo print_r($id_e);
		$num_b = explode(",",$_POST["num_b"]); //Publico esperado
		if(sizeof($id_e > 1))
		{
			for($i=0; $i<(sizeof($id_e) - 1); $i++)
			{
				$sql = "select Id from programacion where Reporte = ".$id_e[$i]." and Biblioteca = ".$_POST["id_b"]." and Fecha = '".$_POST["fecha_s"]."'";
				$exc = mysqli_query($conect, $sql);
				if($_POST["bib_".$id_e[$i]] == "S_P_D")
				{
					$_POST["bib_".$id_e[$i]] = "255";
				}
				if(mysqli_num_rows($exc) > 0)
				{
					$sql = "update programacion set Sesiones = ".$_POST["bib_".$id_e[$i]].", Publico = ".$num_b[$i]." where Reporte = ".$id_e[$i]." and Biblioteca = ".$_POST["id_b"]." and Fecha = '".$_POST["fecha_s"]."'";
					$excs = mysqli_query($conect, $sql);
				}
				elseif($_POST["bib_".$id_e[$i]] > 0)
				{
					$sql = "insert into programacion (Id, Biblioteca, Reporte, Sub, Sesiones, Fecha, Publico) values ('', ".$_POST["id_b"].", ".$id_e[$i].", '0', ".$_POST["bib_".$id_e[$i]].", '".$_POST["fecha_s"]."', ".$num_b[$i].")";
					$excs = mysqli_query($conect, $sql);
				}
				//echo $sql;
			}
			if($excs)
			{
				echo "<h4 align='center'>Registro(s) creado(s) - actualizado(s)</h4>";
				echo "<h4 align='center'>Fecha para registrar ".date("n/Y")."</h4>";
				?>
				<script language="javascript">
				alert("Registro(s) creado(s) - actualizado(s).");
				parent.contenido.location = "../fill.php";
				</script>
				<?php
			}
			else
			{
				echo "<h4 align='center'>Error al registrar la informaci&oacute;n</h4>";
			}
			unset($sql,$exc,$i,$id_e,$excs,$num_b);
			mysqli_close($conect);
		}
	}
	else
	{
		?>
		<script language="javascript">
        alert("El tiempo de espera para cambios ha finalizado");
        parent.contenido.location = "../fill.php";
        </script>
        <?php
		echo "<h3 align='center'>Error al registrar la informaci&oacute;n, han transcurrido ".($f3 / 86400)." días del cierre del mes</h3>";
	}
	unset($f_r, $f1, $f2, $f3);
} ?>
<!-- InstanceEndEditable -->
<form name="chang_elemt" id="chang_elemt" target="contenido" method="post">
<!-- InstanceBeginEditable name="campos" -->

<!-- InstanceEndEditable -->
</form>
</div></body>

<script language="javascript">
verif();
</script>

<!-- InstanceEnd --></html>