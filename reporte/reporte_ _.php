﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/contenedor.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php
include("../script/scripts/session.php");
//include("../script/breadcrumbs.php");
?>
<link href="../css/css.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../script/scripts/jquery.min.js"></script>
<script type="text/javascript" src="../script/menu_ocultar.js"></script>
<script type="text/javascript">
function verif() {
document.getElementById('datos').style.display='none';
}
function foco_in(z) {
document.getElementById(z).focus();
}
</script>
<title>.: Sistema general de estad&iacute;stica :.</title>
<link rel="shortcut icon" href="../img/favicon.ico" />
<!-- InstanceBeginEditable name="doctitle" -->
<link href="../menu/css/css.css" rel="stylesheet" type="text/css" />
<script src="../script/c_color.js"></script>
<script language="javascript" src="../script/datosxml.js"></script>
<script type="text/javascript" src="../script/mes.js"></script>
<script language="javascript">
var err = "";
function cancelar_rep(w,x,y,z) {
  document.getElementById("elimin_"+w).style.backgroundColor = "#00CC99";	
  var z = window.open('e_reporte.php?id_rep='+w+"&Reporte="+x+"&Dat="+y+"&Form="+z, 'Servicio mensual', 'resizable=yes,menubar=no,scrollbars=yes,width=720,height=650');
  z.focus();	
}
function crear_alias(w,x,z) {
var	alias = prompt("Crear un alias para el reporte y biblioteca actual:\n"+z, x);
if(alias != null && alias != x && alias != z)
{
	nuevoalias(w, document.getElementById("biblo")[document.getElementById("biblo").selectedIndex].value, document.getElementById("fecha").value, alias)
	document.getElementById("alias_"+w).innerHTML = alias;
}
else
	alert("Nombre no válido.");
}

function nuevoalias(w,x,y,z) {
  var sql = "insert into prog_alias (Id, Reporte, Biblioteca, Fecha, Alias) values ('', "+w+", "+x+", '"+y+"', '"+z+"')";
  http.open("GET", "../script/insertalias.php?dat="+sql+"&rep="+w+"&bib="+x+"&fec="+y+"&ali="+z, false); //False para que no continue el flujo hasta compeltar la petición ajax
  http.onreadystatechange = aliasHttpResp;
  return http.send(null);
}

function aliasHttpResp() {
if(http.readyState == 4)
{
	if(http.status == 200)
	{
		var timeValue = http.responseXML.getElementsByTagName("timenow")[0];
	    if(timeValue.childNodes.length > 0)
		{
			var regs = timeValue.childNodes[0].nodeValue;
			if(regs == "OK")
			{
				alert("Alias creado satisfactoriamente");
			}
			else
			{
				alert("Error al crear el alias");
			}
		}
		else
		{
			alert("Error al crear el alias");
		}
		verif();
	}
} 
else
{
	document.getElementById('datos').style.display = "";
}
}

function borrar() {
document.getElementById('estadisticas').innerHTML = "";	
parent.contenido.location = "../fill.php";
}

function edit_mensual(w) {
alert("Acción no permitida, han transcurrido \n"+w+" días del cierre del mes.");
}

function datosserver(w,x,y) {
  var sql = "select programacion.Id as Reporte, reporte.Nombre, reporte.Formulario, reporte.Id, reporte.Campo, reporte.Tipo, reporte.Continuo from reporte, programacion where reporte.Area = "+x+" and programacion.Sesiones > 0 and programacion.Fecha = '"+y+"' and programacion.Biblioteca = "+w+" and programacion.Sub != 1 and reporte.Estado = 1 and programacion.Reporte=reporte.Id order by reporte.Nombre asc, programacion.Sub asc";
  http.open("GET", "../script/datosxml_ll.php?d1=Nombre&d2="+w+"&dat="+sql+"&fecha_s="+y, true); //False para que no continue el flujo hasta compeltar la petición ajax
  http.onreadystatechange = datosHttpResp;
  return http.send(null);
}

function datosHttpResp() {
if(http.readyState == 4)
{
	if(http.status == 200)
	{
		var salida = "";
	    borrar();
		var timeValue = http.responseXML.getElementsByTagName("timenow")[0];
	    if(timeValue.childNodes.length > 0)
		{
			var regs = timeValue.childNodes[0].nodeValue;
			regs = regs.split(";");
			var listado = document.getElementById('estadisticas');
			//listado.length = regs.length;
			listado.innerHTML += "<h3 align='center'> Estadísticas disponibles </h3>";
			for(i = 0; i < (regs.length) - 1; i ++)
			{
			   salida = regs[i].split(",,");
			   programas = "<div class='list_legen' style='padding-top:6px;'><strong>"
			   if(parseInt(salida[0]) < 5 || document.getElementById('area_r').value == 6)
			   {
			   		programas += "<a href=\"javascript:void(0);\" onclick=\"serv_mensual('"+salida[3]+"?bib="+document.getElementById('biblo')[document.getElementById("biblo").selectedIndex].value+"&nom="+document.getElementById('biblo')[document.getElementById("biblo").selectedIndex].text+"&Reporte="+salida[2]+"&Continuo="+salida[7]+"&id_rep="+salida[1]+"&fech_rep="+document.getElementById("fecha").value+"&id_report="+salida[4]+"&alias="+salida[8]+"&campo="+salida[5]+"&tip_repor="+salida[6]+"',"+i+");\"> "+salida[2]+"</a></strong><div class='list_cln' id='elimin_"+salida[1]+"'><a href='javascript:void(0);' onclick=\"cancelar_rep('"+salida[1]+"','"+salida[2]+"','"+salida[5]+"','"+salida[3]+"')\" class=\"cssToolTip\"><span class='span'>Cancelar el reporte de las estadísticas de la biblioteca</span>&nbsp; Suprimir reporte &nbsp;</a></div></div>";
			   }
			   else
			   {
			   		programas += "<a href=\"javascript:void(0);\" onclick=\"edit_mensual('"+salida[0]+"');\"> "+salida[2]+"</a></strong><div class='list_cln' id='elimin_"+salida[1]+"'><a href='javascript:void(0);' onclick=\"edit_mensual('"+salida[0]+"')\" class=\"cssToolTip\"><span class='span'>Cancelar el reporte de las estadísticas de la biblioteca</span>&nbsp; Suprimir reporte &nbsp;</a></div></div>";
			   }
			   alias = "<div class='list_prgm' id='alias_"+salida[1]+"'>";
			   if(parseInt(salida[0]) < 5 || document.getElementById('area_r').value == 6)
			   {
			   		alias += "(<a href='javascript:void(0);' onclick=\"crear_alias("+salida[1]+",'"+salida[8]+"','"+salida[2]+"');\" class=\"cssToolTip\"><span>Haga clic para crearle un alias al programa, solo aplica para la biblioteca seleccionada</span>"+salida[8]+"</a>)</div>";
			   }
			   else
			   {
			   		alias += "(<a href='javascript:void(0);' onclick=\"edit_mensual('"+salida[0]+"');\" class=\"cssToolTip\"><span>Haga clic para crearle un alias al programa, solo aplica para la biblioteca seleccionada</span>"+salida[8]+"</a>)</div>";
			   }
			   reportes = "<div class='list_cln'><a  href=\"javascript:void(0);\" onclick=\"modificar('l_reporte.php?bib="+document.getElementById('biblo')[document.getElementById("biblo").selectedIndex].value+"&nom="+document.getElementById('biblo')[document.getElementById("biblo").selectedIndex].text+"&Reporte="+salida[2]+"&id_rep="+salida[1]+"&fech_rep="+document.getElementById("fecha").value+"&id_report="+salida[4]+"&alias="+salida[8]+"&modifica=_"+salida[3]+"&campo="+salida[5]+"&tip_repor="+salida[6]+"',"+i+");serv_clones('"+salida[1]+"')\" class=\"cssToolTip\" id='clones_"+salida[1]+"'><span class='span'>Consultar reportes creados hasta el momento</span>Consultar reportes</a></div>";
			   listado.innerHTML += "<div class=\"list_js\" id=\"conter_"+i+"\">"+programas+alias+reportes+"</div>";
			}
			//Otras actividades que se presentan y que no estaban programadas
			//listado.innerHTML += "<div class=\"list_js\" id=\"conter_"+i+"\"><div class='list_legen' style='padding-top:6px;'><a href=\"javascript:void(0);\" onclick=\"serv_mensual('otras_actividades.php?bib="+document.getElementById('biblo')[document.getElementById("biblo").selectedIndex].value+"&nom="+document.getElementById('biblo')[document.getElementById("biblo").selectedIndex].text+"&fech_rep="+document.getElementById("fecha").value+"&area="+document.getElementById('area_r')[document.getElementById("area_r").selectedIndex].value+"',"+i+");\" class=\"cssToolTip\"><span>Otras actividades desarrolladas durante el mes y que no hacen parte del plan de acción o de la programación establecida por la biblioteca.</span><strong> Otras actividades</strong></a></div><div class='list_cln'><a  href=\"javascript:void(0);\" onclick=\"modificar('l_reporte.php?bib="+document.getElementById('biblo')[document.getElementById("biblo").selectedIndex].value+"&nom="+document.getElementById('biblo')[document.getElementById("biblo").selectedIndex].text+"&Reporte=Otras actividades&id_rep="+salida[1]+"&fech_rep="+document.getElementById("fecha").value+"&id_report="+document.getElementById('area_r')[document.getElementById("area_r").selectedIndex].value+"&alias=Sin alias creado&modifica=_otras_actividades.php&campo="+salida[5]+"&tip_repor="+salida[6]+"',"+i+");serv_clones("+i+")\" class=\"cssToolTip\" id='clones_"+i+"'><span class='span'>Consultar reportes creados hasta el momento</span>Consultar reportes</a></div></div>";
			//fin otras actividades
			listado.innerHTML += "<div style='clear:both;' align='center'>. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .</div>";
		}
		else
			alert("No se encontraron reportes para los datos ingresados.");
		verif();
	}
} 
else
{
	document.getElementById('datos').style.display = "";
}
}

function busqueda() {
var op, bib;
err = "";
bib = document.getElementById("area_r").selectedIndex;
if(bib == null || isNaN(bib) || bib == 0 || /^\s+$/.test(bib))
	err += "Se requiere el área responsable. \n";
op = document.getElementById("nodo").selectedIndex;
if(op == null || isNaN(op) || op == 0 || /^\s+$/.test(op))
	err += "Se requiere el nombre del nodo. \n";
op = document.getElementById("biblo").selectedIndex;
if(op == null || isNaN(op) || op == 0 || /^\s+$/.test(op))
	err += "Se requiere el nombre de la biblioteca. \n";
if(err.length > 0)
{
	alert("Verifique los siguientes errores: \n\n"+err+"\n");
	return false;
}
else
{
	datosserver(document.getElementById("biblo")[op].value, document.getElementById("area_r")[bib].value, document.getElementById("fecha").value);
	return false;
}
}
function serv_clones(x) {
//document.getElementById("conter_"+x).className = "list_vis";
document.getElementById("clones_"+x).style.backgroundColor = "#00CC99";	
}
function serv_marcar(x) {
//Color de fondo para el div
document.getElementById("conter_"+x).className = "list_vis";
//document.getElementById("clones_"+y).style.backgroundColor = "#00CC99";
}
function serv_mensual(w,x) {
serv_marcar(x);
var z = window.open(w, 'Servicio mensual', 'resizable=yes,menubar=no,scrollbars=yes,width=820,height=800');
z.focus();
}
window.onload = function() {
document.getElementById("area_r").onchange = function(){borrar()};
}
</script>
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" --><!-- InstanceEndEditable -->
</head>
<body>
<div id="datos" align="center">
 <h4>Consolidando datos...<br />
 <img src="../img/loader.gif" width="16" height="16" /></h4>
 </div>
<div align="left"><?php //echo breadcrumbs(); ?></div>
<div align="center" class="popup">
<!-- InstanceBeginEditable name="contenidos" -->
<h4 align="left">&nbsp; Reporte estadístico para el mes</h4>
<?php 
$envio = "#";
include("../script/filtro_a_b.php"); ?>
<div style="width:100%; height:auto; margin:0px auto; z-index:0;" id="estadisticas"></div>
<!-- InstanceEndEditable -->
<form name="chang_elemt" id="chang_elemt" target="contenido" method="post"></form>
</div></body>

<script language="javascript">
verif();
</script>

<!-- InstanceEnd --></html>