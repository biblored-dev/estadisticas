<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/contenedor.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php
include("../script/scripts/session.php");
//include("../script/breadcrumbs.php");
?>
<link href="../css/css.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../script/scripts/jquery.min.js"></script>
<script type="text/javascript" src="../script/menu_ocultar.js"></script>
<script type="text/javascript">
function verif() {
document.getElementById('datos').style.display='none';
}
function foco_in(z) {
document.getElementById(z).focus();
}
</script>
<title>.: Sistema general de estad&iacute;stica :.</title>
<link rel="shortcut icon" href="../img/favicon.ico" />
<!-- InstanceBeginEditable name="doctitle" -->
<script language="javascript" src="../script/datosxml.js"></script>
<script language="javascript" src="../script/afiliacion.js.php"></script>
<script language="javascript">
function get_fech(w, x) {
w = w.split('/');
var ret = "";
if(w[0] < 10)
	ret = w[1]+"0"+w[0];
else
	ret = w[1]+""+w[0];
if(x == 'i')
{
	ret += "01";
}
if(x == 'f')
{
	ret += new Date(w[1] || new Date().getFullYear(), w[0], 0).getDate();
}
return ret;
}

function get_renov(w, x) {
w = w.split('/');
w[1] = (parseInt(w[1]) + 1);
var ret = "";
if(w[0] < 10)
	ret = w[1]+"0"+w[0];
else
	ret = w[1]+""+w[0];
if(x == 'i')
{
	ret += "01";
}
if(x == 'f')
{
	ret += new Date(parseInt(w[1]) || new Date().getFullYear() + 1, w[0], 0).getDate();
}
return ret;
}

function get_corte(w) {
w = w.split('/');
var ret = "";
w[0] = (parseInt(w[0]) - 1)
if(w[0] < 10)
	ret = w[1]+"0"+w[0];
ret += new Date(w[1] || new Date().getFullYear(), w[0], 0).getDate();
return ret;
}

function datosserver(w) {
for(var dato in afilaicion){
if(document.getElementById(dato))
{
	document.getElementById(dato).value = 0;
}
}
  var fi = get_fech(document.getElementById('fecha').value, 'i');
  var ff = get_fech(document.getElementById('fecha').value, 'f');
  document.getElementById('datos').style.display = "";
  //afiliaciones
  var sql = "select count(Z305_REC_KEY) as Total, case Z303.Z303_GENDER when coalesce(Z303.Z303_GENDER,' ') then Z303.Z303_GENDER else 'NI' end, Z303.Z303_FIELD_3, Z305.Z305_BOR_TYPE from BIB00.Z303, BIB50.Z305, BIB00.Z308 where Z303_REC_KEY = SUBSTR(Z305_REC_KEY,1,12) and Z308_ID = Z303_REC_KEY and substr(Z308_REC_KEY,1,2) = '01' and (Z305.Z305_OPEN_DATE  BETWEEN '"+fi+"' and '"+ff+"' or Z303.Z303_OPEN_DATE  BETWEEN '"+fi+"' and '"+ff+"') and Z303.Z303_HOME_LIBRARY = '"+w+"' group by Z303.Z303_GENDER, Z303.Z303_FIELD_3, Z305.Z305_BOR_TYPE order by Z303.Z303_GENDER, Z305.Z305_BOR_TYPE asc";
  //vencimientos
  var sql2 = "select count(*) from BIB00.Z303, BIB50.Z305, BIB00.Z308 where Z303_REC_KEY = SUBSTR(Z305_REC_KEY,1,12) and Z308_ID = Z303_REC_KEY and substr(Z308_REC_KEY,1,2) = '01' and Z305.Z305_EXPIRY_DATE BETWEEN '"+fi+"' and '"+ff+"' and Z303.Z303_HOME_LIBRARY = '"+w+"'";
  //renovaciones
  var fir = get_renov(document.getElementById('fecha').value, 'i');
  var ffr = get_renov(document.getElementById('fecha').value, 'f');
  var sql1 = "select count(*) from BIB00.Z303, BIB50.Z305, BIB00.Z308 where Z303_REC_KEY = SUBSTR(Z305_REC_KEY,1,12) and Z308_ID = Z303_REC_KEY and substr(Z308_REC_KEY,1,2) = '01' and Z305.Z305_EXPIRY_DATE BETWEEN '"+fir+"' and '"+ffr+"' and Z305.Z305_OPEN_DATE < '"+fi+"' and Z303.Z303_HOME_LIBRARY = '"+w+"'";
  
  http.open("GET", "../script/alephxml_l.php?d1=0&d2=1&d3=2&d4=3&dat="+sql+"&dat1="+sql1+"&dat2="+sql2, true); //False para que no continue el flujo hasta compeltar la petición ajax
  http.onreadystatechange = datosHttpResp;
  return http.send(null);
}

function datosHttpResp() {
if(http.readyState == 4)
{
	if(http.status == 200)
	{
		var salida = "";
		var timeValue = http.responseXML.getElementsByTagName("timenow")[0];
	    if(timeValue.childNodes.length > 0)
		{
			document.getElementById("otros_pres").innerHTML = "";
			var totral_afil = 0;
			var total_afil = 0;
			var regs = timeValue.childNodes[0].nodeValue;
			regs = regs.split(";");
			for(i = 0; i < (regs.length) - 1; i ++)
			{
			    salida = regs[i].split(",,");
				if(salida[1] == "R")
				{
					document.getElementById("Renov").value =  parseInt(salida[0]);
					total_afil = parseInt(salida[0]);
				}
				else if(salida[1] == "V")
				{
					document.getElementById("Vencim").value =  parseInt(salida[0]);
					//total_afil = parseInt(salida[0]);
				}
				else
				{
					afilaicion[salida[1]] = parseInt(afilaicion[salida[1]]) + parseInt(salida[0]);
					if(salida[1] == "F" || salida[1] == "M")
						document.getElementById("Afi_"+salida[1]).value =  parseInt(document.getElementById("Afi_"+salida[1]).value) + parseInt(salida[0]);
					if(salida[1] == "NI" || salida[1] == " ")
						document.getElementById("Afi_NI").value =  parseInt(document.getElementById("Afi_NI").value) + parseInt(salida[0]);
					if(salida[2] == "DISCAPACITADO")
						document.getElementById("Afi_D").value =  parseInt(document.getElementById("Afi_D").value) + parseInt(salida[0]);
					if(salida[3] == "01")
						document.getElementById("Cat_A").value =  parseInt(document.getElementById("Cat_A").value) + parseInt(salida[0]);
					else
						document.getElementById("Cat_B").value =  parseInt(document.getElementById("Cat_B").value) + parseInt(salida[0]);
					totral_afil +=  parseInt(salida[0]);
				}
			}
		}
		document.getElementById("otros_pres").innerHTML = totral_afil;
		document.getElementById("total_pres").innerHTML = parseInt(totral_afil + total_afil);
		verif();
	}
}
else
{
	document.getElementById('datos').style.display = "";
}
}

function sel_resp(w) {
for(i = 1; i < document.getElementById("funcionario").length - 1; i++)
{
	if(document.getElementById("funcionario")[i].value.toUpperCase() == w.toUpperCase())
		document.getElementById("funcionario").selectedIndex = i;
}
}
function busqueda() {
var val, err;
err = "";
for(var dato in afilaicion){
if(document.getElementById(dato))
{
	val = document.getElementById(dato).value;
	if(val == null || isNaN(val) || /^\s+$/.test(val) || val.length < 1 )
		err += "Se requiere el valor del "+afilaicion[dato]+". \n";
}
}
val = document.getElementById("funcionario")[document.getElementById("funcionario").selectedIndex].value;
if(val == null || val.length < 5 || /^\s+$/.test(val) || val == "Adicionar funcionario")
	err += "Se requiere el nombre del responsable. \n";
val = document.getElementById("descrp").value;
if(val == null || !isNaN(val) || val.length < 20 || /^\s+$/.test(val))
	err += "Se requiere la descripción. \n";
if(err.length > 0)
{
	alert("Verifique los siguientes errores: \n\n"+err+"\n");
	return false;
}
else
	return true;	
}
</script>
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" --><!-- InstanceEndEditable -->
</head>
<body>
<div id="datos" align="center">
 <h4>Consolidando datos...<br />
 <img src="../img/loader.gif" width="16" height="16" /></h4>
 </div>
<div align="left"><?php //echo breadcrumbs(); ?></div>
<div align="center" class="popup">
<!-- InstanceBeginEditable name="contenidos" -->
<?php
include("../Connections/conect.inc.php");
if(isset($_POST["button"], $_POST["Afi_F"], $_POST["Afi_M"], $_POST["bib"]))
{
include("../script/cant.php");

	$claus = $_POST["fecha"]."' and Biblioteca = '".$_POST["bib"];
	if(cant("afiliaciones","Fecha",$claus) == 0) //tabla, campo, dato
	{
		$sql = "insert into afiliaciones (Id, Fecha, Fecha_R, Biblioteca, Id_Report, Afi_F, Afi_M, Afi_NI, Afi_D, Cat_A, Cat_B, Renov, Vencim, Responsable, Descripcion) VALUES ('', '".$_POST["fecha"]."', '".date("Y-n-j")."', '".$_POST["bib"]."', '".$_POST["id_rep"]."', '".$_POST["Afi_F"]."', '".$_POST["Afi_M"]."', '".$_POST["Afi_NI"]."', '".$_POST["Afi_D"]."', '".$_POST["Cat_A"]."', '".$_POST["Cat_B"]."', '".$_POST["Renov"]."', '".$_POST["Vencim"]."', '".strtoupper($_POST["funcionario"])."', '".addslashes($_POST["descrp"])."')";
	}
	else
	{
		$sql = "update afiliaciones set Afi_F = '".$_POST["Afi_F"]."', Afi_M = '".$_POST["Afi_M"]."', Afi_NI = '".$_POST["Afi_NI"]."', Afi_D = '".$_POST["Afi_D"]."', Cat_A = '".$_POST["Cat_A"]."', Cat_B = '".$_POST["Cat_B"]."', Renov = '".$_POST["Renov"]."', Vencim = '".$_POST["Vencim"]."', Responsable = '".strtoupper($_POST["funcionario"])."', Descripcion = '".addslashes($_POST["descrp"])."', Fecha_R = '".date("Y-n-j")."', Id_Report = '".$_POST["id_rep"]."' where Biblioteca = '".$_POST["bib"]."' and Fecha = '".$_POST["fecha"]."'";
	}
	//echo $sql;
	$exc = mysqli_query($conect, $sql);
//$sql = "update programacion set Sesiones = ".$_POST["cantidad"]." where Id = ".$_POST["id_rep"];
$sql = "update programacion set Sesiones = '1' where Id = ".$_POST["id_rep"];
$exc = mysqli_query($conect, $sql);
if($exc)
{
    echo "<h3 align='center'>Registro actualizado</h3>";
    ?><script language="javascript">
    alert("Registro actualizado");
    window.close(); 
    </script>
	<?php
	exit;
}
else
{
    echo "<h3 align='center'>Error al actualizar el registro</h3>";
    exit;
}
unset($exc, $sql, $i, $row, $ids, $idsal);
mysqli_close($conect);
}
?>
<div align="left">
<h3 align="center">Reporte estad&iacute;stico para <?php echo $_GET["Reporte"] ?></h3>
<div align="center">(<strong>&Aacute;lias:&nbsp;<?php echo $_GET["alias"] ?></strong>)<br />
<strong>Biblioteca: <?php echo $_GET["nom"] ?></strong>, mes <?php echo $_GET["fech_rep"]; ?></div></div><br />
<?php
if(isset($_GET["bib"], $_GET["nom"]))
{ 
$sql = "select Abrev from biblioteca where biblioteca.Id = ".$_GET["bib"];
$excs = mysqli_query($conect, $sql);
$rows = mysqli_fetch_array($excs);
$abrev = $rows["Abrev"];
$sql = "select Fecha_R, Afi_F, Afi_M, Afi_NI, Afi_D, Cat_A, Cat_B, Renov, Vencim, Responsable from afiliaciones where Biblioteca = ".$_GET["bib"]." and Fecha = '".$_GET["fech_rep"]."'";
$excs = mysqli_query($conect, $sql);
$rows = mysqli_fetch_array($excs);
$fecha_r = "0000-00-00";
if($excs)
	$fecha_r = $rows["Fecha_R"];
?>
<div align="center" s style="width:100%; margin:0px auto;">
<form name ="formiden" method ="POST" action ="afiliado.php?bib=<?php echo $_GET["bib"]; ?>&nom=<?php echo $_GET["nom"]; ?>" onsubmit="return busqueda();">
<input name="id_rep" id="id_rep" type="hidden" value="<?php echo $_GET["id_rep"]; ?>" />
<input name="bib" id="bib" type="hidden" value="<?php echo $_GET["bib"]; ?>" />
<input name="nombre" id="nombre" type="hidden" value="<?php echo $_GET["nom"]; ?>" />
<input name="fecha" id="fecha" type="hidden" value="<?php echo $_GET["fech_rep"]; ?>" />

<div class="list_js" align="left"><strong>Afiliaciones femeninas</strong>
<input name="Afi_F" id="Afi_F" type="text" value="<?php echo $rows["Afi_F"]; ?>" placeholder="000" readonly="readonly" />
</div>
<div class="list_js" align="left"><strong>Afiliaciones masculinas</strong>
<input name="Afi_M" id="Afi_M" type="text" value="<?php echo $rows["Afi_M"]; ?>" placeholder="000" readonly="readonly" />
</div>
<div class="list_js" align="left"><strong>Afiliaciones institucionales</strong>
<input name="Afi_NI" id="Afi_NI" type="text" value="<?php echo $rows["Afi_NI"]; ?>" placeholder="000" readonly="readonly" />
</div>
<div class="list_js" align="left"><strong>Afiliaciones de usuarios en condici&oacute;n de discapacidad</strong>
<input name="Afi_D" id="Afi_D" type="text" value="<?php echo $rows["Afi_D"]; ?>" placeholder="000" readonly="readonly" />
</div>
<div class="list_js" align="left"><strong>Afiliaciones categoria A (<span class="infos">Menores a 8 años</span>)</strong>
<input name="Cat_A" id="Cat_A" type="text" value="<?php echo $rows["Cat_A"]; ?>" placeholder="000" readonly="readonly" />
</div>
<div class="list_js" align="left"><strong>Afiliaciones categoria B (<span class="infos">Desde los  8 años</span>)</strong>
  <input name="Cat_B" id="Cat_B" type="text" value="<?php echo $rows["Cat_B"]; ?>" placeholder="000" readonly="readonly" />
</div>
<div class="list_js" align="left"><strong>Vencimientos (<span class="infos">Total: usuarios e instituciones</span>)</strong>
  <input name="Vencim" id="Vencim" type="text" value="<?php echo $rows["Vencim"]; ?>" placeholder="000" readonly="readonly" />
</div>
<div class="list_js" align="left"><strong>Renovaciones (<span class="infos">Total: usuarios e instituciones</span>)</strong>
  <input name="Renov" id="Renov" type="text" value="<?php echo $rows["Renov"]; ?>" placeholder="000" readonly="readonly" />
</div>
<div align="right" style="clear:both;"><strong><a href="javascript:void(0);" onclick="datosserver('<?php echo $abrev; ?>');">Actualizar informacion desde Aleph</a></strong></div>
<div align="left">&Uacute;ltima fecha de actualizaci&oacute;n de registros: <?php echo $fecha_r; ?></div>
<strong>
<div align="left" style="clear:both; background-color:#EBEEE5; padding:5px; width:90%; border-radius:3px;">
<div align="left">Total afiliaciones:</div>
<div align="left" id="otros_pres"><?php echo ($rows["Afi_F"] + $rows["Afi_M"] + $rows["Afi_NI"]); ?></div>
<div align="left">Total afiliaciones m&aacute;s renovaciones:</div>
<div align="left" id="total_pres"><?php echo ($rows["Afi_F"] + $rows["Afi_M"] + $rows["Afi_NI"] + $rows["Renov"]); ?></div>
</div></strong>
<div align="left" style="clear:both;"><strong>Nombre del responsable:</strong></div>
<div align="left" style="width:50%; float:left;"><?php include("../script/funcionarios.php"); ?></div>
<?php
if($rows["Responsable"])
{ ?>
<script language="javascript">sel_resp('<?php echo $rows["Responsable"]; ?>');</script>
<?php } ?>
<div align="left" style="clear:both;"><strong>Descripci&oacute;n de la actividad realizada:</strong></div>
<?php include("../script/caracteristicas.php"); ?>
<div align="center">
<input type="submit" name="button" id="button" value="Actualizar" />
</div>
</form>
<?php include("../script/observacion.php"); ?>
<div class="infos" align="left" style="padding:10px; background-color:#EBEEE5;">La informaci&oacute;n es consolidada a la fecha y hora de la consulta, desde el sistema bibliogr&aacute;fico, se debe actualizar la informaci&oacute;n para que sea almacenada. El proceso puede tardar unos minutos, por favor no cierre la ventana hasta que termine.</div>
</div>
<?php 
@ mysqli_free_result($excs);
@ mysqli_free_result($excss);
unset($excs, $excss, $sql, $i, $rows, $rowss, $ids_v, $ids_s);
}
?>
<!-- InstanceEndEditable -->
<form name="chang_elemt" id="chang_elemt" target="contenido" method="post">
<!-- InstanceBeginEditable name="campos" -->

<!-- InstanceEndEditable -->
</form>
</div></body>

<script language="javascript">
verif();
</script>

<!-- InstanceEnd --></html>