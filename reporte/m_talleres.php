<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/m_reporte.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php
include("../script/scripts/session.php");
//include("../script/breadcrumbs.php");
?>
<link href="../css/css.css" rel="stylesheet" type="text/css" />
<script src="../script/calendario.js"></script>
<script src="../script/horario_2.js"></script>
<script type="text/javascript">
function observ_mensual(w) {
var zx = window.open(w, 'Observación mensual', 'resizable=yes,menubar=no,scrollbars=yes,width=660,height=400');
zx.focus();
}
function sel_resp(w) {
for(i = 1; i < document.getElementById("funcionario").length - 1; i++)
{
	if(document.getElementById("funcionario")[i].value.toUpperCase() == w.toUpperCase())
		document.getElementById("funcionario").selectedIndex = i;
}
}
function sel_franjas(w) {
w = w.split(",");
for(i = 0; i < w.length - 1; i++)
{
	document.getElementById("franja_pob")[w[i]].selected = true;
}
}
function sel_tipo(w) {
	document.getElementById("t_resp")[w].selected = true;
}
function serv_mensual(w) {
var zz = window.open(w, 'Servicio mensual', 'resizable=yes,menubar=no,scrollbars=yes,width=660,height=600');
zz.focus();
}
function verif() {
document.getElementById('datos').style.display='none';
}
function foco_in(z) {
document.getElementById(z).focus();
}
function busqueda() {
var val, op;
err = "";
document.getElementById("f_pob").value = "";
for(i = 0; i < 4; i++)
{
	if(document.getElementById("franja_pob")[i].selected == true)
       	document.getElementById("f_pob").value += document.getElementById("franja_pob")[i].value+",";
}
val = document.getElementById("fecha").value;
if(val == null || val.length < 5 || /^\s+$/.test(val))
	err += "Se requiere la fecha de la actividad. \n";
val = document.getElementById("hora").value;
if(val == null || val.length < 3 || /^\s+$/.test(val))
	err += "Se requiere el tiempo empleado en la actividad. \n";
val = document.getElementById("participantes").value;
if(isNaN(val) || val == 0 || /^\s+$/.test(val))
	err += "Se requiere el número de participantes. \n";
val = document.getElementById("funcionario")[document.getElementById("funcionario").selectedIndex].value;
if(val == null || val.length < 5 || /^\s+$/.test(val) || val == "Adicionar funcionario")
	err += "Se requiere el nombre del responsable de la actividad. \n";
val = document.getElementById("f_pob").value;
if(val == null || val.length < 2 || /^\s+$/.test(val))
	err += "Se requiere la franja poblacional. \n";
val = document.getElementById("descrp").value;
if(val == null || val.length < 20 || /^\s+$/.test(val))
	err += "Se requiere la descripción de la actividad. \n";
op = document.getElementById("tip_r").selectedIndex;
val = document.getElementById("tip_r")[op].value;
if(val == null || val.length == 0 || /^\s+$/.test(val))
	err += "Se requiere el tipo de reporte. \n";
op = document.getElementById("t_resp").selectedIndex;
val = document.getElementById("t_resp")[op].value;
if(val == null || val.length == 0 || /^\s+$/.test(val))
	err += "Se requiere el tipo de responsable de realizar la actividad. \n";
if(err.length > 0)
{
	alert("Verifique los siguientes errores: \n\n"+err+"\n");
	return false;
}
else
	return true;
}
</script>
<title>.: Sistema general de estad&iacute;stica :.</title>
<link href="../menu/css/css.css" rel="stylesheet" type="text/css" />
<link rel="shortcut icon" href="../img/favicon.ico" />
<!-- InstanceBeginEditable name="doctitle" -->

<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" --><!-- InstanceEndEditable -->
</head>
<body>
<noscript>
<h3 align="center" class="contend">Estimado usuario, esta p&aacute;gina requiere para su funcionamiento el uso de JavaScript. Si lo ha deshabilitado intencionadamente, por favor vuelva a activarlo durante el proceso de registro de informaci&oacute;n o solicite la ayuda a CST.</h3>
</noscript>
<div id="datos" align="center">
 <h4>Consolidando datos...<br />
 <img src="../img/loader.gif" width="16" height="16" /></h4>
 </div>
<div align="left"><?php //echo breadcrumbs(); ?></div>
<div align="center" class="popup">
<!-- InstanceBeginEditable name="contenidos" -->
<!-- InstanceEndEditable -->
<?php
include("../Connections/conect.inc.php");
if(isset($_POST["fecha"], $_POST["hora"], $_POST["participantes"], $_POST["id_rep"], $_POST["bib"]))
{
$sql = "update estadistica set Fecha = '".$_POST["fecha"]."', Hora = '".$_POST["hora"]."', Participantes = '".$_POST["participantes"]."', Responsable = '".strtoupper($_POST["funcionario"])."', Franja = '".$_POST["f_pob"]."', Actividad = '".$_POST["tip_r"]."', Alcance = '".$_POST["t_resp"]."', Descripcion = '".addslashes($_POST["descrp"])."', Fecha_R = '".date("Y-n-j")."' where Id = ".$_POST["id_rep"];
//echo $sql;
$exc = mysqli_query($conect, $sql);
if($exc)
{
    echo "<h3 align='center'>Registro actualizado</h3>";
    ?><script language="javascript">
    alert("Registro actualizado");
	opener.location.reload();
    window.close(); 
    </script><?php
}
else
{
    echo "<h3 align='center'>Error al actualizar el registro</h3>";
    exit;
}
}
$sql = "select Fecha, Hora, Participantes, Responsable, Franja, Tipo, Alcance, Descripcion from estadistica where Id = ".$_GET["id_report"];
$excs = mysqli_query($conect, $sql);
$rows = mysqli_fetch_array($excs);
?>
<form name ="formulario" method ="POST" action ="m_talleres.php" onSubmit="return busqueda()">
<input name="id_rep" id="id_rep" type="hidden" value="<?php echo $_GET["id_report"]; ?>" />
<input name="bib" id="bib" type="hidden" value="<?php echo $_GET["bib"]; ?>" />
<input name="nombre" id="nombre" type="hidden" value="<?php echo $_GET["nom"]; ?>" />
<input name="f_pob" id="f_pob" type="hidden" value="" />
<table width="99%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="10%"><h3><a href="javascript:void(0);" onclick="javascript:window.history.back(1);" title="Regresar"> << </a></h3></td>
    <td width="80%"><div align="center"><h3>Reporte estad&iacute;stico para <?php echo $_GET["report"]; ?></h3>
      (<strong>&Aacute;lias:&nbsp;<?php echo $_GET["alias"] ?></strong>)<br />
      <strong>Biblioteca: <?php echo $_GET["nom"] ?></strong></div>
      </td>
    <td width="10%">&nbsp;</td>
  </tr>
  <tr>
    <td rowspan="10">&nbsp;</td>
    <td><div align="center" style="background-color:#<?php if($rows["Tipo"] == 0) echo "408080"; else echo "FF8040"; ?>">
    <?php 
    if($rows["Tipo"] == 0) echo "Actividad programada"; else echo "Actividad adicional";
    ?>
    </div></td>
    <td rowspan="10">&nbsp;</td>
  </tr>
  <tr>
    <td>
    <div style="float:left; width:49%"><div align="left"><strong>Fecha de la actividad:</strong></div>
      <div align="left" style="width:99%;"><span class="cssToolTip"><input name="fecha" id="fecha" type="text" size="50" style="width:85%;" onFocus="doShow('date_p','formulario','fecha');" readonly="readonly" value="<?php echo $rows["Fecha"]; ?>" placeholder="aaaa-m-d" />
        <img src="../icon/calendar_.gif" alt="seleccione" width="24" height="12" onClick="doShow('date_p','formulario','fecha')" /><span>Haga clic para abrir el calendario y seleccionar la fecha</span></span><br />
        <div class="date_p" id="date_p" align="left" style="display:none;">&nbsp;</div>
      </div></div>
      <div style="float:right; width:49%">
      <div align="left"><strong> &nbsp; &nbsp;Tipo de actividad:</strong></div>
      <div align="left" style="padding-left:10px;">
      <?php include("../script/tipo_report.php"); ?>
      </div></div>
    </td>
    </tr>
  <tr>
    <td><div style="float:left; width:49%"><div align="left"><strong>Tiempo utilizado en la actividad:</strong></div>
    <div align="left" style="width:99%;"><span class="cssToolTip"><input name="hora" id="hora" type="text" size="50" style="width:85%;" onFocus="doHour('asin_hor','formulario','hora');" readonly="readonly" value="<?php echo $rows["Hora"]; ?>" />
    <img src="../icon/wait.png" alt="seleccione" width="12" height="16" onClick="doHour('asin_hor','formulario','hora');" /><span>Haga click para abrir el horario</span></span><br />
    <div class="date_p" id="asin_hor" align="left" style="display:none;">&nbsp;</div>
    </div></div>
    <div style="float:right; width:49%">
    <div align="left" style="padding-left:10px;">
    <?php include("../script/tipo_respon.php"); ?>
    <script language="javascript">sel_tipo('<?php echo $rows["Alcance"]; ?>');</script>
    </div></div>
    </td>
  </tr>
  <tr>
    <td><div align="left"><strong><?php
    if(strlen($_GET["campo"]) > 0)
    	echo $_GET["campo"]; 
    else
    	echo "N&uacute;mero de usuarios o cantidad a reportar";
    ?>:</strong></div>
    <div align="left" style="width:50%;"><input name="participantes" id="participantes" type="text" size="50" value="<?php echo $rows["Participantes"]; ?>" /></div></td>
  </tr>
  <tr>
    <td><div align="left"><strong>Responsable de reportar de la actividad:</strong></div>
    <div align="left"><?php include("../script/funcionarios.php"); ?>
    <script language="javascript">sel_resp('<?php echo $rows["Responsable"]; ?>');</script>
    </div></td>
  </tr>
  <tr>
    <td><div align="left"><strong>Seleccione la franja poblacional:</strong></div>
    <div align="left"><?php include("../script/franja_p.php"); ?>
    <script language="javascript">sel_franjas('<?php echo $rows["Franja"]; ?>');</script>
    </div>
    </td>
  </tr>
  <tr>
    <td><div align="left"><strong>Descripci&oacute;n de la actividad realizada:</strong></div>
    <div align="left"><textarea name="descrp" id="descrp" rows="3"><?php echo $rows["Descripcion"]; ?></textarea>
    <div class="infos">M&aacute;ximo 500 caracteres. Describa la información que está reportando, si durante el mes se presentaron inconvenientes o acciones que afecten el dato reportado.</div></div>
    </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
   </tr>
   <tr>
    <td><div align="center" id="activ_norm"></div></td>
  </tr>
  <tr>
    <td>
    <?php include("../script/observacion.php"); ?>
    </td>
  </tr>
</table>
</form>
<?php unset($sql,$excs,$rows); ?>
<form name="chang_elemt" id="chang_elemt" target="contenido" method="post"></form>
</div>
</body>
<script language="javascript">
verif();
if(document.getElementById("activ_norm"))
{
	document.getElementById("activ_norm").innerHTML = "<input name='guardar' id='guardar' type='submit' value='Actualizar' />";
}
</script>
<!-- InstanceEnd --></html>