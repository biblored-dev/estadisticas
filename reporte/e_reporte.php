<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/contenedor.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php
include("../script/scripts/session.php");
//include("../script/breadcrumbs.php");
?>
<link href="../css/css.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../script/scripts/jquery.min.js"></script>
<script type="text/javascript" src="../script/menu_ocultar.js"></script>
<script type="text/javascript">
function verif() {
document.getElementById('datos').style.display='none';
}
function foco_in(z) {
document.getElementById(z).focus();
}
</script>
<title>.: Sistema general de estad&iacute;stica :.</title>
<link rel="shortcut icon" href="../img/favicon.ico" />
<!-- InstanceBeginEditable name="doctitle" -->
<script language="javascript">
function marcar() {
if(document.getElementById("princ_1").checked == true)
{
	document.getElementById("rep_elim").value = "";
	var val = document.getElementById("id_rep").value.split(",");
	for(var i=1; i<(val.length - 1); i++)
	{
		document.getElementById("elim_"+val[i]).checked = true;
	}
}
}
function busqueda() {
var err = "";
var con = 0;
document.getElementById("rep_elim").value = "";
var val = document.getElementById("id_rep").value.split(",");
if(document.getElementById("princ_1").checked == true)
{
	err = "";
}
else
{
	for(var i=1; i<(val.length - 1); i++)
	{
		if(document.getElementById("elim_"+val[i]).checked == true)
		{
			con ++;
			document.getElementById("rep_elim").value += val[i]+",";
		}
	}
	alert("Reportes: "+(val.length - 1)+", Seleccionados: "+con);
	if(con == 0)
	{
		err += "No hay reportes seleccionados. \n";
	}
	else
	{
		if((val.length - 1) - con == 1 || (val.length - 1) - con == 3)
		{
			return true;
		}
		else
		{
			err += "Deben haber mínimo dos reportes relacionados. \n";
		}
	}
}
if(err.length > 0)
{
	alert("Verifique los siguientes errores: \n\n"+err+"\n");
	return false;
}
else
{
	if(confirm("Se dispone a eliminar "+(val.length - 1)+" registros.\n¿Está seguro?"))
	{
		return true;
	}
	else
	{
		return false;
	}
}
}
</script>
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" --><!-- InstanceEndEditable -->
</head>
<body>
<div id="datos" align="center">
 <h4>Consolidando datos...<br />
 <img src="../img/loader.gif" width="16" height="16" /></h4>
 </div>
<div align="left"><?php //echo breadcrumbs(); ?></div>
<div align="center" class="popup">
<!-- InstanceBeginEditable name="contenidos" -->
<?php
include("../Connections/conect.inc.php");
if(isset($_POST["id_rep"], $_POST["button"]))
{
	if(strlen($_POST["rep_elim"]) > 0)
	{
		$id = explode(",", $_POST["rep_elim"]);
		$id = array_filter($id);
		$ids = implode(",",$id);
	}
	else
	{
		$id = explode(",", $_POST["id_rep"]);
		$id = array_filter($id);
		$ids = implode(",",$id);
	}
	$sql = "delete from programacion where Id in (".$ids.")";
	$exc = mysqli_query($conect, $sql);
	$sql = "select sum(Sesiones) as Sesiones from programacion where Id = ".$_POST["id_rep_prin"];
	$exc = mysqli_query($conect, $sql);
	$row = mysqli_fetch_array($exc);
	if($row["Sesiones"] == 0)
	{
		$sql = "update programacion set Sub = 0, Sesiones = 0 where Id = ".$_POST["id_rep_prin"];
	}
	else
	{
		$sql = "update programacion set Sub = 0, Sesiones = ".$row["Sesiones"]." where Id = ".$_POST["id_rep_prin"];
	}
	$exc = mysqli_query($conect, $sql);
	if($exc)
	{
		echo "<h3 align='center'>Registro actualizado</h3>";
		unset($ids, $id, $i, $sql, $exc);
		mysqli_close($conect);
		?><script language="javascript">
		alert("Registro eliminado");
		opener.location.reload();
		window.close();
		</script><?php
	}
	else
	{
		echo "<h3 align='center'>Error al actualizar el registro</h3>";
		exit;
	}
}
else
{
$sql = "select programacion.Sesiones, programacion.Sub from programacion where programacion.Id = ".$_GET["id_rep"];
$sum = 0;
$id = "";
$exc = mysqli_query($conect, $sql);
$row = mysqli_fetch_array($exc);
if($row["Sub"] != 0)
{
	$_GET["id_rep"] = $row["Sub"];
	$sql = "select programacion.Sesiones, programacion.Sub from programacion where programacion.Id = ".$_GET["id_rep"];
	$exc = mysqli_query($conect, $sql);
	$row = mysqli_fetch_array($exc);
}
//echo $sql;
$sql = "select case when prog_alias.Alias then prog_alias.Alias else 'Sin alias creado' end as Alias from prog_alias where prog_alias.Reporte = ".$_GET["id_rep"];
$excs = mysqli_query($conect, $sql);
$rows = mysqli_fetch_array($excs);
$id .= $_GET["id_rep"].",";
?>
<h4 align="left" style="padding:5px;">Reporte principal:</h4>
<form name ="formiden" id ="formiden" method ="POST" action ="e_reporte.php" onsubmit ="return busqueda();">
<div style="padding-top:6px;" class="list_legen" align="left">Reporte estad&iacute;stico: <?php echo $_GET["Reporte"]; ?></div>
<div style="padding-top:6px;" class="list_legen" align="left">Alias: <?php echo $rows["Alias"]; ?></div>
<div style="padding-top:6px;" class="list_legen" align="left">Sesiones programadas: <span id="Sesiones"><?php echo $row["Sesiones"]; ?></span></div>
<div style="padding-top:6px; width:48%; margin:auto 0px;" class="list_legen" align="left">
<input name="id_rep_prin" id="id_rep_prin" type="hidden" value="<?php echo $_GET["id_rep"]; ?>" />
<div class="list_cln"> <input name="princ_1" id="princ_1" type="checkbox" value="<?php echo $_GET["id_rep"]; ?>" onclick="marcar();" /> Eliminar registro principal y adicionados</div>
</div>
<h4 align="left" style="padding:5px; clear:both;">Reportes adicionados:</h4>
<div style="padding-top:6px; overflow:hidden;">
<?php
$sql = "select programacion.Id, programacion.Sesiones, programacion.Publico, prog_alias.Alias from programacion, prog_alias where programacion.Sub = ".$_GET["id_rep"]." and prog_alias.Reporte = programacion.Id";
$exc = mysqli_query($conect, $sql);
for($i=1; $i<=mysqli_num_rows($exc); $i++)
{  
	$row = mysqli_fetch_array($exc);
?>    
	 <div class="list_vis">
        <div><strong><?php echo $i; ?>.</strong></div>
        <div class="list_princ" align="left">
            <div>Sesiones: <?php echo $row["Sesiones"]; ?></div>
        </div>
        <div class="list_princ" align="left">
            <div>Alias: <?php echo $row["Alias"]; ?></div>
        </div>
        <div class="list_princ" align="left">
            <div><?php echo $_GET["Dat"]; ?>: <?php echo $row["Publico"]; ?></div>
        </div>
        <div class="list_princ" style="overflow:hidden;" align="left">
            <div class="list_cln"> <input name="elim_<?php echo $row["Id"]; ?>" id="elim_<?php echo $row["Id"]; ?>" type="checkbox" value="<?php echo $row["Id"]; ?>" /> Eliminar registro</div>
        </div>
    </div>
<?php
$sum += $row["Sesiones"];
$id .= $row["Id"].",";
}
if(mysqli_num_rows($exc) > 1)
{
?>
<script language="javascript">
document.getElementById("Sesiones").innerHTML = <?php echo $sum; ?>;
</script>
<?php
}
if($_SESSION['MM_Usr_Pri'] <= 4)
{ ?>
<input name="id_rep" id="id_rep" type="hidden" value="<?php echo $id; ?>" />
<input name="rep_elim" id="rep_elim" type="hidden" value="" />
<div align="center" style="padding-top:10px; clear:both;"><input type="submit" name="button" id="button" value="Eliminar registro" /></div>
<?php
}
else
{ ?>
<div style="overflow:hidden; padding:3px; clear:both;" class="inform">No posee privilegios para eliminar</div>
<?php } ?>
</div>
</form>
<?php } ?>
<div align="justify" id="db_guardar">&nbsp; <!--Insertar mensaje de ayuda para la página -->
<div class="div_menu" id="aa_1"><a href="javascript:void(0);" onclick="menu('a_b_','1'); mostrar('bb','1');" title="Ayuda">? +</a></div>
<div class="div_menu" style="display:none;" id="bb_1"><a href="javascript:void(0);" onclick="menu('a_b_','2'); mostrar('aa','1');" title="Ayuda">? -</a></div>
<div class="div_ayuda" id="a_b_" style="display:none;">Puede eliminar todos los reportes incluido el principal, caso contrario, seleccione los que desea eliminar teniendo en cuenta que puede eliminar todos los reportes adicionados o debe dejar por lo menos dos (2).</div></div>
<!-- InstanceEndEditable -->
<form name="chang_elemt" id="chang_elemt" target="contenido" method="post">
<!-- InstanceBeginEditable name="campos" -->

<!-- InstanceEndEditable -->
</form>
</div></body>

<script language="javascript">
verif();
</script>

<!-- InstanceEnd --></html>
<?php
mysqli_free_result($exc);
unset($sum, $id, $i, $sql, $exc, $row, $excs, $rows);
mysqli_close($conect);
?>