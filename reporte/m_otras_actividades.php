<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/contenedor.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php
include("../script/scripts/session.php");
//include("../script/breadcrumbs.php");
?>
<link href="../css/css.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../script/scripts/jquery.min.js"></script>
<script type="text/javascript" src="../script/menu_ocultar.js"></script>
<script type="text/javascript">
function verif() {
document.getElementById('datos').style.display='none';
}
function foco_in(z) {
document.getElementById(z).focus();
}
</script>
<title>.: Sistema general de estad&iacute;stica :.</title>
<link rel="shortcut icon" href="../img/favicon.ico" />
<!-- InstanceBeginEditable name="doctitle" -->
<script language="javascript" src="../script/datosxml.js"></script>
<script src="../script/calendario.js"></script>
<script src="../script/horario_2.js"></script>
<script type="text/javascript">
function serv_mensual(w) {
var zz = window.open(w, 'Servicio mensual', 'resizable=yes,menubar=no,scrollbars=yes,width=660,height=600');
zz.focus();
}
function verif() {
document.getElementById('datos').style.display='none';
}
function foco_in(z) {
document.getElementById(z).focus();
}
function busqueda() {
var val, op;
err = "";
document.getElementById("f_pob").value = "";
for(i = 0; i < 4; i++)
{
	if(document.getElementById("franja_pob")[i].selected == true)
       	document.getElementById("f_pob").value += document.getElementById("franja_pob")[i].value+",";
}
val = document.getElementById("fecha").value;
if(val == null || val.length < 5 || /^\s+$/.test(val))
	err += "Se requiere la fecha de la actividad. \n";
val = document.getElementById("hora").value;
if(val == null || val.length < 3 || /^\s+$/.test(val))
	err += "Se requiere el tiempo empleado en la actividad. \n";
val = document.getElementById("participantes").value;
if(isNaN(val) || val == 0 || /^\s+$/.test(val))
	err += "Se requiere el número de participantes. \n";
op = document.getElementById("categori").selectedIndex;
val = document.getElementById("categori")[op].value;
if(val == null || val.length == 0 || /^\s+$/.test(val))
	err += "Se requiere la categoría de la actividad. \n";
val = document.getElementById("funcionario")[document.getElementById("funcionario").selectedIndex].value;
if(val == null || val.length < 5 || /^\s+$/.test(val) || val == "Adicionar funcionario")
	err += "Se requiere el nombre del responsable. \n";
val = document.getElementById("f_pob").value;
if(val == null || val.length < 2 || /^\s+$/.test(val))
	err += "Se requiere la franja poblacional. \n";
val = document.getElementById("descrp").value;
if(val == null || val.length < 20 || /^\s+$/.test(val))
	err += "Se requiere la descripción de la actividad. \n";
op = document.getElementById("tip_r").selectedIndex;
val = document.getElementById("tip_r")[op].value;
if(val == null || val.length == 0 || /^\s+$/.test(val))
	err += "Se requiere el tipo de actividad. \n";
op = document.getElementById("pag_r").selectedIndex;
val = document.getElementById("pag_r")[op].value;
if(val == null || val.length == 0 || /^\s+$/.test(val))
	err += "Se requiere el enfoque del reporte. \n";
if(err.length > 0)
{
	alert("Verifique los siguientes errores: \n\n"+err+"\n");
	return false;
}
else
	return true;
}
</script>
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" --><!-- InstanceEndEditable -->
</head>
<body>
<div id="datos" align="center">
 <h4>Consolidando datos...<br />
 <img src="../img/loader.gif" width="16" height="16" /></h4>
 </div>
<div align="left"><?php //echo breadcrumbs(); ?></div>
<div align="center" class="popup">
<!-- InstanceBeginEditable name="contenidos" -->
<?php
include("../Connections/conect.inc.php");

if(isset($_POST["fecha"], $_POST["hora"], $_POST["participantes"], $_POST["id_report"], $_POST["bib"]))
{
$sql = "update otros set Fecha = '".$_POST["fecha"]."', Hora = '".$_POST["hora"]."', Participantes = '".$_POST["participantes"]."', Categoria = '".$_POST["categori"]."', Responsable = '".strtoupper($_POST["funcionario"])."', Franja = '".$_POST["f_pob"]."', Actividad = '".$_POST["tip_r"]."', Tipo = '".$_POST["pag_r"]."', Descripcion = '".addslashes($_POST["descrp"])."' where Id = ".$_POST["id_report"];
//echo $sql;
$exc = mysqli_query($conect, $sql);
if($exc)
{
    echo "<h3 align='center'>Registro actualizado</h3>";
    ?><script language="javascript">
    alert("Registro actualizado");
	opener.location.reload();
    window.close(); 
    </script><?php
}
else
{
    echo "<h3 align='center'>Error al actualizar el registro</h3>";
    exit;
}
}
$sql = "select Fecha, Area, Hora, Participantes, Responsable, Categoria, Franja, Tipo, Actividad, Descripcion from otros where Id = ".$_GET["id_report"];
$excs = mysqli_query($conect, $sql);
$rows = mysqli_fetch_array($excs);
$_GET["tip_repor"] = $rows["Actividad"];
$_GET["franja_pob"] = $rows["Franja"];
if(!isset($_GET["area"]))
	$_GET["area"] = $rows["Area"];
?>
<div align="center"><h3>Reporte estad&iacute;stico de otras actividades</h3>
<strong>Biblioteca: <?php echo $_GET["nom"] ?></strong></div>
<form name ="formulario" method ="POST" action ="m_otras_actividades.php" onsubmit="return busqueda();">
<input name="id_report" id="id_report" type="hidden" value="<?php echo $_GET["id_report"]; ?>" />
<input name="fech_rep" id="fech_rep" type="hidden" value="<?php echo $_GET["fech_rep"]; ?>" />
<input name="bib" id="bib" type="hidden" value="<?php echo $_GET["bib"]; ?>" />
<input name="area_r" id="area_r" type="hidden" value="<?php echo $_GET["area"]; ?>" />
<input name="nombre" id="nombre" type="hidden" value="<?php echo $_GET["nom"]; ?>" />
<input name="f_pob" id="f_pob" type="hidden" value="" />
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="8%">&nbsp;</td>
    <td width="84%" align="center">&nbsp;</td>
    <td width="8%">&nbsp;</td>
  </tr>
  <tr>
    <td rowspan="9">&nbsp;</td>
    <td>
    <div style="float:left; width:49%"><div align="left"><strong>Fecha de la actividad:</strong></div>
      <div align="left" style="width:99%;"><span class="cssToolTip"><input name="fecha" id="fecha" type="text" size="50" style="width:85%;" onFocus="doShow('date_p','formulario','fecha');" readonly="readonly" value="<?php echo $rows["Fecha"]; ?>" placeholder="aaaa-m-d" />
        <img src="../icon/calendar_.gif" alt="seleccione" width="24" height="12" onClick="doShow('date_p','formulario','fecha')" /><span>Haga clic para abrir el calendario y seleccionar la fecha</span></span><br />
        <div class="date_p" id="date_p" align="left" style="display:none;">&nbsp;</div>
      </div></div>
      <div style="float:right; width:49%">
      <div align="left"><strong> &nbsp; &nbsp;Tipo de actividad:</strong></div>
      <div align="left" style="padding-left:10px;">
      <?php include("../script/tipo_report.php"); ?>
      </div>
      </div>
    </td>
    <td rowspan="9">&nbsp;</td>
  </tr>
  <tr>
    <td>
      <div style="float:left; width:49%"><div align="left"><strong>Tiempo utilizado en la actividad:</strong></div>
        <div align="left" style="width:99%;"><span class="cssToolTip"><input name="hora" id="hora" type="text" size="50" style="width:85%;" onFocus="doHour('asin_hor','formulario','hora');"  value="<?php echo $rows["Hora"]; ?>" placeholder="1:00" />
          <img src="../icon/wait.png" alt="seleccione" width="12" height="16" onClick="doHour('asin_hor','formulario','hora');" /><span>Haga clic para abrir el horario. Ajuste el tiempo requerido y luego haga clic en ‘Seleccionar’</span></span><br />
          <div class="date_p" id="asin_hor" align="left" style="display:none;">&nbsp;</div>
          </div></div>
      <div style="float:right; width:49%">
        <div align="left"><strong> &nbsp; &nbsp;Enfoque de la actividad:</strong></div>
        <div align="left" style="padding-left:10px;">
          <select name ="pag_r" id="pag_r">
            <option value="">Seleccione el alcance del reporte</option>
            <option value="0" <?php if(isset($rows["Tipo"]) && $rows["Tipo"] == 0) echo "selected='selected'"; ?>>Gratuito</option>
            <option value="1" <?php if(isset($rows["Tipo"]) && $rows["Tipo"] == 1) echo "selected='selected'"; ?>>Pago a tallerista</option>
          </select>
          </div>
        </div>
    </td>
    </tr>
  <tr>
    <td>
      <div align="left"><strong>N&uacute;mero de usuarios o cantidad a reportar:</strong></div>
      <div align="left" style="width:50%;"><input name="participantes" id="participantes" type="text" size="50" value="<?php echo $rows["Participantes"]; ?>" placeholder="000" /></div>
    </td>
  </tr>    
  <tr>
    <td>
    <div align="left" style="width:50%;">
    <?php include("../script/categorias.php"); ?>
    </div>
    <script language="javascript">
	categorias_ar('<?php echo $_GET["area"]; ?>');
	sel_categoria('<?php echo $rows["Categoria"]; ?>');
	</script>
    </td>
  </tr>
  <tr>
    <td><div align="left"><strong>Seleccione la franja poblacional:</strong></div>
        <div align="left"><?php include("../script/franja_p.php"); ?>
          <div class="infos">Para seleccionar más de una opción mantenga la tecla "Ctrl" oprimida.</div
    ></div>
    </td>
  </tr>
  <tr>
    <td>
      <div align="left"><strong>Responsable de la actividad:</strong></div>
      <div align="left"><?php include("../script/funcionarios.php"); ?></div>
    </td>
    </tr>
  <tr>
    <td><div align="left"><strong>Descripci&oacute;n de la actividad realizada:</strong></div>
      <div align="left"><textarea name="descrp" id="descrp" rows="5" placeholder="Descripción de la actividad realizada"><?php echo $rows["Descripcion"]; ?></textarea>
        <div class="infos">M&aacute;ximo 1000 caracteres. Describa la información que está reportando, si durante el mes se presentaron inconvenientes o acciones que afecten el dato reportado<strong> o desea resaltar los contenidos desarrollados en la actividad</strong>.</div></div>
      <script language="javascript">
    document.getElementById("descrp").select();
    </script>
    </td>
    </tr>
  <tr>
    <td>
      <div align="center" id="activ_add">
        <input name="guardar" id="guardar" type="submit" value="Guardar actividad" />
        <input name="tipo_act" id="tipo_act" type="hidden" value="0" />
        <?php
    unset($sql,$excs,$rows,$i);
    ?>
        </div>
    </td>
    </tr>
    <tr>
    <td>&nbsp;</td>
  </tr>
</table>
</form>
<div align="justify" id="db_guardar">&nbsp; <!--Insertar mensaje de ayuda para la página -->
<div class="div_menu" id="aa_1"><a href="javascript:void(0);" onclick="menu('a_b_','1'); mostrar('bb','1');" title="Ayuda">? +</a></div>
<div class="div_menu" style="display:none;" id="bb_1"><a href="javascript:void(0);" onclick="menu('a_b_','2'); mostrar('aa','1');" title="Ayuda">? -</a></div>
<div class="div_ayuda" id="a_b_" style="display:none;">Otras actividades desarrolladas durante el mes y que no hacen parte del plan de acci&oacute;n o de la programaci&oacute;n establecida por la biblioteca. Estas pueden ser resultado de actividades propuestas por personas o entidades externas y que har&aacute;n uso de los espacios o recursos de la biblioteca y cuentan con el acomodamiento de los funcionarios.</div></div>
<!-- InstanceEndEditable -->
<form name="chang_elemt" id="chang_elemt" target="contenido" method="post">
<!-- InstanceBeginEditable name="campos" -->

<!-- InstanceEndEditable -->
</form>
</div></body>

<script language="javascript">
verif();
</script>

<!-- InstanceEnd --></html>