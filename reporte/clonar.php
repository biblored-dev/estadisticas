<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/contenedor.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php
include("../script/scripts/session.php");
//include("../script/breadcrumbs.php");
?>
<link href="../css/css.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../script/scripts/jquery.min.js"></script>
<script type="text/javascript" src="../script/menu_ocultar.js"></script>
<script type="text/javascript">
function verif() {
document.getElementById('datos').style.display='none';
}
function foco_in(z) {
document.getElementById(z).focus();
}
</script>
<title>.: Sistema general de estad&iacute;stica :.</title>
<link rel="shortcut icon" href="../img/favicon.ico" />
<!-- InstanceBeginEditable name="doctitle" -->
<script language="javascript">
function validate() {
var err, op;
var al1 = al2 = false;
err = "";
for(i=1; i<3; i++)
{
	op = document.getElementById("s_"+i).value;
	if(isNaN(op) || op == 0 || /^\s+$/.test(op))
		err += "Se requiere el número de sesiones "+i+". \n";
	op = document.getElementById("a_"+i).value;
	if(op.length <= 3 || /^\s+$/.test(op))
		err += "Se requiere el álias del registro "+i+". \n";
	else if(op.length > 60)
		err += "El álias del registro "+i+" no debe superar 60 caracteres. \n";
	op = document.getElementById("p_"+i).value;
	if(op.length > 0 && op > 0 && (isNaN(op) || /^\s+$/.test(op)))
		err += document.getElementById("dat").value + " "+i+" no es válido. \n";	
}
if(document.getElementById("a_1").value.length > 0 && document.getElementById("a_1").value == document.getElementById("a_2").value)
	err += "Los álias no pueden ser iguales. \n";
if(document.getElementById("s_3").value.length > 0 || document.getElementById("a_3").value.length > 0 || document.getElementById("p_3").value.length > 0)
{
	al1 = true;
	op = document.getElementById("s_3").value;
	if(isNaN(op) || op == 0 || /^\s+$/.test(op))
	{
		err += "Se requiere el número de sesiones 3. \n";
		al1 = false;
	}
	op = document.getElementById("a_3").value;
	if(op.length <= 3 || /^\s+$/.test(op))
	{
		err += "Se requiere el álias del registro 3. \n";
		al1 = false;
	}
	else if(op.length > 60)
	{
		err += "El álias del registro 3 no debe superar 60 caracteres. \n";
		al1 = false;
	}
	op = document.getElementById("p_3").value;
	if(op.length > 0 && op > 0 && (isNaN(op) || /^\s+$/.test(op)))
	{
		err += document.getElementById("dat").value + " "+3+" no es válido. \n";
		al1 = false;
	}
	if(document.getElementById("a_3").value == document.getElementById("a_1").value || document.getElementById("a_3").value == document.getElementById("a_2").value)
	{
		err += "Los álias no pueden ser iguales. \n";
		al1 = false;
	}
}
if(document.getElementById("s_4").value.length > 0 || document.getElementById("a_4").value.length > 0 || document.getElementById("p_4").value.length > 0)
{
	al2 = true;
	op = document.getElementById("s_4").value;
	if(isNaN(op) || op == 0 || /^\s+$/.test(op))
	{
		err += "Se requiere el número de sesiones 4. \n";
		al2 = false;
	}
	op = document.getElementById("a_4").value;
	if(op.length <= 3 || /^\s+$/.test(op))
	{
		err += "Se requiere el álias del registro 4. \n";
		al2 = false;
	}
	else if(op.length > 60)
	{
		err += "El álias del registro 4 no debe superar 60 caracteres. \n";
		al1 = false;
	}
	op = document.getElementById("p_4").value;
	if(op.length > 0 && op > 0 && (isNaN(op) || /^\s+$/.test(op)))
	{
		err += document.getElementById("dat").value + " "+4+" no es válido. \n";
		al2 = false;
	}
	if(document.getElementById("a_4").value == document.getElementById("a_1").value || document.getElementById("a_4").value == document.getElementById("a_2").value || document.getElementById("a_4").value == document.getElementById("a_3").value)
	{
		err += "Los álias no pueden ser iguales. \n";
		al2 = false;
	}
}
if(al1 == false && al2 == true)
{
	err += "Se debe completar cada adición en forma secuencial. \n";
}
if(err.length > 0)
{
	alert("Verifique los siguientes errores: \n\n"+err+"\n");
	return false;
}
else
	return true;	
}
</script>
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" --><!-- InstanceEndEditable -->
</head>
<body>
<div id="datos" align="center">
 <h4>Consolidando datos...<br />
 <img src="../img/loader.gif" width="16" height="16" /></h4>
 </div>
<div align="left"><?php //echo breadcrumbs(); ?></div>
<div align="center" class="popup">
<!-- InstanceBeginEditable name="contenidos" -->
<?php
if(isset($_POST["guardar"], $_POST["s_1"], $_POST["a_1"]))
{
	require_once("../Connections/conect.inc.php");
	$sesiones = $_POST["s_1"] + $_POST["s_2"];
	$publico = $_POST["p_1"] + $_POST["p_2"];
	if(isset($_POST["s_3"], $_POST["a_3"]))
	{
		$sesiones += $_POST["s_3"];
		$publico += $_POST["p_3"];
	}
	if(isset($_POST["s_4"], $_POST["a_4"]))
	{
		$sesiones += $_POST["s_4"];
		$publico += $_POST["p_4"];
	}
	$sql = "select Id from programacion where Reporte = ".$_POST["rep"]." and Biblioteca = ".$_POST["bib"]." and Fecha = '".$_POST["fec"]."'";
	$exc = mysqli_query($conect, $sql);
	if(mysqli_num_rows($exc) == 0)
	{
		$sql = "insert into programacion (Id, Biblioteca, Reporte, Sub, Sesiones, Fecha, Publico) values ('', ".$_POST["bib"].", ".$_POST["rep"].", '1', ".$sesiones.", '".$_POST["fec"]."', ".$publico.")";
		$excs = mysqli_query($conect, $sql);
		$id = mysqli_insert_id($conect);
	}
	else
	{
		$sql = "update programacion set Sesiones = ".$sesiones.", Publico = ".$publico.", Sub = 1 where Reporte = ".$_POST["rep"]." and Biblioteca = ".$_POST["bib"]." and Fecha = '".$_POST["fec"]."'";
		$excs = mysqli_query($conect, $sql);
		$row = mysqli_fetch_array($exc);
		$id = $row["Id"];
	}
	
	//clon 1
	$sql = "insert into programacion (Id, Biblioteca, Reporte, Sub, Sesiones, Fecha, Publico) values ('', ".$_POST["bib"].", ".$_POST["rep"].", '".$id."', ".$_POST["s_1"].", '".$_POST["fec"]."', '".$_POST["p_1"]."')";
	//echo $sql;
	$excs = mysqli_query($conect, $sql);
	$sql = "insert into prog_alias (Id, Reporte, Biblioteca, Fecha, Alias) values ('', ".mysqli_insert_id($conect).", ".$_POST["bib"].", '".$_POST["fec"]."', '".$_POST["a_1"]."')";
	$exc = mysqli_query($conect, $sql);
	//clon 2
	$sql = "insert into programacion (Id, Biblioteca, Reporte, Sub, Sesiones, Fecha, Publico) values ('', ".$_POST["bib"].", ".$_POST["rep"].", '".$id."', ".$_POST["s_2"].", '".$_POST["fec"]."', '".$_POST["p_2"]."')";
	//echo $sql;
	$excs = mysqli_query($conect, $sql);
	$sql = "insert into prog_alias (Id, Reporte, Biblioteca, Fecha, Alias) values ('', ".mysqli_insert_id($conect).", ".$_POST["bib"].", '".$_POST["fec"]."', '".$_POST["a_2"]."')";
	$exc = mysqli_query($conect, $sql);
	//clon 3
	if(isset($_POST["s_3"], $_POST["a_3"]) && (strlen($_POST["s_3"]) > 0 && strlen($_POST["a_3"]) > 3))
	{
		$sql = "insert into programacion (Id, Biblioteca, Reporte, Sub, Sesiones, Fecha, Publico) values ('', ".$_POST["bib"].", ".$_POST["rep"].", '".$id."', ".$_POST["s_3"].", '".$_POST["fec"]."', '".$_POST["p_3"]."')";
		//echo $sql;
		$excs = mysqli_query($conect, $sql);
			$sql = "insert into prog_alias (Id, Reporte, Biblioteca, Fecha, Alias) values ('', ".mysqli_insert_id($conect).", ".$_POST["bib"].", '".$_POST["fec"]."', '".$_POST["a_3"]."')";
			$exc = mysqli_query($conect, $sql);
	}
	//clon 4
	if(isset($_POST["s_4"], $_POST["a_4"]) && (strlen($_POST["s_4"]) > 0 && strlen($_POST["a_4"]) > 3))
	{
		$sql = "insert into programacion (Id, Biblioteca, Reporte, Sub, Sesiones, Fecha, Publico) values ('', ".$_POST["bib"].", ".$_POST["rep"].", '".$id."', ".$_POST["s_4"].", '".$_POST["fec"]."', '".$_POST["p_4"]."')";
		//echo $sql;
		$excs = mysqli_query($conect, $sql);
			$sql = "insert into prog_alias (Id, Reporte, Biblioteca, Fecha, Alias) values ('', ".mysqli_insert_id($conect).", ".$_POST["bib"].", '".$_POST["fec"]."', '".$_POST["a_4"]."')";
			$exc = mysqli_query($conect, $sql);
	}
	if($excs)
	{
		echo "<h4 align='center'>Registro(s) creado(s) - actualizado(s)</h4>";
		echo "<h4 align='center'>Fecha para registrar ".date("n/Y")."</h4>";
		?>
		<script language="javascript">
		parent.consulta.formconf.<?php echo $_POST["tex"]; ?>.readOnly = true;
		parent.consulta.formconf.<?php echo $_POST["bot"]; ?>.disabled = true;
		parent.consulta.formconf.<?php echo $_POST["tex"]; ?>.value = <?php echo $sesiones; ?>;
		window.parent.consulta.clon_creado('<?php echo $publico; ?>','<?php echo $_POST["rep"]; ?>','<?php echo $_POST["dat"]; ?>','0','<?php echo $id; ?>','<?php echo $_GET["Nom"]; ?>');
		alert("Registro(s) creado(s) - actualizado(s).");
		document.location = "../fill.php";
		</script>
		<?php
	}
	else
	{
		echo "<h4 align='center'>Error al registrar la informaci&oacute;n</h4>";
	}
    unset($sql,$exc,$sesiones,$id,$publico);
	mysqli_close($conect);
}
?>
<h4 align="left" style="padding:5px;">&nbsp; Configuraci&oacute;n de instancias adicionales para el reporte "<?php echo $_GET["Nom"]; ?>"</h4>
<form name="add_clone" id="add_clone" method="post" action ="clonar.php?Nom=<?php echo $_GET["Nom"]; ?>" onsubmit="return validate();" >
<input name="tex" id="tex" type="hidden" value="<?php echo $_GET["Tex"]; ?>" />
<input name="bib" id="bib" type="hidden" value="<?php echo $_GET["Bib"]; ?>" />
<input name="fec" id="fec" type="hidden" value="<?php echo $_GET["Fec"]; ?>" />
<input name="bot" id="bot" type="hidden" value="<?php echo $_GET["Bot"]; ?>" />
<input name="rep" id="rep" type="hidden" value="<?php echo $_GET["Rep"]; ?>" />
<input name="pub" id="pub" type="hidden" value="<?php echo $_GET["Pub"]; ?>" />
<input name="dat" id="dat" type="hidden" value="<?php echo $_GET["Dat"]; ?>" />
<div class="list_vis">
    <div style="float:left; width:4%;"><strong>1.</strong></div>
    <div style="float:left; width:12%; padding:2px;">
    	<div>Sesiones:</div>
        <div style="width:94%"><input name="s_1" id="s_1" type="text" placeholder="000" /></div>
    </div>
    <div style="float:left; width:28%; padding:2px;">
    	<div>Alias para la actividad:</div>
        <div style="width:95%"><input name="a_1" id="a_1" type="text" placeholder="Nombre para identificar la actividad" /></div>
    </div>
    <div style="float:left; padding:2px;">
    	<div><?php echo $_GET["Dat"]; ?>:</div>
        <div style="width:80%"><input name="p_1" id="p_1" type="text" placeholder="<?php echo $_GET["Dat"]; ?>" /> </div>
    </div>
</div>
<div class="list_vis">
	<div style="float:left; width:4%;"><strong>2.</strong></div>
    <div style="float:left; width:12%; padding:2px;">
    	<div>Sesiones:</div>
        <div style="width:94%"><input name="s_2" id="s_2" type="text" placeholder="000" /></div>
    </div>
    <div style="float:left; width:28%; padding:2px;">
    	<div>Alias para la actividad:</div>
        <div style="width:95%"><input name="a_2" id="a_2" type="text" placeholder="Nombre para identificar la actividad" /></div>
    </div>
    <div style="float:left; padding:2px;">
    	<div><?php echo $_GET["Dat"]; ?>:</div>
        <div style="width:80%"><input name="p_2" id="p_2" type="text" placeholder="<?php echo $_GET["Dat"]; ?>" /> </div>
    </div>
</div>
<div class="list_js">
	<div style="float:left; width:4%;"><strong>3.</strong></div>
    <div style="float:left; width:12%; padding:2px;">
    	<div>Sesiones:</div>
        <div style="width:94%"><input name="s_3" id="s_3" type="text" placeholder="000" /></div>
    </div>
    <div style="float:left; width:28%; padding:2px;">
    	<div>Alias para la actividad:</div>
        <div style="width:95%"><input name="a_3" id="a_3" type="text" placeholder="Nombre para identificar la actividad" /></div>
    </div>
    <div style="float:left; padding:2px;">
    	<div><?php echo $_GET["Dat"]; ?>:</div>
        <div style="width:80%"><input name="p_3" id="p_3" type="text" placeholder="<?php echo $_GET["Dat"]; ?>" /> </div>
    </div>
</div>
<div class="list_js">
	<div style="float:left; width:4%;"><strong>4.</strong></div>
    <div style="float:left; width:12%; padding:2px;">
    	<div>Sesiones:</div>
        <div style="width:94%"><input name="s_4" id="s_4" type="text" placeholder="000" /></div>
    </div>
    <div style="float:left; width:28%; padding:2px;">
    	<div>Alias para la actividad:</div>
        <div style="width:95%"><input name="a_4" id="a_4" type="text" placeholder="Nombre para identificar la actividad" /></div>
    </div>
    <div style="float:left; padding:2px;">
    	<div><?php echo $_GET["Dat"]; ?>:</div>
        <div style="width:80%"><input name="p_4" id="p_4" type="text" placeholder="<?php echo $_GET["Dat"]; ?>" /> </div>
    </div>
</div>
<div style="clear:both;">&nbsp;</div>
<div align="center"><input name="guardar" id="guardar" type="submit" value="Guardar" /></div>
</form>
<div align="left" style="padding-top:10px;">Debe adicionar por lo menos dos instancias y máximo cuatro de la actividad seleccionada, las cuales estarán disponibles para generar reportes.</div>
<div align="justify" id="db_guardar">&nbsp; <!--Insertar mensaje de ayuda para la página -->
<div class="div_menu" id="aa_1"><a href="javascript:void(0);" onclick="menu('a_b_','1'); mostrar('bb','1');" title="Ayuda">? +</a></div>
<div class="div_menu" style="display:none;" id="bb_1"><a href="javascript:void(0);" onclick="menu('a_b_','2'); mostrar('aa','1');" title="Ayuda">? -</a></div>
<div class="div_ayuda" id="a_b_" style="display:none;">Cuando el mismo taller es desarrollado por más de un funcionario o la temática es diferente. Se debe completar cada adición en forma secuencial.</div></div>
<!-- InstanceEndEditable -->
<form name="chang_elemt" id="chang_elemt" target="contenido" method="post">
<!-- InstanceBeginEditable name="campos" -->

<!-- InstanceEndEditable -->
</form>
</div></body>

<script language="javascript">
verif();
</script>

<!-- InstanceEnd --></html>