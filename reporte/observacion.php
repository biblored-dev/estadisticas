<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/contenedor.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php
include("../script/scripts/session.php");
//include("../script/breadcrumbs.php");
?>
<link href="../css/css.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../script/scripts/jquery.min.js"></script>
<script type="text/javascript" src="../script/menu_ocultar.js"></script>
<script type="text/javascript">
function verif() {
document.getElementById('datos').style.display='none';
}
function foco_in(z) {
document.getElementById(z).focus();
}
</script>
<title>.: Sistema general de estad&iacute;stica :.</title>
<link rel="shortcut icon" href="../img/favicon.ico" />
<!-- InstanceBeginEditable name="doctitle" -->
<script language="javascript">
function busqueda() {
var val;
err = "";
val = document.getElementById("descrp").value;
if(val == null || val.length < 20 || /^\s+$/.test(val))
	err += "Se requiere la descripción de la actividad. \n";
if(err.length > 0)
{
	alert("Verifique los siguientes errores: \n\n"+err+"\n");
	return false;
}
else
	return true;
}
</script>
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" --><!-- InstanceEndEditable -->
</head>
<body>
<div id="datos" align="center">
 <h4>Consolidando datos...<br />
 <img src="../img/loader.gif" width="16" height="16" /></h4>
 </div>
<div align="left"><?php //echo breadcrumbs(); ?></div>
<div align="center" class="popup">
<!-- InstanceBeginEditable name="contenidos" -->
<?php
include("../Connections/conect.inc.php");
if(isset($_POST["guardar"], $_POST["descrp"]))
{
	$sql = "update programacion set Observacion = '".addslashes($_POST["descrp"])."' where Id = ".$_POST["id_report"];
	//echo $sql;
	$exc = mysqli_query($conect, $sql);
	if($exc)
	{
		unset($exc, $sql);
		mysqli_close($conect);
		?>
		<script language="javascript">
		alert("Registro actualizado");
		window.close(); 
		</script><?php
	}
}
if(isset($_GET["id_report"]) && $_GET["id_report"] > 0)
{
$sql = "select Observacion from programacion where Id = ".$_GET["id_report"];
$exc = mysqli_query($conect, $sql);
$row = mysqli_fetch_array($exc);
}
?>
<form name ="formulario" method ="POST" action ="observacion.php" onSubmit="return busqueda()">
<input name="id_report" id="id_report" type="hidden" value="<?php echo $_GET["id_report"]; ?>" />
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="2%">&nbsp;</td>
    <td width="96%" valign="baseline"><strong>Comentario general o novedades de la actividad para el mes. &Uacute;nico comentario</strong>:</td>
    <td width="2%"><h4 align="left"><a href="#" onclick="javascript:window.close();">X</a></h4>
    
    </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>
    <div align="left"><textarea name="descrp" id="descrp" rows="8" placeholder="Descripción de la (s) actividad (es) realizada (s) para todo el mes"><?php if($exc) echo $row["Observacion"]; ?></textarea></div>
<div class="infos">M&aacute;ximo 1000 caracteres. Describa la información que está reportando, si durante el mes se presentaron inconvenientes o acciones que afecten el dato reportado <strong>o desea resaltar los contenidos desarrollados</strong>.</div>
    </td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><div align="center">
    <input name="guardar" id="guardar" type="submit" value="Guardar observación" />
    </div>
    </td>
    <td>&nbsp;</td>
  </tr>
</table>
</form>
<div align="justify" id="db_guardar">&nbsp; <!--Insertar mensaje de ayuda para la página -->
<div class="div_menu" id="aa_1"><a href="javascript:void(0);" onclick="menu('a_b_','1'); mostrar('bb','1');" title="Ayuda">? +</a></div>
<div class="div_menu" style="display:none;" id="bb_1"><a href="javascript:void(0);" onclick="menu('a_b_','2'); mostrar('aa','1');" title="Ayuda">? -</a></div>
<div class="div_ayuda" id="a_b_" style="display:none;">Comentario general o novedades de la actividad para el mes. Descripci&oacute;n de las dificultades por las cuales no se cumpli&oacute; con la meta de sesiones o por el contrario si se super&oacute;. Novedades a resaltar.</div></div>
<?php
@ mysqli_free_result($exc);
unset($exc, $sql, $row);
mysqli_close($conect);
?>
<!-- InstanceEndEditable -->
<form name="chang_elemt" id="chang_elemt" target="contenido" method="post">
<!-- InstanceBeginEditable name="campos" -->

<!-- InstanceEndEditable -->
</form>
</div></body>

<script language="javascript">
verif();
</script>

<!-- InstanceEnd --></html>