<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/contenedor.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php
include("../script/scripts/session.php");
//include("../script/breadcrumbs.php");
?>
<link href="../css/css.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../script/scripts/jquery.min.js"></script>
<script type="text/javascript" src="../script/menu_ocultar.js"></script>
<script type="text/javascript">
function verif() {
document.getElementById('datos').style.display='none';
}
function foco_in(z) {
document.getElementById(z).focus();
}
</script>
<title>.: Sistema general de estad&iacute;stica :.</title>
<link rel="shortcut icon" href="../img/favicon.ico" />
<!-- InstanceBeginEditable name="doctitle" -->
<script language="javascript">
var campo = new Array();
var salas = new Array();
function get_fech(w, x) {
w = w.split('/');
var ret = "";
if(w[0] < 10)
	ret = w[1]+"/0"+w[0];
else
	ret = w[1]+"/"+w[0];
if(x == 'i')
{
	ret += "/01";
}
if(x == 'f')
{
	ret += "/"+ new Date(w[1] || new Date().getFullYear(), w[0], 0).getDate();
}
return ret;
}
function datosserver(w,x) {
if(campo.length > 0)
{
	//document.getElementById('bib_total').innerHTML = '0';
	document.getElementById('automat_visitas').innerHTML = '0';
	for(var dato in campo){
	if(document.getElementById("bib_"+campo[dato]))
	{
		document.getElementById("bib_"+campo[dato]).value = 0;
	}
	}
	  var fi = get_fech(document.getElementById('fecha').value, 'i');
	  var ff = get_fech(document.getElementById('fecha').value, 'f');
	  document.getElementById('datos').style.display = "";
	  //var sql = "SELECT datos.puerta AS PUERTA, (EXTRACT(DAY FROM DATE(historialconteo.fechalect))) AS DIA, CASE WHEN SUM(CASE WHEN movconteoid in (1,3) THEN cantidad ELSE 0 END) > SUM(CASE WHEN movconteoid in (2,4) THEN cantidad ELSE 0 END) THEN SUM(CASE WHEN movconteoid in (1,3) THEN cantidad ELSE 0 END) ELSE SUM(CASE WHEN movconteoid in (2,4) THEN cantidad ELSE 0 END) END AS TE FROM historialconteo RIGHT OUTER JOIN (SELECT antena.antenaid as puerta FROM empresa, zona, lectora, antena WHERE empresa.empresaid = zona.empresaid AND empresa.ciudadid = zona.ciudadid AND empresa.sucursal = zona.sucursal AND zona.zonaid = lectora.zonaid AND lectora.lectoraid = antena.lectoraid AND lectora.tipolectora = 'Micro PC' AND empresa.web = '"+w+"' GROUP BY antena.antenaid) datos ON (historialconteo.antenaid = datos.puerta AND historialconteo.fechalect BETWEEN '"+fi+" 00:00:00' AND '"+ff+" 23:59:59' AND date_part('hour', historialconteo.fechalect) BETWEEN 8 AND 20) GROUP BY datos.puerta, date(historialconteo.fechalect) ORDER BY date(historialconteo.fechalect) ASC";
	  var sql = "SELECT datos.puerta AS PUERTA, (EXTRACT(DAY FROM DATE(historialconteo.fechalect))) AS DIA, SUM(CASE WHEN movconteoid in (1,3) THEN cantidad ELSE 0 END) AS TE FROM historialconteo RIGHT OUTER JOIN (SELECT antena.antenaid as puerta FROM empresa, zona, lectora, antena WHERE empresa.empresaid = zona.empresaid AND empresa.ciudadid = zona.ciudadid AND empresa.sucursal = zona.sucursal AND zona.zonaid = lectora.zonaid AND lectora.lectoraid = antena.lectoraid AND lectora.tipolectora = 'Micro PC' AND empresa.web = '"+w+"' GROUP BY antena.antenaid) datos ON (historialconteo.antenaid = datos.puerta AND historialconteo.fechalect BETWEEN '"+fi+" 00:00:00' AND '"+ff+" 23:59:59' AND date_part('hour', historialconteo.fechalect) BETWEEN 8 AND 20) GROUP BY datos.puerta, date(historialconteo.fechalect) ORDER BY date(historialconteo.fechalect) ASC";
	  http.open("GET", "../script/posghxml.php?d1="+x+"&d2=1&dat="+sql, true); //False para que no continue el flujo hasta compeltar la petición ajax
	  http.onreadystatechange = datosHttpResp;
	  return http.send(null);
}
else
 alert("La biblioteca no tiene espacios configurados con conteo automático.");
}

function datosHttpResp() {
if(http.readyState == 4)
{
	if(http.status == 200)
	{
		var salida = "";
		var total_prest = 0;
		var timeValue = http.responseXML.getElementsByTagName("timenow")[0];
	    if(timeValue.childNodes.length > 0)
		{
			var regs = timeValue.childNodes[0].nodeValue;
			regs = regs.split(";");
			for(i = 0; i < (regs.length) - 1; i ++)
			{
			   salida = regs[i].split(",,");
			   if(document.getElementById('bib_'+campo[salida[0]]))
			   {
			   		document.getElementById('bib_'+campo[salida[0]]).value = parseInt(document.getElementById('bib_'+campo[salida[0]]).value) + parseInt(salida[1]);
					document.getElementById('automat_visitas').innerHTML = parseInt(document.getElementById('automat_visitas').innerHTML) + parseInt(salida[1]);
			   }
			   else
			   {
			   		//document.getElementById('otras_visitas').value = parseInt(document.getElementById('otras_visitas').value) + parseInt(salida[1]);
					//document.getElementById('bib_total').innerHTML = parseInt(document.getElementById('bib_total').innerHTML) + parseInt(salida[1]);
					document.getElementById('bib_'+document.getElementById('bib').value+"0").value = parseInt(document.getElementById('bib_total').innerHTML);
			   }
			}
		}
		actualizacion();
		verif();
	}
} 
else
{
	document.getElementById('datos').style.display = "";
}
}

function sel_resp(w) {
for(i = 1; i < document.getElementById("funcionario").length - 1; i++)
{
	if(document.getElementById("funcionario")[i].value.toUpperCase() == w.toUpperCase())
		document.getElementById("funcionario").selectedIndex = i;
}
}
function actualizacion() {
var auto = princ = val = 0;
var id = document.getElementById("id_esp").value.split(",");
for(i=0;i<(id.length - 1);i++)
{
	val += parseInt(document.getElementById("bib_"+id[i]).value);
}
for(i=0;i<salas.length;i++)
{
	princ += parseInt(document.getElementById("bib_"+salas[i]).value);
}
document.getElementById("princ_visitas").innerHTML = val;
document.getElementById("otras_visitas").innerHTML = princ;
}
function busqueda() {
var val, err;
err = "";
var id = document.getElementById("id_esp").value.split(",");
for(i=0;i<(id.length - 1);i++)
{
	val = document.getElementById("bib_"+id[i]).value;
	if(val == null || isNaN(val) || /^\s+$/.test(val) || val.length < 1 )
		err += "Se requiere el valor del reporte "+(i + 1)+". \n";
}
val = document.getElementById("funcionario")[document.getElementById("funcionario").selectedIndex].value;
if(val == null || val.length < 5 || /^\s+$/.test(val) || val == "Adicionar funcionario")
	err += "Se requiere el nombre del responsable. \n";
val = document.getElementById("descrp").value;
if(val == null || !isNaN(val) || val.length < 20 || /^\s+$/.test(val))
	err += "Se requiere la descripción. \n";
if(err.length > 0)
{
	alert("Verifique los siguientes errores: \n\n"+err+"\n");
	return false;
}
else
	return true;	
}
</script>
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" --><!-- InstanceEndEditable -->
</head>
<body>
<div id="datos" align="center">
 <h4>Consolidando datos...<br />
 <img src="../img/loader.gif" width="16" height="16" /></h4>
 </div>
<div align="left"><?php //echo breadcrumbs(); ?></div>
<div align="center" class="popup">
<!-- InstanceBeginEditable name="contenidos" -->
<?php
include("../Connections/conect.inc.php");
if(isset($_POST["button"], $_POST["id_esp"], $_POST["id_rep"], $_POST["bib"]))
{
include("../script/cant.php");
$ids = explode(",",$_POST["id_esp"]);
for($i=0; $i<$_POST["cantidad"]; $i++)
{ 
	$claus = $_POST["fecha"]."' and Sala = '".$ids[$i];
	if(cant("visitas","Fecha",$claus) == 0) //tabla, campo, dato
	{
		$sql = "insert into visitas (Id, Fecha, Fecha_R, Biblioteca, Sala, Id_Report, Visitantes, Responsable, Descripcion) VALUES ('', '".$_POST["fecha"]."', '".date("Y-n-j")."', '".$_POST["bib"]."', '".$ids[$i]."', '".$_POST["id_rep"]."', '".$_POST["bib_".$ids[$i]]."', '".strtoupper($_POST["funcionario"])."', '".addslashes($_POST["descrp"])."')";
	}
	else
	{
		$sql = "update visitas set Visitantes = '".$_POST["bib_".$ids[$i]]."', Fecha_R = '".date("Y-n-j")."', Id_Report = '".$_POST["id_rep"]."', Descripcion = '".addslashes($_POST["descrp"])."' where Sala = '".$ids[$i]."' and Fecha = '".$_POST["fecha"]."'";
	}
	//echo $sql;
	$exc = mysqli_query($conect, $sql);
}
//$sql = "update programacion set Sesiones = ".$_POST["cantidad"]." where Id = ".$_POST["id_rep"];
//$sql = "update programacion set Sesiones = '1' where Id = ".$_POST["id_rep"];
//$exc = mysqli_query($conect, $sql);
if($exc)
{
    echo "<h3 align='center'>Registro actualizado</h3>";
    ?><script language="javascript">
    alert("Registro actualizado");
    window.close(); 
    </script><?php
}
else
{
    echo "<h3 align='center'>Error al actualizar el registro</h3>";
    exit;
}
unset($exc, $sql, $i, $row, $ids);
mysqli_close($conect);
}
?>
<div align="left">
<h3 align="center">Reporte estad&iacute;stico para <?php echo $_GET["Reporte"] ?></h3>
<div align="center">(<strong>&Aacute;lias:&nbsp;<?php echo $_GET["alias"] ?></strong>)<br />
<strong>Biblioteca: <?php echo $_GET["nom"] ?></strong>, mes <?php echo $_GET["fech_rep"]; ?></div></div><br />
<?php
if(isset($_GET["bib"], $_GET["nom"]))
{ 
$sql = "select salas.Id, salas.Sub, salas.Nombre, salas.Cod_Conteo, salas.Sum_Conteo, biblioteca.Abrev from salas, biblioteca where salas.Biblioteca = ".$_GET["bib"]." and biblioteca.Id = ".$_GET["bib"]." order by salas.Sum_Conteo, salas.Nombre asc";
//echo $sql;
$excs = mysqli_query($conect, $sql);
$ids = "";
$cods = "";
$fecha_r = "0000-00-00";
$visitas = 0; //Sumatoria de todos los reportes de visitas
$principales = 0; //Salas principales de la biblioteca, puede contener otros espacios con o sin conteo automático
$automatico = 0; //Espacios con conteo automático
?>
<div align="center">
<form name ="formiden" method ="POST" action ="visitas.php?bib=<?php echo $_GET["bib"]; ?>&nom=<?php echo $_GET["nom"]; ?>" onsubmit="return busqueda();">
<input name="id_rep" id="id_rep" type="hidden" value="<?php echo $_GET["id_rep"]; ?>" />
<input name="bib" id="bib" type="hidden" value="<?php echo $_GET["bib"]; ?>" />
<input name="nombre" id="nombre" type="hidden" value="<?php echo $_GET["nom"]; ?>" />
<input name="cantidad" id="cantidad" type="hidden" value="<?php echo mysqli_num_rows($excs); ?>" />
<input name="fecha" id="fecha" type="hidden" value="<?php echo $_GET["fech_rep"]; ?>" />
<?php
for($i=0; $i<mysqli_num_rows($excs); $i++)
{ 		
	$rows = mysqli_fetch_array($excs);
	$sql = "select Fecha_R, Visitantes, Responsable from visitas where Sala = ".$rows["Id"]." and Fecha = '".$_GET["fech_rep"]."'";
	$excss = mysqli_query($conect, $sql);
	if(mysqli_num_rows($excss))
	{
		$rowss = mysqli_fetch_array($excss);
		$fecha_r = $rowss["Fecha_R"];
	}
	else
	{
		$rowss["Visitantes"] = 0;
		$fecha_r = "0000-00-00";
	}
	?>
    <div class="list_js" align="left"><strong><?php echo ($i + 1).". ".$rows["Nombre"]; ?><?php if($rows["Cod_Conteo"] != 0) echo " (<span class='infos'>Espacio con conteo autom&aacute;tico</span>)"; ?></strong>
    <input name="bib_<?php echo $rows["Id"]; ?>" id="bib_<?php echo $rows["Id"]; ?>" type="text" value="<?php echo $rowss["Visitantes"]; ?>" placeholder="000" <?php if($rows["Cod_Conteo"] != 0) echo "readonly='readonly'"; ?> />
    </div>
	<?php
    $ids .= $rows["Id"].",";
    $visitas += $rowss["Visitantes"];
    $puntos;
    if($rows["Sub"] == 0)
    {
        $principales += $rowss["Visitantes"];
        //$visitas += $rowss["Visitantes"]; ///verificar para cuando se realiza la actualizacion......
        ?>
        <script language="javascript">
        salas.push(<?php echo $rows["Id"]; ?>);
        </script>
        <?php
    }
    if($rows["Cod_Conteo"] != 0)
    {
        $cods .= $rows["Cod_Conteo"].",";
        $puntos = explode(",", $rows["Cod_Conteo"]);
        if(sizeof($puntos) > 1)
        {
            for($ii=0; $ii<sizeof($puntos); $ii++)
            { 
                ?>
                <script language="javascript">
                campo[<?php echo $puntos[$ii]; ?>] = <?php echo $rows["Id"]; ?>;
                </script>
                <?php
            }
            unset($ii);
        }
        else
        {
            ?>
            <script language="javascript">
            campo[<?php echo $rows["Cod_Conteo"]; ?>] = <?php echo $rows["Id"]; ?>;
            </script>
            <?php
        }
        $automatico += $rowss["Visitantes"];
        //$visitas += $rowss["Visitantes"];
        unset($puntos);
    }
}
/*if($rows["Conteo"] != 0)
{
	$ids .= $_GET["bib"]."0,";
	$cods .= $rows["Conteo"].",";
	$puntos = explode(",", $rows["Conteo"]);
	if(sizeof($puntos) > 1)
	{
		for($ii=0; $ii<sizeof($puntos); $ii++)
		{ 
			?>
			<script language="javascript">
			campo[<?php echo $puntos[$ii]; ?>] = <?php echo $puntos[0]; ?>;
			</script>
			<?php
		}
		unset($ii);
	}
	else
	{
		?>
		<script language="javascript">
		campo[<?php echo $rows["Conteo"]; ?>] = <?php echo $rows["Conteo"]; ?>;
		</script>
		<?php
	} ?>
	<script language="javascript">
	document.getElementById("cantidad").value =  parseInt(document.getElementById("cantidad").value) + 1;
	</script>
	<?php
	$sql = "select Visitantes from visitas where Sala = ".$_GET["bib"]."0"." and Fecha = '".$_GET["fech_rep"]."'";
	$excss = mysqli_query($conect, $sql);
	$rowss = mysqli_fetch_array($excss);
	$visitas += $rowss["Visitantes"];
}*/
?>
<div align="right" style="clear:both;"><strong><a href="javascript:void(0);" onclick="datosserver('<?php echo $rows["Abrev"]; ?>','<?php echo $cods; ?>');">Actualizar informacion desde el sistema de conteo y totales</a></strong></div>
<div align="left">&Uacute;ltima fecha de actualizaci&oacute;n de registros: <?php echo $fecha_r; ?></div>
<strong>
<div align="left" style="clear:both; background-color:#EBEEE5; padding:5px; width:90%; border-radius:3px;">
<input name="bib_<?php echo $_GET["bib"]."0"; ?>" id="bib_<?php echo $_GET["bib"]."0"; ?>" type="hidden" value="<?php echo $rowss["Visitantes"]; ?>" readonly="readonly" />
<div align="left"> &nbsp; &nbsp; &nbsp; Total Visitas espacios con conteo autom&aacute;tico: <span id="automat_visitas"><?php echo $automatico; ?></span></div>
<div align="left">** &nbsp; Total Visitas espacios o salas principales: <span id="otras_visitas"><?php echo $principales; ?></span></div>
<div align="left"> &nbsp; &nbsp; &nbsp; Total Visitas (servicios) reportadas: <span id="princ_visitas"><?php echo $visitas; ?></span></div></div></strong>
<div align="left" style="clear:both;"><strong>Nombre del responsable:</strong></div>
<div align="left" style="width:50%; float:left;"><?php include("../script/funcionarios.php"); ?></div>
<?php
if(isset($rowss["Responsable"]))
{ ?>
<script language="javascript">sel_resp('<?php echo $rowss["Responsable"]; ?>');</script>
<?php } ?>
<div align="left" style="clear:both;"><strong>Descripci&oacute;n de la actividad realizada:</strong></div>
<?php include("../script/caracteristicas.php"); ?>
<div align="center"><input type="submit" name="button" id="button" value="Actualizar" /></div>
<input name="id_esp" id="id_esp" type="hidden" value="<?php echo $ids; ?>" />
<div align="left">* &nbsp; Conteo con que cuentan algunas bibliotecas locales y de barrio</div>
<div align="left">** Espacios o salas que pueden contener otros espacios asociados</div>
</form>
</div>
<?php include("../script/observacion.php"); ?>
<?php
mysqli_free_result($excs);
@ mysqli_free_result($excss);
unset($excs, $excss, $sql, $i, $rows, $rowss, $ids, $fecha_r, $puntos, $visitas, $principales, $automatico);
}
?>
<p align="justify">Para validar la información o hacer ajustes ingrese a la herramienta de <a href="http://192.168.200.24:7938/conteo/" target="_blank"><strong>conteo</strong> </a></p>
<!-- InstanceEndEditable -->
<form name="chang_elemt" id="chang_elemt" target="contenido" method="post">
<!-- InstanceBeginEditable name="campos" -->

<!-- InstanceEndEditable -->
</form>
</div></body>

<script language="javascript">
verif();
</script>

<!-- InstanceEnd --></html>