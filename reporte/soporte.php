<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/contenedor.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php
include("../script/scripts/session.php");
//include("../script/breadcrumbs.php");
?>
<link href="../css/css.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../script/scripts/jquery.min.js"></script>
<script type="text/javascript" src="../script/menu_ocultar.js"></script>
<script type="text/javascript">
function verif() {
document.getElementById('datos').style.display='none';
}
function foco_in(z) {
document.getElementById(z).focus();
}
</script>
<title>.: Sistema general de estad&iacute;stica :.</title>
<link rel="shortcut icon" href="../img/favicon.ico" />
<!-- InstanceBeginEditable name="doctitle" -->
<script language="javascript" src="../script/datosxml.js"></script>
<script language="javascript">
var err1 = err2 = "";
var div = "";
var docs = new Array("doc","docx","pdf");
function inArray(w) {
for(var i = 0; i < docs.length; i++) 
{
	if(docs[i] == w)
	{
		i = docs.length;
		return true;
	}
}
return false;
}
function diferencia (w) {
w = w.split("/");
var ret = 0;
f = new Date();
ret += ((parseInt(f.getFullYear()) - parseInt(w[2])) * 365);
ret += (((parseInt(f.getMonth()) + 1) - (parseInt(w[1]))) * 30);
ret += (parseInt(f.getDate())) - (parseInt(w[0]));
return ret;
}
function handleFiles(files, w, x) {
  if(!files.length) {
    alert("No se ha seleccionado archivos!");
  } 
  else
  {
      var ret = diferencia(files[0].lastModifiedDate.toLocaleDateString());
	  var ext = files[0].name.split(".");
	  if((files[0].size / 1048576).toFixed(3) > 5.120 || inArray(ext[(ext.length - 1)]) == false || ret > 31 || ret < 0)
	  {
		document.getElementById("info_"+w).style.backgroundColor = "#FF5B5B";
	  }
	  else
	  {
		  document.getElementById("info_"+w).style.backgroundColor = "#EBEEE5";
	  }
	  if(x == 1)
	  {
		  err1 = "";
		  if((files[0].size / 1048576).toFixed(3) > 5.120)
		  {
				err1+="El archivo "+x+" supera el tamaño mínimo (5,0 Mb). \n";
		  }
		  if(inArray(ext[(ext.length - 1)]) == false)
		  {
				err1+="El archivo "+x+" adjunto no tiene formato válido. \n";
		  }
		  if(ret > 31 || ret < 0)
		  {
				err1+="La fecha del archivo adjunto "+x+" tiene mas de "+ret+" días. \n";  
		  }
	  }
	  else if(x == 2)
	  {
		  err2 = "";
		  if((files[0].size / 1048576).toFixed(3) > 5.120)
		  {
				err2+="El archivo "+x+" supera el tamaño mínimo (5,0 Mb). \n";
		  }
		  if(inArray(ext[(ext.length - 1)]) == false)
		  {
				err2+="El archivo "+x+" adjunto no tiene formato válido. \n";
		  }
		  if(ret > 31 || ret < 0)
		  {
				err2+="La fecha del archivo adjunto "+x+" tiene mas de "+ret+" días. \n";  
		  }
	  }
	  document.getElementById("info_"+w).innerHTML = "Archivo: "+files[0].name + ", Tamaño: " + (files[0].size / 1048576).toFixed(3) + " Mb"+", Fecha: "+files[0].lastModifiedDate.toLocaleDateString();
  }
}
function busqueda() {
var err = "";
if(document.getElementById("doc1").value.length == 0 && document.getElementById("doc2").value.length == 0)
	err+= "No se ha seleccionado archivos.\n";
else if(document.getElementById("doc1").value == document.getElementById("doc2").value)
	err+= "El nombre de los archivos seleccionado es el mismo.\n";
err += err1 + err2;
if(err.length > 0)
{
	alert("Verifique los siguientes errores: \n\n"+err+"\n");
	return false;
}
else
	return true;
}
function predataserver(wx,w,x,y) {
  div = y;
  var sql = "update soportes set Doc"+y+" = '' where Reporte = "+wx;
  http.open("GET", "../script/soporte.php?tab="+w+"&camp=Doc_"+y+"&dat="+sql+"&archivo="+x, false); //False para que no continue el flujo hasta compeltar la petición ajax
  http.onreadystatechange = useHttpResp;
  return http.send(null);
}

function useHttpResp() {
if(http.readyState == 4)
{
	if(http.status == 200)
	{
		var timeValue = http.responseXML.getElementsByTagName("timenow")[0];
		if(parseInt(timeValue.childNodes[0].nodeValue) > 0)
		{
			if(parseInt(timeValue.childNodes[0].nodeValue) == 1)
				document.getElementById("info_doc"+div).innerHTML = "Error al eliminar el archivo";
			if(parseInt(timeValue.childNodes[0].nodeValue) == 2)
			{
				document.getElementById("info_doc"+div).innerHTML = "Archivo eliminado. Seleccione el nuevo archivo a adjuntar.";	
				document.getElementById("doc"+div).disabled = false;
			}
		}
		verif();
	}
} 
else
{
	document.getElementById('datos').style.display = "";
}
}
</script>
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" --><!-- InstanceEndEditable -->
</head>
<body>
<div id="datos" align="center">
 <h4>Consolidando datos...<br />
 <img src="../img/loader.gif" width="16" height="16" /></h4>
 </div>
<div align="left"><?php //echo breadcrumbs(); ?></div>
<div align="center" class="popup">
<!-- InstanceBeginEditable name="contenidos" -->
<?php
include("../Connections/conect.inc.php");
if(isset($_POST["guardar"], $_POST["fech_rep"], $_POST["id_report"]))
{
	if(!file_exists("../soportes/".$_POST["fech_rep"])) 
	{
		if(!mkdir("../soportes/".$_POST["fech_rep"], 0777, true))
		{
			echo "<h3>Error al crear carpeta de trabajo. Informe a CST para continuar.</h3>";
			exit;
		}
	}
	echo "<br />";
	include("../script/cant.php");
	if(isset($_FILES["doc1"]))
	{
		$ext = explode(".",$_FILES["doc1"]["name"]);
		if(move_uploaded_file($_FILES["doc1"]["tmp_name"], "../soportes/".$_POST["fech_rep"]."/".$_FILES["doc1"]["name"]))
		{
			rename("../soportes/".$_POST["fech_rep"]."/".$_FILES["doc1"]["name"], "../soportes/".$_POST["fech_rep"]."/".$_POST["id_report"]."_1.".$ext[sizeof($ext) - 1]);
			if(cant("soportes","Reporte",$_POST["id_report"]) == 0)
			{
				$sql = "insert into soportes (Id, Reporte, Doc1, Doc2) VALUES ('', '".$_POST["id_report"]."', '".$_POST["id_report"]."_1.".$ext[sizeof($ext) - 1]."', '')";
			}
			else
			{
				$sql = "update soportes set Doc1 = '".$_POST["id_report"]."_1.".$ext[sizeof($ext) - 1]."' where Reporte = ".$_POST["id_report"];
			}
			$exc = mysqli_query($conect, $sql);
			echo "El archivo 1 es v&aacute;lido y se subi&oacute; con &eacute;xito.\n";
		}
	}
	if(isset($_FILES["doc2"]))
	{
		$ext = explode(".",$_FILES["doc2"]["name"]);
		if (move_uploaded_file($_FILES["doc2"]["tmp_name"], "../soportes/".$_POST["fech_rep"]."/".$_FILES["doc2"]["name"]))
		{
			rename("../soportes/".$_POST["fech_rep"]."/".$_FILES["doc2"]["name"], "../soportes/".$_POST["fech_rep"]."/".$_POST["id_report"]."_2.".$ext[sizeof($ext) - 1]);
			if(cant("soportes","Reporte",$_POST["id_report"]) == 0)
			{
				$sql = "insert into soportes (Id, Reporte, Doc1, Doc2) VALUES ('', '".$_POST["id_report"]."', '', '".$_POST["id_report"]."_2.".$ext[sizeof($ext) - 1]."')";
			}
			else
			{
				$sql = "update soportes set Doc2 = '".$_POST["id_report"]."_2.".$ext[sizeof($ext) - 1]."' where Reporte = ".$_POST["id_report"];
			}
			$exc = mysqli_query($conect, $sql);
			echo "El archivo 2 es v&aacute;lido y se subi&oacute; con &eacute;xito.\n";
		}
	}
	if($exc)
	{
		unset($exc, $sql);
		mysqli_close($conect);
		?>
		<script language="javascript">
		alert("Registro actualizado");
		window.close(); 
		</script><?php
	} 
}
if(isset($_GET["id_report"], $_GET["fech_rep"]))
{
$_GET["fech_rep"] = str_replace("/","-",$_GET["fech_rep"]);
$sql = "select Doc1, Doc2 from soportes where Reporte = ".$_GET["id_report"];
$exc = mysqli_query($conect, $sql);
$row = mysqli_fetch_array($exc);

?>
<form name ="formulario" method ="POST" action ="soporte.php" onSubmit="return busqueda()" enctype="multipart/form-data">
<input name="id_report" id="id_report" type="hidden" value="<?php echo $_GET["id_report"]; ?>" />
<input name="fech_rep" id="fech_rep" type="hidden" value="<?php echo $_GET["fech_rep"]; ?>" />
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="2%">&nbsp;</td>
    <td width="96%" valign="baseline"><strong>Adicionar soportes digitales de la actividad realizada, puede tener hasta dos documentos</strong>:</td>
    <td width="2%"><h4 align="left"><a href="#" onclick="javascript:window.close();">X</a></h4>
    
    </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>
    <div align="left">
    <div class="infos" align="left" style="padding:10px; background-color:#EBEEE5; border-radius:10px;">
    <div align="left"><strong>Documento 1:</strong></div>
    <input name="doc1" id="doc1" type="file" size="50" accept=".doc, .docx, .pdf" onchange="handleFiles(this.files,'doc1',1)" <?php if($row["Doc1"] || $_SESSION['MM_Usr_Pri'] > 4) echo "disabled='disabled'"; ?> />
    <div id="info_doc1">
    <?php
	if($row["Doc1"])
	{ ?>
		<strong>Archivo actual: </strong> <a href="../soportes/<?php echo $_GET["fech_rep"]."/".$row["Doc1"]; ?>" target="_blank"><strong>Consultar</strong></a> &nbsp; &nbsp;
        <a href="#" onclick="predataserver('<?php echo $_GET["id_report"]; ?>', '<?php echo $_GET["fech_rep"]; ?>','<?php echo $row["Doc1"]; ?>','1');"><strong>Eliminar</strong></a>
        <div>Para adjuntar un nuevo archivo, elimine el actual.</div>
	<?php
	}
	else
		echo "Seleccione el archivo a adjuntar.";
	?>
    </div>
    </div>
    <div class="infos" align="left" style="padding:10px; background-color:#EBEEE5; border-radius:10px;">
    <div align="left"><strong>Documento 2:</strong></div>
    <input name="doc2" id="doc2" type="file" size="50" accept=".doc, .docx, .pdf" onchange="handleFiles(this.files,'doc2',2)" <?php if($row["Doc2"] || $_SESSION['MM_Usr_Pri'] > 4) echo "disabled='disabled'"; ?> />
    <div id="info_doc2">
    <?php
	if($row["Doc2"])
	{ ?>
		<strong>Archivo actual: </strong> <a href="../soportes/<?php echo $_GET["fech_rep"]."/".$row["Doc2"]; ?>" target="_blank"><strong>Consultar</strong></a> &nbsp; &nbsp;
        <a href="#" onclick="predataserver('<?php echo $_GET["id_report"]; ?>','<?php echo $_GET["fech_rep"]; ?>','<?php echo $row["Doc2"]; ?>','2');"><strong>Eliminar</strong></a>
        <div>Para adjuntar un nuevo archivo, elimine el actual.</div>
	<?php
	}
	else
		echo "Seleccione el archivo a adjuntar.";
	?>
    </div>
    </div></div>
    <div class="infos">Puede adjuntar hasta dos documentos, (documentos de Word o en pdf), pueden contener cualquiera de las siguientes extensiones (doc, docx, pdf), no se admite ningún otro formato. Los archivos no pueden superar las 2 megas de peso, por lo que debe verificar sus características previamente. No pueden tener más de un mes de creado o actualizado.</div>
    </td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><div align="center">
    <input name="guardar" id="guardar" type="submit" value="Guardar soporte" />
    </div>
    </td>
    <td>&nbsp;</td>
  </tr>
</table>
</form>
<div align="justify" id="db_guardar">&nbsp; <!--Insertar mensaje de ayuda para la página -->
<div class="div_menu" id="aa_1"><a href="javascript:void(0);" onclick="menu('a_b_','1'); mostrar('bb','1');" title="Ayuda">? +</a></div>
<div class="div_menu" style="display:none;" id="bb_1"><a href="javascript:void(0);" onclick="menu('a_b_','2'); mostrar('aa','1');" title="Ayuda">? -</a></div>
<div class="div_ayuda" id="a_b_" style="display:none;">Los documentos anexos no pueden ser superiores a 2 Mb cada uno, puede ser un documento de Word con extensión (.doc, .docx) o un documento en PDF. Puede utilizar uno, uno de cada uno o los dos del mismo formato. No pueden tener más de un mes de creado o actualizado.</div></div>
<?php
@ mysqli_free_result($exc);
unset($exc, $sql, $row);
mysqli_close($conect);
}
?>
<!-- InstanceEndEditable -->
<form name="chang_elemt" id="chang_elemt" target="contenido" method="post">
<!-- InstanceBeginEditable name="campos" -->

<!-- InstanceEndEditable -->
</form>
</div></body>

<script language="javascript">
verif();
</script>

<!-- InstanceEnd --></html>