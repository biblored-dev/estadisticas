<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/contenedor.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php
include("../script/scripts/session.php");
//include("../script/breadcrumbs.php");
?>
<link href="../css/css.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../script/scripts/jquery.min.js"></script>
<script type="text/javascript" src="../script/menu_ocultar.js"></script>
<script type="text/javascript">
function verif() {
document.getElementById('datos').style.display='none';
}
function foco_in(z) {
document.getElementById(z).focus();
}
</script>
<title>.: Sistema general de estad&iacute;stica :.</title>
<link rel="shortcut icon" href="../img/favicon.ico" />
<!-- InstanceBeginEditable name="doctitle" -->
<script language="javascript">
function validate() {
var err, op;
var al1 = al2 = false;
err = "";
for(i=1; i<3; i++)
{
	op = document.getElementById("s_"+i).value;
	if(isNaN(op) || op == 0 || /^\s+$/.test(op))
		err += "Se requiere el número de sesiones "+i+". \n";
	op = document.getElementById("a_"+i).value;
	if(op.length <= 3 || /^\s+$/.test(op))
		err += "Se requiere el álias del registro "+i+". \n";
	else if(op.length > 60)
		err += "El álias del registro "+i+" no debe superar 60 caracteres. \n";
	op = document.getElementById("p_"+i).value;
	if(op.length > 0 && op > 0 && (isNaN(op) || /^\s+$/.test(op)))
		err += document.getElementById("dat").value + " "+i+" no es válido. \n";	
}
if(document.getElementById("a_1").value.length > 0 && document.getElementById("a_1").value == document.getElementById("a_2").value)
	err += "Los álias no pueden ser iguales. \n";
if(document.getElementById("s_3").value.length > 0 || document.getElementById("a_3").value.length > 0 || document.getElementById("p_3").value.length > 0)
{
	al1 = true;
	op = document.getElementById("s_3").value;
	if(isNaN(op) || op == 0 || /^\s+$/.test(op))
	{
		err += "Se requiere el número de sesiones 3. \n";
		al1 = false;
	}
	op = document.getElementById("a_3").value;
	if(op.length <= 3 || /^\s+$/.test(op))
	{
		err += "Se requiere el álias del registro 3. \n";
		al1 = false;
	}
	else if(op.length > 60)
	{
		err += "El álias del registro 3 no debe superar 60 caracteres. \n";
		al1 = false;
	}
	op = document.getElementById("p_3").value;
	if(op.length > 0 && op > 0 && (isNaN(op) || /^\s+$/.test(op)))
	{
		err += document.getElementById("dat").value + " "+3+" no es válido. \n";
		al1 = false;
	}
	if(document.getElementById("a_3").value == document.getElementById("a_1").value || document.getElementById("a_3").value == document.getElementById("a_2").value)
	{
		err += "Los álias no pueden ser iguales. \n";
		al1 = false;
	}
}
if(document.getElementById("s_4").value.length > 0 || document.getElementById("a_4").value.length > 0 || document.getElementById("p_4").value.length > 0)
{
	al2 = true;
	op = document.getElementById("s_4").value;
	if(isNaN(op) || op == 0 || /^\s+$/.test(op))
	{
		err += "Se requiere el número de sesiones 4. \n";
		al2 = false;
	}
	op = document.getElementById("a_4").value;
	if(op.length <= 3 || /^\s+$/.test(op))
	{
		err += "Se requiere el álias del registro 4. \n";
		al2 = false;
	}
	else if(op.length > 60)
	{
		err += "El álias del registro 4 no debe superar 60 caracteres. \n";
		al2 = false;
	}
	op = document.getElementById("p_4").value;
	if(op.length > 0 && op > 0 && (isNaN(op) || /^\s+$/.test(op)))
	{
		err += document.getElementById("dat").value + " "+4+" no es válido. \n";
		al2 = false;
	}
	if(document.getElementById("a_4").value == document.getElementById("a_1").value || document.getElementById("a_4").value == document.getElementById("a_2").value || document.getElementById("a_4").value == document.getElementById("a_3").value)
	{
		err += "Los álias no pueden ser iguales. \n";
		al2 = false;
	}
}
if(al1 == false && al2 == true)
{
	err += "Se debe completar cada adición en forma secuencial. \n";
}
if(err.length > 0)
{
	alert("Verifique los siguientes errores: \n\n"+err+"\n");
	return false;
}
else
	return true;	
}
</script>
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" --><!-- InstanceEndEditable -->
</head>
<body>
<div id="datos" align="center">
 <h4>Consolidando datos...<br />
 <img src="../img/loader.gif" width="16" height="16" /></h4>
 </div>
<div align="left"><?php //echo breadcrumbs(); ?></div>
<div align="center" class="popup">
<!-- InstanceBeginEditable name="contenidos" -->
<?php
require_once("../Connections/conect.inc.php");
if(isset($_POST["guardar"], $_POST["s_1"], $_POST["a_1"]))
{
	$sesiones = $_POST["s_1"] + $_POST["s_2"];
	$publico = $_POST["p_1"] + $_POST["p_2"];
	if(isset($_POST["s_3"], $_POST["a_3"]))
	{
		$sesiones += $_POST["s_3"];
		$publico += $_POST["p_3"];
	}
	if(isset($_POST["s_4"], $_POST["a_4"]))
	{
		$sesiones += $_POST["s_4"];
		$publico += $_POST["p_4"];
	}
	$sql = "update programacion set Sesiones = ".$sesiones.", Publico = ".$publico." where Id = ".$_POST["con"];
	$excs = mysqli_query($conect, $sql);
	for($i=1; $i<=$_POST["res"]; $i++)
	{ 
		$sql = "update programacion, prog_alias set programacion.Sesiones = ".$_POST["s_".$i].", programacion.Publico = ".$_POST["p_".$i].", prog_alias.Alias = '".$_POST["a_".$i]."' where programacion.Id = ".$_POST["id_".$i]." and prog_alias.Reporte = ".$_POST["id_".$i];
		$excs = mysqli_query($conect, $sql);
	}
	if($_POST["res"] == 2 && (isset($_POST["s_3"], $_POST["a_3"]) && (strlen($_POST["s_3"]) > 0 && strlen($_POST["a_3"]) > 3)))
	{
		$sql = "insert into programacion (Id, Biblioteca, Reporte, Sub, Sesiones, Fecha, Publico) values ('', ".$_POST["bib"].", ".$_POST["rep"].", '".$_POST["con"]."', ".$_POST["s_3"].", '".$_POST["fec"]."', '".$_POST["p_3"]."')";
		$excs = mysqli_query($conect, $sql);
		$sql = "insert into prog_alias (Id, Reporte, Biblioteca, Fecha, Alias) values ('', ".mysqli_insert_id($conect).", ".$_POST["bib"].", '".$_POST["fec"]."', '".$_POST["a_3"]."')";
		$exc = mysqli_query($conect, $sql);
	}
	if($_POST["res"] == 3 && (isset($_POST["s_4"], $_POST["a_4"]) && (strlen($_POST["s_4"]) > 0 && strlen($_POST["a_4"]) > 3)))
	{
		$sql = "insert into programacion (Id, Biblioteca, Reporte, Sub, Sesiones, Fecha, Publico) values ('', ".$_POST["bib"].", ".$_POST["rep"].", '".$_POST["con"]."', ".$_POST["s_4"].", '".$_POST["fec"]."', '".$_POST["p_4"]."')";
		$excs = mysqli_query($conect, $sql);
		$sql = "insert into prog_alias (Id, Reporte, Biblioteca, Fecha, Alias) values ('', ".mysqli_insert_id($conect).", ".$_POST["bib"].", '".$_POST["fec"]."', '".$_POST["a_4"]."')";
		$exc = mysqli_query($conect, $sql);
	}
	if($excs)
	{
		echo "<h4 align='center'>Registro(s) creado(s) - actualizado(s)</h4>";
		echo "<h4 align='center'>Fecha para registrar ".date("n/Y")."</h4>";
		?>
		<script language="javascript">
		parent.consulta.formconf.<?php echo $_POST["tex"]; ?>.value = <?php echo $sesiones; ?>;
		window.parent.consulta.clon_creado('<?php echo $publico; ?>','<?php echo $_POST["rep"]; ?>','<?php echo $_POST["dat"]; ?>','1','','');
		alert("Registro(s) creado(s) - actualizado(s).");
		document.location = "../fill.php";
		</script>
		<?php
	}
	else
	{
		echo "<h4 align='center'>Error al registrar la informaci&oacute;n</h4>";
	}
    unset($sql,$exc,$sesiones,$id,$publico);
	mysqli_close($conect);
}
else
{
	$sql = "select programacion.Id, programacion.Sesiones, programacion.Fecha, programacion.Publico, prog_alias.Alias from programacion, prog_alias where programacion.Sub = ".$_GET["Con"]." and prog_alias.Reporte = programacion.Id";
	$exc = mysqli_query($conect, $sql);
	echo $sql;
	$f_r = "";
?>
<h4 align="left" style="padding:5px;">&nbsp; Consultar las instancias adicionales para el reporte "<?php echo $_GET["Nom"]; ?>"</h4>
<form name="add_clone" id="add_clone" method="post" action ="c_clonar.php?Nom=<?php echo $_GET["Nom"]; ?>" onsubmit="return validate();" >
<input name="tex" id="tex" type="hidden" value="<?php echo $_GET["Tex"]; ?>" />
<input name="bib" id="bib" type="hidden" value="<?php echo $_GET["Bib"]; ?>" />
<input name="fec" id="fec" type="hidden" value="<?php echo $_GET["Fec"]; ?>" />
<input name="bot" id="bot" type="hidden" value="<?php echo $_GET["Bot"]; ?>" />
<input name="rep" id="rep" type="hidden" value="<?php echo $_GET["Rep"]; ?>" />
<input name="pub" id="pub" type="hidden" value="<?php echo $_GET["Pub"]; ?>" />
<input name="dat" id="dat" type="hidden" value="<?php echo $_GET["Dat"]; ?>" />
<input name="con" id="con" type="hidden" value="<?php echo $_GET["Con"]; ?>" />
<input name="res" id="res" type="hidden" value="<?php echo mysqli_num_rows($exc); ?>" />
<?php
//echo mysqli_num_rows($exc);
for($i=1; $i<=mysqli_num_rows($exc); $i++)
{
	$row = mysqli_fetch_array($exc);
	if(isset($row["Fecha"]))
		$f_r = $row["Fecha"];
?>
	 <div class="list_vis">
     <input name="id_<?php echo $i; ?>" id="id_<?php echo $i; ?>" type="hidden" value="<?php echo $row["Id"]; ?>" />
        <div style="float:left; width:4%;"><strong><?php echo $i; ?>.</strong></div>
        <div style="float:left; width:12%; padding:2px;">
            <div>Sesiones:</div>
            <div style="width:94%"><input name="s_<?php echo $i; ?>" id="s_<?php echo $i; ?>" type="text" placeholder="000" value="<?php echo $row["Sesiones"]; ?>" /></div>
        </div>
        <div style="float:left; width:28%; padding:2px;">
            <div>Alias para la actividad:</div>
            <div style="width:94%"><input name="a_<?php echo $i; ?>" id="a_<?php echo $i; ?>" type="text" placeholder="Nombre para identificar la actividad" value="<?php echo $row["Alias"]; ?>" /></div>
        </div>
        <div style="float:left; padding:2px;">
            <div><?php echo $_GET["Dat"]; ?>:</div>
            <div style="width:80%"><input name="p_<?php echo $i; ?>" id="p_<?php echo $i; ?>" type="text" placeholder="<?php echo $_GET["Dat"]; ?>" value="<?php echo $row["Publico"]; ?>" /> </div>
        </div>
    </div>
    
<?php
}
if(mysqli_num_rows($exc) <= 3)
{
	for($i=(mysqli_num_rows($exc) + 1); $i<=4; $i++)
	{ ?>
		<div class="list_js">
            <div style="float:left; width:4%;"><strong><?php echo $i; ?>.</strong></div>
            <div style="float:left; width:12%; padding:2px;">
                <div>Sesiones:</div>
                <div style="width:95%"><input name="s_<?php echo $i; ?>" id="s_<?php echo $i; ?>" type="text" placeholder="000" /></div>
            </div>
            <div style="float:left; width:28%; padding:2px;">
                <div>Alias para la actividad:</div>
                <div style="width:95%"><input name="a_<?php echo $i; ?>" id="a_<?php echo $i; ?>" type="text" placeholder="Nombre para identificar la actividad" /></div>
            </div>
            <div style="float:left; padding:2px;">
                <div><?php echo $_GET["Dat"]; ?>:</div>
                <div style="width:80%"><input name="p_<?php echo $i; ?>" id="p_<?php echo $i; ?>" type="text" placeholder="<?php echo $_GET["Dat"]; ?>" /> </div>
            </div>
        </div>
<?php }
}
?>
<div style="clear:both;">&nbsp;</div>
<div align="center">
<?php
$f_r = explode("/",$f_r);
$f1 = strtotime(date("Y")."-".date("m")."-".date("d"));
$f2 = strtotime(date($f_r[1]."-".$f_r[0]."-".date('t', mktime(0, 0, 0, $f_r[0], 1, $f_r[1]))));
$f3 = $f1 - $f2;
//echo ($f3 / 86400)." Días";
if(($f3 / 86400) <= 4)
{
?>
<input name="guardar" id="guardar" type="submit" value="Guardar" />
<?php
}
else
{
	echo "<h3 align='center'>No se pueden hacer cambios, han transcurrido ".($f3 / 86400)." días del cierre del mes</h3>";
}
?>
</div>
</form>
<?php
mysqli_free_result($exc);
unset($sql,$exc,$i,$row);
unset($f_r, $f1, $f2, $f3);
mysqli_close($conect);
}
?>
<div align="justify" id="db_guardar">&nbsp; <!--Insertar mensaje de ayuda para la página -->
<div class="div_menu" id="aa_1"><a href="javascript:void(0);" onclick="menu('a_b_','1'); mostrar('bb','1');" title="Ayuda">? +</a></div>
<div class="div_menu" style="display:none;" id="bb_1"><a href="javascript:void(0);" onclick="menu('a_b_','2'); mostrar('aa','1');" title="Ayuda">? -</a></div>
<div class="div_ayuda" id="a_b_" style="display:none;">Cuando el mismo taller es desarrollado por más de un funcionario o la temática es diferente. Se debe completar cada adición en forma secuencial.</div></div>
<!-- InstanceEndEditable -->
<form name="chang_elemt" id="chang_elemt" target="contenido" method="post">
<!-- InstanceBeginEditable name="campos" -->

<!-- InstanceEndEditable -->
</form>
</div></body>

<script language="javascript">
verif();
</script>

<!-- InstanceEnd --></html>