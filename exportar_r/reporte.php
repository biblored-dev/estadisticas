<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/contenedor.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php
include("../script/scripts/session.php");
//include("../script/breadcrumbs.php");
?>
<link href="../css/css.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../script/scripts/jquery.min.js"></script>
<script type="text/javascript" src="../script/menu_ocultar.js"></script>
<script type="text/javascript">
function verif() {
document.getElementById('datos').style.display='none';
}
function foco_in(z) {
document.getElementById(z).focus();
}
</script>
<title>.: Sistema general de estad&iacute;stica :.</title>
<link rel="shortcut icon" href="../img/favicon.ico" />
<!-- InstanceBeginEditable name="doctitle" -->
<link href="../menu/css/css.css" rel="stylesheet" type="text/css" />
<script src="../script/c_color.js"></script>
<script src="../script/meses.js"></script>
<script language="javascript" src="../script/datosxml.js"></script>
<script language="javascript">
var err = "";
function borrar() {
//document.getElementById("biblo").value = "";	
}
function busqueda() {
var err, op;
err = "";
document.getElementById("rango_f").value = "";
op = document.getElementById("area_r").selectedIndex;
if(op == null || isNaN(op) || op == 0 || /^\s+$/.test(op))
	err += "Se requiere el área responsable. \n";
else
{
	document.formiden.action = "responsable_a_excel.php";
}
op = document.getElementById("fecha").value.split("-").length;
if(op < 1)
	err += "Se requiere las fechas de consulta. \n";
else
{
	op = document.getElementById("fecha").value.split("-");
	op[0] = new Date("1/"+op[0]);
	op[1] = new Date("1/"+op[1]);
	if(op[1] < op[0])
		err += "El rango de fechas no es válido. \n";
}
if(err.length > 0)
{
	alert("Verifique los siguientes errores: \n\n"+err+"\n");
	return false;
}
else
{
	op = document.getElementById("fecha").value.split("-");
	fi = op[0].split("/");
	mis = parseInt(fi[0]);
	anio = parseInt(fi[1]);
	if(op.length == 2)
	{
		
		ff = op[1].split("/");
		ret = true;
		
		do
		{
			if(document.getElementById("rango_f").value.length > 0)
				document.getElementById("rango_f").value += ",";
			document.getElementById("rango_f").value +=  "'"+mis+"/"+anio+"'";
			mis ++;
			if(mis > 12)
			{
				mis = 1;
				anio ++;
			}
			if(parseInt(ff[0]) == mis && parseInt(ff[1]) == anio)
			{
				document.getElementById("rango_f").value +=  ",'"+mis+"/"+anio+"'";
				ret = false;
			}
		}while(ret);
	}
	else
	{
		document.getElementById("rango_f").value +=  "'"+mis+"/"+anio+"'";
	}
	//alert(document.getElementById("rango_f").value);
	enviaformulario();
	//return true;
}
}

function enviaformulario() {
   //win = window.open('','myRep','toolbars=0,scrollbars=yes,width='+((screen.width) - 50)+',height='+screen.height+',left='+((screen.width - 50)/2));            
   //document.formiden.target='myRep';
   document.formiden.target="contenido";
   document.formiden.submit();
   //win.focus();
}

window.onload = function() {
document.getElementById("area_r").onchange = function(){asig_non_are(this.selectedIndex);};
}
</script>
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" --><!-- InstanceEndEditable -->
</head>
<body>
<div id="datos" align="center">
 <h4>Consolidando datos...<br />
 <img src="../img/loader.gif" width="16" height="16" /></h4>
 </div>
<div align="left"><?php //echo breadcrumbs(); ?></div>
<div align="center" class="popup">
<!-- InstanceBeginEditable name="contenidos" -->
<h4 align="left">&nbsp; Informe por responsable</h4>
<?php 
$envio = "responsable_a_excel.php";
include("../script/filtro_d.php"); ?>
<!-- InstanceEndEditable -->
<form name="chang_elemt" id="chang_elemt" target="contenido" method="post">
<!-- InstanceBeginEditable name="campos" -->

<!-- InstanceEndEditable -->
</form>
</div></body>

<script language="javascript">
verif();
</script>

<!-- InstanceEnd --></html>