<?php
include("../script/scripts/session.php");
include("../script/franja.php");
include("../script/reporte.php");
include("../script/colores_claros.php");
include("../Connections/conect.inc.php");
header("Content-Disposition: attachment; filename=\"".$_POST['nom_are_sel']."\"");
header("Content-Type: application/vnd.ms-excel");
header('Cache-Control: max-age=0');
$nodo = array(); //array con lainformación del nodo
$todo = array(array()); //array con array de la informacion por biblioteca
$subtodo = array(0=>0,1=>0,2=>0); //array con subtorales por nodo
$participantes = "";
$fran = $ids = 0;
$sum = array(0=>0,1=>0,2=>0);
$max_b = 5; //Máximo número de bibliotecas por nodo
$sql = "select group_concat(biblioteca.Id order by biblioteca.Id asc) as Id, nodo.Nombre from biblioteca, nodo where biblioteca.Nodo = nodo.Id and biblioteca.Id != 20 group by nodo.Nombre order by nodo.Nombre asc";
$exc = mysqli_query($conect, $sql);
for($i=0; $i<mysqli_num_rows($exc); $i++)
{
	$row = mysqli_fetch_array($exc);
	$nodo[$i] = array(0=>$row["Nombre"], 1=>$row["Id"]);
}
for($i=0; $i<(sizeof($nodo) * $max_b); $i++)
{
	$todo[$i] = array(0=>"", 1=>0, 2=>0, 3=>0);
}
for($i=0; $i<sizeof($nodo); $i++)
{
	$sql = "(select reporte.Formulario, group_concat(programacion.Id) as Id, sum(programacion.Sesiones) as Sesiones, programacion.Biblioteca, biblioteca.Nombre as Nombre_b from reporte, programacion, biblioteca where reporte.Area = ".$_POST["area_r"]." and reporte.Estado = 1 and programacion.Fecha in (".$_POST["rango_f"].") and programacion.Biblioteca in (".$nodo[$i][1].") and programacion.Sub <> 1 and programacion.Reporte = reporte.Id and biblioteca.Id = programacion.Biblioteca group by programacion.Biblioteca, reporte.Formulario) union (select reporte.Formulario, reporte.Id, count(otros.Id) as Sesiones, otros.Biblioteca, '' as Nombre_b from reporte, otros where reporte.Area = ".$_POST["area_r"]." and reporte.Nombre = 'Otras actividades' and otros.Area = ".$_POST["area_r"]." and otros.Biblioteca in (".$nodo[$i][1].") and otros.Mes in (".$_POST["rango_f"].") group by otros.Biblioteca)";
	//echo $sql;
	$exc = mysqli_query($conect, $sql);
	for($ii=0; $ii<mysqli_num_rows($exc); $ii++)
	{
		$row = mysqli_fetch_array($exc);
		include($row["Formulario"]);
		$todo[$row["Biblioteca"]][0] .= $participantes;
		$todo[$row["Biblioteca"]][1] += $row["Sesiones"]; //sesiones programadas
		$todo[$row["Biblioteca"]][2] += $fran; //Sesiones reportadas
		$todo[$row["Biblioteca"]][3] += $ids; //Paricipantes reportados
		$todo[$row["Biblioteca"]][4] = $row["Nombre_b"]; //Nombre de la biblioteca
	}
}
?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>.: Sistema general de estad&iacute;stica :.</title>
</head>
<body><div align="center">
<table width="650" border="1">
  <tr>
  <td align="center" valign="middle" bgcolor="#C4D1E3"><strong>Nodo / Biblioteca</strong></td>
  <?php
  //for($ii=0; $ii<$max_b; $ii++)
  { ?>
      <td align="center" bgcolor="#CCDDEE">Funcionario (Reportes)</td>
      <td align="center" bgcolor="#CCDDEE">Programados</td>
      <td align="center" bgcolor="#CCDDEE">Realizados</td>
      <td align="center" bgcolor="#CCDDEE">Asistencia</td>
  <?php } ?>
  </tr>
  <?php
  for($i=0; $i<sizeof($nodo); $i++)
  {
  ?>	
  <tr>
    <td colspan="5" align="center" valign="middle" bgcolor="#C4D1E3"><strong><?php echo ($nodo[$i][0]); ?></strong></td>
    </tr>
  	<?php
	$bibi = explode(",",$nodo[$i][1]);
	for($iq=0; $iq<sizeof($bibi); $iq++)
	{
		if(isset($bibi[$iq]))
		{ ?>
        	<tr>
            <td align="center" valign="top"><?php echo $todo[$bibi[$iq]][4]; ?></td>
            <td valign="top"><?php echo $todo[$bibi[$iq]][0]; ?></td>
            <td align="center" valign="top"><?php echo $todo[$bibi[$iq]][1]; $sum[0] += $todo[$bibi[$iq]][1]; $subtodo[0] += $todo[$bibi[$iq]][1]; ?></td>
            <td align="center" valign="top"><?php echo $todo[$bibi[$iq]][2]; $sum[1] += $todo[$bibi[$iq]][2]; $subtodo[1] += $todo[$bibi[$iq]][2]; ?></td>
            <td align="center" valign="top"><?php echo $todo[$bibi[$iq]][3]; $sum[2] += $todo[$bibi[$iq]][3]; $subtodo[2] += $todo[$bibi[$iq]][3]; ?></td>
            </tr>
		<?php
        }
		else
		{ ?>
			<tr><td colspan="5" align="center" valign="middle">N / A</td></tr>
		<?php
        }
	}
	?>
    <tr>
    <td colspan="2" align="right" bgcolor="#CCDDEE">Subtotales</td>
    <td align="center" bgcolor="#CCDDEE"><?php echo $subtodo[0]; ?></td>
    <td align="center" bgcolor="#CCDDEE"><?php echo $subtodo[1]; ?></td>
    <td align="center" bgcolor="#CCDDEE"><?php echo $subtodo[2]; ?></td>
    </tr>
    <?php
	$subtodo[0] = 0;
	$subtodo[1] = 0;
	$subtodo[2] = 0;
} 
?> 

<tr>
  <td align="right"><strong>Totales</strong></td>	
	<?php
    //for($iq=0; $iq<$max_b; $iq++)
    { ?>
        <td valign="top">&nbsp;</td>
        <td align="center" valign="middle"><?php echo $sum[0]; ?></td>
        <td align="center" valign="middle"><?php echo $sum[1]; ?></td>
        <td align="center" valign="middle"><?php echo $sum[2]; ?></td>
<?php } ?>   
</tr>
</table>
</div></body>
<?php
unset($sum, $row, $rows, $rowx, $nodo, $sql, $exc, $excs, $todo, $i, $ii, $subtodo);
?>
</html>