<?php
include("../script/scripts/session.php");
include("../script/franja.php");
include("../script/reporte.php");
include("../script/colores_claros.php");
include("../Connections/conect.inc.php");
header("Content-Disposition: attachment; filename=\"Reporte_area\"");
header("Content-Type: application/vnd.ms-excel");
header('Cache-Control: max-age=0');
$nodo = array();
$todo = array(0);
$sum = array(0=>0,1=>0,2=>0);
$franja_t = array(0);
$reporte_t = array(0);
$t_franja = array(0);
$t_actividad = array(0);
$exp = 1;
$categ = array();
if(sizeof($franja_pob) >= sizeof($reporte))
	$exp = sizeof($franja_pob);
else
	$exp = sizeof($reporte);
$sql = "select Id from categorias where Area = ".$_POST["area_r"];
$exc = mysqli_query($conect, $sql);
for($i=0; $i<mysqli_num_rows($exc); $i++)
{
	$row = mysqli_fetch_array($exc);
	$categ[$row["Id"]] = $ccolor[$i];
}
?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>.: Sistema general de estad&iacute;stica :.</title>
</head>
<body><div align="center">

<table width="100%" border="1">
  <tr>
    <td rowspan="2" align="center" valign="middle" bgcolor="#C4D1E3"><strong>Colecci&oacute;n / Nodo</strong></td>
    <td colspan="13" align="center" bgcolor="#C4D1E3"><strong>Total colecci&oacute;n</strong></td>
  </tr>
  <tr>
    <td align="center" bgcolor="#CCDDEE">General</td>
    <td align="center" bgcolor="#CCDDEE">Infantil</td>
    <td align="center" bgcolor="#CCDDEE">Sonoteca</td>
    <td align="center" bgcolor="#CCDDEE">Videoteca</td>
    <td align="center" bgcolor="#CCDDEE">Distrito gr&aacute;fico</td>
    <td align="center" bgcolor="#C4D1E3">Multimedia</td>
    <td align="center" bgcolor="#C4D1E3">Bebeteca</td>
    <td align="center" bgcolor="#C4D1E3">Hemeroteca</td>
    <td align="center" bgcolor="#C4D1E3">Ludoteca</td>
    <td align="center" bgcolor="#C4D1E3">Libro viajero</td>
    <td align="center" bgcolor="#C4D1E3">Ebook</td>
    <td align="center" bgcolor="#C4D1E3">Port&aacute;tiles</td>
    <td align="center" bgcolor="#C4D1E3">Referencia</td>
  </tr>
  	<?php
	
	$sql = "select group_concat(biblioteca.Id) as Id, nodo.Nombre from biblioteca, nodo where biblioteca.Nodo = Nodo.Id group by Nodo.Id";
	$exc = mysqli_query($conect, $sql);
	for($i=0; $i<mysqli_num_rows($exc); $i++)
	{
		$row = mysqli_fetch_array($exc);
		
	$sql = "(select Id, Nombre, Formulario, Sub from reporte where Area = ".$_POST["area_r"]." and Estado = 1 and Id > 18) union (select Id, Nombre, Formulario, Sub from reporte where Area = ".$_POST["area_r"]." and Nombre = 'Otras actividades') order by Sub desc, Nombre asc";
	//echo $sql;
	$exc = mysqli_query($conect, $sql);
	for($i=0; $i<mysqli_num_rows($exc); $i++)
	{
		$row = mysqli_fetch_array($exc);
		for($z=0; $z<sizeof($franja_pob); $z++)
		{ 
			$franja_t[$z] = $reporte_t[$z] = 0;
		}
		?>
        <tr bgcolor="<?php echo $categ[$row["Sub"]]; ?>">
		<td style="text-align:left; vertical-align:middle;"><?php echo $row["Nombre"]; ?></td>
		<?php
		for($ii=0; $ii<sizeof($nodo); $ii++)
		{
			if($row["Nombre"] == 'Otras actividades')
				$sql = "select group_concat(Id) as Id, 0 as Sesiones, 0 as Publico from programacion where Biblioteca in (".$nodo[$ii][1].") and Reporte = ".$_POST["area_r"]." and Fecha = '".$_POST["fecha"]."'";
			else
				$sql = "select group_concat(Id) as Id, sum(Sesiones) as Sesiones, sum(Publico) as Publico from programacion where Biblioteca in (".$nodo[$ii][1].") and Reporte = ".$row["Id"]." and Fecha = '".$_POST["fecha"]."' and Sub <> 1 group by Reporte order by programacion.Sub asc";
			//echo $sql;
			$excs = mysqli_query($conect, $sql);
			$rows = mysqli_fetch_array($excs);
	?>
    <td style="text-align:left; vertical-align:middle;"><?php 
	if(isset($rows["Sesiones"])) 
	{
		if($rows["Sesiones"] != 255)
		{
			if($rows["Sesiones"] > 0)
			{
				echo $rows["Sesiones"];
				$sum[0] += $rows["Sesiones"];
				$todo[$ii][0] += $rows["Sesiones"];
			}
			else
				echo "N / A";
		}
		else
			echo "Por demanda";	
	}
	else
		echo "0"; 
	?></td>
    <td style="text-align:left; vertical-align:middle;"><?php include($row["Formulario"]); ?></td>
    <td style="text-align:left; vertical-align:middle;"><?php echo $rowx["Participantes"]; ?></td>
    <td style="text-align:left; vertical-align:top;" colspan="2"><?php echo $rowx["Franjas"]; ?></td>
    <td style="text-align:left; vertical-align:top;" colspan="2"><?php echo $rowx["Reporte"]; ?></td>
    <?php } ?>
    <td style="text-align:center; vertical-align:middle;" bgcolor="#C4D1E3"><?php echo $sum[0]; ?></td>
    <td style="text-align:center; vertical-align:middle;" bgcolor="#C4D1E3"><?php echo $sum[1]; ?></td>
    <td style="text-align:center; vertical-align:middle;" bgcolor="#C4D1E3"><?php echo $sum[2]; ?></td>
    <td style="text-align:left; vertical-align:top;" bgcolor="#C4D1E3">
	<?php 
	for($z=0; $z<sizeof($franja_pob); $z++)
	{ 
		echo $franja_pob[$z][0]."<br />";
		$t_franja[$ii][$z] += $franja_pob[$z][0];
	}
	?></td>
    <td style="text-align:center; vertical-align:top;" bgcolor="#C4D1E3">
	<?php 
	for($z=0; $z<sizeof($franja_pob); $z++)
	{ 
		echo $franja_t[$z]."<br />";
		$t_franja[sizeof($nodo)][$z] += $franja_t[$z];
	}
	?></td>
    <td style="text-align:left; vertical-align:top;" bgcolor="#C4D1E3">
	<?php
	for($z=1; $z<sizeof($reporte); $z++)
	{ 
		echo $reporte[$z]."<br />";
	}
	?></td>
    <td style="text-align:center; vertical-align:top;" bgcolor="#C4D1E3">
	<?php
	for($z=1; $z<sizeof($reporte); $z++)
	{ 
		echo $reporte_t[$z]."<br />";
		$t_actividad[sizeof($nodo)][$z] += $reporte_t[$z];
	}
	?></td>
    </tr>
    <?php
	$todo[$ii][3] += $sum[0];
	$todo[$ii][4] += $sum[1];
	$todo[$ii][5] += $sum[2];
	$sum[0] = $sum[1] = $sum[2] = 0;
	} ?>
  <tr>
  <td style="text-align:right; vertical-align:bottom;"><strong>Totales</strong></td>
  <?php
  for($ii=0; $ii<sizeof($nodo); $ii++)
  { ?>
  	<td style="text-align:left; vertical-align:middle;"><?php echo $todo[$ii][0]; ?></td>
    <td style="text-align:left; vertical-align:middle;"><?php echo $todo[$ii][1]; ?></td>
    <td style="text-align:left; vertical-align:middle;"><?php echo $todo[$ii][2]; ?></td>
    <td style="text-align:left; vertical-align:top;"><?php
	for($z=0; $z<sizeof($franja_pob); $z++)
	{ 
		echo $franja_pob[$z][0]."<br />";
	}
	?></td>
    <td style="text-align:left; vertical-align:top;"><?php
	for($z=0; $z<sizeof($franja_pob); $z++)
	{ 
		echo $t_franja[$ii][$z]."<br />";
	}
	?></td>
    <td style="text-align:left; vertical-align:top;"><?php
	for($z=1; $z<sizeof($reporte); $z++)
	{ 
		echo $reporte[$z]."<br />";
	}
	?></td>
    <td style="text-align:left; vertical-align:top;"><?php
	for($z=1; $z<sizeof($reporte); $z++)
	{ 
		echo $t_actividad[$ii][$z]."<br />";
	}
	?></td>
<?php } ?>
	<td style="text-align:left; vertical-align:middle;"><?php echo $todo[$ii][3]; ?></td>
    <td style="text-align:left; vertical-align:middle;"><?php echo $todo[$ii][4]; ?></td>
    <td style="text-align:left; vertical-align:middle;"><?php echo $todo[$ii][5]; ?></td>
    <td style="text-align:left; vertical-align:top;"><?php
	for($z=0; $z<sizeof($franja_pob); $z++)
	{ 
		echo $franja_pob[$z][0]."<br />";
	}
	?></td>
    <td style="text-align:left; vertical-align:top;"><?php
	for($z=0; $z<sizeof($franja_pob); $z++)
	{ 
		echo $t_franja[sizeof($nodo)][$z]."<br />";
	}
	?></td>
    <td style="text-align:left; vertical-align:top;"><?php
	for($z=1; $z<sizeof($reporte); $z++)
	{ 
		echo $reporte[$z]."<br />";
	}
	?></td>
    <td style="text-align:left; vertical-align:top;"><?php
	for($z=1; $z<sizeof($reporte); $z++)
	{ 
		echo $t_actividad[sizeof($nodo)][$z]."<br />";
	}
	}
	?></td>
</table>
<div>&nbsp;</div>

</div></body>
<?php
unset($sum, $row, $rows, $rowx, $nodo, $sql, $exc, $excs, $excx, $franja_t, $reporte_t, $exp, $todo, $t_franja, $t_actividad, $categ, $ccolor);
?>
</html>