<?php if(!isset($_SESSION)) { session_start(); } ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="css/css.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="script/scripts/jquery.min.js"></script>
<script type="text/javascript" src="script/menu_ocultar.js"></script>
<script type="text/javascript">
function verif() {
document.getElementById('datos').style.display='none';
}
function foco_in(z) {
document.getElementById(z).focus();
}
</script>
<title>.: Sistema general de estad&iacute;stica :.</title>
<link rel="shortcut icon" href="img/favicon.ico" />
<script type="text/javascript">
var ar_color = ['#058DC7', '#50B432', '#ED561B', '#DDDF00', '#24CBE5', '#64E572', '#FF9655', '#7CB5EC', '#FFF263', '#6AF9C4', '#00008C', '#004020', '#F15C80', '#2B908F', '#DFBFFF', '#E4D354', '#B20000', '#8085E9', '#FF7F00', '#007FFF', '#FF0000', '#ACAC9D', '#FF00FF', '#7798BF'];
function graficar(w, x, y, z) //w = valor para renderTo, x = array de datos, z = título del diagrama
{
	var options = {
		chart: {
			renderTo: w,
			plotBackgroundColor: null,
			plotBorderWidth: 0,
			plotShadow: false,
			style: {
				 fontFamily: "Arial, sans-serif"
			  }
			},
		title: {
			text: z
		},
		tooltip: {
			formatter: function() {
				return '<b>' + this.point.name + '</b>: ' + this.y;
			}
		},
		legend: {
		  itemStyle: {
			 fontWeight: 'normal',
			 fontSize: '10px'
		   }
	    },
		colors: [],
		plotOptions: [],
		series: [],
	};
	ar_color = ar_color.sort(function() {return Math.random() - 0.5});
	options.colors = ar_color
	if(y == "Cirq")
	{
		options.plotOptions = {
			pie: {
				size: 160,
				allowPointSelect: true,
				cursor: 'pointer',
				dataLabels: {
					enabled: true,
					color: '#000000',
					connectorColor: '#000000',
					formatter: function() {
						return '<b>' + this.point.name + '</b>: ' + this.y;
					}
				},
				startAngle: -120,
				endAngle: 120,
				center: ['50%', '75%'],
				showInLegend: true
			}
		}
	}
	else
	{
		options.plotOptions = {
			pie: {
				size: 110,
				allowPointSelect: true,
				cursor: 'pointer',
				dataLabels: {
					enabled: true,
					color: '#000000',
					connectorColor: '#000000',
					formatter: function() {
						return '<b>' + this.point.name + '</b>: ' + this.y;
					}
				},
				showInLegend: true
			}
		}
	}
	$.getJSON("script/pie/data/data-pie-chart2.php?dats="+x+"&grafic="+y, function(json) {
	options.series = json;
	async: false;
	chart = new Highcharts.Chart(options);
	});
}
</script>
<script src="script/scripts/highcharts.js"></script>
<script src="script/scripts/exporting.js"></script>

<script type="text/javascript" src="script/datosxml.js"></script>
<script type="text/javascript" src="script/coleccion.js.php"></script>
<script language="javascript">
function datosbiblioteca(w) {
//var sql = "select count(z30_barcode) as Total, INITCAP(Z30_SUB_LIBRARY) from bib50.z30 where Z30_SUB_LIBRARY in('BOSA', 'CAMPI', 'FERIA', 'GIRAL', 'JMSDO', 'MARIC', 'PENA', 'PERDO', 'PTEAR', 'RAFUR', 'RESTR', 'SUBA', 'TIMIZ', 'TINTL', 'TUNAL', 'USAQU', 'VBARC', 'VENEC', 'VICTO') group by Z30_SUB_LIBRARY";
var sql = "select count(*) as Total, Z30_SUB_LIBRARY from bib50.z30 where Z30_SUB_LIBRARY IN ('BOSA', 'CAMPI', 'FERIA', 'GIRAL', 'JMSDO', 'MARIC', 'PENA', 'PERDO', 'PTEAR', 'RAFUR', 'RESTR', 'SUBA', 'TIMIZ', 'TINTL', 'TUNAL', 'USAQU', 'VBARC', 'VENEC', 'VICTO') and Z30_OPEN_DATE < '"+w+"'group by Z30_SUB_LIBRARY";
http.open("GET", "script/alpinxml.php?d1=0&d2=1&dat="+sql, false); //False para que no continue el flujo hasta compeltar la petición ajax
document.getElementById("n_graf").value = "Biblioteca";
document.getElementById("t_graf").value = "Coleccion registrada por biblioteca a la fecha";
document.getElementById("f_graf").value = "Revenue";
http.onreadystatechange = datosHttpResp;
return http.send(null);
}

function datoscoleccion() {
var sql = "select count(*) as Total, case z30_collection when 'GEN' then 'General' when 'INF' then 'Infantil' when 'DISGR' then 'Distrito gráfico' when 'SONOT' then 'Sonoteca' when 'VIDET' then 'Videoteca' when 'MULT' then 'Multimedia' when 'BEBET' then 'Bebeteca' when 'HEMER' then 'Hemeroteca' when 'LIBVI' then 'Libro viajero' when 'LUDOT' then 'Ludoteca' when 'PCPOR' then 'Portatil' when 'INTER' then 'Ebook' when 'REF' then 'Referencia' else 'Otros' end as Coleccion from bib50.z30 where z30_collection in('GEN', 'INF', 'DISGR', 'SONOT', 'VIDET', 'MULT', 'BEBET', 'HEMER', 'LIBVI', 'LUDOT', 'PCPOR', 'INTER', 'REF') group by z30_collection";
http.open("GET", "script/alpinxml.php?d1=0&d2=1&dat="+sql, false); //False para que no continue el flujo hasta compeltar la petición ajax
document.getElementById("n_graf").value = "Coleccion";
document.getElementById("t_graf").value = "Coleccion registrada del mes a la fecha";
document.getElementById("f_graf").value = "Pelawat";
http.onreadystatechange = datosHttpResp;
return http.send(null);
}

function datosaleph(w,x) {
//var sql = "select count(Z305_REC_KEY) as Total, case Z303.Z303_GENDER when coalesce(Z303.Z303_GENDER,' ') then Z303.Z303_GENDER else 'IN' end from BIB00.Z303, BIB50.Z305, BIB00.Z308 where Z303_REC_KEY = SUBSTR(Z305_REC_KEY,1,12) and Z308_ID = Z303_REC_KEY and substr(Z308_REC_KEY,1,2) = '01' and Z305.Z305_BOR_TYPE < 10 and (Z305.Z305_OPEN_DATE  BETWEEN '20160501' and '20160531' or Z303.Z303_OPEN_DATE  BETWEEN '"+w+"' and '"+x+"') group by Z303.Z303_GENDER order by Z303.Z303_GENDER asc";
var sql = "select count(*) as Total, case Z303.Z303_GENDER when 'F' then 'Femenino' when'M' then 'Masculino' else 'Institucional' end from BIB00.Z303, BIB50.Z305, BIB00.Z308 where Z303_REC_KEY = SUBSTR(Z305_REC_KEY,1,12) and Z308_ID = Z303_REC_KEY and substr(Z308_REC_KEY,1,2) = '01' and Z305.Z305_BOR_TYPE < 10 and (Z305.Z305_OPEN_DATE  BETWEEN '"+w+"' and '"+x+"' or Z303.Z303_OPEN_DATE  BETWEEN '"+w+"' and '"+x+"') group by Z303.Z303_GENDER order by Z303.Z303_GENDER asc";

http.open("GET", "script/alpinxml.php?d1="+x+"&d2=1&dat="+sql, false); //False para que no continue el flujo hasta compeltar la petición ajax
document.getElementById("n_graf").value = "Afiliaciones";
document.getElementById("t_graf").value = "Afiliaciones registradas del mes a la fecha";
document.getElementById("f_graf").value = "Cirq";
http.onreadystatechange = datosHttpResp;
return http.send(null);
}

function datosserver(w,x) {
//var sql = "SELECT (EXTRACT(DAY FROM DATE(historialconteo.fechalect))) AS DIA, CASE WHEN SUM(CASE WHEN movconteoid in (1) THEN cantidad ELSE 0 END) > SUM(CASE WHEN movconteoid in (2) THEN cantidad ELSE 0 END) THEN SUM(CASE WHEN movconteoid in (1) THEN cantidad ELSE 0 END) ELSE SUM(CASE WHEN movconteoid in (2) THEN cantidad ELSE 0 END) END AS TEN, CASE WHEN SUM(CASE WHEN movconteoid in (3) THEN cantidad ELSE 0 END) > SUM(CASE WHEN movconteoid in (4) THEN cantidad ELSE 0 END) THEN SUM(CASE WHEN movconteoid in (3) THEN cantidad ELSE 0 END) ELSE SUM(CASE WHEN movconteoid in (4) THEN cantidad ELSE 0 END) END AS TEA FROM historialconteo where historialconteo.fechalect BETWEEN '"+w+" 00:00:00' AND '"+x+" 23:59:59' AND date_part('hour', historialconteo.fechalect) BETWEEN 8 AND 20 GROUP BY date(historialconteo.fechalect) ORDER BY date(historialconteo.fechalect) ASC";
var sql = "SELECT SUM(CASE movconteoid WHEN 1 THEN cantidad ELSE 0 END) AS TEN, SUM(CASE movconteoid WHEN 3 THEN cantidad ELSE 0 END) AS TEA FROM historialconteo where historialconteo.fechalect BETWEEN '"+w+" 00:00:00' AND '"+x+" 23:59:59' AND date_part('hour', historialconteo.fechalect) BETWEEN 8 AND 20";
http.open("GET", "script/posinxml.php?d1="+x+"&d2=1&dat="+sql, false); //False para que no continue el flujo hasta compeltar la petición ajax
document.getElementById("n_graf").value = "Visitas";
document.getElementById("t_graf").value = "Visitas registradas del mes a la fecha";
document.getElementById("f_graf").value = "Cirq";
http.onreadystatechange = datosHttpResp;
return http.send(null);
}

function datosHttpResp() {
if(http.readyState == 4)
{
	if(http.status == 200)
	{
		var salida = "";
		var total_prest = 0;
		var timeValue = http.responseXML.getElementsByTagName("timenow")[0];
	    if(timeValue.childNodes.length > 0)
		{
			graficar(document.getElementById("n_graf").value, timeValue.childNodes[0].nodeValue, document.getElementById("f_graf").value, document.getElementById("t_graf").value);
		}
		verif();
	}
} 
else
{
	document.getElementById('datos').style.display = "";
}
}
</script>
</head>
<body>
<div id="datos" align="center">
 <h4>Consolidando datos...<br />
 <img src="img/loader.gif" width="16" height="16" /></h4>
 </div>
<div align="left"><?php //echo breadcrumbs(); ?></div>
<div align="center" class="popup">
<table width="100%" border="1" cellspacing="1" cellpadding="1">
  <tr>
    <td width="50%"><div id="Visitas" style="min-width: 100%; max-width: 100%; height: 220px; margin: 0 auto"></div></td>
    <td width="50%"><div id="Afiliaciones" style="min-width: 100%; height: 220px; margin: 0 auto"></div></td>
  </tr>
  <tr>  
    <td colspan="2"><div id="Coleccion" style="height: 310px; width:50%; margin: 0 auto;"></div></td>
    <!--<td><div id="Biblioteca" style="min-width: 100%; height: 310px; margin: 0 auto"></div></td>-->
  </tr>
</table>
<div align="justify" id="db_guardar">&nbsp; <!--Insertar mensaje de ayuda para la página -->
<div class="div_menu" id="aa_1"><a href="javascript:void(0);" onclick="menu('a_b_','1'); mostrar('bb','1');" title="Ayuda">? +</a></div>
<div class="div_menu" style="display:none;" id="bb_1"><a href="javascript:void(0);" onclick="menu('a_b_','2'); mostrar('aa','1');" title="Ayuda">? -</a></div>
<div class="div_ayuda" id="a_b_" style="display:none;"><!-- Quitar comentarios e insertar el texto de ayuda. Aparecerá flotando en el pie de la página.--></div></div>
<form name="chang_elemt" id="chang_elemt" target="contenido" method="post">
<input name="n_graf" id="n_graf" type="hidden" value="" />
<input name="t_graf" id="t_graf" type="hidden" value="" />
<input name="f_graf" id="f_graf" type="hidden" value="" />
</form>
</div></body>

<script language="javascript">
verif();
</script>
<?php
$f_act = date('Ymd');
$f_ant = strtotime ('-1 day', strtotime($f_act));
$ayer = date ('Ymd', $f_ant);
?>
<script language="javascript">
datosserver(<?php echo "'".date("Y")."/".date("m")."/01'"; ?>,<?php echo "'".date("Y")."/".date("m")."/".date("t")."'"; ?>);
datosaleph(<?php echo "'".date("Ym")."01'"; ?>,<?php echo "'".date("Ym").date("t")."'"; ?>);
datoscoleccion();
//datosbiblioteca(<?php //echo $ayer; ?>); demaciado lenta con parámetros de fecha
</script>
</html>
<?php
unset($f_act, $f_ant, $ayer);
?>