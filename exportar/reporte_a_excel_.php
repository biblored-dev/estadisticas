<?php
include("../script/scripts/session.php");
include("../script/franja.php");
include("../script/reporte.php");
include("../script/colores_claros.php");
include("../Connections/conect.inc.php");
header("Content-Disposition: attachment; filename=\"".$_POST['nom_are_sel']."\"");
header("Content-Type: application/vnd.ms-excel");
header('Cache-Control: max-age=0');
$restric = true;
$nodo = array();
$todo = array(0);
$sum = array(0=>0,1=>0,2=>array(0));
$franja_t = array(0);
$reporte_t = array(0);
$t_franja = array(0);
$t_actividad = array(0);
$exp = 1;
$categ = array();
if(sizeof($franja_pob) >= sizeof($reporte))
	$exp = sizeof($franja_pob);
else
	$exp = sizeof($reporte);
$sql = "select Id from categorias where Area = ".$_POST["area_r"];
$exc = mysqli_query($conect, $sql);
for($i=0; $i<mysqli_num_rows($exc); $i++)
{
	$row = mysqli_fetch_array($exc);
	$categ[$row["Id"]] = $ccolor[$i];
}
?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>.: Sistema general de estad&iacute;stica :.</title>
</head>
<body><div align="center">
<!--<div align="center"><strong>Consolidado por nodo y reporte - &Aacute;rea <?php //echo $_POST['nom_are_sel']; ?></strong></div>-->
<table width="100%" border="1">
  <tr>
  <td align="center" valign="middle" bgcolor="#C4D1E3"><strong>Reporte / Nodo</strong></td>
  <?php
	$sql = "select group_concat(biblioteca.Id) as Id, nodo.Nombre from biblioteca, nodo where biblioteca.Nodo = Nodo.Id and biblioteca.Id != 20 group by Nodo.Id";
	$exc = mysqli_query($conect, $sql);
	for($z=0; $z<sizeof($franja_pob); $z++)
	{
		$sum[2][$z] = 0;
	}
	for($i=0; $i<mysqli_num_rows($exc); $i++)
	{
		$row = mysqli_fetch_array($exc);
		$nodo[$i] = array(0=>utf8_decode($row["Nombre"]), 1=>$row["Id"]);
		$todo[$i] = array(0=>0, 1=>0, 2=>array(0), 3=>0, 4=>0, 5=>array(0));
		$t_franja[$i] = array(0=>0, 1=>0, 2=>0, 3=>0, 4=>0, 5=>0);
		$t_actividad[$i] = array(0=>0, 1=>0, 2=>0, 3=>0, 4=>0, 5=>0);
		for($z=0; $z<sizeof($franja_pob); $z++)
		{
			$todo[$i][2][$z] = 0;
			$todo[$i][5][$z] = 0;
		}
	?>
    <td align="center" bgcolor="#CCDDEE" nowrap>Programados</td>
    <td align="center" bgcolor="#CCDDEE" nowrap>Realizados</td>
    <td align="center" bgcolor="#CCDDEE" nowrap>Asistencia</td>
    <td align="center" bgcolor="#CCDDEE" colspan="2" nowrap>Franjas</td>
    <td align="center" bgcolor="#CCDDEE" colspan="2" nowrap>Actividad</td>
    <?php
	}
	$todo[$i] = array(0=>0, 1=>0, 2=>array(0), 3=>0, 4=>0, 5=>array(0));
	for($z=0; $z<sizeof($franja_pob); $z++)
	{
		$todo[$i][2][$z] = 0;
		$todo[$i][5][$z] = 0;
	}
	$t_franja[$i] = array(0=>0, 1=>0, 2=>0, 3=>0, 4=>0, 5=>0);
	$t_actividad[$i] = array(0=>0, 1=>0, 2=>0, 3=>0, 4=>0, 5=>0);
	?>
    <td align="center" bgcolor="#C4D1E3" nowrap>Programados</td>
    <td align="center" bgcolor="#C4D1E3" nowrap>Realizados</td>
    <td align="center" bgcolor="#C4D1E3" nowrap>Asistencia</td>
    <td align="center" bgcolor="#C4D1E3" colspan="2" nowrap>Franjas / Total</td>
    <td align="center" bgcolor="#C4D1E3" colspan="2" nowrap>Actividad / Total</td>
    
  </tr>
  <tr>
    <td align="center" bgcolor="#C4D1E3">&nbsp;</td>
  	<?php
  	for($i=0; $i<sizeof($nodo); $i++)
	{ ?>
    <td colspan="7" align="left" bgcolor="#C4D1E3"><strong><?php echo utf8_encode($nodo[$i][0]); ?></strong></td>
    <?php } ?>
    <td colspan="7" align="center" bgcolor="#C4D1E3"><strong>Total consolidado</strong></td>
  
  	<?php
	$sql = "(select Id, Nombre, Formulario, Campo, Sub from reporte where Area = ".$_POST["area_r"]." and Estado = 1) union (select Id, Nombre, Formulario, Campo, Sub from reporte where Area = ".$_POST["area_r"]." and Nombre = 'Otras actividades') order by Sub desc, Nombre asc";
	//echo $sql;
	$exc = mysqli_query($conect, $sql);
	for($i=0; $i<mysqli_num_rows($exc); $i++)
	{
		$row = mysqli_fetch_array($exc);
		for($z=0; $z<sizeof($franja_pob); $z++)
		{ 
			$franja_t[$z] = $reporte_t[$z] = 0;
		}
		?>
        <tr bgcolor="<?php echo $categ[$row["Sub"]]; ?>">
		<td style="text-align:left; vertical-align:middle;"><?php echo $row["Nombre"]."<br />(Dato a reportar:<br />".$row["Campo"].")"; ?></td>
		<?php
		for($ii=0; $ii<sizeof($nodo); $ii++)
		{
			if($row["Nombre"] == 'Otras actividades')
				$sql = "select group_concat(Id) as Id, count(*) as Sesiones, 0 as Publico from otros where Biblioteca in (".$nodo[$ii][1].") and Area = ".$_POST["area_r"]." and Mes = '".$_POST["fecha"]."'";
			else
				$sql = "select group_concat(Id) as Id, sum(Sesiones) as Sesiones, sum(Publico) as Publico from programacion where Biblioteca in (".$nodo[$ii][1].") and Reporte = ".$row["Id"]." and Fecha = '".$_POST["fecha"]."' and Sub <> 1 group by Reporte order by programacion.Sub asc";
			//echo $sql;
			$excs = mysqli_query($conect, $sql);
			$rows = mysqli_fetch_array($excs);
		?>
		<td style="text-align:left; vertical-align:middle;"><?php 
		if(isset($rows["Sesiones"]) && $rows["Sesiones"] != NULL) 
		{
			if($rows["Sesiones"] != 255)
			{
				if($rows["Sesiones"] > 0)
				{
					echo $rows["Sesiones"];
					$sum[0] += $rows["Sesiones"];
					$todo[$ii][0] += $rows["Sesiones"];
				}
				else
				{
					echo "N / A";
				}
			}
			else
			{
				echo "Por demanda";
			}
		}
		else
		{
			echo "0";
		}
	?></td>
    <td style="text-align:left; vertical-align:middle;"><?php include($row["Formulario"]); ?></td>
    <td style="text-align:left; vertical-align:middle;"><?php echo $rowx["Participantes"]; ?></td>
    <td style="text-align:left; vertical-align:top;" colspan="2"><?php echo $rowx["Franjas"]; ?></td>
    <td style="text-align:left; vertical-align:top;" colspan="2"><?php echo $rowx["Reporte"]; ?></td>
    <?php
	$rowx["Participantes"] = 0;
	}
	?>
    <td style="text-align:center; vertical-align:middle;" bgcolor="#C4D1E3"><?php echo $sum[0]; ?></td>
    <td style="text-align:center; vertical-align:middle;" bgcolor="#C4D1E3"><?php echo $sum[1]; ?></td>
    <td style="text-align:left; vertical-align:middle;" bgcolor="#C4D1E3">
	<?php 
	for($z=0; $z<sizeof($franja_pob); $z++)
	{
		echo $franja_pob[$z][0]." (".$sum[2][$z].")<br />";
	}
	//echo $sum[2]; 
	?></td>
    <td style="text-align:left; vertical-align:top;" bgcolor="#C4D1E3">
	<?php 
	for($z=0; $z<sizeof($franja_pob); $z++)
	{ 
		echo $franja_pob[$z][0]."<br />";
		$t_franja[$ii][$z] += $franja_pob[$z][0];
	}
	?></td>
    <td style="text-align:center; vertical-align:top;" bgcolor="#C4D1E3">
	<?php 
	for($z=0; $z<sizeof($franja_pob); $z++)
	{ 
		echo $franja_t[$z]."<br />";
		$t_franja[sizeof($nodo)][$z] += $franja_t[$z];
	}
	?></td>
    <td style="text-align:left; vertical-align:top;" bgcolor="#C4D1E3">
	<?php
	for($z=1; $z<sizeof($reporte); $z++)
	{ 
		echo $reporte[$z]."<br />";
	}
	?></td>
    <td style="text-align:center; vertical-align:top;" bgcolor="#C4D1E3">
	<?php
	for($z=1; $z<sizeof($reporte); $z++)
	{ 
		echo $reporte_t[$z]."<br />";
		$t_actividad[sizeof($nodo)][$z] += $reporte_t[$z];
	}
	?></td>
    </tr>
    <?php
	$todo[$ii][3] += $sum[0];
	$todo[$ii][4] += $sum[1];
	for($z=0; $z<sizeof($franja_pob); $z++)
	{
		$todo[$ii][5][$z] +=  $sum[2][$z];
		$sum[2][$z] = 0;
	}
	$sum[0] = $sum[1] = 0;
	} ?>
  <tr>
  <td style="text-align:right; vertical-align:bottom;"><strong>Totales</strong></td>
  <?php
  for($ii=0; $ii<sizeof($nodo); $ii++)
  { 
  ?>
  	<td style="text-align:left; vertical-align:middle;"><?php echo $todo[$ii][0]; ?></td>
    <td style="text-align:left; vertical-align:middle;"><?php echo $todo[$ii][1]; ?></td>
    <td style="text-align:left; vertical-align:middle;">
	<?php
	for($z=0; $z<sizeof($franja_pob); $z++)
	{ 
		echo $franja_pob[$z][0]." (".$todo[$ii][2][$z].")<br />";
	}
	echo "Total (".array_sum($todo[$ii][2]).")";
	?></td>
    <td style="text-align:left; vertical-align:top;"><?php
	for($z=0; $z<sizeof($franja_pob); $z++)
	{ 
		echo $franja_pob[$z][0]."<br />";
	}
	?></td>
    <td style="text-align:left; vertical-align:top;"><?php
	for($z=0; $z<sizeof($franja_pob); $z++)
	{ 
		echo $t_franja[$ii][$z]."<br />";
	}
	?></td>
    <td style="text-align:left; vertical-align:top;"><?php
	for($z=1; $z<sizeof($reporte); $z++)
	{ 
		echo $reporte[$z]."<br />";
	}
	?></td>
    <td style="text-align:left; vertical-align:top;"><?php
	for($z=1; $z<sizeof($reporte); $z++)
	{ 
		echo $t_actividad[$ii][$z]."<br />";
	}
	?></td>
<?php } ?>
	<td style="text-align:left; vertical-align:middle;"><?php echo $todo[$ii][3]; ?></td>
    <td style="text-align:left; vertical-align:middle;"><?php echo $todo[$ii][4]; ?></td>
    <td style="text-align:left; vertical-align:middle;">
	<?php
	for($z=0; $z<sizeof($franja_pob); $z++)
	{ 
		echo $franja_pob[$z][0]." (".$todo[$ii][5][$z].")<br />";
	}
	echo "Total (".array_sum($todo[$ii][5]).")";
	//echo $todo[$ii][5];  
	?></td>
    <td style="text-align:left; vertical-align:top;"><?php
	for($z=0; $z<sizeof($franja_pob); $z++)
	{ 
		echo $franja_pob[$z][0]."<br />";
	}
	?></td>
    <td style="text-align:left; vertical-align:top;"><?php
	for($z=0; $z<sizeof($franja_pob); $z++)
	{ 
		echo $t_franja[sizeof($nodo)][$z]."<br />";
	}
	?></td>
    <td style="text-align:left; vertical-align:top;"><?php
	for($z=1; $z<sizeof($reporte); $z++)
	{ 
		echo $reporte[$z]."<br />";
	}
	?></td>
    <td style="text-align:left; vertical-align:top;"><?php
	for($z=1; $z<sizeof($reporte); $z++)
	{ 
		echo $t_actividad[sizeof($nodo)][$z]."<br />";
	}
	?></td>
</table>
<div>&nbsp;</div>
<div align="center"><strong>Consolidado por nodo y categor&iacute;a de reportes</strong></div>
<div>&nbsp;</div>
<?php
$restric = false;
unset($todo, $sum, $franja_t,  $reporte_t, $t_franja, $t_actividad);
$todo = array(0);
$sum = array(0=>0,1=>0,2=>array(0));
$franja_t = array(0);
$reporte_t = array(0);
$t_franja = array(0);
$t_actividad = array(0);
$t_programad = array(0);
$info_categ = array(); //informacion de cada una de las categorias por nodo, permite consolidar, en especial cuando son diferentes tipos de reportes
$id_categ = array(); //Nombre de cada categoria inicio
$id_color = array(); //Nombre de cada categoria inicio

for($z=0; $z<sizeof($franja_pob); $z++)
{
	$sum[2][$z] = 0;
}
//echo "Nodo ".sizeof($nodo);
for($i=0; $i<=sizeof($nodo); $i++)
{
	$todo[$i] = array(0=>0, 1=>0, 2=>array(0), 3=>0, 4=>0, 5=>array(0));
	$t_franja[$i] = array(0=>0, 1=>0, 2=>0, 3=>0, 4=>0, 5=>0);
	$t_actividad[$i] = array(0=>0, 1=>0, 2=>0, 3=>0, 4=>0, 5=>0);
	$t_programad[$i] = array(0=>0, 1=>0, 2=>0, 3=>0, 4=>0, 5=>0);
	for($z=0; $z<sizeof($franja_pob); $z++)
	{
		$todo[$i][2][$z] = 0;
		$todo[$i][5][$z] = 0;
	}
}
$sql = "(select categorias.Id, categorias.Nombre, group_concat(reporte.Id) as Reporte, reporte.Formulario from categorias, reporte where categorias.Area = ".$_POST["area_r"]." and reporte.Area = ".$_POST["area_r"]." and reporte.Estado = 1 and reporte.Sub = categorias.Id group by categorias.Id, reporte.Formulario) union (select '0' as Id, 'Otras actividades' as Nombre, Id as Reporte, Formulario from reporte where Area = ".$_POST["area_r"]." and Nombre = 'Otras actividades') order by Id desc, Nombre asc";
//echo $sql;
$exc = mysqli_query($conect, $sql);
for($i=0; $i<mysqli_num_rows($exc); $i++)
{
	$row = mysqli_fetch_array($exc);
	if(!in_array($row["Nombre"], $id_categ))
	{
		$info_categ[$row["Nombre"]] = array();
		array_push($id_categ,$row["Nombre"]);
		array_push($id_color,$row["Id"]);
		for($ii=0; $ii<sizeof($nodo); $ii++)
		{
			$info_categ[$row["Nombre"]][$ii][0] = 0;
			$info_categ[$row["Nombre"]][$ii][1] = 0;
			$info_categ[$row["Nombre"]][$ii][2] = array();
			$info_categ[$row["Nombre"]][$ii][3] = array();
			$info_categ[$row["Nombre"]][$ii][4] = array();
			for($z=0; $z<sizeof($franja_pob); $z++)
			{ 
				$info_categ[$row["Nombre"]][$ii][2][$z] = 0;
				$info_categ[$row["Nombre"]][$ii][3][$z] = 0;
			}
			for($z=0; $z<sizeof($reporte); $z++)
			{ 
				$info_categ[$row["Nombre"]][$ii][4][$z] = 0;
			}
		}
	}
	for($z=0; $z<sizeof($franja_pob); $z++)
	{ 
		$franja_t[$z] = $reporte_t[$z] = 0;
	}
	for($ii=0; $ii<sizeof($nodo); $ii++)
	{
		if($row["Nombre"] == 'Otras actividades')
			$sql = "select group_concat(Id) as Id, count(*) as Sesiones, 0 as Publico from otros where Biblioteca in (".$nodo[$ii][1].") and Area = ".$_POST["area_r"]." and Mes = '".$_POST["fecha"]."'";
		else
			$sql = "select group_concat(Id) as Id, sum(Sesiones) as Sesiones, sum(Publico) as Publico from programacion where Biblioteca in (".$nodo[$ii][1].") and Reporte in (".$row["Reporte"].") and Fecha = '".$_POST["fecha"]."' and Sub <> 1 order by programacion.Sub asc";
		//echo $sql;
		$excs = mysqli_query($conect, $sql);
		$rows = mysqli_fetch_array($excs);
		if(isset($rows["Sesiones"]) && $rows["Sesiones"] != NULL) 
		{
			if($rows["Sesiones"] != 255)
			{
				$info_categ[$row["Nombre"]][$ii][0] += $rows["Sesiones"];
			}
			else
			{
				$info_categ[$row["Nombre"]][$ii][0] += 0;	
			}
			include("categorias/".$row["Formulario"]);
		}
	}
}
//print_r($info_categ);
?>
<table width="100%" border="1">
  <tr>
  <td rowspan="2" align="center" valign="middle" bgcolor="#C4D1E3"><strong>Categor&iacute;a / Nodo</strong></td>
  <?php
	for($i=0; $i<sizeof($nodo); $i++)
	{ ?>
    <td colspan="7" align="left" bgcolor="#C4D1E3"><strong><?php echo utf8_encode($nodo[$i][0]); ?></strong></td>
    <?php } ?>
    <td colspan="7" align="center" bgcolor="#C4D1E3"><strong>Total consolidado</strong></td>
  </tr>
  <tr>
  <?php
  for($i=0; $i<sizeof($nodo); $i++)
  { ?>
    <td align="center" bgcolor="#CCDDEE">Programados</td>
    <td align="center" bgcolor="#CCDDEE">Realizados</td>
    <td align="center" bgcolor="#CCDDEE">Asistencia</td>
    <td align="center" bgcolor="#CCDDEE" colspan="2">Franjas</td>
    <td align="center" bgcolor="#CCDDEE" colspan="2">Actividad</td>
  <?php } ?>
    <td align="center" bgcolor="#C4D1E3">Programados</td>
    <td align="center" bgcolor="#C4D1E3">Realizados</td>
    <td align="center" bgcolor="#C4D1E3">Asistencia</td>
    <td align="center" bgcolor="#C4D1E3" colspan="2">Franjas / Total</td>
    <td align="center" bgcolor="#C4D1E3" colspan="2">Actividad / Total</td>
  </tr>
  	<?php
	for($i=0; $i<sizeof($id_categ); $i++)
	{ ?>
    <tr bgcolor="<?php echo $categ[$id_color[$i]]; ?>">
	<td style="text-align:left; vertical-align:middle;"><?php echo $id_categ[$i]; ?></td>
    <?php
	for($ii=0; $ii<sizeof($nodo); $ii++)
    { ?>
	<td style="text-align:left; vertical-align:middle;"><?php
	echo $info_categ[$id_categ[$i]][$ii][0]; ?></td>
    <td style="text-align:left; vertical-align:middle;"><?php
	echo $info_categ[$id_categ[$i]][$ii][1]; ?></td>
    <td style="text-align:left; vertical-align:middle;"><?php
	for($z=0; $z<sizeof($franja_pob); $z++)
	{
		$t_franja[$ii][$z] += $info_categ[$id_categ[$i]][$ii][2][$z];
		echo $franja_pob[$z][0]." (".$info_categ[$id_categ[$i]][$ii][2][$z].")<br />";
	}
	?></td>
    <td style="text-align:left; vertical-align:top;" colspan="2"><?php 
	for($z=0; $z<sizeof($franja_pob); $z++)
	{
		$t_actividad[$ii][$z] += $info_categ[$id_categ[$i]][$ii][3][$z];
		echo $franja_pob[$z][0]." (".$info_categ[$id_categ[$i]][$ii][3][$z].")<br />";
	}
	?></td>
    <td style="text-align:left; vertical-align:top;" colspan="2"><?php
	for($z=1; $z<sizeof($reporte); $z++)
	{
		$t_programad[$ii][$z] += $info_categ[$id_categ[$i]][$ii][4][$z];
		echo $reporte[$z]." (".$info_categ[$id_categ[$i]][$ii][4][$z].")<br />";
	}
	?></td>
    <?php 

	} ?>
    <td style="text-align:center; vertical-align:middle;" bgcolor="#C4D1E3"><?php
	$val_tot = 0;
	for($ii=0; $ii<sizeof($nodo); $ii++)
  	{
		$val_tot += $info_categ[$id_categ[$i]][$ii][0];
	}
	echo $val_tot;
	?></td>
    <td style="text-align:center; vertical-align:middle;" bgcolor="#C4D1E3"><?php
	$val_tot = 0;
	for($ii=0; $ii<sizeof($nodo); $ii++)
  	{
		$val_tot += $info_categ[$id_categ[$i]][$ii][1];
	}
	echo $val_tot;
	?></td>
    <td style="text-align:left; vertical-align:middle;" bgcolor="#C4D1E3">
	<?php 
	for($z=0; $z<sizeof($franja_pob); $z++)
	{
		$val_tot = 0;
		for($ii=0; $ii<sizeof($nodo); $ii++)
  		{
			$val_tot += $info_categ[$id_categ[$i]][$ii][2][$z];
		}
		echo $franja_pob[$z][0]." (".$val_tot.")<br />";
	}
	?></td>
    <td style="text-align:left; vertical-align:top;" bgcolor="#C4D1E3">
	<?php 
	for($z=0; $z<sizeof($franja_pob); $z++)
	{
		echo $franja_pob[$z][0]."<br />";
	}
	?></td>
    <td style="text-align:center; vertical-align:top;" bgcolor="#C4D1E3">
	<?php 
	for($z=0; $z<sizeof($franja_pob); $z++)
	{
		$val_tot = 0;
		for($ii=0; $ii<sizeof($nodo); $ii++)
  		{
			$val_tot += $info_categ[$id_categ[$i]][$ii][3][$z];
		}
		echo $val_tot."<br />";
	}
	?></td>
    <td style="text-align:left; vertical-align:top;" bgcolor="#C4D1E3">
	<?php
	for($z=1; $z<sizeof($reporte); $z++)
	{
		echo $reporte[$z]."<br />";
	}
	?></td>
    <td style="text-align:center; vertical-align:top;" bgcolor="#C4D1E3">
	<?php
	for($z=1; $z<sizeof($reporte); $z++)
	{
		$val_tot = 0;
		for($ii=0; $ii<sizeof($nodo); $ii++)
  		{
			$val_tot += $info_categ[$id_categ[$i]][$ii][4][$z];
		}
		echo $val_tot."<br />";
	}
	?></td>
    </tr>
    <?php
	
	} ?>
  <tr>
  <td style="text-align:right; vertical-align:bottom;"><strong>Totales</strong></td>
  <?php
  for($ii=0; $ii<sizeof($nodo); $ii++)
  { ?>
  	<td style="text-align:center; vertical-align:middle;"><?php  
	$val_tot = 0;
	for($i=0; $i<sizeof($id_categ); $i++)
	{
		$val_tot += $info_categ[$id_categ[$i]][$ii][0];
	}
	echo $val_tot;
	?></td>
    <td style="text-align:center; vertical-align:middle;"><?php
	$val_tot = 0;
	for($i=0; $i<sizeof($id_categ); $i++)
	{
		$val_tot += $info_categ[$id_categ[$i]][$ii][1];
	}
	echo $val_tot;
	?></td>
    <td style="text-align:center; vertical-align:middle;">
	<?php
	$val_tot = 0;
	for($z=0; $z<sizeof($franja_pob); $z++)
	{
		$val_tot += $t_franja[$ii][$z];
		echo $franja_pob[$z][0]." (".$t_franja[$ii][$z].")<br />";
	}
	echo "Total ( ".$val_tot." )";
	?></td>
    <td style="text-align:center; vertical-align:top;"><?php
	$val_tot = 0;
	for($z=0; $z<sizeof($franja_pob); $z++)
	{
		$val_tot += $t_actividad[$ii][$z];
		echo $franja_pob[$z][0]."<br />";
	}
	echo "Total";
	?></td>
    <td style="text-align:left; vertical-align:top;"><?php
	for($z=0; $z<sizeof($franja_pob); $z++)
	{
		echo $t_actividad[$ii][$z]."<br />";
	}
	echo $val_tot;
	?></td>
    <td style="text-align:center; vertical-align:top;"><?php
	for($z=1; $z<sizeof($reporte); $z++)
	{
		echo $reporte[$z]."<br />";
	}
	?></td>
    <td style="text-align:center; vertical-align:top;"><?php
	for($z=1; $z<sizeof($reporte); $z++)
	{ 
		echo $t_programad[$ii][$z]."<br />";
	}
	?></td>
<?php } ?>
	<td style="text-align:center; vertical-align:middle;"><?php 
	$val_tot = 0;
	for($i=0; $i<sizeof($id_categ); $i++)
	{
		for($ii=0; $ii<sizeof($nodo); $ii++)
	  	{
			$val_tot += $info_categ[$id_categ[$i]][$ii][0];
		}
	}
	echo $val_tot;
	?></td>
    <td style="text-align:center; vertical-align:middle;"><?php 
	$val_tot = 0;
	for($i=0; $i<sizeof($id_categ); $i++)
	{
		for($ii=0; $ii<sizeof($nodo); $ii++)
	  	{
			$val_tot += $info_categ[$id_categ[$i]][$ii][1];
		}
	}
	echo $val_tot;
	?></td>
    <td style="text-align:center; vertical-align:middle;">
	<?php 
	for($z=0; $z<sizeof($franja_pob); $z++)
	{
		$val_tot = 0;
		for($i=0; $i<sizeof($id_categ); $i++)
		{
			for($ii=0; $ii<sizeof($nodo); $ii++)
			{
				$val_tot += $info_categ[$id_categ[$i]][$ii][2][$z];
			}
		}
		echo $franja_pob[$z][0]." (".$val_tot.")<br />";
	}
	$val_tot = 0;
	for($ii=0; $ii<sizeof($nodo); $ii++)
	{
		for($z=0; $z<sizeof($franja_pob); $z++)
		{	
			$val_tot += $t_franja[$ii][$z];
		}
	}
	echo "Total ( ".$val_tot." )";
	?></td>
    <td style="text-align:center; vertical-align:top;"><?php
	for($z=0; $z<sizeof($franja_pob); $z++)
	{
		echo $franja_pob[$z][0]."<br />";
	}
	?></td>
    <td style="text-align:center; vertical-align:top;"><?php
	for($z=0; $z<sizeof($franja_pob); $z++)
	{
		$val_tot = 0;
		for($i=0; $i<sizeof($id_categ); $i++)
		{
			for($ii=0; $ii<sizeof($nodo); $ii++)
			{
				$val_tot += $info_categ[$id_categ[$i]][$ii][3][$z];
			}
		}
		echo $val_tot."<br />";
	}
	?></td>
    <td style="text-align:center; vertical-align:top;"><?php
	for($z=1; $z<sizeof($reporte); $z++)
	{
		echo $reporte[$z]."<br />";
	}
	?></td>
    <td style="text-align:center; vertical-align:top;"><?php
	for($z=1; $z<sizeof($reporte); $z++)
	{
		$val_tot = 0;
		for($i=0; $i<sizeof($id_categ); $i++)
		{
			for($ii=0; $ii<sizeof($nodo); $ii++)
			{
				$val_tot += $info_categ[$id_categ[$i]][$ii][4][$z];
			}
		}
		echo $val_tot."<br />";
	}
	?></td>
</table>
</div></body>
<?php
unset($sum, $row, $rows, $rowx, $nodo, $sql, $exc, $excs, $excx, $franja_t, $reporte_t, $exp, $todo, $t_franja, $t_actividad, $categ, $ccolor, $restric, $ids, $info_categ, $id_categ, $val_tot);
?>
</html>