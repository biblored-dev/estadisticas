<?php
function sesiones($w) {
if($w == 255)
	return "S_P_D";
else
	return $w; 
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/contenedor.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php
include("../script/scripts/session.php");
//include("../script/breadcrumbs.php");
?>
<link href="../css/css.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../script/scripts/jquery.min.js"></script>
<script type="text/javascript" src="../script/menu_ocultar.js"></script>
<script type="text/javascript">
function verif() {
document.getElementById('datos').style.display='none';
}
function foco_in(z) {
document.getElementById(z).focus();
}
</script>
<title>.: Sistema general de estad&iacute;stica :.</title>
<link rel="shortcut icon" href="../img/favicon.ico" />
<!-- InstanceBeginEditable name="doctitle" -->
<script type="text/javascript" src="../script/scripts/jquery.min.js"></script>
<script type="text/javascript" src="../script/menu_ocultar.js"></script>
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" --><!-- InstanceEndEditable -->
</head>
<body>
<div id="datos" align="center">
 <h4>Consolidando datos...<br />
 <img src="../img/loader.gif" width="16" height="16" /></h4>
 </div>
<div align="left"><?php //echo breadcrumbs(); ?></div>
<div align="center" class="popup">
<!-- InstanceBeginEditable name="contenidos" -->
<div style="width:99%; margin:0px auto;">
<div style="position:fixed; z-index:5px; background-color:#FFF; width:97%; text-align:center; margin-left:8px; z-index:1; clear:both;"><div style="float:left; width:150px;"><strong>Categorias:</strong></div>
<?php
include("../Connections/conect.inc.php");	
include("../script/colores_claros.php");
$categ = array();
$sql = "select Id, Nombre from categorias where Area = ".$_POST["area_r"];
$exc = mysqli_query($conect, $sql);
for($i=0; $i<mysqli_num_rows($exc); $i++)
{
	$row = mysqli_fetch_array($exc);
	$categ[$row["Id"]] = $ccolor[$i];
	?>
    <div style="float:left; width:150px; background-color:<?php echo $ccolor[$i]; ?>"><?php echo $row["Nombre"]; ?></div>
    <?php
}
?>
</div><br /><br />
<h3 align='center' style="clear:both;"> Estadísticas disponibles </h3>
<?php
$sql = "(select reporte.Id as Reporte, reporte.Nombre, reporte.Campo, reporte.Descripcion, reporte.Formulario, reporte.Continuo, reporte.Sub as Categoria, programacion.Id, programacion.Reporte as Programacion, programacion.Sesiones, programacion.Publico, programacion.Observacion, programacion.Sub from reporte, programacion where reporte.Area = ".$_POST["area_r"]." and programacion.Biblioteca = ".$_POST["biblo"]." and programacion.Reporte = reporte.Id and programacion.Sesiones > 0 and programacion.Fecha = '".$_POST["fecha"]."') union (select reporte.Id as Reporte, reporte.Nombre, reporte.Campo, reporte.Descripcion, reporte.Formulario, reporte.Continuo, reporte.Sub as Categoria, programacion.Id, programacion.Reporte as Programacion, programacion.Sesiones, programacion.Publico, programacion.Observacion, programacion.Sub from reporte, programacion where reporte.Area = ".$_POST["area_r"]." and reporte.Nombre = 'Otras actividades' and programacion.Biblioteca = ".$_POST["biblo"]." and programacion.Reporte = ".$_POST["area_r"]." and programacion.Fecha = '".$_POST["fecha"]."') order by Categoria desc, Nombre asc";
//echo $sql;
$exc = mysqli_query($conect, $sql);
for($i=0; $i<mysqli_num_rows($exc); $i++)
{ 
	$row = mysqli_fetch_array($exc);
?>
<?php
if($row["Sub"] == 1)
{ ?>
	<div class="list_princ">
    <strong><?php echo $row["Nombre"]; ?>:</strong> &nbsp;<span class="cssToolTip"><?php echo html_entity_decode(substr($row["Descripcion"],0,120)); ?> ...
    <div><?php echo $row["Descripcion"]; ?></div></span>
    <?php
	if(strlen($row["Observacion"]) > 0)
	{ ?>
		<div class="inform" style="text-align:justify; padding-left:5px; padding-right:5px;">
        <strong>Novedades de la actividad:</strong> &nbsp;<?php echo $row["Observacion"]; ?>
        </div>
	<?php 
	}
	$excs = mysqli_query($conect, "select Alias from prog_alias where Reporte = ".$row["Id"]);
	if(mysqli_num_rows($excs) > 0)
	{
		$rows = mysqli_fetch_array($excs);
		echo "<strong>&Aacute;lias del programa:</strong>&nbsp;".$rows["Alias"];
	}
	?>
	<div><strong>Total programadas para el mes:&nbsp;</strong><?php echo sesiones($row["Sesiones"]); ?></div>
    </div>
<?php }
else if($row["Sub"] > 1)
{
?>
<div style="border-radius:5px; margin-top:5px; margin-left:15px; border:#000 1px solid; padding:5px;">
<table width="100%" border="0" cellspacing="1" cellpadding="0" bgcolor="<?php echo $categ[$row["Categoria"]]; ?>">
  <tr>
    <td width="18%"><div align="justify"><strong>Programados:&nbsp;</strong>
	<?php
	if($row["Sesiones"] >= 255)
		echo "Por demanda";
	else if($row["Sesiones"] > 0)
		echo $row["Sesiones"]; 
	else
		echo "N / A"; 
	?>
    </div></td>
    <td width="16%"><div align="justify"><strong>Realizados:&nbsp;</strong>
    <span id="realizadas_<?php echo $i; ?>">&nbsp;</span></div></td>
    <td width="16%"><div align="justify" id="cumplimiento<?php echo $i; ?>"><strong>Cumplimiento:&nbsp;</strong>
    <span id="cumplimiento_<?php echo $i; ?>">&nbsp;</span>%</div></td>
    <td width="30%"><div align="justify"><strong><?php 
    if(strlen($row["Campo"]) > 0)	
		echo $row["Campo"]." esperado:";
	else
		echo "P&uacute;blico esperado:";
    ?>    
    &nbsp;
    </strong>
	<?php 
	if($row["Publico"] == 0)
		echo "N / A";
	else
		echo $row["Publico"];
	?></div></td>
    <td width="20%"><div align="justify" id="tot_asis_<?php echo $i; ?>">&nbsp;</div></td>
  </tr>
  <tr>
    <td colspan="5"><?php include($row["Formulario"]); ?></td>
  </tr>
  <tr>
    <td colspan="5"><div align="justify">&nbsp;
    <?php
	$excs = mysqli_query($conect, "select Alias from prog_alias where Reporte = ".$row["Id"]);
	if(mysqli_num_rows($excs) > 0)
	{
		$rows = mysqli_fetch_array($excs);
		echo "<strong>&Aacute;lias del programa:</strong>&nbsp;".$rows["Alias"];
	}
	if(strlen($row["Observacion"]) > 0)
	{ ?>
		<div class="inform" style="text-align:justify; padding-left:5px; padding-right:5px;">
        <strong>Novedades de la actividad:</strong> &nbsp;<?php echo $row["Observacion"]; ?>
        </div>
	<?php } ?>
    </div></td>
  </tr>
</table>
</div>
<?php }
else
{ ?>
<div style="border-radius:5px; margin-top:10px; border:#000 1px solid; padding:5px;">
<table width="100%" border="0" cellspacing="1" cellpadding="0" bgcolor="<?php echo $categ[$row["Categoria"]]; ?>">
  <tr>
    <td colspan="5"><div align="justify">
    <strong><?php echo $row["Nombre"]; ?>:</strong> &nbsp;<span class="cssToolTip"><?php echo substr($row["Descripcion"],0,120); ?> ...
    <div><?php echo $row["Descripcion"]; ?></div></span>
    </div>
    <?php
	if(strlen($row["Observacion"]) > 0)
	{ ?>
		<div class="inform" style="text-align:justify; padding-left:5px; padding-right:5px;">
        <strong>Novedades de la actividad:</strong> &nbsp;<?php echo $row["Observacion"]; ?>
        </div>
	<?php } ?>
    </td>
    </tr>
  <tr>
    <td width="18%"><div align="justify"><strong>Programados:&nbsp;</strong>
	<?php
	if($row["Sesiones"] >= 255)
		echo "Por demanda";
	else if($row["Sesiones"] > 0)
		echo $row["Sesiones"]; 
	else
		echo "N / A"; ;
	?>
    </div></td>
    <td width="16%"><div align="justify"><strong>Realizados:&nbsp;</strong>
    <span id="realizadas_<?php echo $i; ?>">&nbsp;</span></div></td>
    <td width="16%"><div align="justify" id="cumplimiento<?php echo $i; ?>"><strong>Cumplimiento:&nbsp;</strong>
    <span id="cumplimiento_<?php echo $i; ?>">&nbsp;</span>%</div></td>
    <td width="30%"><div align="justify"><strong><?php 
    if(strlen($row["Campo"]) > 0)	
		echo $row["Campo"]." esperado:";
	else
		echo "P&uacute;blico esperado:";
    ?>    
    &nbsp;
    </strong>
	<?php 
	if($row["Publico"] == 0)
		echo "N / A";
	else
		echo $row["Publico"];
	?></div></td>
    <td width="20%"><div align="justify" id="tot_asis_<?php echo $i; ?>">&nbsp;</div></td>
  </tr>
  <tr>
    <td colspan="5"><?php include($row["Formulario"]); ?></td>
  </tr>
  <tr>
    <td colspan="5"><div align="justify">&nbsp;
    <?php
	$excs = mysqli_query($conect, "select Alias from prog_alias where Reporte = ".$row["Id"]);
	if(mysqli_num_rows($excs) > 0)
	{
		$rows = mysqli_fetch_array($excs);
		echo "<strong>&Aacute;lias del programa:</strong>&nbsp;".$rows["Alias"];
	} ?>
    </div></td>
  </tr>
</table>
</div>
<?php } ?>
<?php } ?>
</div>
<p>&nbsp;</p><p>&nbsp;</p>
<!-- InstanceEndEditable -->
<form name="chang_elemt" id="chang_elemt" target="contenido" method="post">
<!-- InstanceBeginEditable name="campos" -->

<!-- InstanceEndEditable -->
</form>
</div></body>

<script language="javascript">
verif();
</script>

<!-- InstanceEnd --></html>
<?php
@ mysqli_free_result($exc);
@ mysqli_free_result($excs);
unset($exc, $sql, $i, $row, $ii, $rows, $cumplimiento, $campo, $realizadas, $color, $sum, $categ, $ccolor);
mysqli_close($conect);
?>