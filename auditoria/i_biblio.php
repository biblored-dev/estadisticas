<?php
if($_SERVER["SCRIPT_NAME"] == "/estadistica/informe/info_nod.php")
	$sql = "select sum(dat_bib.Area) as Area, sum(dat_bib.P_Lectura) as P_Lectura, sum(dat_bib.Catalogos) as Catalogos, sum(dat_bib.Computadores_A) as Computadores_A, sum(dat_bib.Computadores_P) as Computadores_P, sum(dat_bib.Portatiles_A) as Portatiles_A, sum(dat_bib.Portatiles_P) as Portatiles_P, sum(dat_bib.Tabletas) as Tabletas, sum(dat_bib.Personal) as Personal, sum(dat_bib.Sub_Personal) as Sub_Personal, sum(dat_bib.Tiflotecnicos) as Tiflotecnicos, count(salas.Id) as Salas from dat_bib, salas where dat_bib.Biblioteca in (".$_POST["biblo"].") and salas.Biblioteca in (".$_POST["biblo"].")";
else
	$sql = "select sum(dat_bib.Area) as Area, sum(dat_bib.P_Lectura) as P_Lectura, sum(dat_bib.Catalogos) as Catalogos, sum(dat_bib.Computadores_A) as Computadores_A, sum(dat_bib.Computadores_P) as Computadores_P, sum(dat_bib.Portatiles_A) as Portatiles_A, sum(dat_bib.Portatiles_P) as Portatiles_P, sum(dat_bib.Tabletas) as Tabletas, sum(dat_bib.Personal) as Personal, sum(dat_bib.Sub_Personal) as Sub_Personal, sum(dat_bib.Tiflotecnicos) as Tiflotecnicos, count(salas.Id) as Salas from dat_bib, salas where dat_bib.Biblioteca in (".$_POST["biblo"].") and salas.Biblioteca in (".$_POST["biblo"].")";
echo $sql;
$excs = mysqli_query($conect, $sql);
?>
<script language="javascript">
document.getElementById("realizadas_<?php echo $i; ?>").innerHTML = <?php echo mysqli_num_rows($excs); ?>;
document.getElementById("cumplimiento_<?php echo $i; ?>").innerHTML = <?php echo number_format(((mysqli_num_rows($excs) * 100) / $row["Sesiones"]),2,',',''); ?>;
</script>
<?php
if(mysqli_num_rows($excs) > 1)                 
{ ?>
<div class="div_menu" id="aa_<?php echo $i; ?>"><a href="javascript:void(0);" onclick="menu('a_b_<?php echo $i; ?>','1'); mostrar('bb','<?php echo $i; ?>');">+</a></div>
<div class="div_menu" style="display:none;" id="bb_<?php echo $i; ?>"><a href="javascript:void(0);" onclick="menu('a_b_<?php echo $i; ?>','2'); mostrar('aa','<?php echo $i; ?>');">-&nbsp;</a></div>
<div align="justify" style="clear:both; <?php if(mysqli_num_rows($excs) > 1) echo "display:none;"; ?>" id="a_b_<?php echo $i; ?>">
<?php }
for($ii=0; $ii<mysqli_num_rows($excs); $ii++)
{ 
	$rows = mysqli_fetch_array($excs);
	?>
	<div align="justify" style="clear:both;">
	<div style="float:left; font-size:130%;" id="a_<?php echo $i."".$ii; ?>"><a href="javascript:void(0);" onclick="menu('flotante_<?php echo $i."".$ii; ?>','1'); mostrar('b','<?php echo $i."".$ii; ?>');">+</a></div>
    <div style="float:left; font-size:130%; display:none;" id="b_<?php echo $i."".$ii; ?>"><a href="javascript:void(0);" onclick="menu('flotante_<?php echo $i."".$ii; ?>','2'); mostrar('a','<?php echo $i."".$ii; ?>');">-</a></div>
    <div id="flotante_<?php echo $i."".$ii; ?>" style="display:none; width:96%; float:right">
	<div style="float:left; width:2%;"><?php echo $ii + 1; ?>.</div>
	<div style="float:right; width:98%;">
	<strong>
    <?php
	if($_SERVER["SCRIPT_NAME"] == "/estadistica/informe/info_nod.php")
    	echo "Informaci&oacute;n general del nodo:";
    else
		echo "Informaci&oacute;n general de la biblioteca:";
	?>	
    </strong>
    <table width="100%" border="0" cellspacing="1" cellpadding="0" style="border:#AAA 1px solid;">
	  <tr>
		<td width="34%"><div align="justify">Metros cuadrados: &nbsp;<?php echo $rows["Area"]; ?></div></td>
		<td width="33%"><div align="justify">N&uacute;mero de salas o espacios de uso: &nbsp;<?php echo $rows["Salas"]; ?></div></td>
		<td width="33%"><div align="justify">Número de Puestos de lectura: &nbsp;<?php echo $rows["P_Lectura"]; ?></div></td>
	  </tr>
      <tr>
        <td align="left"><div align="justify">N&uacute;mero de cat&aacute;logos: &nbsp;<?php echo $rows["Catalogos"]; ?></div></td>
        <td align="left"><div align="justify">Planta de personal propia: &nbsp;<?php echo $rows["Personal"]; ?></div></td>
        <td align="left"><div align="justify">Planta de personal subcontratada: &nbsp;<?php echo $rows["Sub_Personal"]; ?></div></td>
      </tr>
      <tr>
		<td><div align="justify">N&uacute;mero de computadores de uso p&uacute;blico: &nbsp;<?php echo $rows["Computadores_P"]; ?></div></td>
		<td><div align="justify">Número de port&aacute;tiles de uso p&uacute;blico: &nbsp;<?php echo $rows["Portatiles_P"]; ?></div></td>
		<td><div align="justify">N&uacute;mero  de tabletas de uso p&uacute;blico: &nbsp;<?php echo $rows["Tabletas"]; ?></div></td>
	  </tr>
      <tr>
		<td><div align="justify">N&uacute;mero de computadores de uso administrativo: &nbsp;<?php echo $rows["Computadores_A"]; ?></div></td>
		<td><div align="justify">Número de port&aacute;tiles de uso administrativo: &nbsp;<?php echo $rows["Portatiles_A"]; ?></div></td>
		<td><div align="justify">N&uacute;mero  de equipos tiflot&eacute;cnicos: &nbsp;<?php echo $rows["Tiflotecnicos"]; ?></div></td>
	  </tr>
      <!-- <tr>
        <td colspan="3"><div align="justify">Descripci&oacute;nde equipos tiflot&eacute;cnicos:</div>
        <div align="justify"><?php //echo $rows["Descrip_T"]; ?></div>
        </td>
      </tr> -->
    </table> 
    <?php
	$sql = "select Id, D_Servicio, H_Servicio, Coleccion_G, Coleccion_I, Coleccion_S, Coleccion_D, Coleccion_V, Coleccion_M, Coleccion_B, Coleccion_H, Coleccion_L, Coleccion_LV, Coleccion_E, Coleccion_P, Coleccion_R, Observacion from info_men_bib where Biblioteca in (".$_POST["biblo"].") and Fecha = '".$_POST["fecha"]."'";
	$exc = mysqli_query($conect, $sql);
	$rows = mysqli_fetch_array($exc);
	?>
    <strong>Informaci&oacute;n del mes:</strong>
    <table width="100%" border="0" cellspacing="1" cellpadding="0" style="border:#AAA 1px solid;">
	  <tr>
        <td width="33%"><div align="justify"><strong>Colecci&oacute;n general: &nbsp;</strong><?php echo $rows["Coleccion_G"]; ?></div></td>
		<td width="34%"><div align="justify"><strong>Colecci&oacute;n infantil: &nbsp;</strong><?php echo $rows["Coleccion_I"]; ?></div></td>
		<td width="33%"><div align="justify"><strong>Colecci&oacute;n sonoteca y videoteca: &nbsp;</strong><?php echo ($rows["Coleccion_S"] + $rows["Coleccion_V"]); ?></div></td>
	  </tr>
      <tr>
        <td width="33%"><div align="justify"><strong>Colecci&oacute;n distrito gr&aacute;fico: &nbsp;</strong><?php echo $rows["Coleccion_D"]; ?></div></td>
		<td width="34%"><div align="justify"><strong>Colecci&oacute;n multimedia: &nbsp;</strong><?php echo $rows["Coleccion_M"]; ?></div></td>
		<td width="33%"><div align="justify"><strong>Colecci&oacute;n bebeteca: &nbsp;</strong><?php echo ($rows["Coleccion_B"] + $rows["Coleccion_V"]); ?></div></td>
	  </tr>
      <tr>
        <td width="33%"><div align="justify"><strong>Colecci&oacute;n hemeroteca: &nbsp;</strong><?php echo $rows["Coleccion_H"]; ?></div></td>
		<td width="34%"><div align="justify"><strong>Colecci&oacute;n ludoteca: &nbsp;</strong><?php echo $rows["Coleccion_L"]; ?></div></td>
		<td width="33%"><div align="justify"><strong>Colecci&oacute;n libro viajero: &nbsp;</strong><?php echo $rows["Coleccion_LV"]; ?></div></td>
	  </tr>
      <tr>
        <td width="33%"><div align="justify"><strong>Colecci&oacute;n ebook: &nbsp;</strong><?php echo $rows["Coleccion_E"]; ?></div></td>
		<td width="34%"><div align="justify"><strong>Port&aacute;tiles para pr&eacute;stamo: &nbsp;</strong><?php echo $rows["Coleccion_P"]; ?></div></td>
		<td width="33%"><div align="justify"><strong>Colecci&oacute;n de referencia: &nbsp;</strong><?php echo $rows["Coleccion_R"]; ?></div></td>
	  </tr>
      <tr>
        <td width="33%"><div align="justify"><strong>D&iacute;as de servicio al mes: &nbsp;</strong><?php echo $rows["D_Servicio"]; ?></div></td>
		<td width="34%"><div align="justify"><strong>Horas de servicio al mes: &nbsp;</strong><?php echo $rows["H_Servicio"]; ?></div></td>
		<td width="33%">&nbsp;</td>
	  </tr>
      <tr>
		<td colspan="3">
        <div align="justify"><strong>Observaciones por afectación en la prestación del servicio: &nbsp;</strong><?php echo $rows["Observacion"]; ?></div></td>
	  </tr>
	</table>
    <?php
	include("soportes.php");
	?>
	</div>
	</div></div>
<?php
}
?>
</div>
<script language="javascript">
document.getElementById("tot_asis_<?php echo $i; ?>").innerHTML = "<strong>Total colección: &nbsp;</strong>"+<?php echo ($rows["Coleccion_G"]+$rows["Coleccion_I"]+$rows["Coleccion_S"]+$rows["Coleccion_V"]+$rows["Coleccion_H"]+$rows["Coleccion_L"]+$rows["Coleccion_LV"]+$rows["Coleccion_E"]+$rows["Coleccion_P"]+$rows["Coleccion_R"]); ?>;
</script>
