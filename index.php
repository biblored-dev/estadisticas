﻿<?php
$pag = "http://estadistica.biblored.gov.co/index_.php?date=".md5(md5("ynj"));
$pag_ = "fill.php";
if(!isset($_SESSION)) { session_start(); } ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="expires" content="0">
<title>.: Sistema general de estad&iacute;stica :.</title>
<link href="css/css.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="script/scripts/jquery.min.js"></script>
<script src="script/scripts/highcharts.js"></script>
<script src="script/scripts/exporting.js"></script>
<script src="script/pie/est_loc.j..php"></script>
<script src="SpryAssets/SpryMenuBar.js" type="text/javascript"></script>
<script language="javascript">
function index() {
document.location.reload();
}
function scroll_pos(w) {
var ifr = document.getElementById("consulta");
ifr.contentWindow.scrollBy(0,w)
}
function des_menu() {
document.getElementById("MenuBar1").style.display = "";
}
function disp(a,b){ 
  z = window.open(a, ".: SGE :.", "resizable=1,menubar=0,scrollbars=1,location=0,width="+b+",height=520");
  z.focus();
}
function cons(a,b){ 
  if(b == 1)
  {
	  document.getElementById("consulta").height = 390;
	  document.getElementById("contenido").height = 420;
  }
  if(b == 2)
  {
	  document.getElementById("consulta").height = 550;
	  document.getElementById("contenido").height = 260;
  }
  parent.consulta.location = a;
  parent.contenido.location = "fill.php";
}
function signOut() {
	document.getElementById("MenuBar1").style.display = "none";
	parent.consulta.location = "log_out.php";
	parent.contenido.location = "fill.php";
	/*var auth2 = gapi.auth2.getAuthInstance();
	auth2.disconnect();
    auth2.signOut().then(function () {
      console.log('User signed out.');
    });*/
	
}
</script>
<link rel="shortcut icon" href="img/favicon.ico" />
<link href="SpryAssets/SpryMenuBarHorizontal.css" rel="stylesheet" type="text/css" />
</head>
<body>
<?php
//if($_SERVER["REMOTE_ADDR"] == "192.168.40.41")
{ ?>
<noscript>
<h2 align="center" class="contend">Estimado usuario, esta p&aacute;gina requiere para su funcionamiento el uso de JavaScript. Si lo ha deshabilitado intencionadamente, por favor vuelva a activarlo durante el proceso de registro de informaci&oacute;n o solicite la ayuda a CST.</h2>
</noscript>
<nav class="bgcentro"><a href="javascript:void(0);" onclick="index();">
<div class="bgcentro_iz"></div><div class="bgcentro_de"></div>
</a></nav>
<div align="center" class="central">
<img src="img/titulo.png" width="400" height="34" />
<?php 
if(isset($_SESSION['MM_Usr_Pri']) && $_SESSION['MM_Usr_Pri'] <= 6)
{ 
$pag = "menu.php";
$pag_ = "fill.php";
?>
<ul id="MenuBar1" class="MenuBarHorizontal">
  <li><a class="MenuBarItemSubmenu" href="javascript:void(0);">Configuraci&oacute;n</a>
    <ul>
      <li><a href="javascript:void(0);" onclick="cons('biblioteca/nodo.php',1);">Nodo</a></li>
      <li><a href="javascript:void(0);" onclick="cons('biblioteca/biblio.php',1);">Bibliotecas</a></li>
      <?php if(isset($_SESSION['MM_Usr_Pri']) && $_SESSION['MM_Usr_Pri'] <= 4) { ?>
      <li><a href="javascript:void(0);" onclick="cons('biblioteca/areas.php',1);">&Aacute;reas Nivel Central</a></li>
      <li><a href="javascript:void(0);" onclick="cons('biblioteca/users.php',1);">Autorizar usuario</a></li>
      <li><a href="javascript:void(0);" onclick="cons('estadisticas/categoria.php',1);">Categor&iacute;as por &aacute;rea</a></li>
      <?php } ?>
      <li><a href="javascript:void(0);" onclick="cons('estadisticas/reportes.php',1);">Reportes de estad&iacute;stica</a></li>
      <li><a href="https://accounts.google.com/Logout" onclick="signOut();" target="_blank">Cerrar sesión</a></li>
      <?php if(isset($_SESSION['MM_Usr_Pri']) && $_SESSION['MM_Usr_Pri'] == 1) { ?>
      <li><a href="javascript:void(0);" onclick="cons('estadisticas/normalizar.php',1);">Normalizar</a></li>
      <?php } ?>
    </ul>
  </li>
  <li><a href="javascript:void(0);" class="MenuBarItemSubmenu">Estad&iacute;sticas</a>
    <ul>
      <li><a href="javascript:void(0);" onclick="cons('reporte/configuracion.php',2);">Configuración mensual</a></li>
      <li><a href="javascript:void(0);" onclick="cons('reporte/reporte.php',2);">Diligenciar</a></li>
    </ul>
  </li>
  <li><a class="MenuBarItemSubmenu" href="javascript:void(0);">Informes</a>
    <ul>
      <li><a href="javascript:void(0);" onclick="cons('informe/index.php',1);">Por biblioteca</a></li>
      <?php if(isset($_SESSION['MM_Usr_Pri']) && $_SESSION['MM_Usr_Pri'] <= 3) { ?>
      <li><a href="javascript:void(0);" onclick="cons('informe/nodo.php',1);">Por nodo</a></li>
      <?php } ?> 
      <li><a href="javascript:void(0);" onclick="cons('exportar/reporte.php',1);">Exportar reporte</a></li>
      <li><a href="javascript:void(0);" onclick="cons('exportar_r/reporte.php',1);">Exportar por funcionario</a></li>
      <li><a href="075/">Concesión 075</a></li>
    </ul>
  </li>
</ul> 
<?php } ?>  
</div>
<div class="contend" id="contend">
<iframe frameborder="0" name="consulta" id="consulta" height="200" width="100%" src="<?php echo $pag; ?>" scrolling="yes"></iframe>
<div align="center"><hr width="95%" /></div>
<iframe frameborder="0" name="contenido" id="contenido" height="600" width="100%" src="<?php echo $pag_; ?>"></iframe>
</div>
<?php unset($pag); ?> 
<script type="text/javascript">
//document.getElementById("MenuBar1").style.display = "";
var MenuBar1 = new Spry.Widget.MenuBar("MenuBar1", {imgDown:"SpryAssets/SpryMenuBarDownHover.gif", imgRight:"SpryAssets/SpryMenuBarRightHover.gif"});
</script>
<?php
}
/*else
{ ?>
	<h3 align="center">Este servidor es de pruebas por lo que no está permitido el acceso. Ingrese a <br />
    <a href="http://192.168.200.26/estadistica/">Servicio de estadísticas</a></h3>
<?php } */?>
</body>
</html>
<?php unset($pag, $pag_); ?>