<?php
$host = "ldap://192.168.200.50/";
$port = "389";
$db = ldap_connect($host, $port) or die("connection failed {$host}");
if(!$db) {
	echo "Error : Unable to open {$host}\n";
	exit;
}
else
{
	ldap_set_option($db, LDAP_OPT_PROTOCOL_VERSION, 3) or die ("Could not set LDAP Protocol version");
  	ldap_set_option($db, LDAP_OPT_REFERRALS, 0);
	unset($host,$port);
}
?>