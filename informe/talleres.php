<?php
if($_SERVER["SCRIPT_NAME"] == "/estadistica/informe/info_nod.php")
	$sql = "select count(estadistica.Id) as Ids, group_concat(estadistica.Fecha) as Fecha, group_concat(estadistica.Hora) as Hora, sum(estadistica.Participantes) as Participantes, estadistica.Responsable, group_concat(estadistica.Franja) as Franja, estadistica.Descripcion, biblioteca.Nombre as Biblioteca from estadistica, biblioteca where estadistica.Id_Report in (".$row["Id"].") and biblioteca.Id = estadistica.Biblioteca group by biblioteca.Nombre order by biblioteca.Nombre asc";
else
	$sql = "select Id, Fecha, Hora, Participantes, Responsable, Franja, Descripcion from estadistica where Id_Report in (".$row["Id"].")";
$excs = mysqli_query($conect, $sql);
//echo $sql;
include("enc_1.php");
for($ii=0; $ii<mysqli_num_rows($excs); $ii++)
{ 
	$rows = mysqli_fetch_array($excs);
	?>
	<div align="justify" style="clear:both;">
	<div style="float:left; font-size:130%;" id="a_<?php echo $i."".$ii; ?>"><a href="javascript:void(0);" onclick="menu('flotante_<?php echo $i."".$ii; ?>','1'); mostrar('b','<?php echo $i."".$ii; ?>');">+</a></div>
	<div style="float:left; font-size:130%; display:none;" id="b_<?php echo $i."".$ii; ?>"><a href="javascript:void(0);" onclick="menu('flotante_<?php echo $i."".$ii; ?>','2'); mostrar('a','<?php echo $i."".$ii; ?>');">-</a></div>
	<div id="flotante_<?php echo $i."".$ii; ?>" style="display:none; width:96%; float:right">
	<div style="float:left; width:2%;"><?php echo $ii + 1; ?>.</div>
	<div style="float:right; width:98%;">
	<hr />
    <table width="100%" border="0" cellspacing="1" cellpadding="0" style="border:#AAA 1px solid;">
    <?php include("enc_2.php"); ?>
	  <tr>
		<td width="33%"><div align="justify"><strong>Fecha: &nbsp;</strong><?php echo str_replace(",",", ",$rows["Fecha"]); ?></div></td>
		<td width="34%"><div align="justify"><strong>Tiempo empleado: &nbsp;</strong><?php echo str_replace(",",", ",$rows["Hora"]); ?></div></td>
		<td width="33%"><div align="justify"><strong><?php echo $campo; ?>: &nbsp;</strong><?php echo $rows["Participantes"]; ?></div></td>
	  </tr>
      <tr>
        <td colspan="2"><div align="justify"><strong>Franja poblacional: &nbsp;</strong>
        <?php include("../script/franja.php");
		$f_pob = explode(",",$rows["Franja"]);
		//limpiar el array de vacios y duplicados, reeorganizando los indices
		$f_pob = array_filter($f_pob);
		$f_pob = array_values(array_unique($f_pob));
		//print_r($f_pob);
		$d_pob = "";
		for($iii=0; $iii<sizeof($f_pob); $iii++)
		{
			$d_pob .= $franja_pob[$f_pob[$iii]][0];
			if($iii < ($iii<sizeof($f_pob) - 1))
				$d_pob .= ", ";
		}
		echo $d_pob;
		?>
        </div>
        </td>
		<td>
		<div align="justify"><strong>Responsable: </strong><?php echo $rows["Responsable"]; ?></div>
		</td>
	  </tr>
      <tr>
		<td colspan="3"><div align="justify" style="float:left;"><strong>Descripcion:</strong></div>
        <div align="justify" style="width:100%; clear:both; background-color:#FFF;"><?php echo $rows["Descripcion"]; ?></div>
        </td>
      </tr>  
	</table>
    <?php
	unset($iii, $d_pob, $f_pob);
	include("soportes.php");
	?>
	</div></div></div>
<?php
$sum += $rows["Participantes"];
}
include("enc_3.php");
if($row["Publico"] > 0 && $sum < $row["Publico"])
{ ?>
	<script language="javascript">
	document.getElementById("tot_asis_<?php echo $i; ?>").style.backgroundColor = "#FF4D4D";
	</script>
<?php 
}
?>
</div>
<script language="javascript">
document.getElementById("tot_asis_<?php echo $i; ?>").innerHTML = "<strong>Total asistentes: &nbsp;</strong>"+<?php echo $sum; ?>;
</script>