<?php
$sum = $cumplimiento = $realizadas = 0;
$campo = "";
$color = "#FFFFFF";
if($row["Continuo"] == 1 && mysqli_num_rows($excs) > 0 && $_SERVER["SCRIPT_NAME"] == "/estadistica/informe/info_bib.php")
	$realizadas = 1;
else
	$realizadas = mysqli_num_rows($excs);
if($_SERVER["SCRIPT_NAME"] == "/estadistica/informe/info_bib.php")
{
	if($row["Sesiones"] > 0)
	{
		$cumplimiento = number_format(((mysqli_num_rows($excs) * 100) / $row["Sesiones"]),2,',','')."&nbsp;%";
		if($cumplimiento < 100 && $row["Sesiones"] != 255)
		{ 
			if($cumplimiento < 60)
				$color = "#FF4D4D";
			else
				$color = "#CCCC99";
		}
		else
		{
			$cumplimiento = "N / A";
		}
	?>
		<script language="javascript">
		document.getElementById("cumplimiento<?php echo $i; ?>").style.backgroundColor = "<?php echo $color; ?>";
		</script>
	<?php	
	}
}
if(strlen($row["Campo"]) > 0)
	$campo = $row["Campo"];
else
	$campo = "Total asistentes";
?>
<script language="javascript">
document.getElementById("realizadas_<?php echo $i; ?>").innerHTML = "<?php echo $realizadas; ?>";
document.getElementById("cumplimiento_<?php echo $i; ?>").innerHTML = "<?php echo $cumplimiento; ?>";
</script>
<?php
if(mysqli_num_rows($excs) > 1)
{ ?>
<div class="div_menu" id="aa_<?php echo $i; ?>"><a href="javascript:void(0);" onclick="menu('a_b_<?php echo $i; ?>','1'); mostrar('bb','<?php echo $i; ?>');">+</a></div>
<div class="div_menu" style="display:none;" id="bb_<?php echo $i; ?>"><a href="javascript:void(0);" onclick="menu('a_b_<?php echo $i; ?>','2'); mostrar('aa','<?php echo $i; ?>');">-&nbsp;</a></div>
<div align="justify" style="clear:both; <?php if(mysqli_num_rows($excs) > 1) echo "display:none;"; ?>" id="a_b_<?php echo $i; ?>">
<?php } ?>