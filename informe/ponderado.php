<?php
$avance = 0;
if($_SERVER["SCRIPT_NAME"] == "/estadistica/informe/info_nod.php")
	$sql = "select group_concat(ponderado.Id) as Ids, sum(ponderado.Ponderado) as Avance, biblioteca.Nombre as Biblioteca from ponderado, programacion,biblioteca  where programacion.Reporte = ".$row["Programacion"]." and ponderado.Id_Report =  programacion.Id and biblioteca.Id = ponderado.Biblioteca group by biblioteca.Nombre order by biblioteca.Nombre asc";
else
	$sql = "select sum(ponderado.Ponderado) as Avance from ponderado, programacion where programacion.Reporte = ".$row["Programacion"]." and ponderado.Id_Report =  programacion.Id";
//echo $sql;
$excs = mysqli_query($conect, $sql);
if(mysqli_num_rows($excs) > 0)
{
	$rows = mysqli_fetch_array($excs);
	$avance = $rows["Avance"];
}
$sql = "select Fecha, Ponderado, Responsable, Descripcion from ponderado where Id_Report = ".$row["Id"];
$excs = mysqli_query($conect, $sql);
include("enc_1.php");
for($ii=0; $ii<mysqli_num_rows($excs); $ii++)
{ 
	$rows = mysqli_fetch_array($excs);
	$sum += $rows["Ponderado"];
	?>
	<div align="justify" style="clear:both;">
	<div style="float:left; font-size:130%;" id="a_<?php echo $i."".$ii; ?>"><a href="javascript:void(0);" onclick="menu('flotante_<?php echo $i."".$ii; ?>','1'); mostrar('b','<?php echo $i."".$ii; ?>');">+</a></div>
	<div style="float:left; font-size:130%; display:none;" id="b_<?php echo $i."".$ii; ?>"><a href="javascript:void(0);" onclick="menu('flotante_<?php echo $i."".$ii; ?>','2'); mostrar('a','<?php echo $i."".$ii; ?>');">-</a></div>
	<div id="flotante_<?php echo $i."".$ii; ?>" style="display:none; width:96%; float:right">
	<div style="float:left; width:2%;"><?php echo $ii + 1; ?>.</div>
	<div style="float:right; width:98%;">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <?php include("enc_2.php"); ?>
	  <tr>
		<td width="33%"><div align="justify"><strong>Fecha: &nbsp;</strong><?php echo str_replace(",",", ",$rows["Fecha"]); ?></div></td>
        <td width="34%"><div align="justify"><strong>Porcentaje logrado: &nbsp;</strong><?php echo $rows["Ponderado"]; ?> %</div></td>
		<td width="33%"><div align="justify"><strong>Responsable: &nbsp;</strong><?php echo $rows["Responsable"]; ?></div></td>
	  </tr>
	  <tr>
		<td><div align="justify"><strong>Porcentaje acumulado: &nbsp;</strong><?php echo $avance; ?> %</div></td>
        <td colspan="2"><div align="justify" style="float:left;"><strong>Descripcion:</strong></div>
		<div align="justify" style="width:100%; clear:both;"><?php echo $rows["Descripcion"]; ?></div>
		</td>
	  </tr>
	</table>
    <?php
	include("soportes.php");
	?>
	</div>
	</div></div>
<?php
}
include("enc_3.php");
if($row["Publico"] > 0 && $sum < $row["Publico"])
{ ?>
	<script language="javascript">
	document.getElementById("tot_asis_<?php echo $i; ?>").style.backgroundColor = "#FF4D4D";
	</script>
<?php 
}
?>
</div>
<script language="javascript">
document.getElementById("tot_asis_<?php echo $i; ?>").innerHTML = "<strong>Porcentaje pendiente: &nbsp;</strong>"+<?php echo number_format((100 - $avance),2,',',''); ?>+" '%'";
</script>
<?php unset($avance); ?>