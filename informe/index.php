<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/contenedor.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php
include("../script/scripts/session.php");
//include("../script/breadcrumbs.php");
?>
<link href="../css/css.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../script/scripts/jquery.min.js"></script>
<script type="text/javascript" src="../script/menu_ocultar.js"></script>
<script type="text/javascript">
function verif() {
document.getElementById('datos').style.display='none';
}
function foco_in(z) {
document.getElementById(z).focus();
}
</script>
<title>.: Sistema general de estad&iacute;stica :.</title>
<link rel="shortcut icon" href="../img/favicon.ico" />
<!-- InstanceBeginEditable name="doctitle" -->
<link href="../menu/css/css.css" rel="stylesheet" type="text/css" />
<script src="../script/c_color.js"></script>
<script src="../script/mes.js"></script>
<script language="javascript" src="../script/datosxml.js"></script>
<script language="javascript">
var err = "";
function borrar() {
//document.getElementById('estadisticas').innerHTML = "";	
}
function predataserver(w) {
  borrar();
  var sql = "select Id, Nombre from Biblioteca where Nodo = "+w;
  http.open("GET", "../script/datosxml_l.php?d1=Id&d2=Nombre&dat="+sql, true); //False para que no continue el flujo hasta compeltar la petición ajax
  http.onreadystatechange = useHttpResp;
  return http.send(null);
}

function useHttpResp() {
if(http.readyState == 4)
{
	if(http.status == 200)
	{
		var salida = "";
	    var timeValue = http.responseXML.getElementsByTagName("timenow")[0];
	    var regs = timeValue.childNodes[0].nodeValue;
		regs = regs.split(";");
	    var listado = document.getElementById('biblo');
	    listado.length = regs.length;
	    listado.options[0].value = "";
	    listado.options[0].text = " Seleccione la biblioteca ";
	    listado.options.selectedIndex = 0;
		for(i = 0; i < (regs.length) - 1; i ++)
	    {
		   salida = regs[i].split(",,");
		   listado.options[i + 1].value = salida[0];
		   listado.options[i + 1].text = salida[1];
	    }
		verif();
	}
} 
else
{
	document.getElementById('datos').style.display = "";
}
}

function busqueda() {
var err, op;
err = "";
op = document.getElementById("area_r").selectedIndex;
if(op == null || isNaN(op) || op == 0 || /^\s+$/.test(op))
	err += "Se requiere el área responsable. \n";
op = document.getElementById("nodo").selectedIndex;
if(op == null || isNaN(op) || op == 0 || /^\s+$/.test(op))
	err += "Se requiere el nombre del nodo. \n";
op = document.getElementById("biblo").selectedIndex;
if(op == null || isNaN(op) || op == 0 || /^\s+$/.test(op))
	err += "Se requiere el nombre de la biblioteca. \n";
op = document.getElementById("fecha").value;
if(op == null || op.length < 5 || /^\s+$/.test(op))
	err += "Se requiere la fecha de consulta. \n";
if(err.length > 0)
{
	alert("Verifique los siguientes errores: \n\n"+err+"\n");
	return false;
}
else
{
	enviaformulario();
	//return true;
}
}

window.onload = function() {
document.getElementById("area_r").onchange = function(){borrar(); asig_non_are(this.selectedIndex);};
}

function enviaformulario() {
   win = window.open('','myRep','toolbars=0,scrollbars=yes,width=960,height='+screen.height+',left='+((screen.width - 980)/2));            
   document.formiden.target='myRep';
   document.formiden.submit();
   win.focus();
}
</script>
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" --><!-- InstanceEndEditable -->
</head>
<body>
<div id="datos" align="center">
 <h4>Consolidando datos...<br />
 <img src="../img/loader.gif" width="16" height="16" /></h4>
 </div>
<div align="left"><?php //echo breadcrumbs(); ?></div>
<div align="center" class="popup">
<!-- InstanceBeginEditable name="contenidos" -->
<h4 align="left">&nbsp; Informe mensual por biblioteca</h4>
<?php 
$envio = "info_bib.php";
include("../script/filtro_a_b.php"); ?>
<!-- InstanceEndEditable -->
<form name="chang_elemt" id="chang_elemt" target="contenido" method="post">
<!-- InstanceBeginEditable name="campos" -->

<!-- InstanceEndEditable -->
</form>
</div></body>

<script language="javascript">
verif();
</script>

<!-- InstanceEnd --></html>