<?php
$dir_ip = array(0=>"201.244.104.26",1=>"190.85.181.194",2=>"190.85.179.18",3=>"190.85.178.130",4=>"190.85.181.178",5=>"186.154.200.75",6=>"186.154.201.66",7=>"186.154.200.122",8=>"186.154.204.2",9=>"186.155.242.106",10=>"186.154.199.90",11=>"186.154.197.194",12=>"186.154.199.50",13=>"186.154.193.242",14=>"186.154.193.250",15=>"186.154.200.26",16=>"186.154.204.10",17=>"186.154.198.90",18=>"186.154.193.118",19=>"186.154.200.60",20=>"186.154.197.162",21=>"186.154.193.218",22=>"186.154.201.82",23=>"186.154.199.142");
if(in_array($_SERVER["REMOTE_ADDR"], array_values($dir_ip),true) && (isset($_GET["date"]) && $_GET["date"] == md5(md5("ynj"))))
	$pag = "http://192.168.200.26/estadistica/aut_v2.php";
else
	header("Location: ". "http://www.biblored.gov.co");	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="google-signin-scope" content="profile email">
<meta name="google-signin-client_id" content="292751117875-7hc3ra4u3lha2fkmv44fpns2v2ko65mm.apps.googleusercontent.com">
<link href="css/css.css" rel="stylesheet" type="text/css" />
<title>.: Sistema general de estad&iacute;stica :.</title>
<script src="https://apis.google.com/js/platform.js" async defer></script>
<script language="javascript">
function verif() {
document.getElementById('datos').style.display='none';
}
</script>
</head>
<body>
<div id="datos" align="center">
<h4>Esperando datos de usuario ...<br />
<img src="img/loader.gif" width="16" height="16" /></h4>
</div>
<div style="clear:both; margin-top:10px;" align="center" id="users"></div>
<div style="clear:both;" align="center" id="sesion">
<p>&nbsp;</p>
<form name="logg" id="logg" action="<?php echo $pag; ?>" method="post" enctype="application/x-www-form-urlencoded">
<input name="token" id="token" type="hidden" value="" />
<input name="corr" id="corr" type="hidden" value="" />
<input name="nomm" id="nomm" type="hidden" value="" />
</form>
<p>&nbsp;</p>
<h3 align="center">Utilice sus datos de correo electrónico para iniciar sesi&oacute;n en la herramienta</h3>
    <div class="g-signin2" data-onsuccess="onSignIn" data-theme="dark" data-title="BibloRed" data-width="200" data-longtitle="true"></div>
    <p>&nbsp;</p>
    <div align="center">Haga clic en el bot&oacute;n de Google para acceder al servicio de autenticaci&oacute;n con su cuenta de correo institucional &uacute;nicamente</div>
    <script language="javascript">
      var profile;
	  function onSignIn(googleUser) {
        profile = googleUser.getBasicProfile(); 
		var dominio = profile.getEmail();
		dominio = dominio.split("@");
		if(dominio[1] != "biblored.gov.co")
		{
			alert("La cuenta de correo no es válida. @"+dominio[1]+"\n Cierre la sesión y vuelva a ingresar,\n con una cuenta @biblored.gov.co");
			document.getElementById("log_out").style.display = "";
		}
		else
		{
			var id_token = googleUser.getAuthResponse().id_token;
        	document.getElementById("token").value = id_token;
			document.getElementById("nomm").value = profile.getName();
			document.getElementById("corr").value = profile.getEmail();
			document.getElementById("logg").submit();
		}
		document.getElementById("sesion").style.display = "none";
      };
    </script>
</div>
<div id="log_out" style="display:none; clear:both;" align="center">
<p>&nbsp;</p>
<h3>Cierre la sesión y vuelva a empezar.</h3>
<h3>
<a href="https://accounts.google.com/Logout" onclick="signOut();" target="_blank">Cerrar sesión</a>
</h3>
<script language="javascript">
  function signOut() {
	var auth2 = gapi.auth2.getAuthInstance();
	auth2.disconnect();
    auth2.signOut().then(function () {
      //console.log('User signed out.');
    });
	document.location = "index.php?date="+"<?php echo md5(md5("ynj")); ?>";
  }
</script>
</div>
</body>
</html>