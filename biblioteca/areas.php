<?php
require_once("../Connections/conect.inc.php");
$sql = "select * from areas";
$exc = mysqli_query($conect, $sql);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/contenedor.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php
include("../script/scripts/session.php");
//include("../script/breadcrumbs.php");
?>
<link href="../css/css.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../script/scripts/jquery.min.js"></script>
<script type="text/javascript" src="../script/menu_ocultar.js"></script>
<script type="text/javascript">
function verif() {
document.getElementById('datos').style.display='none';
}
function foco_in(z) {
document.getElementById(z).focus();
}
</script>
<title>.: Sistema general de estad&iacute;stica :.</title>
<link rel="shortcut icon" href="../img/favicon.ico" />
<!-- InstanceBeginEditable name="doctitle" -->
<link href="../menu/css/css.css" rel="stylesheet" type="text/css" />
<script src="../script/c_color.js"></script>
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" --><!-- InstanceEndEditable -->
</head>
<body>
<div id="datos" align="center">
 <h4>Consolidando datos...<br />
 <img src="../img/loader.gif" width="16" height="16" /></h4>
 </div>
<div align="left"><?php //echo breadcrumbs(); ?></div>
<div align="center" class="popup">
<!-- InstanceBeginEditable name="contenidos" -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="10%">&nbsp;</td>
    <td width="80%" align="center">&nbsp;</td>
    <td width="10%">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td align="center">
    <?php if($_SESSION['MM_Usr_Pri'] == 1) { ?>
    <div class="x_fieldset"><a href="javascript:void(0);" onclick="parent.contenido.location = 'n_area.php';" title="Nueva Área">Nueva &Aacute;rea</a></div>
    <?php } ?>
    <div align="center" style="clear:both;">
    <fieldset>
    <legend align="center"><strong>Listado de  nodo del sistema</strong></legend>
    <table width="100%" border="1" cellspacing="1" cellpadding="1" id="area_1">
    <tr>
    <td width="5%"><div align="center"><strong>N&deg;</strong></div></td>
    <td><div align="center"><strong>Nombre del &Aacute;rea</strong></div></td>
    <td width="20%"><div align="center"><strong>Opciones</strong></div></td>
    </tr>
	<?php
    for($i=0; $i<mysqli_num_rows($exc); $i++)
	{ 
		$row = mysqli_fetch_array($exc);
	?>	
      <tr onclick="n_color('<?php echo ($i+1); ?>','area_1');">
        <td align="center"><?php echo ($i+1); ?></td>
        <td align="left"><?php echo $row["Nombre"]; ?></td>
        <td><div id="left_men">
        <ul>
            <li><a href="javascript:void(0);">Opciones</a>
            <ul>
            <?php if($_SESSION['MM_Usr_Pri'] == 1) { ?>
			<li><a href="javascript:void(0);" onclick="modificar('<?php echo "m_area.php?bib=".$row['Id']."&nom=".$row["Nombre"]; ?>');">Modificar</a></li>
			<li><a href="javascript:void(0);" onclick="modificar('<?php echo "e_area.php?bib=".$row['Id']."&nom=".$row["Nombre"]; ?>');">Eliminar</a></li>
            <?php } ?>
            <li><a href="<?php echo "../estadisticas/reportes.php?area_r=".$row["Id"]."&id=".$row["Nombre"]; ?>">Reportes</a></li>
            <?php if($row["Id"] == 3) { ?>
            <li><a href="javascript:void(0);" onclick="modificar('<?php echo "coleccion.php?bib=".$row['Id']."&nom=".$row["Nombre"]; ?>');">Colecci&oacute;n</a></li>
            <?php } ?>
            </ul></li>
        </ul>    
        </div></td>
      </tr>
	<?php } ?>
    </table>
    </fieldset>
    </div>
    </td>
    <td>&nbsp;</td>
  </tr>
</table>
<!-- InstanceEndEditable -->
<form name="chang_elemt" id="chang_elemt" target="contenido" method="post">
<!-- InstanceBeginEditable name="campos" -->

<!-- InstanceEndEditable -->
</form>
</div></body>

<script language="javascript">
verif();
</script>

<!-- InstanceEnd --></html>
<?php
@ mysqli_free_result($exc);
unset($exc, $sql, $i, $row);
mysqli_close($conect);
?>