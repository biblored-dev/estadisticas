<?php include "../script/breadcrumbs.php"; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/contenedor.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php
include("../script/scripts/session.php");
//include("../script/breadcrumbs.php");
?>
<link href="../css/css.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../script/scripts/jquery.min.js"></script>
<script type="text/javascript" src="../script/menu_ocultar.js"></script>
<script type="text/javascript">
function verif() {
document.getElementById('datos').style.display='none';
}
function foco_in(z) {
document.getElementById(z).focus();
}
</script>
<title>.: Sistema general de estad&iacute;stica :.</title>
<link rel="shortcut icon" href="../img/favicon.ico" />
<!-- InstanceBeginEditable name="doctitle" -->
<script language="javascript" src="../script/datosxml.js"></script>
<script language="javascript">
var err = "";
function predataserver(w,x,y) {
  http.open("GET", "../script/datosxml.php?tab="+w+"&camp="+x+"&dat="+y, false); //False para que no continue el flujo hasta compeltar la petición ajax
  http.onreadystatechange = useHttpResp;
  return http.send(null);
}

function useHttpResp() {
if(http.readyState == 4)
{
	if(http.status == 200)
	{
		var timeValue = http.responseXML.getElementsByTagName("timenow")[0];
		if(parseInt(timeValue.childNodes[0].nodeValue) > 0)
		{
			err += "El nombre de la biblioteca ya existe. \n";
		}
		verif();
	}
} 
else
{
	document.getElementById('datos').style.display = "";
}
}

function busqueda() {
var val;
err = "";
document.getElementById("horario").value = "";
val = document.getElementById("nombre").value;
if(val == null || !isNaN(val) || val.length < 3 || /^\s+$/.test(val))
	err += "Se requiere el nombre de la biblioteca. \n";
else	
	predataserver('biblioteca','Nombre',document.getElementById("nombre").value) //tabla, campo, dato
val = document.getElementById("abrev").value;
if(val == null || val.length < 3 || /^\s+$/.test(val))
	err += "Se requiere abreviatura de la biblioteca. \n";
val = document.getElementById("cod_cont").value;
if(val == null || val.length == 0 || /^\s+$/.test(val))
	document.getElementById("cod_cont").value = 0;
if((document.getElementById("ini_lu").selectedIndex == 0 && document.getElementById("cie_lu").selectedIndex > 0) || (document.getElementById("ini_lu").selectedIndex > 0 && document.getElementById("cie_lu").selectedIndex == 0))
	err += "El horario del lunes no es válido. \n";
if((document.getElementById("ini_ma").selectedIndex == 0 && document.getElementById("cie_ma").selectedIndex > 0) || (document.getElementById("ini_ma").selectedIndex > 0 && document.getElementById("cie_ma").selectedIndex == 0))
	err += "El horario de martes a sábado no es válido. \n";
if((document.getElementById("ini_do").selectedIndex == 0 && document.getElementById("cie_do").selectedIndex > 0) || (document.getElementById("ini_do").selectedIndex > 0 && document.getElementById("cie_do").selectedIndex == 0))
	err += "El horario de domingo no es válido. \n";
if(err.length > 0)
{
	alert("Verifique los siguientes errores: \n\n"+err+"\n");
	return false;
}
else
{
	val = document.getElementById("ini_lu")[document.getElementById("ini_lu").selectedIndex].value + "-" + document.getElementById("cie_lu")[document.getElementById("cie_lu").selectedIndex].value;
	val += "," + document.getElementById("ini_ma")[document.getElementById("ini_ma").selectedIndex].value + "-" + document.getElementById("cie_ma")[document.getElementById("cie_ma").selectedIndex].value;
	val += "," + document.getElementById("ini_do")[document.getElementById("ini_do").selectedIndex].value + "-" + document.getElementById("cie_do")[document.getElementById("cie_do").selectedIndex].value;
	document.getElementById("horario").value = val;
	return true;
}
}
</script>
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" --><!-- InstanceEndEditable -->
</head>
<body>
<div id="datos" align="center">
 <h4>Consolidando datos...<br />
 <img src="../img/loader.gif" width="16" height="16" /></h4>
 </div>
<div align="left"><?php //echo breadcrumbs(); ?></div>
<div align="center" class="popup">
<!-- InstanceBeginEditable name="contenidos" -->
<?php
if(isset($_POST["nombre"], $_POST["guardar"], $_POST["id_n"]))
{
	require_once("../Connections/conect.inc.php");
	/*include("../script/cant.php");
	if(cant("biblioteca","Nombre",$_POST["nombre"]) == 0)*/ //tabla, campo, dato
	{
		$sql = "insert into biblioteca (Id, Nodo, Nombre, Abrev, Cod_Conteo, Horario) VALUES ('', '".$_POST["id_n"]."', '".$_POST["nombre"]."', '".strtoupper($_POST["abrev"])."', '".$_POST["cod_cont"]."', '".$_POST["horario"]."')";
		$exc = mysqli_query($conect, $sql);
		if($exc)
		{
			echo "<br /><h4 align='center'>Registro creado</h4>";
			?><script language="javascript">parent.consulta.location.reload();</script><?php
		}
		else
		{
			echo "<br /><h4 align='center'>Error al crear el registro</h4>";
		}
	}
	/*else
	{
		echo "<h4 align='center'>El nombre de la biblioteca ya existe</h4>";
	}*/
	//mysqli_free_result($exc);
	unset($sql,$exc,$row);
	mysqli_close($conect);
}
else
{
if(!isset($_SESSION['MM_Biblio_Autentic']))
{ 
include("../script/loggin.php");
?>
<center><strong>No posee privilegios para este m&oacute;dulo.<br /><br />
<a href="javascript:form();">Inicie sesi&oacute;n.</a></strong><br /><br /></center>
<?php
}
else
{
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="15%" valign="top"><div align="right"><img src="../icon/bib.png" width="39" height="35" alt="icono" /></div></td>
    <td width="70%" align="center">
    <fieldset>
    <legend align="center"><strong>Formulario para ingresar biblioteca al sistema</strong></legend>
    <div class="x_fieldset"><a href="javascript:void(0);" onclick="document.location = '../fill.php';" title="Cerrar">X</a></div>
    <form name ="formiden" method ="POST" action ="n_biblio.php" onsubmit="return busqueda();">
    <input name="horario" id="horario" type="hidden" value="" />
    <input name="id_n" id="id_n" type="hidden" value="<?php echo $_GET["id"]; ?>" />
    <div align="left">
    <div align="left" style="float:left; width:400px; height:25px;">
       <strong><a href="#" onclick="menu('flotante_1','1');">Horario de la biblioteca</a></strong>
       <div id="flotante_1" class="flotante_1" style="display:none;" align="center">
           <div align="right" style="padding-right:5px;"><a href="#" onclick="menu('flotante_1','2');"> X </a></div>
           <div style="padding:8px;"><strong>Horario de atención de la biblioteca</strong></div>
           <div>
           <table width="100%" border="0" cellspacing="2" cellpadding="2" style="background-color:#EEE;">
              <tr>
                <td width="30%"><div align="center">Día</div></td>
                <td width="35%"><div align="center">Inicio</div></td>
                <td width="35%"><div align="center">Cierre</div></td>
              </tr>
              <tr>
                <td>Lunes</td>
                <td>
                <select name="ini_lu" id="ini_lu">
                <?php include("../script/inicio.php"); ?>
                </select>
                </td>
                <td><select name="cie_lu" id="cie_lu">
                <?php include("../script/cierre.php"); ?>
                </select></td>
              </tr>
              <tr>
                <td>Martes - Sábado</td>
                <td><select name="ini_ma" id="ini_ma">
                <?php include("../script/inicio.php"); ?>
                </select></td>
                <td><select name="cie_ma" id="cie_ma">
                <?php include("../script/cierre.php"); ?>
                </select></td>
              </tr>
              <tr>
                <td>Domingo</td>
                <td><select name="ini_do" id="ini_do">
                <?php include("../script/inicio.php"); ?>
                </select></td>
                <td><select name="cie_do" id="cie_do">
                <?php include("../script/cierre.php"); ?>
                </select></td>
              </tr>
            </table>
           </div>
       </div>
    </div>  
    </div>   
    <div align="left" style="clear:both;"><strong>Nombre del nodo:</strong></div>
    <div align="left"><input name="no_n" id="no_n" type="text" value="<?php echo $_GET["nod"]; ?>" size="50" readonly="readonly" /></div>
    <div align="left"><strong>Nombre de la biblioteca:</strong></div>
    <div align="left"><input name="nombre" id="nombre" type="text" size="50" placeholder="Nombre de la biblioteca" /></div>
    <div align="left"><strong>Abreviatura de la biblioteca:</strong></div>
    <div align="left"><input name="abrev" id="abrev" type="text" size="50" placeholder="XXXXX" /></div>
    <div align="left" class="infos">Caracteres con los cuales se identifica la biblioteca en los sistemas bibliogr&aacute;fico y conteo.</div>
    <div align="left"><strong>C&oacute;digo de conteo principal:</strong></div>
    <div align="left"><input name="cod_cont" id="cod_cont" type="text" size="50" placeholder="0000,0000" /></div>
    <div align="left" class="infos">Si la biblioteca cuenta con doble portal de conteo (entrada - salida), indique los dos c&oacute;digos separados por coma, sin espacios.</div>
    <div align="center"><input name="guardar" id="guardar" type="submit" value="Guardar" /></div>
    </form>
    <script>
		foco_in('nombre');
	</script>
    </fieldset>
    </td>
    <td width="15%">&nbsp;</td>
  </tr>
</table>
<?php } } ?>
<!-- InstanceEndEditable -->
<form name="chang_elemt" id="chang_elemt" target="contenido" method="post">
<!-- InstanceBeginEditable name="campos" -->

<!-- InstanceEndEditable -->
</form>
</div></body>

<script language="javascript">
verif();
</script>

<!-- InstanceEnd --></html>