<?php
$v_consul = array(0=>"No aplica", 1=>"Con material");
require_once("../Connections/conect.inc.php");
$sql = "select Id, Sub, Nombre,	Consulta, Descripcion from salas where Biblioteca = ".$_GET["bib"]." order by salas.Nombre asc";
$exc = mysqli_query($conect, $sql);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/contenedor.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php
include("../script/scripts/session.php");
//include("../script/breadcrumbs.php");
?>
<link href="../css/css.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../script/scripts/jquery.min.js"></script>
<script type="text/javascript" src="../script/menu_ocultar.js"></script>
<script type="text/javascript">
function verif() {
document.getElementById('datos').style.display='none';
}
function foco_in(z) {
document.getElementById(z).focus();
}
</script>
<title>.: Sistema general de estad&iacute;stica :.</title>
<link rel="shortcut icon" href="../img/favicon.ico" />
<!-- InstanceBeginEditable name="doctitle" -->
<link href="../menu/css/css.css" rel="stylesheet" type="text/css" />
<script src="../script/c_color.js"></script>
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" --><!-- InstanceEndEditable -->
</head>
<body>
<div id="datos" align="center">
 <h4>Consolidando datos...<br />
 <img src="../img/loader.gif" width="16" height="16" /></h4>
 </div>
<div align="left"><?php //echo breadcrumbs(); ?></div>
<div align="center" class="popup">
<!-- InstanceBeginEditable name="contenidos" -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="99%" align="center">
    <div class="y_fieldset"><a href="javascript:void(0);" onclick="location = 'biblio.php?nodo=<?php echo $_GET["id_nodo"]; ?>&id=<?php echo $_GET["nom_nodo"]; ?>'; parent.contenido.location = '../fill.php';" title="Volver a bibliotecas"> &nbsp; << &nbsp; Bibliotecas</a></div>
    <?php if($_SESSION['MM_Usr_Pri'] == 1) { ?>
    <div class="x_fieldset"><a href="javascript:void(0);" onclick="parent.contenido.location = 'n_salas.php?bib=<?php echo $_GET["bib"]; ?>&nom=<?php echo $_GET["nom"]; ?>';" title="Nueva sala">Nueva sala</a> &nbsp; &nbsp; </div>
    <?php } ?>
    <div align="center" style="clear:both;">
    <fieldset>
    <legend align="center"><strong>Listado de salas de la biblioteca <?php echo $_GET["nom"]; ?></strong></legend>
    <table width="100%" border="1" cellspacing="1" cellpadding="1" id="area_1">
    <tr>
    <td width="3%"><div align="center"><strong>N&deg;</strong></div></td>
    <td width="3%"><div align="center"><strong>Sub</strong></div></td>
    <td width="21%"><div align="center"><strong>Nombre de la sala o espacio</strong></div></td>
    <td width="10%"><div align="center"><strong>Consulta</strong></div></td>
    <td><div align="center"><strong>Descripci&oacute;n</strong></div></td>
    <td width="19%"><div align="center"><strong>Opciones</strong></div></td>
    </tr>
	<?php
    for($i=0; $i<mysqli_num_rows($exc); $i++)
	{ 
		$row = mysqli_fetch_array($exc);
	?>	
      <tr onclick="n_color('<?php echo ($i+1); ?>','area_1');">
        <td align="center"><?php echo ($i+1); ?></td>
        <td align="left"><?php if($row["Sub"] == 0) echo "No"; else echo "Si"; ?></td>
        <td align="left"><?php echo $row["Nombre"]; ?></td>
        <td align="left"><?php echo $v_consul[$row["Consulta"]]; ?></td>
        <td align="left"><?php echo substr($row["Descripcion"],0,70); ?> ...</td>
        <td><div id="left_men">
        <ul>
            <li><a href="javascript:void(0);">Opciones</a>
            <ul>
            <?php if($_SESSION['MM_Usr_Pri'] == 1) { ?>
			<li><a href="javascript:void(0);" onclick="modificar('<?php echo "m_salas.php?id_esp=".$row['Id']."&nom=".$row["Nombre"]."&bib=".$_GET["bib"]; ?>');">Modificar</a></li>
            <li><a href="javascript:void(0);" onclick="modificar('<?php echo "e_salas.php?id_esp=".$row['Id']."&nom=".$row["Nombre"]."&bib=".$_GET["bib"]; ?>');">Eliminar</a></li>
            <?php } ?>
            <li><a href="javascript:void(0);" onclick="modificar('<?php echo "i_salas.php?id_esp=".$row['Id']."&nom=".$row["Nombre"]."&bib=".$_GET["bib"]; ?>');">Informaci&oacute;n</a></li>
            </ul></li>
        </ul>    
        </div></td>
      </tr>
	<?php } ?>
    </table>
    </fieldset>
    </div>
    </td>
  </tr>
</table>
<!-- InstanceEndEditable -->
<form name="chang_elemt" id="chang_elemt" target="contenido" method="post">
<!-- InstanceBeginEditable name="campos" -->

<!-- InstanceEndEditable -->
</form>
</div></body>

<script language="javascript">
verif();
</script>

<!-- InstanceEnd --></html>
<?php
@ mysqli_free_result($exc);
unset($exc, $sql, $i, $row,$v_consul);
mysqli_close($conect);
?>