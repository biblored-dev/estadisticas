<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/contenedor.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php
include("../script/scripts/session.php");
//include("../script/breadcrumbs.php");
?>
<link href="../css/css.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../script/scripts/jquery.min.js"></script>
<script type="text/javascript" src="../script/menu_ocultar.js"></script>
<script type="text/javascript">
function verif() {
document.getElementById('datos').style.display='none';
}
function foco_in(z) {
document.getElementById(z).focus();
}
</script>
<title>.: Sistema general de estad&iacute;stica :.</title>
<link rel="shortcut icon" href="../img/favicon.ico" />
<!-- InstanceBeginEditable name="doctitle" -->
<link href="../menu/css/css.css" rel="stylesheet" type="text/css" />
<script src="../script/c_color.js"></script>
<script language="javascript">
function busqueda() {
var err,op;
err = "";
op = document.getElementById("nodo").selectedIndex;
if(op == null || isNaN(op) || op == 0 || /^\s+$/.test(op))
	err += "Se requiere el nombre del nodo. \n";
if(err.length > 0)
{
	alert("Verifique los siguientes errores: \n\n"+err+"\n");
	return false;
}
else
{
	document.getElementById("id").value = document.getElementById("nodo")[op].text;
	return true;
}
}
</script>
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" --><!-- InstanceEndEditable -->
</head>
<body>
<div id="datos" align="center">
 <h4>Consolidando datos...<br />
 <img src="../img/loader.gif" width="16" height="16" /></h4>
 </div>
<div align="left"><?php //echo breadcrumbs(); ?></div>
<div align="center" class="popup">
<!-- InstanceBeginEditable name="contenidos" -->
<?php
if(isset($_GET["nom"], $_GET["bib"]))
{ 
include("../Connections/conect.inc.php");
if($_SESSION['MM_Usr_Pri'] <= 3)
	$sql = "select * from biblioteca where Nodo = ".$_GET["bib"]." order by biblioteca.Nombre";
else
	$sql = "select * from biblioteca where Nodo = ".$_GET["bib"]." and Id = ".$_SESSION['MM_Bib_Id']." order by biblioteca.Nombre";
$exc = mysqli_query($conect, $sql);
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="10%">&nbsp;</td>
    <td width="80%" align="center">
    <?php if($_SESSION['MM_Usr_Pri'] == 1) { ?>
    <div class="x_fieldset"><a href="javascript:void(0);" onclick="parent.contenido.location = 'n_biblio.php?id=<?php echo $_GET["bib"]."&nod=".$_GET["nom"]; ?>';" title="Cerrar">Nueva biblioteca</a></div>
    <?php } ?>
    <div align="center" style="clear:both;">
    <fieldset>
    <legend align="center"><strong>Listado de  bibliotecas por nodo: </strong><?php echo $_GET["nom"]; ?></legend>
    <table width="100%" border="1" cellspacing="1" cellpadding="1" id="area_1">
    <tr>
    <td width="5%"><div align="center"><strong>N&deg;</strong></div></td>
    <td width="70%"><div align="center"><strong>Nombre de la biblioteca</strong></div></td>
    <td width="25%"><div align="center"><strong>Opciones</strong></div></td>
    </tr>
	<?php
    for($i=0; $i<mysqli_num_rows($exc); $i++)
	{ 
		$row = mysqli_fetch_array($exc);
	?>	
      <tr onclick="n_color('<?php echo ($i+1); ?>','area_1');">
        <td align="center"><?php echo ($i+1); ?></td>
        <td align="left"><?php echo $row["Nombre"]; ?></td>
        <td><div id="left_men">
        <ul>
            <li><a href="javascript:void(0);">Opciones</a>
            <ul>
            <?php if($_SESSION['MM_Usr_Pri'] == 1) { ?>
			<li><a href="javascript:void(0);" onclick="modificar('<?php echo "m_biblio.php?bib=".$row['Id']."&nom=".$row["Nombre"]."&nod=".$_GET["bib"]."&cod=".$row["Cod_Conteo"]."&abrev=".$row["Abrev"]; ?>');">Modificar</a></li>
            <li><a href="javascript:void(0);" onclick="modificar('<?php echo "e_biblio.php?bib=".$row['Id']."&nom=".$row["Nombre"]; ?>');">Eliminar</a></li>
            <?php } ?>
            <li><a href="javascript:void(0);" onclick="modificar('<?php echo "i_biblio.php?bib=".$row['Id']."&nom=".$row["Nombre"]."&abr=".$row["Abrev"]; ?>');">Info. general</a></li>
            <li><a href="javascript:void(0);" onclick="modificar('<?php echo "im_biblio.php?bib=".$row['Id']."&nom=".$row["Nombre"]."&abr=".$row["Abrev"]; ?>');">Info. mensual</a></li>
            <li><a href="javascript:void(0);" onclick="document.location = '<?php echo "salas.php?bib=".$row['Id']."&nom=".$row["Nombre"]; ?>';">Salas / Espacios</a></li>
            </ul></li>
        </ul>
        </div></td>
      </tr>
	<?php } ?>
    </table>
    </fieldset>
    </div>
    </td>
    <td width="10%">&nbsp;</td>
  </tr>
</table>
<?php
@ mysqli_free_result($exc);
unset($exc, $sql, $i, $row);
mysqli_close($conect);
}
?>
<!-- InstanceEndEditable -->
<form name="chang_elemt" id="chang_elemt" target="contenido" method="post">
<!-- InstanceBeginEditable name="campos" -->

<!-- InstanceEndEditable -->
</form>
</div></body>

<script language="javascript">
verif();
</script>

<!-- InstanceEnd --></html>