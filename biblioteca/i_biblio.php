<?php
require_once("../Connections/conect.inc.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/contenedor.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php
include("../script/scripts/session.php");
//include("../script/breadcrumbs.php");
?>
<link href="../css/css.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../script/scripts/jquery.min.js"></script>
<script type="text/javascript" src="../script/menu_ocultar.js"></script>
<script type="text/javascript">
function verif() {
document.getElementById('datos').style.display='none';
}
function foco_in(z) {
document.getElementById(z).focus();
}
</script>
<title>.: Sistema general de estad&iacute;stica :.</title>
<link rel="shortcut icon" href="../img/favicon.ico" />
<!-- InstanceBeginEditable name="doctitle" -->
<script src="../script/c_color.js"></script>
<script language="javascript">
function serv_mensual(w) {
var z = window.open(w, 'Servicio mensual', 'resizable=yes,menubar=no,scrollbars=yes,width=700,height=420');
z.focus();
}
var obj;
function bg_color(bg) {
obj = document.getElementById(bg);
obj.style.background="#E53C67";
}
function bg_blanco(bg) {
obj = document.getElementById(bg);
obj.style.background="#FFFFFF";
}
function busqueda() {
var err,val;
err = "";
for(i=1; i<=14; i++)
{
	bg_blanco("b_d_"+i);
}
val = document.getElementById("t_d_1").value;
if(val < 0 || /^\s+$/.test(val) || val.length == 0 || isNaN(val))
{
	err += "Se requiere el área de la biblioteca. \n";
	bg_color("b_d_1");
}
val = document.getElementById("t_d_3").value;
if(val < 0 || /^\s+$/.test(val) || val.length == 0 || isNaN(val))
{
	err += "Se requiere el número de Puestos de lectura. \n";
	bg_color("b_d_3");
}
val = document.getElementById("t_d_4").value;
if(val < 0 || /^\s+$/.test(val) || val.length == 0 || isNaN(val))
{
	err += "Se requiere el número de catálogos. \n";
	bg_color("b_d_4");
}
val = document.getElementById("t_d_5").value;
if(val < 0 || /^\s+$/.test(val) || val.length == 0 || isNaN(val))
{
	err += "Se requiere la planta de personal. \n";
	bg_color("b_d_5");
}
val = document.getElementById("t_d_6").value;
if(val < 0 || /^\s+$/.test(val) || val.length == 0 || isNaN(val))
{
	err += "Se requiere la planta de personal subcontratado. \n";
	bg_color("b_d_6");
}
val = document.getElementById("t_d_7").value;
if(val < 0 || /^\s+$/.test(val) || val.length == 0 || isNaN(val))
{
	err += "Se requiere el número de computadores de uso público. \n";
	bg_color("b_d_7");
}
val = document.getElementById("t_d_8").value;
if(val < 0 || /^\s+$/.test(val) || val.length == 0 || isNaN(val))
{
	err += "Se requiere el número de portátiles de uso público. \n";
	bg_color("b_d_8");
}
val = document.getElementById("t_d_9").value;
if(val < 0 || /^\s+$/.test(val) || val.length == 0 || isNaN(val))
{
	err += "Se requiere el número de tabletas. \n";
	bg_color("b_d_9");
}
val = document.getElementById("t_d_10").value;
if(val < 0 || /^\s+$/.test(val) || val.length == 0 || isNaN(val))
{
	err += "Se requiere el número de computadores de uso administrativo. \n";
	bg_color("b_d_10");
}
val = document.getElementById("t_d_11").value;
if(val < 0 || /^\s+$/.test(val) || val.length == 0 || isNaN(val))
{
	err += "Se requiere el número de portátiles de uso administrativo. \n";
	bg_color("b_d_11");
}
val = document.getElementById("t_d_12").value;
if(val < 0 || /^\s+$/.test(val) || val.length == 0 || isNaN(val))
{
	err += "Se requiere el número de equipos tiflotécnicos. \n";
	bg_color("b_d_12");
}
val = document.getElementById("t_d_13").value;
if(val < 0 || /^\s+$/.test(val) || val.length == 0 || isNaN(val))
{
	err += "Se requiere el número de televisores. \n";
	bg_color("b_d_13");
}
val = document.getElementById("t_d_14").value;
if(val < 0 || /^\s+$/.test(val) || val.length == 0 || isNaN(val))
{
	err += "Se requiere el número de equipos de audio. \n";
	bg_color("b_d_14");
}
if(err.length > 0)
{
	alert("Verifique los siguientes errores: \n\n"+err+"\n");
	return false;
}
else
	return true;
}
</script>
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" --><!-- InstanceEndEditable -->
</head>
<body>
<div id="datos" align="center">
 <h4>Consolidando datos...<br />
 <img src="../img/loader.gif" width="16" height="16" /></h4>
 </div>
<div align="left"><?php //echo breadcrumbs(); ?></div>
<div align="center" class="popup">
<!-- InstanceBeginEditable name="contenidos" -->
<?php
if(isset($_POST["t_d_1"],$_POST["t_d_3"],$_POST["t_d_5"],$_POST["guardar"],$_POST["bib"]))
{
	include("../script/cant.php");
	if(cant("dat_bib","Biblioteca",$_POST["bib"]) == 0) //tabla, campo, dato
	{
		$sql = "insert into dat_bib (Id, Biblioteca, Area, P_Lectura, Catalogos, Computadores_A, Computadores_P, Portatiles_A, Portatiles_P, Tabletas, Televisores, Audio, Personal, Sub_Personal, Tiflotecnicos, Descrip_T) VALUES ('', '".$_POST["bib"]."', '".$_POST["t_d_1"]."', '".$_POST["t_d_3"]."', '".$_POST["t_d_4"]."', '".$_POST["t_d_10"]."', '".$_POST["t_d_7"]."', '".$_POST["t_d_11"]."', '".$_POST["t_d_8"]."', '".$_POST["t_d_9"]."', '".$_POST["t_d_5"]."', '".$_POST["t_d_6"]."', '".$_POST["t_d_12"]."', '".$_POST["t_d_13"]."', '".$_POST["t_d_14"]."', '".$_POST["descrp"]."')";
		$exc = mysqli_query($conect, $sql);
	}
	else
	{
		$sql = "update dat_bib set Area = '".$_POST["t_d_1"]."', P_Lectura = '".$_POST["t_d_3"]."', Catalogos = '".$_POST["t_d_4"]."', Computadores_A = '".$_POST["t_d_10"]."', Computadores_P = '".$_POST["t_d_7"]."', Portatiles_A = '".$_POST["t_d_11"]."', Portatiles_P = '".$_POST["t_d_8"]."', Tabletas = '".$_POST["t_d_9"]."', Televisores = '".$_POST["t_d_13"]."', Audio = '".$_POST["t_d_14"]."', Personal = '".$_POST["t_d_5"]."', Sub_Personal = '".$_POST["t_d_6"]."', Tiflotecnicos = '".$_POST["t_d_12"]."', Descrip_T = '".$_POST["descrp"]."' where Biblioteca = ".$_POST["bib"];
		$exc = mysqli_query($conect, $sql);
	}
	if($exc)
	{
		echo "<h4 align='center'>Registro actualizado</h4>";
	}
}
if(isset($_GET["bib"], $_GET["nom"]))
{ 
$sql = "select dat_bib.Area, dat_bib.P_Lectura, dat_bib.Catalogos, dat_bib.Computadores_A, dat_bib.Computadores_P, dat_bib.Portatiles_A, dat_bib.Portatiles_P, dat_bib.Tabletas, dat_bib.Personal, dat_bib.Sub_Personal, dat_bib.Tiflotecnicos, dat_bib.Televisores, dat_bib.Audio, dat_bib.Descrip_T, count(salas.Id) as Salas from dat_bib, salas where dat_bib.Biblioteca = ".$_GET["bib"]." and salas.Biblioteca = ".$_GET["bib"];
$exc = mysqli_query($conect, $sql);
$row = mysqli_fetch_array($exc);
?>
<div align="center">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="10%">&nbsp;</td>
    <td align="center">
    <div class="bk_info">
    <form name ="formiden" method ="POST" action ="i_biblio.php?bib=<?php echo $_GET["bib"]."&nom=".$_GET["nom"]; ?>" onsubmit="return busqueda();">
    <input name="bib" id="bib" type="hidden" value="<?php echo $_GET["bib"]; ?>" />
    <table width="80%" border="0" cellspacing="0" cellpadding="0">
      <tr>
      	<td colspan="7"><strong>Informaci&oacute;n general de la biblioteca:</strong> Recursos disponibles para los usuarios</td>
      </tr>
      <tr>
        <td width="2%" align="left">&nbsp;</td>
        <td width="35%" align="left">Metros cuadrados</td>
        <td width="11%" align="left" id="b_d_1"><input type="text" name="t_d_1" id="t_d_1" size="10" value="<?php echo $row["Area"]; ?>" placeholder="00" /></td>
        <td width="4%" align="left">&nbsp;</td>
        <td width="2%" align="left">&nbsp;</td>
        <td width="35%" align="left">N&uacute;mero de salas o espacios de uso</td>
        <td width="11%" align="left" id="b_d_2"><input type="text" name="t_d_2" id="t_d_2" size="10" readonly="readonly" value="<?php echo $row["Salas"]; ?>" placeholder="00" /></td>
      </tr>
      <tr>
        <td align="left">&nbsp;</td>
        <td align="left">N&uacute;mero total de Puestos de  lectura</td>
        <td align="left" id="b_d_3"><input type="text" name="t_d_3" id="t_d_3" size="10" value="<?php echo $row["P_Lectura"]; ?>" placeholder="00" /></td>
        <td align="left">&nbsp;</td>
        <td align="left">&nbsp;</td>
        <td align="left">N&uacute;mero de cat&aacute;logos</td>
        <td align="left" id="b_d_4"><input type="text" name="t_d_4" id="t_d_4" size="10" value="<?php echo $row["Catalogos"]; ?>" placeholder="00" /></td>
      </tr>
      <tr>
        <td align="left">&nbsp;</td>
        <td align="left">Planta  de personal propia</td>
        <td align="left" id="b_d_5"><input type="text" name="t_d_5" id="t_d_5" size="10" value="<?php echo $row["Personal"]; ?>" placeholder="00" /></td>
        <td align="left">&nbsp;</td>
        <td align="left">&nbsp;</td>
        <td align="left">Planta  de personal subcontratada</td>
        <td align="left" id="b_d_6"><input type="text" name="t_d_6" id="t_d_6" size="10" value="<?php echo $row["Sub_Personal"]; ?>" placeholder="00" /></td>
      </tr>
      <tr>
        <td align="left">&nbsp;</td>
        <td align="left">N&uacute;mero de computadores de uso p&uacute;blico</td>
        <td align="left" id="b_d_7"><input type="text" name="t_d_7" id="t_d_7" size="10" value="<?php echo $row["Computadores_P"]; ?>" placeholder="00" /></td>
        <td align="left">&nbsp;</td>
        <td align="left">&nbsp;</td>
        <td align="left">Número de port&aacute;tiles de uso p&uacute;blico</td>
        <td align="left" id="b_d_8"><input type="text" name="t_d_8" id="t_d_8" size="10" value="<?php echo $row["Portatiles_P"]; ?>" placeholder="00" /></td>
      </tr>
      <tr>
        <td align="left">&nbsp;</td>
        <td align="left">N&uacute;mero  de tabletas de uso p&uacute;blico</td>
        <td align="left" id="b_d_9"><input type="text" name="t_d_9" id="t_d_9" size="10" value="<?php echo $row["Tabletas"]; ?>" placeholder="00" /></td>
        <td align="left">&nbsp;</td>
        <td align="left">&nbsp;</td>
        <td align="left">N&uacute;mero de computadores de uso administrativo</td>
        <td align="left" id="b_d_10"><input type="text" name="t_d_10" id="t_d_10" size="10" value="<?php echo $row["Computadores_A"]; ?>" placeholder="00" /></td>
      </tr>
      <tr>
        <td align="left">&nbsp;</td>
        <td align="left">N&uacute;mero de port&aacute;tiles de uso administrativo</td>
        <td align="left" id="b_d_11"><input type="text" name="t_d_11" id="t_d_11" size="10" value="<?php echo $row["Portatiles_A"]; ?>" placeholder="00" /></td>
        <td align="left">&nbsp;</td>
        <td align="left">&nbsp;</td>
        <td align="left">N&uacute;mero  de equipos tiflot&eacute;cnicos</td>
        <td align="left" id="b_d_12"><input type="text" name="t_d_12" id="t_d_12" size="10" value="<?php echo $row["Tiflotecnicos"]; ?>" placeholder="00" /></td>
      </tr>
      <tr>
        <td align="left">&nbsp;</td>
        <td align="left">N&uacute;mero de televisores</td>
        <td align="left" id="b_d_13"><input type="text" name="t_d_13" id="t_d_13" size="10" value="<?php echo $row["Televisores"]; ?>" placeholder="00" /></td>
        <td align="left">&nbsp;</td>
        <td align="left">&nbsp;</td>
        <td align="left">N&uacute;mero de equipos de audio</td>
        <td align="left" id="b_d_14"><input type="text" name="t_d_14" id="t_d_14" size="10" value="<?php echo $row["Audio"]; ?>" placeholder="00" /></td>
      </tr>
      <tr>
        <td align="left"><img src="../icon/observaciones.png" width="37" height="35" alt="icono" /></td>
        <td align="left" colspan="6">Descripci&oacute;nde equipos tiflot&eacute;cnicos:
        <textarea name="descrp" id="descrp" rows="2"><?php echo $row["Descrip_T"]; ?></textarea>
        <div class="infos">M&aacute;ximo 500 caracteres. Describa la información que está reportando, si durante el mes se presentaron inconvenientes o acciones que afecten el dato reportado.</div>
        </td>
      </tr>  
      <tr>
        <td colspan="7"><div align="center"><input type="submit" name="guardar" id="guardar" value="Actualizar" /></div></td>
      </tr>
    </table>
    </form>
    </div>  
    </td>
    <td width="10%">&nbsp;</td>
  </tr>
</table>
</div>
<?php
mysqli_free_result($exc);
unset($exc, $sql, $i, $row);
mysqli_close($conect);
}
?>
<!-- InstanceEndEditable -->
<form name="chang_elemt" id="chang_elemt" target="contenido" method="post">
<!-- InstanceBeginEditable name="campos" -->

<!-- InstanceEndEditable -->
</form>
</div></body>

<script language="javascript">
verif();
</script>

<!-- InstanceEnd --></html>