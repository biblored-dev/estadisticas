<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/contenedor.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php
include("../script/scripts/session.php");
//include("../script/breadcrumbs.php");
?>
<link href="../css/css.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../script/scripts/jquery.min.js"></script>
<script type="text/javascript" src="../script/menu_ocultar.js"></script>
<script type="text/javascript">
function verif() {
document.getElementById('datos').style.display='none';
}
function foco_in(z) {
document.getElementById(z).focus();
}
</script>
<title>.: Sistema general de estad&iacute;stica :.</title>
<link rel="shortcut icon" href="../img/favicon.ico" />
<!-- InstanceBeginEditable name="doctitle" -->
<script language="javascript" src="../script/datosxml.js"></script>
<script language="javascript">
function editar(w,x,y,z,a) {
	parent.contenido.location = "edduser.php?id="+w+"&correo="+x+"&nombre="+y+"&priv="+z+"&area="+a;
}
function validate() {
var expr = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
err = "";	
var val, bib;
val = document.getElementById("nombre").value;
bib = val.split("@");
if(val.length < 15  || !expr.test(val) || bib[1] != "biblored.gov.co")
	err += "El correo electrónico no es válido. \n";
if(err.length > 0)
{
	alert("Verifique los siguientes errores: \n\n"+err+"\n");
	return false;
}
else
{
	return true;
}
}
function datosserver(w) { //Reportes preconfigurados
  var sql = "select Id, Correo,	Nombre, Acceso, Area from aut_v2 where Biblioteca = "+w+" order by Correo asc";
  http.open("GET", "../script/datosxml_l.php?d1=Nombre&d2="+w+"&dat="+sql, true); //False para que no continue el flujo hasta compeltar la petición ajax
  http.onreadystatechange = datosHttpResp;
  return http.send(null);
}
function datosHttpResp() {
if(http.readyState == 4)
{
	if(http.status == 200)
	{
		var salida = "";
		var timeValue = http.responseXML.getElementsByTagName("timenow")[0];
	    if(timeValue.childNodes.length > 0)
		{
			var regs = timeValue.childNodes[0].nodeValue;
			regs = regs.split(";");
			var listado = document.getElementById('user');
			var campo = "";
			for(i = 0; i < (regs.length) - 1; i ++)
			{
			   campo = "";
			   salida = regs[i].split(",,");
			   campo = "<div id=\"conter_"+salida[0]+"\" class=\"list_js\"><div class=\"list_legen\"><a href='javascript:void(0);' onclick=\"editar('"+salida[0]+"','"+salida[1]+"','"+salida[2]+"','"+salida[3]+"','"+salida[4]+"');\" title=\"Haga clic para editar la información\">"+salida[1]+"</a></div>";
			   campo += "<div class=\"list_extn\">"+salida[2]+"</div></div>";		
			   listado.innerHTML += campo;
			}
			listado.innerHTML += "<div style='clear:both;' align='center'>. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .</div>";
		}
		else //if(cant == 0)
		{
			document.getElementById('user').innerHTML = "";
		}
		verif();
	}
} 
else
{
	document.getElementById('datos').style.display = "";
}
}
</script>
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" --><!-- InstanceEndEditable -->
</head>
<body>
<div id="datos" align="center">
 <h4>Consolidando datos...<br />
 <img src="../img/loader.gif" width="16" height="16" /></h4>
 </div>
<div align="left"><?php //echo breadcrumbs(); ?></div>
<div align="center" class="popup">
<!-- InstanceBeginEditable name="contenidos" -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="50%">
    <div class="list_js">
    <div class="list_legen"><a href="javascript:void(0);" onclick="datosserver('<?php echo $_SESSION['MM_Bib_Id']; ?>');">&nbsp; Consultar usuarios del área y biblioteca</a></div></div>
    <div class="list_js">
    <div class="list_legen"><a href="adduser.php" target="contenido">&nbsp; Autorizar un nuevo usuario</a></div></div>
    </td>
    <td width="50%" valign="middle">
    <div style="margin:0px auto; width:60%; text-align:center; background-color:#E2E2E2; border-radius:5px; border:#E2E2E2 solid 1px; padding:5px;">
    <form name="busc_usr" id="busc_usr" target="contenido" method="post" action="buscuser.php" onsubmit="return validate();">
    <input name="nombre" id="nombre" type="text" placeholder="Correo del usuario" value="" />
    <div style="padding-top:5px;">
    <input name="guardar" id="guardar" type="submit" value="Buscar" />
    </div>
    </form>
    </div>
    </td>
  </tr>
</table>
<div style="clear:both;" align="center">. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .</div>
<div id="user"></div>
<div align="justify" id="db_guardar">&nbsp; <!--Insertar mensaje de ayuda para la página -->
<div class="div_menu" id="aa_1"><a href="javascript:void(0);" onclick="menu('a_b_','1'); mostrar('bb','1');" title="Ayuda">? +</a></div>
<div class="div_menu" style="display:none;" id="bb_1"><a href="javascript:void(0);" onclick="menu('a_b_','2'); mostrar('aa','1');" title="Ayuda">? -</a></div>
<div class="div_ayuda" id="a_b_" style="display:none;"><!-- Quitar comentarios e insertar el texto de ayuda. Aparecerá flotando en el pie de la página.--></div></div>
<!-- InstanceEndEditable -->
<form name="chang_elemt" id="chang_elemt" target="contenido" method="post">
<!-- InstanceBeginEditable name="campos" -->

<!-- InstanceEndEditable -->
</form>
</div></body>

<script language="javascript">
verif();
</script>

<!-- InstanceEnd --></html>
<script language="javascript">
datosserver('<?php echo $_SESSION['MM_Bib_Id']; ?>');
</script>
