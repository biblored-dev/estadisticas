<?php include "../script/breadcrumbs.php"; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/contenedor.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php
include("../script/scripts/session.php");
//include("../script/breadcrumbs.php");
?>
<link href="../css/css.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../script/scripts/jquery.min.js"></script>
<script type="text/javascript" src="../script/menu_ocultar.js"></script>
<script type="text/javascript">
function verif() {
document.getElementById('datos').style.display='none';
}
function foco_in(z) {
document.getElementById(z).focus();
}
</script>
<title>.: Sistema general de estad&iacute;stica :.</title>
<link rel="shortcut icon" href="../img/favicon.ico" />
<!-- InstanceBeginEditable name="doctitle" -->
<script language="javascript" src="../script/datosxml.js"></script>
<script language="javascript">
var err = "";
function predataserver(w,x,y) {
  http.open("GET", "../script/datosxml.php?tab="+w+"&camp="+x+"&dat="+y, false); //False para que no continue el flujo hasta compeltar la petición ajax
  http.onreadystatechange = useHttpResp;
  return http.send(null);
}

function useHttpResp() {
if(http.readyState == 4)
{
	if(http.status == 200)
	{
		var timeValue = http.responseXML.getElementsByTagName("timenow")[0];
		if(parseInt(timeValue.childNodes[0].nodeValue) > 1)
		{
			err += "El nombre de la sala ya existe. \n";
			document.getElementById('no_send').innerHTML = "El nombre de la sala ya existe.";
		}
		verif();
	}
} 
else
{
	document.getElementById('datos').style.display = "";
}
}

function busqueda() {
var val;
err = "";
val = document.getElementById("nombre").value;
if(val == null || !isNaN(val) || val.length < 3 || /^\s+$/.test(val))
	err += "Se requiere el nombre de la sala. \n";
else	
	predataserver('salas','Nombre',document.getElementById("nombre").value+"' and Biblioteca = '"+document.getElementById("id_b").value) //tabla, campo, dato
if(document.getElementById("material").checked)
	document.getElementById("consulta").value = 1;
else
	document.getElementById("consulta").value = 0;
if(err.length > 0)
{
	alert("Verifique los siguientes errores: \n\n"+err+"\n");
	return false;
}
else
	return true;
}
</script>
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" --><!-- InstanceEndEditable -->
</head>
<body>
<div id="datos" align="center">
 <h4>Consolidando datos...<br />
 <img src="../img/loader.gif" width="16" height="16" /></h4>
 </div>
<div align="left"><?php //echo breadcrumbs(); ?></div>
<div align="center" class="popup">
<!-- InstanceBeginEditable name="contenidos" -->
<?php
require_once("../Connections/conect.inc.php");
$sql = "select salas.Sub, salas.Biblioteca, salas.Consulta, coleccion.Coleccion, salas.P_Lectura, salas.Descripcion from salas, coleccion where salas.Id = ".$_GET["id_esp"]." and coleccion.Iden = salas.Coleccion";
$exc = mysqli_query($conect, $sql);
$row = mysqli_fetch_array($exc);
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="15%" valign="top"><div align="right"><img src="../icon/salas.png" width="39" height="35" alt="icono" /></div></td>
    <td width="70%" align="center">
    <div align="center"><strong>Informaci&oacute;n de la sala o espacio</strong>
    <div class="x_fieldset"><a href="javascript:void(0);" onclick="document.location = '../fill.php';" title="Cerrar">X</a></div></div>
    <form name ="formiden" method ="POST" action ="m_salas.php?bib=<?php echo $_GET["bib"]; ?>&nom=<?php echo $_GET["nom"]; ?>" onsubmit="return busqueda();">
    <input name="id_s" id="id_s" type="hidden" value="<?php echo $_GET["bib"]; ?>" />
    <input name="id_b" id="id_b" type="hidden" value="<?php echo $row["Biblioteca"]; ?>" />
    <input name="consulta" id="consulta" type="hidden" value="" />
    <div align="left"><strong>Nombre de la sala o espacio:</strong></div>
    <div align="left"><input name="nombre" id="nombre" type="text" size="50" value="<?php echo $_GET["nom"]; ?>" readonly="readonly" /></div>
    <div>
    <div align="left" style="width:49%; float:left;">
    <div align="left"><strong>Espacio principal:</strong></div>
    <div align="left">
    <?php
	if($row["Sub"] == 0)
		$esp = "No";
	else
	{	
		$sql = "select Nombre from salas where Id = ".$row["Sub"];
		$excs = mysqli_query($conect, $sql);
		$rows = mysqli_fetch_array($excs);
		$esp = $rows["Nombre"];
	}
	?>
    <input name="sub" id="sub" type="text" size="50" value="<?php echo $esp; ?>" readonly="readonly" /></div>
    <div>Si el espacio est&aacute; dentro de otro, indique cual, requerido para la suma de visitantes</div></div>
    <div align="left" style="width:49%; float:right;">
        <div align="left"><strong>N&uacute;mero de puestos de lectura:</strong></div>
        <div align="left"><input name="p_lec" id="p_lec" type="text" size="50" readonly="readonly" value="<?php echo $row["P_Lectura"]; ?>" /></div>
        <div class="infos">Si el espacio no posee puestos de lectura deje, se asume '0'</div>
    </div>
    </div>
    <div align="left" style="clear:both;"><strong>Descripci&oacute;n de la sala o espacio:</strong></div>
    <div align="justify" style="background-color:#EEE; font-size:130%;"><?php echo $row["Descripcion"]; ?></div>
    <div>M&aacute;ximo 250 caracteres</div> 
    <div align="left" style="background-color:#EBEEE5; padding-top:5px; padding-left:5px; border-radius:3px; overflow:hidden;">
    <div align="left" style="width:49%; float:left;"><input name="material" id="material" type="checkbox" value="0" <?php if($row["Consulta"] == 1) echo "checked='checked'"; ?> disabled="disabled" />
    &nbsp; <strong>La sala o espacio cuenta con material de consulta.</strong></div>
    <div align="right" style="width:49%; float:right;"><strong>Colecci&oacute;n: <?php echo $row["Coleccion"]; ?></strong></div>
    </div>
    </form>
    <script>
		foco_in('nombre');
	</script>
    </td>
    <td width="15%">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><div id="no_send"></div></td>
    <td>&nbsp;</td>
  </tr>
</table>
<?php
@ mysqli_free_result($excs);

@ mysqli_free_result($exc);
unset($sql,$exc,$row,$rows,$claus,$excs);
mysqli_close($conect);
?>
<div align="left">&nbsp; Salas o espacios  de la biblioteca que pueden ser utilizados por los usuarios</div>
<!-- InstanceEndEditable -->
<form name="chang_elemt" id="chang_elemt" target="contenido" method="post">
<!-- InstanceBeginEditable name="campos" -->

<!-- InstanceEndEditable -->
</form>
</div></body>

<script language="javascript">
verif();
</script>

<!-- InstanceEnd --></html>