<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/contenedor.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php
include("../script/scripts/session.php");
//include("../script/breadcrumbs.php");
?>
<link href="../css/css.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../script/scripts/jquery.min.js"></script>
<script type="text/javascript" src="../script/menu_ocultar.js"></script>
<script type="text/javascript">
function verif() {
document.getElementById('datos').style.display='none';
}
function foco_in(z) {
document.getElementById(z).focus();
}
</script>
<title>.: Sistema general de estad&iacute;stica :.</title>
<link rel="shortcut icon" href="../img/favicon.ico" />
<!-- InstanceBeginEditable name="doctitle" -->
<script language="javascript" src="../script/datosxml.js"></script>
<script language="javascript">
function busqueda() {
var expr = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
err = "";	
var val;
var op, bib;
val = document.getElementById("no_n").value;
if(val == null || !isNaN(val) || val.length < 10 || /^\s+$/.test(val))
	err += "Se requiere el nombre del usuario. \n";
val = document.getElementById("nombre").value;
bib = val.split("@");
if(val.length < 15  || !expr.test(val) || bib[1] != "biblored.gov.co")
	err += "El correo electrónico no es válido. \n";
op = document.getElementById("area_r").selectedIndex;
if(op == null || isNaN(op) || op == 0 || /^\s+$/.test(op))
	err += "Se requiere el área responsable. \n";
op = document.getElementById("priv_us").selectedIndex;
if(op == null || isNaN(op) || op == 0 || /^\s+$/.test(op))
	err += "Se requieren los privilegios. \n";
if(err.length > 0)
{
	alert("Verifique los siguientes errores: \n\n"+err+"\n");
	return false;
}
else
{
	return true;
}
}
function selecct(w,x) { //select, valor 
for(i=0; i<document.getElementById(w).length; i++)
{
	if(document.getElementById(w)[i].value == x)
	{
		document.getElementById(w).selectedIndex = i;
		break;
	}
}
}
</script>
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" --><!-- InstanceEndEditable -->
</head>
<body>
<div id="datos" align="center">
 <h4>Consolidando datos...<br />
 <img src="../img/loader.gif" width="16" height="16" /></h4>
 </div>
<div align="left"><?php //echo breadcrumbs(); ?></div>
<div align="center" class="popup">
<!-- InstanceBeginEditable name="contenidos" -->
<?php
require_once("../Connections/conect.inc.php");
if(isset($_POST["nombre"], $_POST["guardar"], $_POST["id"], $_POST["no_n"]))
{
	$sql = "update aut_v2 set Correo = '".$_POST["nombre"]."', Nombre = '".$_POST["no_n"]."', Biblioteca = '".$_POST["n_bibs"]."', Acceso = '".$_POST["priv_us"]."', Area = '".$_POST["area_r"]."' where Id = ".$_POST["id"];
	$exc = mysqli_query($conect, $sql);
	if($exc)
	{
		echo "<br /><h4 align='center'>Registro actualizado</h4>";
		?><script language="javascript">parent.consulta.location.reload();</script><?php
	}
	else
	{
		echo "<br /><h4 align='center'>Error al actualizar el registro</h4>";
	}
	unset($sql,$exc);
	mysqli_close($conect);
}
else
{ 
$sql = "SELECT Id, Nombre, Biblioteca, Acceso, Area FROM aut_v2 WHERE Correo = '".$_POST["nombre"]."'";
$excx = mysqli_query($conect, $sql);
if(mysqli_num_rows($excx) > 0)
{
	$rowx = mysqli_fetch_array($excx);
	?>
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	  <tr>
		<td width="20%">&nbsp;</td>
		<td width="60%" align="center">&nbsp;</td>
		<td width="20%">&nbsp;</td>
	  </tr>
	  <tr>
		<td>&nbsp;</td>
		<td>
		<fieldset>
		<legend align="center"><strong>Formulario para autorizar usuario</strong></legend>
		<div class="x_fieldset"><a href="javascript:void(0);" onclick="document.location = '../fill.php';" title="Cerrar">X</a></div>
		<form name ="formiden" method ="POST" action ="buscuser.php" onsubmit="return busqueda();">
		<input name="id" id="id" type="hidden" value="<?php echo $rowx['Id']; ?>" />
		<div style="width:90%; margin:0px auto ;">
		<div align="left"><strong>Nombre del usuario:</strong></div>
		<div align="left"><input name="no_n" id="no_n" type="text" value="<?php echo $rowx['Nombre']; ?>" laceholder="Nombre completo del usuario" /></div>
		<div align="left">
		<div align="left"><strong>Correo del usuario:</strong></div>
		<div align="left"><input name="nombre" id="nombre" type="text" size="50" placeholder="Correo del usuario" value="<?php echo $_POST['nombre']; ?>" /></div>
		<?php include("../script/bibs.php"); ?>
		</div>
		<div align="left" style="float:right; width:49%;">
		<div align="left"><strong>Privilegios de acceso:</strong></div>
		<div align="left"><?php include("../script/privilegios.php"); ?></div>
		</div>
		<div align="left" style="float:left; width:49%;">
		<div align="left"><?php include("../script/areas.php"); ?></div>
		</div>
		<div align="center" style="clear:both; padding-top:10px;"><input name="guardar" id="guardar" type="submit" value="Guardar" /></div>
		</div>
		</form>
		<script>
		foco_in('no_n');
		selecct('n_bibs', '<?php echo $rowx['Biblioteca']; ?>');
		selecct('area_r', '<?php echo $rowx['Area']; ?>');
		selecct('priv_us', '<?php echo $rowx['Acceso']; ?>');
		</script>
		</fieldset>
		</td>
		<td>&nbsp;</td>
	  </tr>
	  <tr>
		<td>&nbsp;</td>
		<td align="center">&nbsp;</td>
		<td>&nbsp;</td>
	  </tr>
	</table>
	<?php
}
else
{ ?>
	<h4 align="center">No se encontró registro</h4>
<?php
}
mysqli_free_result($excx);
unset($sql,$excx,$rowx,$i);
} ?>
<div align="justify" id="db_guardar">&nbsp; <!--Insertar mensaje de ayuda para la página -->
<div class="div_menu" id="aa_1"><a href="javascript:void(0);" onclick="menu('a_b_','1'); mostrar('bb','1');" title="Ayuda">? +</a></div>
<div class="div_menu" style="display:none;" id="bb_1"><a href="javascript:void(0);" onclick="menu('a_b_','2'); mostrar('aa','1');" title="Ayuda">? -</a></div>
<div class="div_ayuda" id="a_b_" style="display:none;"><!-- Quitar comentarios e insertar el texto de ayuda. Aparecerá flotando en el pie de la página.--></div></div>
<!-- InstanceEndEditable -->
<form name="chang_elemt" id="chang_elemt" target="contenido" method="post">
<!-- InstanceBeginEditable name="campos" -->

<!-- InstanceEndEditable -->
</form>
</div></body>

<script language="javascript">
verif();
</script>

<!-- InstanceEnd --></html>