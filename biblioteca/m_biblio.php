<?php
function que($w) {
$ret = "";
$w	= explode("-", $w);
$w[0] = explode(":", $w[0]);
if($w[0][0] > 12)
{
	$ret .= intval($w[0][0] - 12).":".$w[0][1]." PM";	
}
else if($w[0][0] > 0)
{
	$ret .= $w[0][0].":".$w[0][1]." PM";	
}
$ret .= " - ";
$w[1] = explode(":", $w[1]);
if($w[1][0] > 0)
{
	$ret .= intval($w[1][0] - 12).":".$w[1][1]." PM";	
}
if($w[0][0] == 0 || $w[1][0] == 0)
{
	$ret = "N / A";
}
unset($w);
return $ret;
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/contenedor.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php
include("../script/scripts/session.php");
//include("../script/breadcrumbs.php");
?>
<link href="../css/css.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../script/scripts/jquery.min.js"></script>
<script type="text/javascript" src="../script/menu_ocultar.js"></script>
<script type="text/javascript">
function verif() {
document.getElementById('datos').style.display='none';
}
function foco_in(z) {
document.getElementById(z).focus();
}
</script>
<title>.: Sistema general de estad&iacute;stica :.</title>
<link rel="shortcut icon" href="../img/favicon.ico" />
<!-- InstanceBeginEditable name="doctitle" -->
<script language="javascript" src="../script/datosxml.js"></script>
<script language="javascript">
var err = "";
function cual(w,x) {
for(i=0; i<document.getElementById(x).length; i++)
{
	if(document.getElementById(x)[i].value == w)
	{
		document.getElementById(x).selectedIndex = i;
		break;	
	}
}
}

function predataserver(w,x,y) {
  http.open("GET", "../script/datosxml.php?tab="+w+"&camp="+x+"&dat="+y, false); //False para que no continue el flujo hasta compeltar la petición ajax
  http.onreadystatechange = useHttpResp;
  return http.send(null);
}

function useHttpResp() {
if(http.readyState == 4)
{
	if(http.status == 200)
	{
		var timeValue = http.responseXML.getElementsByTagName("timenow")[0];
		if(parseInt(timeValue.childNodes[0].nodeValue) != 1)
		{
			err += "El nombre de la biblioteca ya existe. \n";
		}
		verif();
	}
} 
else
{
	document.getElementById('datos').style.display = "";
}
}

function busqueda() {
var val;
err = "";
val = document.getElementById("nombre").value;
if(val == null || !isNaN(val) || val.length < 3 || /^\s+$/.test(val))
	err += "Se requiere el nombre del nodo. \n";
else	
	predataserver('biblioteca','Nombre',document.getElementById("nombre").value) //tabla, campo, dato
val = document.getElementById("abrev").value;
if(val == null || !isNaN(val) || val.length < 3 || /^\s+$/.test(val))
	err += "Se requiere abreviatura de la biblioteca. \n";
val = document.getElementById("cod_cont").value;
if(val == null || val.length == 0 || /^\s+$/.test(val))
	document.getElementById("cod_cont").value = 0;
if((document.getElementById("ini_lu").selectedIndex == 0 && document.getElementById("cie_lu").selectedIndex > 0) || (document.getElementById("ini_lu").selectedIndex > 0 && document.getElementById("cie_lu").selectedIndex == 0))
	err += "El horario del lunes no es válido. \n";
if((document.getElementById("ini_ma").selectedIndex == 0 && document.getElementById("cie_ma").selectedIndex > 0) || (document.getElementById("ini_ma").selectedIndex > 0 && document.getElementById("cie_ma").selectedIndex == 0))
	err += "El horario de martes a sábado no es válido. \n";
if((document.getElementById("ini_do").selectedIndex == 0 && document.getElementById("cie_do").selectedIndex > 0) || (document.getElementById("ini_do").selectedIndex > 0 && document.getElementById("cie_do").selectedIndex == 0))
	err += "El horario de domingo no es válido. \n";
if(err.length > 0)
{
	alert("Verifique los siguientes errores: \n\n"+err+"\n");
	return false;
}	
else
{
	val = document.getElementById("ini_lu")[document.getElementById("ini_lu").selectedIndex].value + "-" + document.getElementById("cie_lu")[document.getElementById("cie_lu").selectedIndex].value;
	val += "," + document.getElementById("ini_ma")[document.getElementById("ini_ma").selectedIndex].value + "-" + document.getElementById("cie_ma")[document.getElementById("cie_ma").selectedIndex].value;
	val += "," + document.getElementById("ini_do")[document.getElementById("ini_do").selectedIndex].value + "-" + document.getElementById("cie_do")[document.getElementById("cie_do").selectedIndex].value;
	document.getElementById("horario").value = val;
	return true;
}
}
</script>
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" --><!-- InstanceEndEditable -->
</head>
<body>
<div id="datos" align="center">
 <h4>Consolidando datos...<br />
 <img src="../img/loader.gif" width="16" height="16" /></h4>
 </div>
<div align="left"><?php //echo breadcrumbs(); ?></div>
<div align="center" class="popup">
<!-- InstanceBeginEditable name="contenidos" -->
<?php
require_once("../Connections/conect.inc.php");
if(isset($_POST["id"], $_POST["nombre"], $_POST["guardar"]))
{
	
	include("../script/cant.php");
	if(cant("biblioteca","Nombre",$_POST["nombre"]) == 1) //tabla, campo, dato
	{
		$sql = "update biblioteca set Nombre = '".$_POST["nombre"]."', Abrev = '".strtoupper($_POST["abrev"])."', Cod_Conteo = '".$_POST["cod_cont"]."', Horario = '".$_POST["horario"]."' where Id = ".$_POST["id"];
		$exc = mysqli_query($conect, $sql);
		if($exc)
		{
			echo "<br /><h4 align='center'>Registro actualizado</h4>";
			?><script language="javascript">parent.consulta.location.reload();</script><?php
		}
		else
		{
			echo "<br /><h4 align='center'>Error al actualizar el registro</h4>";
		}
	}
	else
	{
		echo "<br /><h4 align='center'>El nombre de la biblioteca ya existe</h4>";
	}
	//mysqli_free_result($exc);
	unset($sql,$exc,$row);
	mysqli_close($conect);
}
else
{
if(!isset($_SESSION['MM_Biblio_Autentic']))
{ 
include("../script/loggin.php");
?>
<center><strong>No posee privilegios para este m&oacute;dulo.<br /><br />
<a href="javascript:form();">Inicie sesi&oacute;n.</a></strong><br /><br /></center>
<?php
}
else
{
	//echo "Sesion: ".$_SESSION['MM_Biblio_Autentic'];
	$sql = "select Horario from biblioteca where Id = ".$_GET["bib"];
	$exc = mysqli_query($conect, $sql);
	$row = mysqli_fetch_array($exc);
	if($row["Horario"])
		$i = explode(",", $row["Horario"]);
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td valign="top" width="20%"><div align="right"><img src="../icon/bib.png" width="39" height="35" alt="icono" /></div></td>
    <td align="center" width="60%">
    <fieldset>
    <legend align="center"><strong>Formulario para modificar biblioteca</strong></legend>
    <div class="x_fieldset"><a href="javascript:void(0);" onclick="document.location = '../fill.php';" title="Cerrar">X</a></div>
    <form name ="formiden" method ="POST" action ="m_biblio.php" onsubmit="return busqueda();">
    <input name="horario" id="horario" type="hidden" value="<?php echo $row["Horario"] ; ?>" />
    <input name="id" id="id" type="hidden" value="<?php echo $_GET["bib"]; ?>" />
    <div align="left">
    <div align="left" style="float:left; width:400px; height:25px;">
       <strong><a href="#" onclick="menu('flotante_1','1');">Definir horario de la biblioteca</a></strong>
       <div id="flotante_1" class="flotante_1" style="display:none;" align="center">
           <div align="right" style="padding-right:5px;"><a href="#" onclick="menu('flotante_1','2');"> X </a></div>
           <div style="padding:8px;"><strong>Horario de atención de la biblioteca</strong></div>
           <div>
           <table width="100%" border="0" cellspacing="2" cellpadding="2" style="background-color:#EEE;">
              <tr>
                <td width="30%"><div align="center">Día</div></td>
                <td width="35%"><div align="center">Inicio</div></td>
                <td width="35%"><div align="center">Cierre</div></td>
              </tr>
              <tr>
                <td>Lunes</td>
                <td>
                <select name="ini_lu" id="ini_lu">
                <?php include("../script/inicio.php"); ?>
                </select>
                </td>
                <td><select name="cie_lu" id="cie_lu">
				<?php include("../script/cierre.php"); ?>
                </select></td>
              </tr>
              <tr>
                <td>Martes - Sábado</td>
                <td><select name="ini_ma" id="ini_ma">
                <?php include("../script/inicio.php"); ?>
                </select></td>
                <td><select name="cie_ma" id="cie_ma">
                <?php include("../script/cierre.php"); ?>
                </select></td>
              </tr>
              <tr>
                <td>Domingo</td>
                <td><select name="ini_do" id="ini_do">
                <?php include("../script/inicio.php"); ?>
                </select></td>
                <td><select name="cie_do" id="cie_do">
                <?php include("../script/cierre.php"); ?>
                </select></td>
              </tr>
            </table>
           </div>
       </div>
    </div>  
    </div>   
    <div align="left" style="clear:both; background-color:#EBEEE5;">
    <table width="100%" border="0" cellspacing="2" cellpadding="2">
      <tr>
        <td colspan="3"><div align="center">Horario de la biblioteca</div></td>
      </tr>
      <tr>
        <td width="33%"><div align="center">Lunes</div></td>
        <td><div align="center">Martes - Sábado</div></td>
        <td width="33%"><div align="center">Domingo</div></td>
      </tr>
      <tr>
        <td style="background-color:#FFF;">
        <?php
		if(isset($i[0]))
		{
			echo que($i[0]);
			$i[0] = explode("-", $i[0]);
		}
        ?>
		</td>
        <td style="background-color:#FFF;">
        <?php
		if(isset($i[0]))
		{
			echo que($i[1]);
			$i[1] = explode("-", $i[1]);
		}
        ?>
        </td>
        <td style="background-color:#FFF;">
        <?php
		if(isset($i[0]))
		{
			echo que($i[2]);
			$i[2] = explode("-", $i[2]);
		}
        ?>
        </td>
      </tr>
    </table>
    </div>
    <div align="left" style="clear:both; overflow:hidden; border-top-color:#000; border-top-style:solid; border-top-width:thin; margin-top:5px;">
    <div style="float:left; padding-top:5px; width:30%;"><strong>Nombre del nodo:</strong> &nbsp;</div>
    <div style="float:left; width:69%;"><input name="no_n" id="no_n" type="text" value="<?php echo $_GET["nod"]; ?>" size="50" readonly="readonly" /></div>
    </div>
    <div align="left" style="clear:both; overflow:hidden; border-top-color:#000; border-top-style:solid; border-top-width:thin; margin-top:5px;">
    <div style="float:left; padding-top:5px; width:30%;"><strong>Nombre de la biblioteca: &nbsp;</strong></div>
    <div style="float:left; width:69%;"><input name="nombre" id="nombre" type="text" size="50" value="<?php echo $_GET["nom"]; ?>" placeholder="Nombre de la biblioteca" /></div>
    </div>
    <div align="left" style="clear:both; overflow:hidden; border-top-color:#000; border-top-style:solid; border-top-width:thin; margin-top:5px;">
    <div style="float:left; padding-top:5px; width:30%;"><strong>Abreviatura de la biblioteca: &nbsp;</strong></div>
    <div style="float:left; width:69%;"><input name="abrev" id="abrev" type="text" size="50" placeholder="XXXXX" value="<?php echo $_GET["abrev"]; ?>" /></div>
    <div align="left" class="infos" style="clear:both;">Caracteres con los cuales se identifica la biblioteca en los sistemas bibliogr&aacute;fico y conteo.</div>
    </div>
    <div align="left" style="clear:both; overflow:hidden; border-top-color:#000; border-top-style:solid; border-top-width:thin; margin-top:5px;">
    <div style="float:left; padding-top:5px; width:30%;"><strong>C&oacute;digo de conteo principal: &nbsp;</strong></div>
    <div style="float:left; width:69%;"><input name="cod_cont" id="cod_cont" type="text" size="50" placeholder="0000" value="<?php echo $_GET["cod"]; ?>" /></div>
    <div align="left" class="infos" style="clear:both;">Si la biblioteca cuenta con doble portal de conteo (entrada - salida), indique los dos c&oacute;digos separados por coma, sin espacios.</div>
    </div>
    <div align="center" style="clear:both; margin-top:10px;"><input name="guardar" id="guardar" type="submit" value="Guardar" /></div>
    </form>
    <script>
		foco_in('nombre');
	</script>
    </fieldset>
    </td>
    <td width="20%">&nbsp;</td>
  </tr>
</table>
<script language="javascript"> cual('<?php echo $i[0][0]; ?>', 'ini_lu'); </script>
<script language="javascript"> cual('<?php echo $i[0][1]; ?>', 'cie_lu'); </script>
<script language="javascript"> cual('<?php echo $i[1][0]; ?>', 'ini_ma'); </script>
<script language="javascript"> cual('<?php echo $i[1][1]; ?>', 'cie_ma'); </script>
<script language="javascript"> cual('<?php echo $i[2][0]; ?>', 'ini_do'); </script>
<script language="javascript"> cual('<?php echo $i[2][1]; ?>', 'cie_do'); </script>
<?php 
}
mysqli_free_result($exc);
unset($exc, $sql, $i, $row);
mysqli_close($conect);
} 
?>
<!-- InstanceEndEditable -->
<form name="chang_elemt" id="chang_elemt" target="contenido" method="post">
<!-- InstanceBeginEditable name="campos" -->

<!-- InstanceEndEditable -->
</form>
</div></body>

<script language="javascript">
verif();
</script>

<!-- InstanceEnd --></html>