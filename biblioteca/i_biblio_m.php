<?php
require_once("../Connections/conect.inc.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/contenedor.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php
include("../script/scripts/session.php");
//include("../script/breadcrumbs.php");
?>
<link href="../css/css.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../script/scripts/jquery.min.js"></script>
<script type="text/javascript" src="../script/menu_ocultar.js"></script>
<script type="text/javascript">
function verif() {
document.getElementById('datos').style.display='none';
}
function foco_in(z) {
document.getElementById(z).focus();
}
</script>
<title>.: Sistema general de estad&iacute;stica :.</title>
<link rel="shortcut icon" href="../img/favicon.ico" />
<!-- InstanceBeginEditable name="doctitle" -->
<script src="../script/c_color.js"></script>
<script language="javascript">
var obj;
function bg_color(bg) {
obj = document.getElementById(bg);
obj.style.background="#E53C67";
}
function bg_blanco(bg) {
obj = document.getElementById(bg);
obj.style.background="#FFFFFF";
}
function busqueda() {
var err,val;
err = "";
for(i=1; i<=6; i++)
{
	bg_blanco("b_d_"+i);
}
val = document.getElementById("t_d_1").value;
if(val < 0 || /^\s+$/.test(val) || val.length == 0 || isNaN(val))
{
	err += "Se requiere el área de la biblioteca. \n";
	bg_color("b_d_1");
}
val = document.getElementById("t_d_2").value;
if(val < 0 || /^\s+$/.test(val) || val.length == 0 || isNaN(val))
{
	err += "Se requiere el número de salas. \n";
	bg_color("b_d_2");
}
val = document.getElementById("t_d_3").value;
if(val < 0 || /^\s+$/.test(val) || val.length == 0 || isNaN(val))
{
	err += "Se requiere el número de Puestos de lectura. \n";
	bg_color("b_d_3");
}
val = document.getElementById("t_d_4").value;
if(val < 0 || /^\s+$/.test(val) || val.length == 0 || isNaN(val))
{
	err += "Se requiere la planta de personal. \n";
	bg_color("b_d_4");
}
val = document.getElementById("t_d_5").value;
if(val < 0 || /^\s+$/.test(val) || val.length == 0 || isNaN(val))
{
	err += "Se requiere el número de computadores. \n";
	bg_color("b_d_5");
}
val = document.getElementById("t_d_6").value;
if(val < 0 || /^\s+$/.test(val) || val.length == 0 || isNaN(val))
{
	err += "Se requiere el número de tabletas. \n";
	bg_color("b_d_6");
}
if(err.length > 0)
{
	alert("Verifique los siguientes errores: \n\n"+err+"\n");
	return false;
}
else
	return true;
}
</script>
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" --><!-- InstanceEndEditable -->
</head>
<body>
<div id="datos" align="center">
 <h4>Consolidando datos...<br />
 <img src="../img/loader.gif" width="16" height="16" /></h4>
 </div>
<div align="left"><?php //echo breadcrumbs(); ?></div>
<div align="center" class="popup">
<!-- InstanceBeginEditable name="contenidos" -->
<?php
if(isset($_POST["id"],$_POST["t_d_1"],$_POST["t_d_5"],$_POST["guardar"],$_POST["bib"]))
{
	include("../script/cant.php");
	if(cant("dat_bib","Biblioteca",$_POST["bib"]) == 0) //tabla, campo, dato
	{
		$sql = "insert into dat_bib (Id, Biblioteca, Area, P_Lectura, Computadores, Tabletas, Personal) VALUES ('', '".$_POST["bib"]."', '".$_POST["t_d_1"]."', '".$_POST["t_d_3"]."', '".$_POST["t_d_5"]."', '".$_POST["t_d_6"]."', '".$_POST["t_d_4"]."')";
		$exc = mysqli_query($conect, $sql);
	}
	else
	{
		$sql = "update dat_bib set Area = '".$_POST["t_d_1"]."', P_Lectura = '".$_POST["t_d_3"]."', Computadores = '".$_POST["t_d_5"]."', Tabletas = '".$_POST["t_d_6"]."', Personal = '".$_POST["t_d_4"]."' where Biblioteca = ".$_POST["bib"];
		$exc = mysqli_query($conect, $sql);
	}
	if($exc)
	{
		echo "<h4 align='center'>Registro actualizado</h4>";
		?><script language="javascript">
		//document.location = "i_biblio.php?bib=<?php echo $_POST['bib'].'&nom='.$_POST['nombre']; ?>";
		opener.location.reload();
		window.close(); 
        </script><?php
	}
}
if(isset($_GET["bib"], $_GET["nom"]))
{ 
$sql = "select dat_bib.Area, dat_bib.P_Lectura, dat_bib.Computadores, dat_bib.Tabletas, dat_bib.Personal, count(salas.Id) as Salas from dat_bib, salas where dat_bib.Biblioteca = ".$_GET["bib"]." and salas.Biblioteca = dat_bib.Biblioteca";
$exc = mysqli_query($conect, $sql);
$row = mysqli_fetch_array($exc);
?>
<div align="left"><strong>Informacion de la biblioteca:</strong> <?php echo $_GET["nom"]; ?></div>
<div align="center">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="5%">&nbsp;</td>
    <td align="center">
    <form name ="formiden" method ="POST" action ="i_biblio_m.php" onsubmit="return busqueda();">
    <input name="id" id="id" type="hidden" value="<?php echo $row["Id"]; ?>" />
    <input name="bib" id="bib" type="hidden" value="<?php echo $_GET["bib"]; ?>" />
    <input name="nombre" id="nombre" type="hidden" value="<?php echo $_GET["nom"]; ?>" />
    <table width="90%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="2%" align="left"><img src="../icon/area.png" width="36" height="35" alt="icono" /></td>
        <td width="33%" align="left">Metros cuadrados</td>
        <td width="13%" align="left" id="b_d_1"><input type="text" name="t_d_1" id="t_d_1" size="10" value="<?php echo $row["Area"]; ?>" /></td>
        <td width="4%" align="left">&nbsp;</td>
        <td width="2%" align="left"><img src="../icon/sala.png" width="39" height="35" alt="icono" /></td>
        <td width="33%" align="left">Número de salas  o espacios de uso</td>
        <td width="13%" align="left" id="b_d_2"><input type="text" name="t_d_2" id="t_d_2" size="10" value="<?php echo $row["Salas"]; ?>" readonly="readonly" /></td>
      </tr>
      <tr>
        <td align="left"><img src="../icon/helpdesk.png" width="40" height="35" alt="icono" /></td>
        <td align="left">Número de Puestos de  lectura</td>
        <td align="left" id="b_d_3"><input type="text" name="t_d_3" id="t_d_3" size="10" value="<?php echo $row["P_Lectura"]; ?>" /></td>
        <td align="left">&nbsp;</td>
        <td align="left"><img src="../icon/users.png" width="36" height="35" alt="icono" /></td>
        <td align="left">Planta  de personal</td>
        <td align="left" id="b_d_4"><input type="text" name="t_d_4" id="t_d_4" size="10" value="<?php echo $row["Personal"]; ?>" /></td>
      </tr>
      <tr>
        <td align="left"><img src="../icon/pc.png" width="40" height="35" alt="icono" /></td>
        <td align="left">Número de computadores</td>
        <td align="left" id="b_d_5"><input type="text" name="t_d_5" id="t_d_5" size="10" value="<?php echo $row["Computadores"]; ?>" /></td>
        <td align="left">&nbsp;</td>
        <td align="left"><img src="../icon/tablet.png" width="30" height="35" alt="icono" /></td>
        <td align="left">Número  de tabletas</td>
        <td align="left" id="b_d_6"><input type="text" name="t_d_6" id="t_d_6" size="10" value="<?php echo $row["Tabletas"]; ?>" /></td>
      </tr>
      <tr>
        <td colspan="7"><div align="center">
          <input type="submit" name="guardar" id="guardar" value="Guardar" />
        </div></td>
        </tr>
    </table>
    </form>
    <script>
		foco_in('t_d_1');
	</script>
    </td>
    <td width="5%">&nbsp;</td>
  </tr>
</table>
</div>
<?php } ?>
<!-- InstanceEndEditable -->
<form name="chang_elemt" id="chang_elemt" target="contenido" method="post">
<!-- InstanceBeginEditable name="campos" -->

<!-- InstanceEndEditable -->
</form>
</div></body>

<script language="javascript">
verif();
</script>

<!-- InstanceEnd --></html>