<?php include "../script/breadcrumbs.php"; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/contenedor.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php
include("../script/scripts/session.php");
//include("../script/breadcrumbs.php");
?>
<link href="../css/css.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../script/scripts/jquery.min.js"></script>
<script type="text/javascript" src="../script/menu_ocultar.js"></script>
<script type="text/javascript">
function verif() {
document.getElementById('datos').style.display='none';
}
function foco_in(z) {
document.getElementById(z).focus();
}
</script>
<title>.: Sistema general de estad&iacute;stica :.</title>
<link rel="shortcut icon" href="../img/favicon.ico" />
<!-- InstanceBeginEditable name="doctitle" -->
<script language="javascript" src="../script/datosxml.js"></script>
<script type="text/javascript" src="../script/scripts/jquery.min.js"></script>
<script type="text/javascript" src="../script/menu_ocultar.js"></script>
<script language="javascript">
var err = "";
function predataserver(w,x,y) {
  http.open("GET", "../script/datosxml.php?tab="+w+"&camp="+x+"&dat="+y, false); //False para que no continue el flujo hasta compeltar la petición ajax
  http.onreadystatechange = useHttpResp;
  return http.send(null);
}

function useHttpResp() {
if(http.readyState == 4)
{
	if(http.status == 200)
	{
		var timeValue = http.responseXML.getElementsByTagName("timenow")[0];
		if(parseInt(timeValue.childNodes[0].nodeValue) > 0)
		{
			err += "El nombre de la sala ya existe. \n";
			document.getElementById('no_send').innerHTML = "El nombre de la sala ya existe.";
		}
		verif();
	}
} 
else
{
	document.getElementById('datos').style.display = "";
}
}

function slect_mat() {
if(document.getElementById("material").checked == true)
{
	document.getElementById("tip-colec").style.display = "";
}
else
{
	document.getElementById("tip-colec").style.display = "none";
}
}
function cambi(w, x) {
	if(document.getElementById(w).checked == true)
		document.getElementById(w).value = x;
	else
		document.getElementById(w).value = '0';
}
function busqueda() {
var val;
err = "";
val = document.getElementById("nombre").value;
if(val == null || !isNaN(val) || val.length < 3 || /^\s+$/.test(val))
	err += "Se requiere el nombre de la sala. \n";
else	
	predataserver('salas','Nombre',document.getElementById("nombre").value+"' and Biblioteca = '"+document.getElementById("id_b").value) //tabla, campo, dato
val = document.getElementById("p_lec").value;
if(val == null || isNaN(val) || val < 0 || val.length <= 0 || /^\s+$/.test(val))
	err += "Se requiere los puestos de lectura. \n";
if(document.getElementById("material").checked)
	document.getElementById("consulta").value = 1;
else
	document.getElementById("consulta").value = 0;
if(document.getElementById("material").checked == true)
{
	if(document.getElementById("colec").selectedIndex == 0)
		err += "Se requiere el tipo de colección. \n";
}
if(document.getElementById("principal").checked == true)
{
	val = document.getElementById("cod_cont").value;
	if(val == null || /^\s+$/.test(val))
		err += "Se requiere el código de conteo. \n";
}
if(err.length > 0)
{
	alert("Verifique los siguientes errores: \n\n"+err+"\n");
	return false;
}
else
	return true;
}
</script>
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" --><!-- InstanceEndEditable -->
</head>
<body>
<div id="datos" align="center">
 <h4>Consolidando datos...<br />
 <img src="../img/loader.gif" width="16" height="16" /></h4>
 </div>
<div align="left"><?php //echo breadcrumbs(); ?></div>
<div align="center" class="popup">
<!-- InstanceBeginEditable name="contenidos" -->
<?php
require_once("../Connections/conect.inc.php");
if(isset($_POST["nombre"], $_POST["guardar"], $_POST["id_b"]))
{
	if(!isset($_POST["principal"]))
		$_POST["principal"] = 0;
	{
		if(isset($_SESSION['MM_Biblio_Autentic']))
			$sql = "insert into salas (Id, Sub, Biblioteca, Nombre, Consulta, Coleccion, Cod_Conteo, Sum_Conteo, P_Lectura, Descripcion) VALUES ('', '".$_POST["sub"]."', '".$_POST["id_b"]."', '".$_POST["nombre"]."', '".$_POST["consulta"]."', '".$_POST["colec"]."', '".$_POST["cod_cont"]."', '".$_POST["principal"]."', '".$_POST["p_lec"]."', '".addslashes($_POST["descrp"])."')";
		else
			$sql = "insert into salas (Id, Sub, Biblioteca, Nombre, Consulta, Coleccion, Cod_Conteo, Sum_Conteo, Descripcion) VALUES ('', '".$_POST["sub"]."', '".$_POST["id_b"]."', '".$_POST["nombre"]."', '".$_POST["consulta"]."', '".$_POST["colec"]."', '0', '".$_POST["principal"]."', '".$_POST["descrp"]."')";
		$exc = mysqli_query($conect, $sql);
		if($exc)
		{
			echo "<h4 align='center'>Registro creado</h4>";
			?><script language="javascript">parent.consulta.location.reload();</script><?php
		}
		else
		{
			echo "<h4 align='center'>Error al crear el registro</h4>";
		}
	}
	/*else
	{
		echo "<h4 align='center'>El nombre de la sala ya existe</h4>";
	}*/
	
}
else
{
if(!isset($_SESSION['MM_Biblio_Autentic']))
{ 
include("../script/loggin.php");
?>
<center><strong>No posee privilegios para este m&oacute;dulo.<br /><br />
<a href="javascript:form();">Inicie sesi&oacute;n.</a></strong><br /><br /></center>
<?php
}
else
{
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="15%" valign="top"><div align="right"><img src="../icon/salas.png" width="39" height="35" alt="icono" /></div></td>
    <td width="70%" align="center">
    <div align="center"><strong>Formulario para ingresar sala o espacio al sistema</strong>
    <div class="x_fieldset"><a href="javascript:void(0);" onclick="document.location = '../fill.php';" title="Cerrar">X</a></div></div>
    <form name ="formiden" method ="POST" action ="n_salas.php?bib=<?php echo $_GET["bib"]."&nom=".$_GET["nom"]; ?>" onsubmit="return busqueda();">
    <input name="id_b" id="id_b" type="hidden" value="<?php echo $_GET["bib"]; ?>" />
    <input name="bi_n" id="bi_n" type="hidden" value="<?php echo $_GET["nom"]; ?>" />
    <input name="consulta" id="consulta" type="hidden" value="" />
    <div align="left"><strong>Nombre de la sala o espacio:</strong></div>
    <div align="left"><input name="nombre" id="nombre" type="text" size="50" placeholder="Nombre de la sala o espacio" /></div>
    <div>
    <div align="left" style="width:49%; float:left;">
    <div align="left"><strong>Espacio principal:</strong></div>
    <div align="left">
    <select name ="sub" id="sub">
    	<option value="0" selected="selected">Seleccione el espacio principal</option>
    <?php
	$sql = "select Id, Nombre from salas where Biblioteca = ".$_GET["bib"]." and Sub = 0";
	$exc = mysqli_query($conect, $sql);
	for($i=0; $i<mysqli_num_rows($exc); $i++)
	{ 
		$row = mysqli_fetch_array($exc);
	?>	
    	<option value="<?php echo $row["Id"]; ?>"><?php echo $row["Nombre"]; ?></option>
	<?php } ?>
    </select>
    </div>
    <div class="infos">Si el espacio est&aacute; dentro de otro, indique cual, requerido para el conteo de visitantes</div></div>
    <div align="left" style="width:49%; float:right;">
        <div align="left"><strong>N&uacute;mero de puestos de lectura:</strong></div>
        <div align="left"><input name="p_lec" id="p_lec" type="text" size="50" placeholder="0000" value="0" /></div>
        <div class="infos">Si el espacio no posee puestos de lectura deje, se asume '0'</div>
        </div>
    </div>
    <div align="left" style="clear:both;"><strong>Descripci&oacute;n de la sala o espacio:</strong></div>
    <div align="left"><textarea name="descrp" id="descrp" rows="2"></textarea>
    <div class="infos">M&aacute;ximo 250 caracteres</div>
    </div>
    <div align="left" style="background-color:#EBEEE5; padding-top:5px; padding-left:5px; border-radius:3px; overflow:hidden; height:25px;">
    <div align="left" style="width:49%; float:left;"><input name="material" id="material" type="checkbox" value="0" onclick="slect_mat();" />
    &nbsp; <strong>La sala o espacio cuenta con material de consulta.</strong></div>
    <div align="right" style="width:49%; float:right; display:none;" id="tip-colec">
    <select name ="colec" id="colec">
    	<option value="0" selected="selected">Seleccione el tipo de colecci&oacute;n</option>
    <?php
	$sql = "select Coleccion, Iden from coleccion";
	$excs = mysqli_query($conect, $sql);
	for($i=0; $i<mysqli_num_rows($excs); $i++)
	{ 
		$rows = mysqli_fetch_array($excs);
	?>	
    	<option value="<?php echo $rows["Iden"]; ?>"><?php echo $rows["Coleccion"]; ?></option>
	<?php } ?>
    </select>
    </div></div>
    <?php
	if(isset($_SESSION['MM_Biblio_Autentic']))
	{ ?>
		<div align="left"><strong>C&oacute;digo de conteo la sala o espacio:</strong></div>
		<div align="left" style="overflow:hidden;">
        <div align="left" style="width:49%; float:left;">
        <div><input name="cod_cont" id="cod_cont" type="text" size="50" placeholder="0000" /></div>
        <div align="left" class="infos">Si el espacio cuenta con doble portal de conteo (entrada - salida), indique los dos c&oacute;digos separados por coma, sin espacios.</div>
        </div>
        <div align="left" style="width:49%; float:right;">
        <div style="padding-top:5px; border: #777 solid 1px; height:18px;"><input name="principal" id="principal" type="checkbox" value="0" onclick="cambi('principal', 1)" />
        &nbsp; <strong>Punto de conteo principal.</strong></div>
        <div align="left" class="infos">Si el espacio es independiente y se lleva el registro de ingreso ya sea automático o manual.</div>
        </div>
        </div>
        
	<?php } ?>
    <div align="center" style="clear:both;"><input name="guardar" id="guardar" type="submit" value="Guardar" /></div>
    </form>
    <script>
		foco_in('nombre');
	</script>
    </td>
    <td width="15%">&nbsp;</td>
  </tr>
</table>
<?php
} 
@ mysqli_free_result($exc);
unset($sql,$exc,$row,$claus);
mysqli_close($conect);
}
?>
<div align="justify" id="db_guardar">&nbsp; Salas o espacios  de la biblioteca que pueden ser utilizados por los usuarios
<div class="div_menu" id="aa_1"><a href="javascript:void(0);" onclick="menu('a_b_','1'); mostrar('bb','1');" title="Ayuda">? +</a></div>
<div class="div_menu" style="display:none;" id="bb_1"><a href="javascript:void(0);" onclick="menu('a_b_','2'); mostrar('aa','1');" title="Ayuda">? -</a></div>
<div class="div_ayuda" id="a_b_" style="display:none;">Espacio principal, cuando la sala que se está creando está dentro de otro espacio se requiere para identificar la cantidad de usuarios o visitantes. Si la sala o espacio cuenta con material de consulta, seleccione la casilla de verificación, por ejemplo, una sala de tareas no cuenta con material, pero es utilizada por los usuarios y al no estar dentro de otro espacio, puede ser un punto de conteo principal.</div></div>
<!-- InstanceEndEditable -->
<form name="chang_elemt" id="chang_elemt" target="contenido" method="post">
<!-- InstanceBeginEditable name="campos" -->

<!-- InstanceEndEditable -->
</form>
</div></body>

<script language="javascript">
verif();
</script>

<!-- InstanceEnd --></html>