<?php
require_once("../Connections/conect.inc.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/contenedor.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php
include("../script/scripts/session.php");
//include("../script/breadcrumbs.php");
?>
<link href="../css/css.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../script/scripts/jquery.min.js"></script>
<script type="text/javascript" src="../script/menu_ocultar.js"></script>
<script type="text/javascript">
function verif() {
document.getElementById('datos').style.display='none';
}
function foco_in(z) {
document.getElementById(z).focus();
}
</script>
<title>.: Sistema general de estad&iacute;stica :.</title>
<link rel="shortcut icon" href="../img/favicon.ico" />
<!-- InstanceBeginEditable name="doctitle" -->
<script src="../script/c_color.js"></script>
<script language="javascript">
var obj;
function bg_color(bg) {
obj = document.getElementById(bg);
obj.style.background="#E53C67";
}
function bg_blanco(bg) {
obj = document.getElementById(bg);
obj.style.background="#FFFFFF";
}
function busqueda() {
var err,val;
err = "";
for(i=1; i<=4; i++)
{
	bg_blanco("b_d_"+i);
}
val = document.getElementById("t_d_1").value;
if(val < 0 || /^\s+$/.test(val) || val.length == 0 || isNaN(val))
{
	err += "Se requieren los días de servicio al mes. \n";
	bg_color("b_d_1");
}
val = document.getElementById("t_d_2").value;
if(val < 0 || /^\s+$/.test(val) || val.length == 0 || isNaN(val))
{
	err += "Se requieren las horas de servicio al mes. \n";
	bg_color("b_d_2");
}
val = document.getElementById("t_d_3").value;
if(val < 0 || /^\s+$/.test(val) || val.length == 0 || isNaN(val))
{
	err += "Se requieren la colección general. \n";
	bg_color("b_d_3");
}
val = document.getElementById("t_d_4").value;
if(val < 0 || /^\s+$/.test(val) || val.length == 0 || isNaN(val))
{
	err += "Se requieren la colección infantil. \n";
	bg_color("b_d_4");
}
val = document.getElementById("descrp").value;
if(val == null || val.length < 20 || /^\s+$/.test(val))
	err += "Se requiere las bservaciones. \n";
if(err.length > 0)
{
	alert("Verifique los siguientes errores: \n\n"+err+"\n");
	return false;
}
else
	return true;
}
</script>
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" --><!-- InstanceEndEditable -->
</head>
<body>
<div id="datos" align="center">
 <h4>Consolidando datos...<br />
 <img src="../img/loader.gif" width="16" height="16" /></h4>
 </div>
<div align="left"><?php //echo breadcrumbs(); ?></div>
<div align="center" class="popup">
<!-- InstanceBeginEditable name="contenidos" -->
<?php
if(isset($_POST["id"],$_POST["t_d_1"],$_POST["t_d_2"],$_POST["guardar"],$_POST["bib"]))
{
	include("../script/cant.php");
	$claus = date("n/Y")."' and Biblioteca = '".$_POST["bib"];
	if(cant("info_men_bib","Fecha",$claus) == 0) //tabla, campo, dato
	{
		$sql = "insert into info_men_bib (Id, Biblioteca, Fecha, D_Servicio, H_Servicio, coleccion_G, coleccion_I, Observacion) VALUES ('', '".$_POST["bib"]."', '".date("n/Y")."', '".$_POST["t_d_1"]."', '".$_POST["t_d_2"]."', '".$_POST["t_d_4"]."', '".$_POST["t_d_3"]."', '".$_POST["descrp"]."')";
		$exc = mysqli_query($conect, $sql);
	}
	else
	{
		$sql = "update info_men_bib set D_Servicio = '".$_POST["t_d_1"]."', H_Servicio = '".$_POST["t_d_2"]."', Coleccion_G = '".$_POST["t_d_4"]."', Coleccion_I = '".$_POST["t_d_3"]."', Observacion = '".$_POST["descrp"]."' where Id = ".$_POST["id"];
		$exc = mysqli_query($conect, $sql);
	}
	if($exc)
	{
		echo "<h4 align='center'>Registro actualizado</h4>";
		?><script language="javascript">
		//document.location = "i_biblio.php?bib=<?php echo $_POST['bib'].'&nom='.$_POST['nombre']; ?>";
		opener.location.reload();
		window.close(); 
        </script><?php
	}
}
if(isset($_GET["bib"], $_GET["nom"]))
{ 
$sql = "select Id, D_Servicio, H_Servicio, Coleccion_G, Coleccion_I, Observacion from info_men_bib where Biblioteca = ".$_GET["bib"]." and Fecha = '".date("n/Y")."'";
$exc = mysqli_query($conect, $sql);
$row = mysqli_fetch_array($exc);
?>
<p align="left"><strong>Informacion de la biblioteca:</strong> <?php echo $_GET["nom"]; ?></p>
<div align="center">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
  	<td colspan="7"><strong>Informaci&oacute;n del mes: <?php echo date("n / Y"); ?></strong></td>
  <tr>
  <tr>
    <td width="5%">&nbsp;</td>
    <td align="center">
    <form name ="formiden" method ="POST" action ="i_biblio_men_m.php" onsubmit="return busqueda();">
    <input name="id" id="id" type="hidden" value="<?php echo $row["Id"]; ?>" />
    <input name="bib" id="bib" type="hidden" value="<?php echo $_GET["bib"]; ?>" />
    <input name="nombre" id="nombre" type="hidden" value="<?php echo $_GET["nom"]; ?>" />
    <table width="90%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="2%" align="left"><img src="../icon/calendar.png" width="35" height="35" alt="icono" /></td>
        <td width="33%" align="left">D&iacute;as de servicio al mes</td>
        <td width="13%" align="left" id="b_d_1"><input type="text" name="t_d_1" id="t_d_1" size="10" value="<?php echo $row["D_Servicio"]; ?>" /></td>
        <td width="4%" align="left">&nbsp;</td>
        <td width="2%" align="left"><img src="../icon/horas.png" width="32" height="35" alt="icono" /></td>
        <td width="33%" align="left">Horas de servicio al mes</td>
        <td width="13%" align="left" id="b_d_2"><input type="text" name="t_d_2" id="t_d_2" size="10" value="<?php echo $row["H_Servicio"]; ?>" /></td>
      </tr>
      <tr>
        <td align="left"><img src="../icon/rapports.png" width="38" height="35" alt="icono" /></td>
        <td align="left">Colecci&oacute;n general</td>
        <td align="left" id="b_d_3"><input type="text" name="t_d_4" id="t_d_4" size="10" value="<?php echo $row["Coleccion_G"]; ?>" /></td>
        <td align="left">&nbsp;</td>
        <td align="left"><img src="../icon/colecc_i.png" width="38" height="35" alt="icono" /></td>
        <td align="left">Colecci&oacute;n infantil</td>
        <td align="left" id="b_d_4"><input type="text" name="t_d_3" id="t_d_3" size="10" value="<?php echo $row["Coleccion_I"]; ?>" /></td>
      </tr>
      <tr>
        <td align="left"><img src="../icon/observaciones.png" width="37" height="35" alt="icono" /></td>
        <td align="left" colspan="6">Observaciones por afectación en la prestación del servicio:
        <textarea name="descrp" id="descrp" rows="3"><?php echo $row["Observacion"]; ?></textarea>
        </td>
        
      </tr>
      <tr>
        <td colspan="7"><div align="center">
          <input type="submit" name="guardar" id="guardar" value="Guardar" />
        </div></td>
        </tr>
    </table>
    </form>
    <script>
		foco_in('t_d_1');
	</script>
    </td>
    <td width="5%">&nbsp;</td>
  </tr>
</table>
</div>
<?php } ?>
<!-- InstanceEndEditable -->
<form name="chang_elemt" id="chang_elemt" target="contenido" method="post">
<!-- InstanceBeginEditable name="campos" -->

<!-- InstanceEndEditable -->
</form>
</div></body>

<script language="javascript">
verif();
</script>

<!-- InstanceEnd --></html>