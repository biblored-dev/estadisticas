<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/contenedor.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php
include("../script/scripts/session.php");
//include("../script/breadcrumbs.php");
?>
<link href="../css/css.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../script/scripts/jquery.min.js"></script>
<script type="text/javascript" src="../script/menu_ocultar.js"></script>
<script type="text/javascript">
function verif() {
document.getElementById('datos').style.display='none';
}
function foco_in(z) {
document.getElementById(z).focus();
}
</script>
<title>.: Sistema general de estad&iacute;stica :.</title>
<link rel="shortcut icon" href="../img/favicon.ico" />
<!-- InstanceBeginEditable name="doctitle" -->
<link href="../menu/css/css.css" rel="stylesheet" type="text/css" />
<script src="../script/c_color.js"></script>
<script language="javascript">
function renombrar(w,x,y,z, zx) {
if(document.getElementById("Est_"+zx).checked == true)
{	
	var alias = "";	
	if(alias = prompt(w+":\n"+x, x))
	{
		alias = alias.toLowerCase();
		if(z == 1)
			alias = alias.charAt(0).toUpperCase() + alias.slice(1);
		else
			alias = alias.toUpperCase();
		if(alias != null && alias != x)
			document.getElementById(y).innerHTML = alias;
		else
			alert("Nombre no válido.");
	}
}
else
	alert("Registro desactivado");
}

function cambiar(w, x) {
	if(document.getElementById(w).checked == true)
	{
		document.getElementById("estado_"+x).innerHTML = "&nbsp; Activo";
		document.getElementById("Co_"+x).disabled = false;
		document.getElementById("Id_"+x).disabled = false;
	}
	else
	{
		document.getElementById("estado_"+x).innerHTML = "Inactivo";
		document.getElementById("Co_"+x).disabled = true;
		document.getElementById("Id_"+x).disabled = true;
	}
}
</script>
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" --><!-- InstanceEndEditable -->
</head>
<body>
<div id="datos" align="center">
 <h4>Consolidando datos...<br />
 <img src="../img/loader.gif" width="16" height="16" /></h4>
 </div>
<div align="left"><?php //echo breadcrumbs(); ?></div>
<div align="center" class="popup">
<!-- InstanceBeginEditable name="contenidos" -->
<?php
require_once("../Connections/conect.inc.php");
$tipos = array(0=>"Inactivo",1=>"&nbsp; Activo");
$sql = "select * from  coleccion";
$exc = mysqli_query($conect, $sql);
?>
<div style="width:80%; margin:0px auto;">
<table width="100%" border="1" cellspacing="2" cellpadding="2" style="border:#000 solid 1px;" id="area">
  <tr>
    <td width="5%"><div align="center" style="background-color:#EEE;"><strong>Item</strong></div></td>
    <td width="30%"><div align="center" style="background-color:#EEE;"><strong>Colecci&oacute;n</strong></div></td>
    <td width="20%"><div align="center" style="background-color:#EEE;"><strong>Clasificaci&oacute;n</strong></div></td>
    <td width="20%"><div align="center" style="background-color:#EEE;"><strong>Estado</strong></div></td>
    <td width="25%"><div align="center" style="background-color:#EEE;"><strong>Opciones</strong></div></td>
  </tr>
  <?php
  for($i=1; $i<=mysqli_num_rows($exc); $i++)
  { 
  	$row = mysqli_fetch_array($exc);
  ?>
  <tr onclick="n_color('<?php echo ($i); ?>','area');">
    <td><div align="center"><?php echo $i; ?></div></td>
    <td><div align="center"><a href="javascript:void(0);" id="Co_<?php echo $i; ?>" onclick="renombrar('Cambiar nombre de la colección', '<?php echo $row["Coleccion"]; ?>', 'Co_<?php echo $i; ?>', '1', '<?php echo $i; ?>');"><?php echo $row["Coleccion"]; ?></a></div></td>
    <td><div align="center"><a href="javascript:void(0);" id="Id_<?php echo $i; ?>" onclick="renombrar('Cambiar nombre de la clasificación', '<?php echo $row["Iden"]; ?>', 'Id_<?php echo $i; ?>', '2', '<?php echo $i; ?>');"><?php echo $row["Iden"]; ?></a></div></td>
    <td id="Es_<?php echo $i; ?>"><div align="center">
    <input name="Est_<?php echo $i; ?>" id="Est_<?php echo $i; ?>" type="checkbox" value="<?php echo $row["Estado"]; ?>" <?php if($row["Estado"] == 1) echo "checked='checked'"; ?> onclick="cambiar('Est_<?php echo $i; ?>', '<?php echo $i; ?>');" />
	<span id="estado_<?php echo $i; ?>"><?php echo $tipos[$row["Estado"]]; ?></span></div></td>
    <td><div id="left_men">
    <ul>
        <li><a href="javascript:void(0);">Opciones</a>
        <ul>
        	<li><a href="javascript:void(0);" onclick="">Actualizar</a></li>
            <li><a href="javascript:void(0);" onclick="">Eliminar</a></li>
        </ul>
    </ul>
    </div></td>
  </tr>
  <?php } ?>
</table>
</div>
<div align="justify" id="db_guardar">&nbsp; <!--Insertar mensaje de ayuda para la página -->
<div class="div_menu" id="aa_1"><a href="javascript:void(0);" onclick="menu('a_b_','1'); mostrar('bb','1');" title="Ayuda">? +</a></div>
<div class="div_menu" style="display:none;" id="bb_1"><a href="javascript:void(0);" onclick="menu('a_b_','2'); mostrar('aa','1');" title="Ayuda">? -</a></div>
<div class="div_ayuda" id="a_b_" style="display:none;">Haga clic sobre cada texto para modificarlo, puede desactivar o activar una colección cambiando el estado de la casilla de verificación y actualizando el registro en el menú “Opciones” o desde este mismo eliminar el registro.</div></div>
<!-- InstanceEndEditable -->
<form name="chang_elemt" id="chang_elemt" target="contenido" method="post">
<!-- InstanceBeginEditable name="campos" -->

<!-- InstanceEndEditable -->
</form>
</div></body>

<script language="javascript">
verif();
</script>

<!-- InstanceEnd --></html>