<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/contenedor.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php
include("../script/scripts/session.php");
//include("../script/breadcrumbs.php");
?>
<link href="../css/css.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../script/scripts/jquery.min.js"></script>
<script type="text/javascript" src="../script/menu_ocultar.js"></script>
<script type="text/javascript">
function verif() {
document.getElementById('datos').style.display='none';
}
function foco_in(z) {
document.getElementById(z).focus();
}
</script>
<title>.: Sistema general de estad&iacute;stica :.</title>
<link rel="shortcut icon" href="../img/favicon.ico" />
<!-- InstanceBeginEditable name="doctitle" -->

<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" --><!-- InstanceEndEditable -->
</head>
<body>
<div id="datos" align="center">
 <h4>Consolidando datos...<br />
 <img src="../img/loader.gif" width="16" height="16" /></h4>
 </div>
<div align="left"><?php //echo breadcrumbs(); ?></div>
<div align="center" class="popup">
<!-- InstanceBeginEditable name="contenidos" -->
<?php
require_once("../Connections/conect.inc.php");
$connect = mysqli_connect("localhost", "administrador", "123456", "estadisticasant") or die("Error de conexi&oacute;n " . mysqli_error($conect)); 

/*$sql = "SELECT Id, Biblioteca, Reporte FROM programacion ORDER BY Id ASC";
$exc = mysqli_query($conect, $sql);
$a=1;
for($i=0; $i<mysqli_num_rows($exc); $i++)
{ 
	$row = mysqli_fetch_array($exc);
	$sql = "SELECT Id FROM programacion where Fecha = '9/2017' and Reporte = ".$row['Reporte']." and Biblioteca = ".$row['Biblioteca'];
	$excs = mysqli_query($connect, $sql);
	
	if(mysqli_num_rows($excs) > 0)
	{
		$rows = mysqli_fetch_array($excs);
		echo $rows['Id'].",";
		//$a ++;
	}
}
*/
if(isset($_POST["Programanacion"]))
{
	$cod = array(0=>0,1=>0);
	//Id, Biblioteca, Reporte, Sub, Sesiones, Fecha, Publico, Observacion
	$sql = "SELECT * FROM programacion ORDER BY Id ASC";
	$exc = mysqli_query($conect, $sql);
	for($i=0; $i<mysqli_num_rows($exc); $i++)
	{ 
		$row = mysqli_fetch_array($exc);
		$sql = "insert into programacion (Biblioteca, Reporte, Sub, Sesiones, Fecha, Publico, Observacion) VALUES ('".$row['Biblioteca']."', '".$row['Reporte']."', '".$row['Sub']."', '".$row['Sesiones']."', '".$row['Fecha']."', '".$row['Publico']."', '".$row['Observacion']."')";
		$excs = mysqli_query($connect, $sql);
		$id = mysqli_insert_id($connect);
		$sql = "update estadistica set Id_Report = ".$id." where Id_Report = ".$row['Id'];
		$ex = mysqli_query($conect, $sql);
		$sql = "update afiliaciones set Id_Report = ".$id." where Id_Report = ".$row['Id'];
		$ex = mysqli_query($conect, $sql);
		$sql = "update consolidado set Id_Report = ".$id." where Id_Report = ".$row['Id'];
		$ex = mysqli_query($conect, $sql);
		$sql = "update consultas set Id_Report = ".$id." where Id_Report = ".$row['Id'];
		$ex = mysqli_query($conect, $sql);
		$sql = "update prestamos set Id_Report = ".$id." where Id_Report = ".$row['Id'];
		$ex = mysqli_query($conect, $sql);
		$sql = "update visitas set Id_Report = ".$id." where Id_Report = ".$row['Id'];
		$ex = mysqli_query($conect, $sql);
		$sql = "update prog_alias set Reporte = ".$id." where Reporte = ".$row['Id'];
		$ex = mysqli_query($conect, $sql);
	}
	echo "<h3 align='center'>Normalizando de registros de la programación</h3>";
}
if(isset($_POST["Reportes"]))
{
	
	$sql = "SELECT * FROM estadistica ORDER BY Id ASC";
	$exc = mysqli_query($conect, $sql);
	for($i=0; $i<mysqli_num_rows($exc); $i++)
	{ 
		$row = mysqli_fetch_array($exc);
		$sql = "insert into estadistica (Fecha, Biblioteca, Hora, Id_Report, Participantes, Responsable, Franja, Tipo, Actividad, Alcance, Descripcion, Fecha_R) VALUES ('".$row['Fecha']."', '".$row['Biblioteca']."', '".$row['Hora']."', '".$row['Id_Report']."', '".$row['Participantes']."', '".$row['Responsable']."', '".$row['Franja']."', '".$row['Tipo']."', '".$row['Actividad']."', '".$row['Alcance']."', '".$row['Descripcion']."', '".$row['Fecha_R']."')";
		$excs = mysqli_query($connect, $sql);
	}
	
	$sql = "SELECT * FROM afiliaciones ORDER BY Id ASC";
	$exc = mysqli_query($conect, $sql);
	for($i=0; $i<mysqli_num_rows($exc); $i++)
	{ 
		$row = mysqli_fetch_array($exc);
		$sql = "insert into afiliaciones (Fecha, Fecha_R, Biblioteca, Id_Report, Afi_F, Afi_M, Afi_NI, Afi_D, Cat_A, Cat_B, Renov, Vencim, Responsable, Descripcion) VALUES ('".$row['Fecha']."', '".$row['Fecha_R']."', '".$row['Biblioteca']."', '".$row['Id_Report']."', '".$row['Afi_F']."', '".$row['Afi_M']."', '".$row['Afi_NI']."', '".$row['Afi_D']."', '".$row['Cat_A']."', '".$row['Cat_B']."', '".$row['Renov']."', '".$row['Vencim']."', '".$row['Responsable']."', '".$row['Descripcion']."')";
		$excs = mysqli_query($connect, $sql);
	}
	
	$sql = "SELECT * FROM consolidado ORDER BY Id ASC";
	$exc = mysqli_query($conect, $sql);
	for($i=0; $i<mysqli_num_rows($exc); $i++)
	{ 
		$row = mysqli_fetch_array($exc);
		$sql = "insert into consolidado (Fecha, Biblioteca, Id_Report, Cantidad, Responsable, Tipo, Actividad, Alcance, Descripcion, Fecha_R) VALUES ('".$row['Fecha']."', '".$row['Biblioteca']."', '".$row['Id_Report']."', '".$row['Cantidad']."', '".$row['Responsable']."', '".$row['Tipo']."', '".$row['Actividad']."', '".$row['Alcance']."', '".$row['Descripcion']."', '".$row['Fecha_R']."')";
		$excs = mysqli_query($connect, $sql);
	}
	
	$sql = "SELECT * FROM consultas ORDER BY Id ASC";
	$exc = mysqli_query($conect, $sql);
	for($i=0; $i<mysqli_num_rows($exc); $i++)
	{ 
		$row = mysqli_fetch_array($exc);
		$sql = "insert into consultas (Fecha, Biblioteca, Material, Id_Report, Consultas, Responsable, Descripcion) VALUES ('".$row['Fecha']."', '".$row['Biblioteca']."', '".$row['Material']."', '".$row['Id_Report']."', '".$row['Consultas']."', '".$row['Responsable']."', '".$row['Descripcion']."')";
		$excs = mysqli_query($connect, $sql);
	}
	
	$sql = "SELECT * FROM prestamos ORDER BY Id ASC";
	$exc = mysqli_query($conect, $sql);
	for($i=0; $i<mysqli_num_rows($exc); $i++)
	{ 
		$row = mysqli_fetch_array($exc);
		$sql = "insert into prestamos (Fecha, Fecha_R, Biblioteca, Sala, Id_Report, Prestamos, Responsable, Descripcion) VALUES ('".$row['Fecha']."', '".$row['Fecha_R']."', '".$row['Biblioteca']."', '".$row['Sala']."', '".$row['Id_Report']."', '".$row['Prestamos']."', '".$row['Responsable']."', '".$row['Descripcion']."')";
		$excs = mysqli_query($connect, $sql);
	}
	
	$sql = "SELECT * FROM visitas ORDER BY Id ASC";
	$exc = mysqli_query($conect, $sql);
	for($i=0; $i<mysqli_num_rows($exc); $i++)
	{ 
		$row = mysqli_fetch_array($exc);
		$sql = "insert into visitas (Fecha, Fecha_R, Biblioteca, Sala, Id_Report, Visitantes, Responsable, Descripcion) VALUES ('".$row['Fecha']."', '".$row['Fecha_R']."', '".$row['Biblioteca']."', '".$row['Sala']."', '".$row['Id_Report']."', '".$row['Visitantes']."', '".$row['Responsable']."', '".$row['Descripcion']."')";
		$excs = mysqli_query($connect, $sql);
	}
	
	$sql = "SELECT * FROM prog_alias ORDER BY Id ASC";
	$exc = mysqli_query($conect, $sql);
	for($i=0; $i<mysqli_num_rows($exc); $i++)
	{ 
		$row = mysqli_fetch_array($exc);
		$sql = "insert into prog_alias (Reporte, Biblioteca, Fecha, Alias) VALUES ('".$row['Reporte']."', '".$row['Biblioteca']."', '".$row['Fecha']."', '".$row['Alias']."')";
		$excs = mysqli_query($connect, $sql);
	}
	echo "<h3 align='center'>Copiado de registros de la programación</h3>";
}
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="20%">&nbsp;</td>
    <td width="60%">&nbsp;</td>
    <td width="20%">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><div align="center">
      <form id="form1" name="form1" method="post" action="index.php">
      <input name="Programanacion" type="submit" value="Normalizar Programanacion" />
      </form>
    </div></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><div align="center">
      <form id="form2" name="form2" method="post" action="index.php">
      <input name="Reportes" type="submit" value="Normalizar Reportes" />
      </form>
    </div>  
    </td>
    <td>&nbsp;</td>
  </tr>
</table>
<div align="justify" id="db_guardar">&nbsp; <!--Insertar mensaje de ayuda para la página -->
<div class="div_menu" id="aa_1"><a href="javascript:void(0);" onclick="menu('a_b_','1'); mostrar('bb','1');" title="Ayuda">? +</a></div>
<div class="div_menu" style="display:none;" id="bb_1"><a href="javascript:void(0);" onclick="menu('a_b_','2'); mostrar('aa','1');" title="Ayuda">? -</a></div>
<div class="div_ayuda" id="a_b_" style="display:none;"><!-- Quitar comentarios e insertar el texto de ayuda. Aparecerá flotando en el pie de la página.--></div></div>
<!-- InstanceEndEditable -->
<form name="chang_elemt" id="chang_elemt" target="contenido" method="post">
<!-- InstanceBeginEditable name="campos" -->

<!-- InstanceEndEditable -->
</form>
</div></body>

<script language="javascript">
verif();
</script>

<!-- InstanceEnd --></html>