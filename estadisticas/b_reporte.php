<?php include "../script/breadcrumbs.php"; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/contenedor.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php
include("../script/scripts/session.php");
//include("../script/breadcrumbs.php");
?>
<link href="../css/css.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../script/scripts/jquery.min.js"></script>
<script type="text/javascript" src="../script/menu_ocultar.js"></script>
<script type="text/javascript">
function verif() {
document.getElementById('datos').style.display='none';
}
function foco_in(z) {
document.getElementById(z).focus();
}
</script>
<title>.: Sistema general de estad&iacute;stica :.</title>
<link rel="shortcut icon" href="../img/favicon.ico" />
<!-- InstanceBeginEditable name="doctitle" -->
<script src="../script/mes.js"></script>
<script src="../script/c_color.js"></script>
<script language="javascript">
function reporte(w) {
if(w == 1)
{
	document.getElementById("formulario").value = "talleres.php";
	document.getElementById("formulario").readOnly = true;
}
if(w == 2)
{
	document.getElementById("formulario").value = document.getElementById("report").value;
	document.getElementById("formulario").readOnly = false;
	document.getElementById("formulario").focus();
}
}

function busqueda() {
var val, op;
err = "";
var num_b = document.getElementById("id_bibs").value.split(",");
for(i=0; i<(num_b.length - 1); i++)
{
	error_color(1,"b_fil_"+num_b[i]);
	val = document.getElementById("bib_"+num_b[i]).value;
	if(isNaN(val) || parseInt(val) < 0 || /^\s+$/.test(val))
	{
		error_color(0,"b_fil_"+num_b[i]);
		err = "El número de sesiones no es válido. \n";
	}
}
if(err.length > 0)
{
	alert("Verifique los siguientes errores: \n\n"+err+"\n");
	return false;
}
else
	return true;
}

function igual_todos() {
var num_b = document.getElementById("id_bibs").value.split(",");
for(i=0; i<(num_b.length - 1); i++)
{
	document.getElementById("bib_"+num_b[i]).value = document.getElementById("valor_todos").value;
}
}
</script>
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" --><!-- InstanceEndEditable -->
</head>
<body>
<div id="datos" align="center">
 <h4>Consolidando datos...<br />
 <img src="../img/loader.gif" width="16" height="16" /></h4>
 </div>
<div align="left"><?php //echo breadcrumbs(); ?></div>
<div align="center" class="popup">
<!-- InstanceBeginEditable name="contenidos" -->
<?php
include("../Connections/conect.inc.php");
if(isset($_POST["id_bibs"], $_POST["id_b"], $_POST["fecha"], $_POST["guardar"]))
{
	$claus = explode(",",$_POST["id_bibs"]);
	for($i=0; $i < (sizeof($claus) - 1); $i++)
	{ 
		if($_POST["bib_".$claus[$i]] > 0)
		{
			$sql = "select Id from programacion where Reporte = ".$_POST["id_b"]." and Biblioteca = ".$claus[$i]." and Fecha = '".$_POST["fecha"]."'";
			$exc = mysqli_query($conect, $sql);
			if(mysqli_num_rows($exc) > 0)
			   $sql = "update programacion set Sesiones = ".$_POST["bib_".$claus[$i]]." where Reporte = ".$_POST["id_b"]." and Biblioteca = ".$claus[$i]." and Fecha = '".$_POST["fecha"]."'";
				//$sql = "update report_bib set Sesiones = '".$_POST["bib_".$claus[$i]]."', Fecha = '".$_POST["fecha"]."' where Reporte = ".$_POST["id_b"]." and Biblioteca = ".$claus[$i];
			else
			   $sql = "insert into programacion (Id, Biblioteca, Reporte, Sub, Sesiones, Fecha, Publico) values ('', ".$claus[$i].", ".$_POST["id_b"].", '0', ".$_POST["bib_".$claus[$i]].", '".$_POST["fecha"]."', '0')";
				//$sql = "insert into report_bib (Id, Biblioteca, Reporte, Sesiones, Fecha) VALUES ('', '".$claus[$i]."', '".$_POST["id_b"]."', '".$_POST["bib_".$claus[$i]]."', '".$_POST["fecha"]."')";	
			$exc = mysqli_query($conect, $sql);
		}
	}
	if($exc)
	{
		echo "<h4 align='center'>Registro actualizado</h4>";
		echo "<h4 align='center'>Fecha para registrar ".date("n/Y")."</h4>";
		?><script language="javascript">parent.consulta.location.reload();</script><?php
	}
	else
	{
		echo "<h4 align='center'>Error al actualizar el registro</h4>";
		echo "<h4 align='center'>Fecha para registrar ".date("n/Y")."</h4>";
	}
	//mysqli_free_result($exc);
	unset($sql,$exc);
	mysqli_close($conect);
}
else
{
if(!isset($_SESSION['MM_Biblio_Autentic']))
{ 
include("../script/loggin.php");
?>
<center><strong>No posee privilegios para este m&oacute;dulo.<br /><br />
<a href="javascript:form();">Inicie sesi&oacute;n.</a></strong></center>
<?php
}
else
{	
?>
<div align="center"><strong>Formulario para modificar reporte del sistema: <?php echo $_GET['nom']; ?></strong></div>
<form name ="formiden" method ="POST" action ="b_reporte.php?poss_alin=<?php echo $_GET["poss_alin"]."&bib=".$_GET["bib"]."&nom=".$_GET['nom']; ?>" onsubmit="return busqueda();">
<input name="id_b" id="id_b" type="hidden" value="<?php echo $_GET["bib"]; ?>" />
<input name="num_b" id="num_b" type="hidden" value="" />
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="20%" valign="top" align="left"><div align="right"><img src="../icon/reporte.png" width="34" height="35" alt="icono" /></div>
    <div class="list_cln" style="overflow:hidden;">
    <div align="center"><strong>Fecha de asignación o consulta de reportes:</strong></div>
    <div id="fech_actualiz">
        <div align="left"><input name="fecha" id="fecha" type="text" style="width:80%;" onFocus="doShow('date_p','formiden','fecha'); borrar();" title="Haga click para cambiar la fecha" readonly="readonly" value="<?php if(isset($_POST["fecha"])) echo $_POST["fecha"]; else echo date("n/Y"); ?>" />
        <img src="../icon/calendar_.gif" alt="seleccione" width="24" height="12" onClick="doShow('date_p','formiden','fecha')" title="Haga click para cambiar la fecha" />
        <div enabled="false" class="date_p" id="date_p" align="left">&nbsp;</div>
        </div></div>
    	<div align="center"><input name="consultar" id="consultar" type="submit" value="Consultar" /></div>
    </div>
    </td>
    <td width="60%" colspan="2">
    <p align="left"><strong>N&uacute;mero de sesiones o mediciones:</strong></p>
    <div align="left" style="vertical-align:top;">
    <?php include("../script/bibliotecas_b.php"); ?>
    </div>
    </td>
    <td width="20%" valign="baseline">
    <div class="x_fieldset"><a href="javascript:void(0);" onclick="document.location = '../fill.php';" title="Cerrar">X </a></div>
    <div class="list_cln">
    <p align="center">Aplicar el mismo valor a todas las bibliotecas:</p>
    <p align="center"><div style="width:40%; margin:0px auto;">
    <input name="valor_todos" id="valor_todos" type="text" placeholder="Nro. Sesiones" style="text-align:center;" /></div></p>
    <p align="center"><input name="Actualizar" id="Actualizar" type="button" value="Actualizar" onclick="igual_todos();" /></p>
    </div>
    <div align="center" style="margin-top:100%;" class="inform">Dejar en cero cuando no aplica el reporte a la biblioteca</div></td>
  </tr>
</table>
<div align="center">
<?php 
include("../Connections/conect.inc.php");
$sql = "select Continuo from reporte where reporte.Id = ".$_GET["bib"];
$exc = mysqli_query($conect, $sql);
$rows = mysqli_fetch_array($exc);
if($rows["Continuo"] == 1)
{ ?>
	<script language="javascript">
	document.getElementById("valor_todos").value = '1';
	document.getElementById("valor_todos").readOnly = true;
	var num_b = document.getElementById("id_bibs").value.split(",");
	for(i=0; i<(num_b.length - 1); i++)
	{
		document.getElementById("bib_"+num_b[i]).value = '1';
		document.getElementById("bib_"+num_b[i]).readOnly = true;
	}
	</script>
<?php	
}
else
{
	if(isset($_POST["fecha"], $_POST["consultar"]) && strlen($_POST["fecha"]) > 5)
		$sql = "select Biblioteca, Sesiones from programacion where Reporte = ".$_GET["bib"]." and Fecha = '".$_POST["fecha"]."'";
	else
		$sql = "select Biblioteca, Sesiones from programacion where Reporte = ".$_GET["bib"]." and Fecha = '".date('n/Y')."'";
	//echo $sql;
	$exc = mysqli_query($conect, $sql);
	if(mysqli_num_rows($exc) == 0)
	{
		echo "<div align='center' class='inform'>No se ha generado la programaci&oacute;n para el presente reporte</div>";
	}
	else
	{
		for($i=0; $i<mysqli_num_rows($exc); $i++)
		{ 
			$rows = mysqli_fetch_array($exc);
			?>
				<script language="javascript">
				document.getElementById("bib_"+<?php echo $rows["Biblioteca"]; ?>).value = <?php echo $rows["Sesiones"]; ?>;
				document.getElementById("id_bibs").value += <?php echo $rows["Biblioteca"]; ?> + ",";
				</script>
			<?php
		} ?>
		<script language="javascript">
			//document.getElementById("fech_actualiz").innerHTML = "<?php //echo $rows["Fecha"]; ?>";
		</script>
		<?php
} } ?>
<div align="center" id="db_guardar" style="clear:both;">
<?php
if($rows["Continuo"] == 1)
{?>
	<div align="center" class="inform">El reporte está configurado por defecto como único al mes, no se puede cambiar el número de sesiones a reportar.</div>
<?php
}
?>
	<div align="center"><input name="guardar" id="guardar" type="submit" value="Actualizar" /></div>
<?php
mysqli_free_result($exc);
unset($sql,$exc,$rows,$i);
mysqli_close($conect);
}
}
?>
<div class="div_menu" id="aa_1"><a href="javascript:void(0);" onclick="menu('a_b_','1'); mostrar('bb','1');" title="Ayuda">? +</a></div>
<div class="div_menu" style="display:none;" id="bb_1"><a href="javascript:void(0);" onclick="menu('a_b_','2'); mostrar('aa','1');" title="Ayuda">? -</a></div>
<div class="div_ayuda" id="a_b_" style="display:none;">Seleccione Reporte genérico o Reporte especial, para mayor información referirse al manual ya que para los reportes especiales se requiere nombrar las páginas de demás contenidos. El tipo de reporte suele ser interno o de extensión cuando las actividades se realizan fuera de la biblioteca. El número de sesiones o mediciones hace referencia a cuando se tiene establecido una cantidad a cumplir, este dato se complementa al hacer la configuración mensual de estadísticas para cada biblioteca.</div></div>
</div>
</form>
<script language="javascript">
 //parent.scroll_pos('<?php echo ($_GET["poss_alin"] * 16); ?>');
</script>
<!-- InstanceEndEditable -->
<form name="chang_elemt" id="chang_elemt" target="contenido" method="post">
<!-- InstanceBeginEditable name="campos" -->

<!-- InstanceEndEditable -->
</form>
</div></body>

<script language="javascript">
verif();
</script>

<!-- InstanceEnd --></html>