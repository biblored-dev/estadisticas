<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/contenedor.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php
include("../script/scripts/session.php");
//include("../script/breadcrumbs.php");
?>
<link href="../css/css.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../script/scripts/jquery.min.js"></script>
<script type="text/javascript" src="../script/menu_ocultar.js"></script>
<script type="text/javascript">
function verif() {
document.getElementById('datos').style.display='none';
}
function foco_in(z) {
document.getElementById(z).focus();
}
</script>
<title>.: Sistema general de estad&iacute;stica :.</title>
<link rel="shortcut icon" href="../img/favicon.ico" />
<!-- InstanceBeginEditable name="doctitle" -->
<link href="../menu/css/css.css" rel="stylesheet" type="text/css" />
<script src="../script/c_color.js"></script>
<script language="javascript">
function serv_poss(w) {
return document.getElementById(w).getBoundingClientRect().top;
//alert(document.getElementById(w).getBoundingClientRect().top)
}
function serv_mensual(w) {
var z = window.open(w, 'Listado de  reportes del sistema', 'resizable=yes,menubar=no,scrollbars=yes,width=720,height=820');
z.focus();
}
function busqueda() {
var err,op;
err = "";
op = document.getElementById("area_r").selectedIndex;
if(op == null || isNaN(op) || op == 0 || /^\s+$/.test(op))
	err += "Se requiere el nombre del área. \n";
if(err.length > 0)
{
	alert("Verifique los siguientes errores: \n\n"+err+"\n");
	return false;
}
else
{
	document.getElementById("id").value = document.getElementById("area_r")[op].text;
	return true;
}
}
</script>
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" --><!-- InstanceEndEditable -->
</head>
<body>
<div id="datos" align="center">
 <h4>Consolidando datos...<br />
 <img src="../img/loader.gif" width="16" height="16" /></h4>
 </div>
<div align="left"><?php //echo breadcrumbs(); ?></div>
<div align="center" class="popup">
<!-- InstanceBeginEditable name="contenidos" -->
<?php
if(isset($_POST["area_r"], $_POST["id"]) || isset($_GET["area_r"], $_GET["id"]))
{
	if(isset($_GET["area_r"]))
	{
		$_POST["area_r"] = $_GET["area_r"];
		$_POST["id"] = $_GET["id"];
	}
	include("../script/estado.php");
	$tipos = array(0=>"",1=>"Interno",2=>"Extensión",3=>"Mixto");
	include("../Connections/conect.inc.php");
	$sql = "select Id, Nombre from categorias where Area = ".$_POST["area_r"]." order by Nombre";
	//echo $sql;
	$excds = mysqli_query($conect, $sql);
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="1%">&nbsp;
    </td>
    <td align="center">
    <div style="float:left; margin-right:15px; margin-top:5px;">
    <h3 align="left"><a href="reportes.php" title="Regresar"> << </a></h3>
    </div>
    <div style="float:left">
    <div align="left" style="background-color:<?php echo $estado[0][0]; ?>;" title="<?php echo $estado[0][2]; ?>"><?php echo $estado[0][1]; ?></div>
    <div align="left" style="background-color:<?php echo $estado[1][0]; ?>;" title="<?php echo $estado[1][2]; ?>"><?php echo $estado[1][1]; ?></div>
    </div>
    <div class="x_fieldset"><a href="javascript:void(0);" onclick="serv_mensual('ejem_generico.php?bib=00&nom=Nombre%20Biblioteca&Reporte=Nombre del reporte tipo genérico&id_rep=0000&fech_rep=date(mes/año)&id_report=000&alias=Nombre%20genéricos%20dado en la biblioteca&campo=Dato%20que%20reportará')">Ejemplos de reporte</a></div>
    <?php if($_SESSION['MM_Usr_Pri'] == 1) { ?>
    <div class="x_fieldset"><a href="javascript:void(0);" onclick="parent.contenido.location = 'n_reporte.php?a_n=<?php echo $_POST["id"]; ?>&a_i=<?php echo $_POST["area_r"]; ?>';" title="Nuevo reporte">Nuevo reporte</a></div>
    <?php } ?>
    <div align="center" style="clear:both;">
    <div align="center"><strong>Listado de reportes del sistema. &Aacute;rea: <?php echo $_POST["id"]; ?></strong></div>
    <?php
	for($iz=0; $iz<mysqli_num_rows($excds); $iz++)
	{ 
		$rowds = mysqli_fetch_array($excds);
		$sql = "select Id, Nombre, Tipo, Descripcion, Estado, Continuo from reporte where Area = ".$_POST["area_r"]." and Sub = ".$rowds["Id"]." order by reporte.Estado, reporte.Nombre";
		//echo $sql;
		$exc = mysqli_query($conect, $sql);
		?>
		<div align="left"><strong>Categor&iacute;a: <?php echo $rowds["Nombre"]; ?></strong></div>
        <table width="100%" border="0" cellspacing="0" cellpadding="0" id="area_<?php echo $iz; ?>" style="border:1px #000 solid;">
		<tr>
		<td width="4%"><div align="center"><strong>N&deg;</strong></div></td>
		<td width="7%"><div align="center"><strong>Tipo</strong></div></td>
		<td width="23%"><div align="center"><strong>Nombre del reporte</strong></div></td>
		<td><div align="center"><strong>Descripci&oacute;n del reporte</strong></div></td>
		<td width="19%"><div align="center"><strong>Opciones</strong></div></td>
		</tr>
		<?php
		for($i=0; $i<mysqli_num_rows($exc); $i++)
		{ 
			$row = mysqli_fetch_array($exc);
		?>	
		  <tr onclick="n_color('<?php echo ($i+1); ?>','area_<?php echo ($iz); ?>');">
			<td><div align="center" style="background-color:<?php echo $estado[$row["Estado"]][0]; ?>"><?php echo ($i+1); ?></div></td>
			<td align="left"><?php echo $tipos[$row["Tipo"]]; ?></td>
			<td align="left"><?php echo $row["Nombre"]; ?></td>
			<td align="left"><span class="cssToolTip"><?php echo substr($row["Descripcion"],0,70); ?> ...
			<span><?php echo $row["Descripcion"]; ?></span></span>
			</td>
			<td id="fill_pos_<?php echo ($i+1); ?>"><div id="left_men">
			<ul>
				<li style="font-size:"><a href="javascript:void(0);">Opciones</a>
				<ul>
                <?php
				if(($row["Estado"] == 1) && (isset($_SESSION['MM_Usr_Pri']) && $_SESSION['MM_Usr_Pri'] <= 2))
				{ ?>
					<li><a href="javascript:void(0);" onclick="modificar('<?php echo "b_reporte.php?bib=".$row['Id']."&nom=".$row["Nombre"]."&poss_alin=".($i+1); ?>');">Programaci&oacute;n</a></li>
                <?php } ?>    
				<li><a href="javascript:void(0);" onclick="modificar('<?php echo "c_reporte.php?bib=".$row['Id']."&nom=".$row["Nombre"]."&poss_alin=".($i+1); ?>');">Consultar</a></li>
				<?php if(isset($_SESSION['MM_Usr_Pri']) && $_SESSION['MM_Usr_Pri'] <= 2) { ?>
				<li><a href="javascript:void(0);" onclick="modificar('<?php echo "d_reporte.php?bib=".$row['Id']."&nom=".$row["Nombre"]."&poss_alin=".($i+1); ?>');">Eliminar</a></li>
				<li><a href="javascript:void(0);" onclick="modificar('<?php echo "m_reporte.php?bib=".$row['Id']."&nom=".$row["Nombre"]."&tip_repor=".$row["Tipo"]."&poss_alin=".($i+1); ?>');">Modificar</a></li>
				
				<li><a href="javascript:void(0);" onclick="modificar('<?php echo "e_reporte.php?bib=".$row['Id']."&nom=".$row["Nombre"]."&poss_alin=".($i+1); ?>');">Suprimir/Activar</a></li>
				<?php } ?>
				</ul></li>
			</ul>    
			</div></td>
		  </tr>
		<?php } ?>
		</table>
    <?php } ?>
    </div>
    </td>
    <td width="1%">&nbsp;</td>
  </tr>
</table>
<p>&nbsp;</p><p>&nbsp;</p>
<?php
@ mysqli_free_result($exc);
unset($exc, $sql, $i, $row, $estado, $tipos);
mysqli_close($conect);
}
else
{ ?>
<script language="javascript">
	parent.contenido.location = "../fill.php";
</script>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="10%">&nbsp;</td>
    <td width="80%" align="center">&nbsp;</td>
    <td width="10%">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td align="center">
    <?php if($_SESSION['MM_Usr_Pri'] == 1) { ?>
    <div class="x_fieldset"><a href="javascript:void(0);" onclick="parent.contenido.location = '../biblioteca/n_area.php';" title="Nueva área">Nueva &Aacute;rea</a></div>
    <?php } ?>
    <div class="x_fieldset"><a href="javascript:void(0);" onclick="serv_mensual('t_reporte.php');" title="Listado de reportes">Listado de reportes</a></div>
    <fieldset>
    <legend align="center"><strong>&Aacute;reas responsables de las estad&iacute;sticas</strong></legend>
    <form name ="formiden" method ="POST" action ="reportes.php" onsubmit="return busqueda();">
    <input name="id" id="id" type="hidden" value="" />
    <div align="left"><?php include("../script/areas.php"); ?></div>
    <p align="center"><input name="guardar" id="guardar" type="submit" value="Consultar" /></p>
    </form>
    </fieldset>
    </td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>
    <?php
	include("../Connections/conect.inc.php");
	$sql = "select count(Id) as Total from reporte";
	$exc = mysqli_query($conect, $sql);
	$row = mysqli_fetch_array($exc);
	echo "Cantidad de reportes creados: ".$row["Total"];
	@ mysqli_free_result($exc);
	unset($exc, $sql, $row, $excds, $rowds, $i, $iz);
	mysqli_close($conect);
	?>
    </td>
    <td>&nbsp;</td>
  </tr>
</table>	
<?php } ?>
<!-- InstanceEndEditable -->
<form name="chang_elemt" id="chang_elemt" target="contenido" method="post">
<!-- InstanceBeginEditable name="campos" -->

<!-- InstanceEndEditable -->
</form>
</div></body>

<script language="javascript">
verif();
</script>

<!-- InstanceEnd --></html>