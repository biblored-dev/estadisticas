<?php include "../script/breadcrumbs.php"; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/contenedor.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php
include("../script/scripts/session.php");
//include("../script/breadcrumbs.php");
?>
<link href="../css/css.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../script/scripts/jquery.min.js"></script>
<script type="text/javascript" src="../script/menu_ocultar.js"></script>
<script type="text/javascript">
function verif() {
document.getElementById('datos').style.display='none';
}
function foco_in(z) {
document.getElementById(z).focus();
}
</script>
<title>.: Sistema general de estad&iacute;stica :.</title>
<link rel="shortcut icon" href="../img/favicon.ico" />
<!-- InstanceBeginEditable name="doctitle" -->
<script language="javascript" src="../script/datosxml.js"></script>
<script language="javascript">
var err = "";
function predataserver(w,x,y) {
  http.open("GET", "../script/datosxml.php?tab="+w+"&camp="+x+"&dat="+y, false); //False para que no continue el flujo hasta compeltar la petición ajax
  http.onreadystatechange = useHttpResp;
  return http.send(null);
}

function useHttpResp() {
if(http.readyState == 4)
{
	if(http.status == 200)
	{
		var timeValue = http.responseXML.getElementsByTagName("timenow")[0];
		if(parseInt(timeValue.childNodes[0].nodeValue) > 1)
		{
			err += "El nombre de la estadística en la (s) biblioteca (s)\n seleccionadas ya existe. \n";
		}
		verif();
	}
} 
else
{
	document.getElementById('datos').style.display = "";
}
}

var num_b = 0;
function marcar_tot(w){
document.getElementById("id_bibs").value = "";
for(i=0; i<num_b; i++)
{ 
	if(w == 1)
	{
		document.getElementById("bib_"+i).checked = true;
	}
	if(w == 3)
	{
		document.getElementById("bib_"+i).checked = false;
	}
	if(document.getElementById("bib_"+i).checked == true)
	{
		document.getElementById("id_bibs").value += document.getElementById("bib_"+i).value + ",";
	}
}
}

function reporte(w) {
if(w == 4)
{
	document.getElementById("formulario").value = document.getElementById("report").value;
	document.getElementById("formulario").readOnly = false;
	document.getElementById("formulario").focus();
}
else
{
	document.getElementById("formulario").value = w;
	document.getElementById("formulario").readOnly = true;
}
}

function busqueda() {
var val;
err = "";
val = document.getElementById("nombre").value;
if(val == null || !isNaN(val) || val.length < 3 || /^\s+$/.test(val))
	err += "Se requiere el nombre del programa. \n";
else
	predataserver('reporte','Nombre',document.getElementById("nombre").value) //tabla, campo, dato
val = document.getElementById("formulario").value;
if(val == null || !isNaN(val) || val.length < 3 || /^\s+$/.test(val))
	err += "Se requiere el nombre del formulario. \n";
op = document.getElementById("area_r").selectedIndex;
val = document.getElementById("area_r")[op].value;
if(val == null || val.length == 0 || /^\s+$/.test(val))
	err += "Se requiere el nombre del área responsable. \n";
op = document.getElementById("tip_r").selectedIndex;
val = document.getElementById("tip_r")[op].value;
if(val == null || val.length == 0 || /^\s+$/.test(val))
	err += "Se requiere el tipo de reporte. \n";
val = document.getElementById("campo").value;
if(val == null || !isNaN(val) || val.length < 5 || /^\s+$/.test(val))
	err += "Se requiere el nombre del dato a reportar. \n";	
if(document.getElementById("mensual").checked == true)
	document.getElementById("mensual").value = "1";
else
	document.getElementById("mensual").value = "0";
if(err.length > 0)
{
	alert("Verifique los siguientes errores: \n\n"+err+"\n");
	return false;
}
else
	return true;
}
function serv_mensual(w) {
var z = window.open(w, 'Servicio mensual', 'resizable=yes,menubar=no,scrollbars=yes,width=660,height=720');
z.focus();
}
</script>
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" --><!-- InstanceEndEditable -->
</head>
<body>
<div id="datos" align="center">
 <h4>Consolidando datos...<br />
 <img src="../img/loader.gif" width="16" height="16" /></h4>
 </div>
<div align="left"><?php //echo breadcrumbs(); ?></div>
<div align="center" class="popup">
<!-- InstanceBeginEditable name="contenidos" -->
<?php
include("../Connections/conect.inc.php");
if(isset($_POST["nombre"], $_POST["guardar"], $_POST["id_b"]))
{
	$mensual = 0;
	if(isset($_POST["mensual"]))
		$mensual = $_POST["mensual"];
	$sql = "update reporte set Nombre = '".$_POST["nombre"]."', Area = '".$_POST["area_r"]."', Sub = '".$_POST["categori"]."', Formulario = '".$_POST["formulario"]."', Tipo = '".$_POST["tip_r"]."',  Campo = '".ucfirst($_POST["campo"])."', Descripcion = '".addslashes($_POST["descrp"])."', Continuo = '".$mensual."' where Id = ".$_POST["id_b"];
	//echo $sql;
	$exc = mysqli_query($conect, $sql);
	if($exc)
	{
		$sql = "select Id from biblioteca where Id <> 20";
		$exc = mysqli_query($conect, $sql);
		for($i=0; $i<mysqli_num_rows($exc); $i++)
		{ 	
			$row = mysqli_fetch_array($exc);
			if(mysqli_num_rows(mysqli_query($conect, "select Id from report_bib where Biblioteca = ".$row['Id']." and Reporte = ".$_POST["id_b"])) == 0)
			{
				$sql = "insert into report_bib (Id, Biblioteca, Reporte, Sesiones, Fecha) VALUES ('', '".$row['Id']."', '".$_POST["id_b"]."', '1', '".date('Y-n-j')."')";
				//echo $sql;
				$excs = mysqli_query($conect, $sql);
			}
			else
			{
				if($mensual == 1)
				{
					$sql = "update report_bib set Sesiones = '1', Fecha = '".date('Y-n-j')."') where Biblioteca = ".$row['Id']." and Reporte = ".$_POST["id_b"];
					//echo $sql;
					$excs = mysqli_query($conect, $sql);
				}
			}
		}
		echo "<h4 align='center'>Registro actualizado</h4>";
		echo "<h4 align='center'>Actualice el n&uacute;mero de sesiones</h4>";
		?><script language="javascript">parent.consulta.location.reload();</script><?php
	}
	else
	{
		echo "<h4 align='center'>Error al actualizar el registro</h4>";
	}
	//mysqli_free_result($exc);
	unset($sql,$exc,$mensual,$excs,$i,$row);
	mysqli_close($conect);
}
else
{
if(!isset($_SESSION['MM_Biblio_Autentic']))
{ 
include("../script/loggin.php");
?>
<center><strong>No posee privilegios para este m&oacute;dulo.<br /><br />
<a href="javascript:form();">Inicie sesi&oacute;n.</a></strong></center>
<?php
}
else
{
$sql = "select reporte.Area, reporte.Sub, reporte.Formulario, reporte.Tipo, reporte.Campo, reporte.Descripcion, reporte.Continuo, areas.Nombre from reporte, areas where reporte.Id = ".$_GET["bib"]." and areas.Id = reporte.Area";
$excs = mysqli_query($conect, $sql);
$rows = mysqli_fetch_array($excs);
?>
<div align="center" style="padding-bottom:5px;"><strong>Formulario para modificar reporte del sistema</strong></div>
<form name ="formiden" method ="POST" action ="m_reporte.php?poss_alin=<?php echo $_GET["poss_alin"]; ?>" onsubmit="return busqueda();">
<input name="id_b" id="id_b" type="hidden" value="<?php echo $_GET["bib"]; ?>" />
<input name="report" id="report" type="hidden" value="<?php echo $rows["Formulario"]; ?>" />
<input name="formulario" id="formulario" type="hidden" value="<?php echo $rows["Formulario"]; ?>" />
<input name="subcategoria" id="subcategoria" type="hidden" value="<?php echo $rows["Sub"]; ?>" />
<table width="90%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="4%" valign="top" rowspan="5"><div align="right"><img src="../icon/reporte.png" width="34" height="35" alt="icono" /></div></td>
    <td width="48%"><div align="left" style="background-color:#EEE;">
    <select name ="t_report" id="t_report" onchange="reporte(this.value);">
    	<option value="">Seleccione el tipo de reporte</option>
        <option value="talleres.php" <?php if($rows["Formulario"] == "talleres.php") echo "selected='selected'"; ?>>Reporte gen&eacute;rico</option>
        <option value="consolidado.php" <?php if($rows["Formulario"] == "consolidado.php") echo "selected='selected'"; ?>>Reporte consolidado</option>
        <option value="ponderado.php" <?php if($rows["Formulario"] == "ponderado.php") echo "selected='selected'"; ?>>Reporte ponderado</option>
    </select>
    <div class="infos">Consulte el enlace de 'Ejemplos de reporte' para identificar el que se ajusta</div>
    </div>
    </td>
    <td width="48%">
    <div align="left"><strong>Nombre del dato a reportar:</strong></div>
    <div align="left"><input name="campo" id="campo" type="text" size="50" value="<?php echo $rows["Campo"]; ?>" />
    <div class="infos">Indique que representa el valor que se reportar&aacute;, por ejemplo, cantidad de pr&eacute;stamos, cantidad de afiliaciones, n&uacute;mero de participantes, porcentaje de avance …</div></div>
    </td>
  </tr>
  <tr>
    <td>
    <div align="left"><strong>Nombre del reporte:</strong></div>
    <div align="left"><input name="nombre" id="nombre" type="text" size="50" value="<?php echo $_GET["nom"]; ?>" /></div>
    </td>
    <td>
    <div align="left"><?php include("../script/areas.php"); ?></div>
    </td>
  </tr>
  <tr>  
    <td>  
    <div align="left"><strong>Tipo de reporte:</strong></div>
    <div align="left" style="padding-left:10px;">
    <?php include("../script/tipo_report.php"); ?>
    </div>
    </td>
    <td>
    <div align="left"><strong>El reporte se debe diligenciar autom&aacute;ticamente, una vez al mes:</strong></div>
    <div align="left"><input name="mensual" id="mensual" type="checkbox" value="" <?php if($rows["Continuo"] == "1") echo "checked='checked'"; ?> /> &nbsp; Ejecutar autom&aacute;ticamente</div>
    </td>
  </tr>
  <tr>
    <td colspan="2">
    <div align="center" style="width:45%; margin:0px auto;"><?php include("../script/categorias.php"); ?></div>
    <script language="javascript">
	window.onload = function() {
	document.getElementById("area_r").onchange = function(){precategoria()};
	precategoria();
	} 
	</script>
    </td>
  </tr>
  <tr>
    <td colspan="2">
    <div align="left"><strong>Descripci&oacute;n del reporte:</strong></div>
    <div align="left"><textarea name="descrp" id="descrp" rows="2"><?php echo $rows["Descripcion"]; ?></textarea>
    <div class="infos">M&aacute;ximo 500 caracteres</div>
    </div>
    </td>
  </tr>
</table>
<div align="center" id="db_guardar"><input name="guardar" id="guardar" type="submit" value="Modificar" />
<div class="div_menu" id="aa_1"><a href="javascript:void(0);" onclick="menu('a_b_','1'); mostrar('bb','1');" title="Ayuda">? +</a></div>
<div class="div_menu" style="display:none;" id="bb_1"><a href="javascript:void(0);" onclick="menu('a_b_','2'); mostrar('aa','1');" title="Ayuda">? -</a></div>
<div class="div_ayuda" id="a_b_" style="display:none;">Reporte gen&eacute;rico: Corresponde a la informaci&oacute;n que se toma de los talleres realizados, fecha, tiempo empleado, n&uacute;mero de participantes, franja poblacional.<br />Reporte consolidado: Corresponde a una cantidad a reportar o que se sumar&aacute;n de cuerdo a la cantidad de reportes creados para la misma actividad.<br />Reporte ponderado: Similar al anterior pero manejado como porcentaje de avance.<br />El tipo de reporte suele ser interno o de extensi&oacute;n cuando las actividades se realizan fuera de la biblioteca.<br />Al activar la casilla "Ejecutar automáticamente", el reporte se marcar&aacute; autom&aacute;ticamente en 1 cada mes.<br /><a href="javascript:void(0);" onclick="serv_mensual('ejem_generico.php?bib=00&nom=Nombre%20Biblioteca&Reporte=Nombre del reporte tipo genérico&id_rep=0000&fech_rep=date(mes/año)&id_report=000&alias=Nombre%20genéricos%20dado en la biblioteca&campo=Dato%20que%20reportará')">Ejemplos de reporte</a></div></div>
</form>
<script>
foco_in('nombre');
</script>
<?php 
}
}
@ mysqli_free_result($exc);
unset($sql,$exc,$row,$rows,$i, $bibs);
//mysqli_close($conect);
?>

<!-- InstanceEndEditable -->
<form name="chang_elemt" id="chang_elemt" target="contenido" method="post">
<!-- InstanceBeginEditable name="campos" -->

<!-- InstanceEndEditable -->
</form>
</div></body>

<script language="javascript">
verif();
</script>

<!-- InstanceEnd --></html>
<script language="javascript">
 //parent.scroll_pos('<?php echo ($_GET["poss_alin"] * 16); ?>');
</script>