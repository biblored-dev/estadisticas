<?php include "../script/breadcrumbs.php"; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/contenedor.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php
include("../script/scripts/session.php");
//include("../script/breadcrumbs.php");
?>
<link href="../css/css.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../script/scripts/jquery.min.js"></script>
<script type="text/javascript" src="../script/menu_ocultar.js"></script>
<script type="text/javascript">
function verif() {
document.getElementById('datos').style.display='none';
}
function foco_in(z) {
document.getElementById(z).focus();
}
</script>
<title>.: Sistema general de estad&iacute;stica :.</title>
<link rel="shortcut icon" href="../img/favicon.ico" />
<!-- InstanceBeginEditable name="doctitle" -->
<script language="javascript">
function busqueda() {
if(confirm("¿Realmente desea eliminar el reporte? \n "))
{
	if(document.getElementById("id_e").value == 0)
		document.getElementById("id_e").value = 1;
	else
		document.getElementById("id_e").value = 0;	
	return true;
}
else
	return false;
}
</script>
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" --><!-- InstanceEndEditable -->
</head>
<body>
<div id="datos" align="center">
 <h4>Consolidando datos...<br />
 <img src="../img/loader.gif" width="16" height="16" /></h4>
 </div>
<div align="left"><?php //echo breadcrumbs(); ?></div>
<div align="center" class="popup">
<!-- InstanceBeginEditable name="contenidos" -->
<?php
require_once("../Connections/conect.inc.php");
if(isset($_POST["nombre"], $_POST["guardar"], $_POST["id_b"]))
{
	$sql = "update reporte set Estado = ".$_POST["id_e"]." where Id = ".$_POST["id_b"];
	$exc = mysqli_query($conect, $sql);
	if($exc)
	{
		echo "<h4 align='center'>Registro actualizado</h4>";
		?><script language="javascript">parent.consulta.location.reload();</script><?php
	}
	else
	{
		echo "<h4 align='center'>Error al modificar el registro</h4>";
	}
	//mysqli_free_result($exc);
	unset($sql,$exc);
	mysqli_close($conect);
}
else
{
if(!isset($_SESSION['MM_Biblio_Autentic']))
{ 
include("../script/loggin.php");
?>
<center><strong>No posee privilegios para este m&oacute;dulo.<br /><br />
<a href="javascript:form();">Inicie sesi&oacute;n.</a></strong></center>
<?php
}
else
{
include("../script/estado.php");
$sql = "select Campo, Descripcion, Estado from reporte where Id = ".$_GET["bib"];
$exc = mysqli_query($conect, $sql);
$row = mysqli_fetch_array($exc);
?>
<p align="center"><strong>Formulario para "Suprimir / Activar" reporte al sistema</strong></p>
<form name ="formiden" method ="POST" action ="e_reporte.php?poss_alin=<?php echo $_GET["poss_alin"]; ?>" onsubmit="return busqueda();">
<input name="id_e" id="id_e" type="hidden" value="<?php echo $row["Estado"]; ?>" />
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="25%" valign="top"><div align="right"><img src="../icon/reporte.png" width="34" height="35" alt="icono" /></div></td>
    <td width="50%" >
      <input name="id_b" id="id_b" type="hidden" value="<?php echo $_GET["bib"]; ?>" />
      <p align="left"><strong>Nombre del reporte:</strong></p>
      <div align="left"><input name="nombre" id="nombre" type="text" size="50" readonly="readonly" value="<?php echo $_GET["nom"]; ?>" /></div>
      <p align="left"><strong>Descripci&oacute;n del reporte:</strong></p>
      <div align="left"><textarea name="descrp" id="descrp" rows="3" readonly="readonly"><?php echo $row["Descripcion"]; ?></textarea>
        <div>M&aacute;ximo 250 caracteres</div>
        </div>
      <p align="left"><strong>Nombre del dato a reportar:</strong></p>
      <div align="left"><input name="formulario" id="formulario" type="text" size="50" readonly="readonly" value="<?php echo $row["Campo"]; ?>" /></div>
      <div class="infos">Indique que representa el valor que se reportar&aacute;, por ejemplo, cantidad de pr&eacute;stamos, cantidad de afiliaciones, n&uacute;mero de participantes, porcentaje de avance …</div>
    </td>
    <td width="25%"><div align="left" style="background-color:<?php echo $estado[$row["Estado"]][0]; ?>;"><strong>Estado: <?php echo $estado[$row["Estado"]][2]; ?></strong></div></td>
    </tr>
  <tr>
    <td colspan="3"><div align="justify">
    El reporte se deshabilitará de la recolección de información y podrá volverse a utilizar con solo activarlo.
    </div></td>
  </tr>
    
</table>
<div align="center"><input name="guardar" id="guardar" type="submit" value="<?php if($row["Estado"] == 1) echo "Suprimir"; else echo "Activar"; ?>" /></div>
</form>
<?php 
mysqli_free_result($exc);
unset($sql,$exc,$row,$i, $bibs, $estado);
mysqli_close($conect);
}
}
?>
<!-- InstanceEndEditable -->
<form name="chang_elemt" id="chang_elemt" target="contenido" method="post">
<!-- InstanceBeginEditable name="campos" -->

<!-- InstanceEndEditable -->
</form>
</div></body>

<script language="javascript">
verif();
</script>

<!-- InstanceEnd --></html>
<script language="javascript">
 //parent.scroll_pos('<?php echo ($_GET["poss_alin"] * 16); ?>');
</script>