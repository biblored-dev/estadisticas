<?php include "../script/breadcrumbs.php"; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/contenedor.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php
include("../script/scripts/session.php");
//include("../script/breadcrumbs.php");
?>
<link href="../css/css.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../script/scripts/jquery.min.js"></script>
<script type="text/javascript" src="../script/menu_ocultar.js"></script>
<script type="text/javascript">
function verif() {
document.getElementById('datos').style.display='none';
}
function foco_in(z) {
document.getElementById(z).focus();
}
</script>
<title>.: Sistema general de estad&iacute;stica :.</title>
<link rel="shortcut icon" href="../img/favicon.ico" />
<!-- InstanceBeginEditable name="doctitle" -->

<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" --><!-- InstanceEndEditable -->
</head>
<body>
<div id="datos" align="center">
 <h4>Consolidando datos...<br />
 <img src="../img/loader.gif" width="16" height="16" /></h4>
 </div>
<div align="left"><?php //echo breadcrumbs(); ?></div>
<div align="center" class="popup">
<!-- InstanceBeginEditable name="contenidos" -->
<?php
$tip_r = array(0=>"",1=>"Interno",2=>"Externo",3=>"Mixto");
include("../Connections/conect.inc.php");
$sql = "select reporte.Formulario, reporte.Tipo,reporte.Campo, reporte.Descripcion, reporte.Continuo, areas.Nombre from reporte, areas where reporte.Id = ".$_GET["bib"]." and areas.Id = reporte.Area";
$exc = mysqli_query($conect, $sql);
$rows = mysqli_fetch_array($exc);
?>
<div align="center"><strong>Formulario para consultar reporte del sistema</strong></div>
<form name ="formiden" method ="POST" action ="c_reporte.php" onsubmit="return busqueda();">
<input name="id_b" id="id_b" type="hidden" value="<?php echo $_GET["bib"]; ?>" />
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="4%" valign="top" rowspan="5"><div align="right"><img src="../icon/reporte.png" width="34" height="35" alt="icono" /></div></td>
    <td width="25%" align="center">
    <div align="left"><strong>Nombre del reporte:</strong></div>
    <div align="left"><input name="nombre" id="nombre" type="text" size="50" readonly="readonly" value="<?php echo $_GET["nom"]; ?>" /></div>
    </td>
    <td width="25%" align="center">
    <div align="left"><strong>Nombre del dato a reportar:</strong></div>
    <div align="left"><input name="formulario" id="formulario" type="text" size="50" readonly="readonly" value="<?php echo $rows["Campo"]; ?>" />
    </td>
    <td width="46%" rowspan="5">
    <div class="x_fieldset"><a href="javascript:void(0);" onclick="document.location = '../fill.php';" title="Cerrar">X </a></div>
    <div align="left"><strong>N&uacute;mero de sesiones o mediciones para el mes <?php echo date("n/Y"); ?>:</strong></div>
    <div align="left" style="vertical-align:top;">
    <?php 
	if($rows["Continuo"] == 0)
		include("../script/bibliotecas_b.php"); 
    else
		echo "<div align='center' class='inform'>Reporte &uacute;nico mensual</div>";
    ?>
    </div>
    </td>
  </tr>
  <tr>  
    <td>  
    <div align="left"><strong>&Aacute;rea responsable del reporte:</strong></div>
    <div align="left"><input name="area_r" id="area_r" type="text" size="50" readonly="readonly" value="<?php echo $rows["Nombre"]; ?>" /></div>
    </td>
    <td><div align="left"><strong>Tipo de reporte:</strong></div>
    <div align="left" style="padding-left:10px;">
    <input name ="tip_r" id="tip_r" type="text" size="50" readonly="readonly" value="<?php echo $tip_r[$rows["Tipo"]]; ?>" />
    </div></td>
  </tr>
  <tr>
    <td colspan="2">
    <div align="left"><strong>El reporte se debe diligenciar autom&aacute;ticamente, una vez al mes:</strong></div>
    <div align="left"><input name="mensual" id="mensual" type="checkbox" value="" <?php if($rows["Continuo"] == "1") echo "checked='checked'"; ?> disabled="disabled" /> &nbsp; Ejecutar autom&aacute;ticamente</div>
    </td>
  </tr>
  <tr>
    <td colspan="2">
    <div align="left"><strong>Descripci&oacute;n del reporte:</strong></div>
    <div align="left"><textarea name="descrp" id="descrp" rows="4" readonly="readonly"><?php echo $rows["Descripcion"]; ?></textarea>
    <div class="infos">M&aacute;ximo 500 caracteres</div>
    </div>
    </td>
  </tr>
</table>
<div align="center">
<div class="div_menu" id="aa_1"><a href="javascript:void(0);" onclick="menu('a_b_','1'); mostrar('bb','1');" title="Ayuda">? +</a></div>
<div class="div_menu" style="display:none;" id="bb_1"><a href="javascript:void(0);" onclick="menu('a_b_','2'); mostrar('aa','1');" title="Ayuda">? -</a></div>
<div class="div_ayuda" id="a_b_" style="display:none;">Seleccione Reporte genérico o Reporte especial, para mayor información referirse al manual ya que para los reportes especiales se requiere nombrar las páginas de demás contenidos. El tipo de reporte suele ser interno o de extensión cuando las actividades se realizan fuera de la biblioteca. El número de sesiones o mediciones hace referencia a cuando se tiene establecido una cantidad a cumplir, este dato se complementa al hacer la configuración mensual de estadísticas para cada biblioteca.</div>
</div>
</form>
<!-- InstanceEndEditable -->
<form name="chang_elemt" id="chang_elemt" target="contenido" method="post">
<!-- InstanceBeginEditable name="campos" -->

<!-- InstanceEndEditable -->
</form>
</div></body>

<script language="javascript">
verif();
</script>

<!-- InstanceEnd --></html>
<?php
include("../Connections/conect.inc.php");
$sql = "select Biblioteca, Sesiones, Fecha from report_bib where Reporte = ".$_GET["bib"]." and Fecha = '".date("n/Y")."'";
$exc = mysqli_query($conect, $sql);
if(mysqli_num_rows($exc) == 0 && $rows["Continuo"] == 0)
{
	echo "<div align='center' class='inform'>No se ha generado la programaci&oacute;n para el presente mes: ".date("n/Y")."</div>";
}
else
{
for($i=0; $i<mysqli_num_rows($exc); $i++)
{ 
	$rows = mysqli_fetch_array($exc);
	?>
    <script language="javascript">
		document.getElementById("bib_"+<?php echo $rows["Biblioteca"]; ?>).value = <?php echo $rows["Sesiones"]; ?>;
		document.getElementById("bib_"+<?php echo $rows["Biblioteca"]; ?>).readOnly = true;
	</script>
	<?php
} 
mysqli_free_result($exc);
}
unset($sql,$exc,$row,$rows,$i, $bibs);
mysqli_close($conect);
?>
<script language="javascript">
 //parent.scroll_pos('<?php echo ($_GET["poss_alin"] * 16); ?>');
</script>