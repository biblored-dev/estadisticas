<?php
include("../Connections/conect.inc.php");
$sql = "select reporte.Nombre, reporte.Descripcion, areas.Nombre as Area from reporte, areas where areas.Id = reporte.Area order by Area";
$exc = mysqli_query($conect, $sql);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/contenedor.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php
include("../script/scripts/session.php");
//include("../script/breadcrumbs.php");
?>
<link href="../css/css.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../script/scripts/jquery.min.js"></script>
<script type="text/javascript" src="../script/menu_ocultar.js"></script>
<script type="text/javascript">
function verif() {
document.getElementById('datos').style.display='none';
}
function foco_in(z) {
document.getElementById(z).focus();
}
</script>
<title>.: Sistema general de estad&iacute;stica :.</title>
<link rel="shortcut icon" href="../img/favicon.ico" />
<!-- InstanceBeginEditable name="doctitle" -->
<script src="../script/c_color.js"></script>
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" --><!-- InstanceEndEditable -->
</head>
<body>
<div id="datos" align="center">
 <h4>Consolidando datos...<br />
 <img src="../img/loader.gif" width="16" height="16" /></h4>
 </div>
<div align="left"><?php //echo breadcrumbs(); ?></div>
<div align="center" class="popup">
<!-- InstanceBeginEditable name="contenidos" -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="2%">&nbsp;</td>
    <td width="96%" align="center">&nbsp;</td>
    <td width="2%">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td align="center">
    <div align="center"><strong>Listado de  reportes del sistema</strong></div>
    <table width="100%" border="1" cellspacing="0" cellpadding="0" id="area_1">
    <tr>
    <td width="4%"><div align="center"><strong>N&deg;</strong></div></td>
    <td width="24%"><div align="center"><strong>Nombre del reporte</strong></div></td>
    <td><div align="center"><strong>Descripci&oacute;n del reporte</strong></div></td>
    <td width="18%"><div align="center"><strong>&Aacute;rea responsable</strong></div></td>
    </tr>
	<?php
    for($i=0; $i<mysqli_num_rows($exc); $i++)
	{ 
		$row = mysqli_fetch_array($exc);
	?>	
      <tr onclick="n_color('<?php echo ($i+1); ?>','area_1');">
        <td align="center"><?php echo ($i+1); ?></td>
        <td align="left"><?php echo $row["Nombre"]; ?></td>
        <td align="left"><?php echo substr($row["Descripcion"],0,70); ?> ...</td>
        <td align="left"><?php echo $row["Area"]; ?></td>
      </tr>
	<?php } ?>
    </table>
    </td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>
<!-- InstanceEndEditable -->
<form name="chang_elemt" id="chang_elemt" target="contenido" method="post">
<!-- InstanceBeginEditable name="campos" -->

<!-- InstanceEndEditable -->
</form>
</div></body>

<script language="javascript">
verif();
</script>

<!-- InstanceEnd --></html>