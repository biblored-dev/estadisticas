<?php include "../script/breadcrumbs.php"; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/contenedor.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php
include("../script/scripts/session.php");
//include("../script/breadcrumbs.php");
?>
<link href="../css/css.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../script/scripts/jquery.min.js"></script>
<script type="text/javascript" src="../script/menu_ocultar.js"></script>
<script type="text/javascript">
function verif() {
document.getElementById('datos').style.display='none';
}
function foco_in(z) {
document.getElementById(z).focus();
}
</script>
<title>.: Sistema general de estad&iacute;stica :.</title>
<link rel="shortcut icon" href="../img/favicon.ico" />
<!-- InstanceBeginEditable name="doctitle" -->
<script src="../script/c_color.js"></script>
<script language="javascript" src="../script/datosxml.js"></script>
<script language="javascript">
var err = "";
var id_b = 0;
function MaysPrimera(string){
  return string.charAt(0).toUpperCase() + string.slice(1);
}
function predataserver(w,x,y) {
  http.open("GET", "../script/datosxml.php?tab="+w+"&camp="+x+"&dat="+y, false); //False para que no continue el flujo hasta compeltar la petición ajax
  http.onreadystatechange = useHttpResp;
  return http.send(null);
}

function useHttpResp() {
if(http.readyState == 4)
{
	if(http.status == 200)
	{
		var timeValue = http.responseXML.getElementsByTagName("timenow")[0];
		if(parseInt(timeValue.childNodes[0].nodeValue) > 0)
		{
			err += "El nombre de la estadística ya existe. \n";
		}
		verif();
	}
} 
else
{
	document.getElementById('datos').style.display = "";
}
}

function reporte(w) {
if(w == 4)
{
	document.getElementById("formulario").value = "";
	document.getElementById("formulario").readOnly = false;
	document.getElementById("formulario").focus();
}
else
{
	document.getElementById("formulario").value = w;
	document.getElementById("formulario").readOnly = true;
}
}

function busqueda() {
var val, op;
err = "";
val = document.getElementById("nombre").value;
document.getElementById("nombre").value = MaysPrimera(val);
if(val == null || !isNaN(val) || val.length < 3 || /^\s+$/.test(val))
	err += "Se requiere el nombre del reporte. \n";
else
	predataserver('reporte','Nombre',document.getElementById("nombre").value) //tabla, campo, dato
val = document.getElementById("formulario").value;
if(val == null || !isNaN(val) || val.length < 3 || /^\s+$/.test(val))
	err += "Se requiere el nombre del formulario. \n";
op = document.getElementById("area_r").selectedIndex;
val = document.getElementById("area_r")[op].value;
if(val == null || val.length == 0 || /^\s+$/.test(val))
	err += "Se requiere el nombre del área responsable. \n";
op = document.getElementById("tip_r").selectedIndex;
val = document.getElementById("tip_r")[op].value;
if(val == null || val.length == 0 || /^\s+$/.test(val))
	err += "Se requiere el tipo de reporte. \n";
val = document.getElementById("campo").value;
document.getElementById("campo").value = MaysPrimera(val);
if(val == null || !isNaN(val) || val.length < 5 || /^\s+$/.test(val))
	err += "Se requiere el nombre del dato a reportar. \n";
if(document.getElementById("mensual").checked == true)
	document.getElementById("mensual").value = "1";
else
	document.getElementById("mensual").value = "0";	
if(err.length > 0)
{
	alert("Verifique los siguientes errores: \n\n"+err+"\n");
	return false;
}
else
	return true;
}
function serv_mensual(w) {
var z = window.open(w, 'Servicio mensual', 'resizable=yes,menubar=no,scrollbars=yes,width=660,height=720');
z.focus();
}
</script>
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" --><!-- InstanceEndEditable -->
</head>
<body>
<div id="datos" align="center">
 <h4>Consolidando datos...<br />
 <img src="../img/loader.gif" width="16" height="16" /></h4>
 </div>
<div align="left"><?php //echo breadcrumbs(); ?></div>
<div align="center" class="popup">
<!-- InstanceBeginEditable name="contenidos" -->
<?php
if(isset($_POST["nombre"], $_POST["guardar"], $_POST["id_b"]))
{
	include("../Connections/conect.inc.php");
	$mensual = 0;
	if(isset($_POST["mensual"]))
		$mensual = $_POST["mensual"];
	$sql = "insert into reporte (Id, Nombre, Area, Sub, Formulario, Tipo, Campo, Descripcion, Continuo) VALUES ('', '".$_POST["nombre"]."', '".$_POST["area_r"]."', '".$_POST["categori"]."', '".$_POST["formulario"]."', '".$_POST["tip_r"]."', '".ucfirst($_POST["campo"])."', '".addslashes($_POST["descrp"])."', '".$mensual."')";
	$exc = mysqli_query($conect, $sql);
	if($exc)
	{
		$id = mysqli_insert_id($conect);
		$sql = "select Id from biblioteca where Id <> 20";
		$exc = mysqli_query($conect, $sql);
		for($i=0; $i<mysqli_num_rows($exc); $i++)
		{ 
			$row = mysqli_fetch_array($exc);
			$sql = "insert into report_bib (Id, Biblioteca, Reporte, Sesiones, Fecha) VALUES ('', '".$row['Id']."', '".$id."', '1', '".date('Y-n-j')."')";
			$excs = mysqli_query($conect, $sql);
		}
		echo "<h4 align='center'>Registro creado</h4>";
		echo "<h4 align='center'>Fecha para registrar ".date("n/Y")."</h4>";
		echo "<h4 align='center'>Actualice el n&uacute;mero de sesiones</h4>";
		?><script language="javascript">parent.consulta.location.reload();</script><?php
	}
	else
	{
		echo "<h4 align='center'>Error al crear el registro</h4>";
	}
	unset($sql,$exc,$mensual,$id,$i,$row);
	mysqli_close($conect);
}
else
{
	if(isset($_GET["a_n"], $_GET["a_i"]))
	{
		$rows["Nombre"] = $_GET["a_n"];
		$rows["Area"] = $_GET["a_i"];
	}
	if(!isset($_SESSION['MM_Biblio_Autentic']))
	{ 
	include("../script/loggin.php");
	?>
	<center><strong>No posee privilegios para este m&oacute;dulo.<br /><br />
	<a href="javascript:form();">Inicie sesi&oacute;n.</a></strong><br /><br />
	<strong><a href="javascript:void(0);" onclick="document.location = 's_reporte.php?a_n=<?php echo $_GET["a_n"]; ?>&a_i=<?php echo $_GET["a_n"]; ?>';" title="Nuevo reporte">Solicitar reporte</a></strong></center>
	<?php
	}
	else
	{
	?>
	<div align="center" style="padding-bottom:5px;"><strong>Formulario para ingresar reporte al sistema</strong></div>
	<form name ="formiden" method ="POST" action ="n_reporte.php" onsubmit="return busqueda();">
	<input name="id_b" id="id_b" type="hidden" value="<?php echo $_GET["bib"]; ?>" />
	<input name="bi_n" id="bi_n" type="hidden" value="<?php echo $_GET["nom"]; ?>" />
	<input name="formulario" id="formulario" type="hidden" />
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	  <tr>
		<td width="4%" valign="top" rowspan="6"><div align="right"><img src="../icon/reporte.png" width="34" height="35" alt="icono" /></div></td>
		<td width="46%">
		<div align="left"><strong>Seleccione el tipo de reporte:</strong></div>
		<select name ="t_report" id="t_report" onchange="reporte(this.value);">
			<option value="">Listado de tipos de reporte</option>
			<option value="talleres.php">Reporte gen&eacute;rico</option>
			<option value="consolidado.php">Reporte consolidado</option>
			<option value="ponderado.php">Reporte ponderado</option>
		</select>
		<div class="infos">Consulte el enlace de 'Ejemplo de reporte' para identificar el que se ajusta al reporte que se est&aacute; creando</div>
		</td>
		<td width="46%">
		  <div align="left"><strong>Nombre del dato a reportar:</strong></div>
		  <div align="left"><input name="campo" id="campo" type="text" size="50" />
		  <div class="infos">Indique que representa el valor que se reportar&aacute;, por ejemplo, cantidad de pr&eacute;stamos, cantidad de afiliaciones, n&uacute;mero de participantes, porcentaje de avance …</div></div>
		</td>
		<td width="4%" valign="top" rowspan="6">
		</tr>
	  <tr>
		<td><div align="left"><strong>Nombre del reporte:</strong></div>
		<div align="left"><input name="nombre" id="nombre" type="text" size="50" /></div></td>
		<td><div align="left"><?php include("../script/areas.php"); ?></div></td>
	  </tr>
	  <tr>
		<td colspan="2">
		<div align="center" style="width:45%; margin:0px auto;"><?php include("../script/categorias.php"); ?></div>
		</td>
	  </tr>
	  <tr>  
		<td>
		<div align="left"><strong>Tipo de reporte:</strong></div>
		<div align="left" style="padding-left:10px;">
		<?php include("../script/tipo_report.php"); ?>
		</div>
		</td>
		<td>
		<div align="left"><strong>El reporte se debe diligenciar autom&aacute;ticamente, una vez al mes:</strong></div>
		<div align="left"><input name="mensual" id="mensual" type="checkbox" value="" /> &nbsp; Ejecutar autom&aacute;ticamente</div>
		</td>
	  </tr>
	  <tr>
		<td colspan="2">
		<div align="left"><strong>Descripci&oacute;n del reporte:</strong></div>
		<div align="left"><textarea name="descrp" id="descrp" rows="2"></textarea>
		<div class="infos">M&aacute;ximo 500 caracteres</div></div>
		</td>
	  </tr>
	</table>
	<div align="center" id="db_guardar"><input name="guardar" id="guardar" type="submit" value="Guardar" />
	<div class="div_menu" id="aa_1"><a href="javascript:void(0);" onclick="menu('a_b_','1'); mostrar('bb','1');" title="Ayuda">? +</a></div>
	<div class="div_menu" style="display:none;" id="bb_1"><a href="javascript:void(0);" onclick="menu('a_b_','2'); mostrar('aa','1');" title="Ayuda">? -</a></div>
	<div class="div_ayuda" id="a_b_" style="display:none;">Reporte gen&eacute;rico: Corresponde a la informaci&oacute;n que se toma de los talleres realizados, fecha, tiempo empleado, n&uacute;mero de participantes, franja poblacional.<br />Reporte consolidado: Corresponde a una cantidad a reportar o que se sumar&aacute;n de cuerdo a la cantidad de reportes creados para la misma actividad.<br />Reporte ponderado: Similar al anterior pero manejado como porcentaje de avance hasta completar el cien por ciento.<br />El tipo de reporte suele ser interno o de extensi&oacute;n cuando las actividades se realizan fuera de la biblioteca.<br />Al activar la casilla "Ejecutar automáticamente", el reporte se marcar&aacute; autom&aacute;ticamente en 1 cada mes.<br /><a href="javascript:void(0);" onclick="serv_mensual('ejem_generico.php?bib=00&nom=Nombre%20Biblioteca&Reporte=Nombre del reporte tipo genérico&id_rep=0000&fech_rep=date(mes/año)&id_report=000&alias=Nombre%20genéricos%20dado en la biblioteca&campo=Dato%20que%20reportará')">Ejemplos de reporte</a></div></div>
	</form>
	<script>
		foco_in('nombre');
	</script>
	<?php 
	unset($rows);
	}
?>
<script language="javascript">
window.onload = function() {
document.getElementById("area_r").onchange = function(){precategoria()};
precategoria();
}
</script>
<?php
}
?>
<!-- InstanceEndEditable -->
<form name="chang_elemt" id="chang_elemt" target="contenido" method="post">
<!-- InstanceBeginEditable name="campos" -->

<!-- InstanceEndEditable -->
</form>
</div></body>

<script language="javascript">
verif();
</script>

<!-- InstanceEnd --></html>