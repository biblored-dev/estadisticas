<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/contenedor.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php
include("../script/scripts/session.php");
//include("../script/breadcrumbs.php");
?>
<link href="../css/css.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../script/scripts/jquery.min.js"></script>
<script type="text/javascript" src="../script/menu_ocultar.js"></script>
<script type="text/javascript">
function verif() {
document.getElementById('datos').style.display='none';
}
function foco_in(z) {
document.getElementById(z).focus();
}
</script>
<title>.: Sistema general de estad&iacute;stica :.</title>
<link rel="shortcut icon" href="../img/favicon.ico" />
<!-- InstanceBeginEditable name="doctitle" -->
<script language="javascript">
function busqueda() {
var val, op;
err = "";
val = document.getElementById("nombre").value;
if(val == null || !isNaN(val) || val.length < 3 || /^\s+$/.test(val))
	err += "Se requiere el nombre del reporte. \n";
op = document.getElementById("area_r").selectedIndex;
val = document.getElementById("area_r")[op].value;
if(val == null || val.length == 0 || /^\s+$/.test(val))
	err += "Se requiere el nombre del área responsable. \n";
op = document.getElementById("tip_r").selectedIndex;
val = document.getElementById("tip_r")[op].value;
if(val == null || val.length == 0 || /^\s+$/.test(val))
	err += "Se requiere el tipo de reporte. \n";
val = document.getElementById("campo").value;
if(val == null || !isNaN(val) || val.length < 5 || /^\s+$/.test(val))
	err += "Se requiere el nombre del dato a reportar. \n";
if(err.length > 0)
{
	alert("Verifique los siguientes errores: \n\n"+err+"\n");
	return false;
}
else
	return true;
}
</script>
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" --><!-- InstanceEndEditable -->
</head>
<body>
<div id="datos" align="center">
 <h4>Consolidando datos...<br />
 <img src="../img/loader.gif" width="16" height="16" /></h4>
 </div>
<div align="left"><?php //echo breadcrumbs(); ?></div>
<div align="center" class="popup">
<!-- InstanceBeginEditable name="contenidos" -->
<?php
if(isset($_POST["nombre"], $_POST["guardar"], $_POST["campo"]))
{
	include("../Connections/conect.inc.php");
	$sql = "insert into solicitud (Id, Nombre, Campo, Area, Tipo, Fecha, Descripcion) VALUES ('', '".$_POST["nombre"]."', '".ucfirst($_POST["campo"])."', '".$_POST["area_r"]."', '".$_POST["tip_r"]."', '".date("Y-n-j")."', '".addslashes($_POST["descrp"])."')";
	$exc = mysqli_query($conect, $sql);
	if($exc)
	{
		echo "<h4 align='center'>Registro creado</h4>";
		echo "<h4 align='center'>Fecha de solicitud ".date("Y-n-j")."</h4>";
	}
	else
	{
		echo "<h4 align='center'>Error al crear el registro</h4>";
	}
	unset($sql,$exc);
	mysqli_close($conect);
}
else
{
?>
<form name ="formiden" method ="POST" action ="s_reporte.php" onsubmit="return busqueda();">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="10%">&nbsp;</td>
    <td width="80%">&nbsp;</td>
    <td width="10%">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><table width="100%" border="0" cellspacing="1" cellpadding="0">
      <tr>
        <td width="50%"><div align="left"><strong>Nombre del reporte:</strong></div>
    <div align="left"><input name="nombre" id="nombre" type="text" size="50" /></div></td>
        <td width="50%" rowspan="2"><div align="left"><strong>Nombre del dato a reportar:</strong></div>
      <div align="left"><input name="campo" id="campo" type="text" size="50" />
      <div class="infos">Indique que representa el valor que se reportar&aacute;, por ejemplo, cantidad de pr&eacute;stamos, cantidad de afiliaciones, n&uacute;mero de participantes, porcentaje de avance …</div></div></td>
      </tr>
      <tr>
      	<td>
        <div align="left"><strong>&Aacute;rea responsable del reporte:</strong></div>
        <div align="left"><?php include("../script/areas.php"); ?></div>
        </td>
        </tr>
      <tr>
        <td><div align="left"><strong>Tipo de reporte:</strong></div>
        <div align="left" style="padding-left:10px;">
        <?php include("../script/tipo_report.php"); ?>
        </div></td>
        <td>&nbsp;
        
        </td>
      </tr>
      <tr>
        <td colspan="2">
        <div align="left"><strong>Descripci&oacute;n del reporte:</strong></div>
        <div align="left"><textarea name="descrp" id="descrp" rows="2"></textarea>
        <div class="infos">M&aacute;ximo 500 caracteres</div></div>
        </td>
      </tr>
      <tr>
        <td colspan="2"><div align="center"><input name="guardar" id="guardar" type="submit" value="Solicitar" /></div></td>
        </tr>
    </table></td>
    <td>&nbsp;</td>
  </tr>
</table>
</form>
<?php } ?>
<div align="justify" id="db_guardar">&nbsp; <!--Insertar mensaje de ayuda para la página -->
<div class="div_menu" id="aa_1"><a href="javascript:void(0);" onclick="menu('a_b_','1'); mostrar('bb','1');" title="Ayuda">? +</a></div>
<div class="div_menu" style="display:none;" id="bb_1"><a href="javascript:void(0);" onclick="menu('a_b_','2'); mostrar('aa','1');" title="Ayuda">? -</a></div>
<div class="div_ayuda" id="a_b_" style="display:none;"><!-- Quitar comentarios e insertar el texto de ayuda. Aparecerá flotando en el pie de la página.--></div></div>
<!-- InstanceEndEditable -->
<form name="chang_elemt" id="chang_elemt" target="contenido" method="post">
<!-- InstanceBeginEditable name="campos" -->

<!-- InstanceEndEditable -->
</form>
</div></body>

<script language="javascript">
verif();
</script>

<!-- InstanceEnd --></html>