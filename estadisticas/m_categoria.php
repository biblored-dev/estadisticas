<?php include "../script/breadcrumbs.php"; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/contenedor.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php
include("../script/scripts/session.php");
//include("../script/breadcrumbs.php");
?>
<link href="../css/css.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../script/scripts/jquery.min.js"></script>
<script type="text/javascript" src="../script/menu_ocultar.js"></script>
<script type="text/javascript">
function verif() {
document.getElementById('datos').style.display='none';
}
function foco_in(z) {
document.getElementById(z).focus();
}
</script>
<title>.: Sistema general de estad&iacute;stica :.</title>
<link rel="shortcut icon" href="../img/favicon.ico" />
<!-- InstanceBeginEditable name="doctitle" -->
<script language="javascript" src="../script/datosxml.js"></script>
<script language="javascript">
var err = "";
function MaysPrimera(string){
  return string.charAt(0).toUpperCase() + string.slice(1);
}
function predataserver(w,x,y) {
  http.open("GET", "../script/datosxml.php?tab="+w+"&camp="+x+"&dat="+y, false); //False para que no continue el flujo hasta compeltar la petición ajax
  http.onreadystatechange = useHttpResp;
  return http.send(null);
}

function useHttpResp() {
if(http.readyState == 4)
{
	if(http.status == 200)
	{
		var timeValue = http.responseXML.getElementsByTagName("timenow")[0];
		if(parseInt(timeValue.childNodes[0].nodeValue) > 0)
		{
			err += "El nombre del área ya existe. \n";
			document.getElementById('no_send').innerHTML = "El nombre del área ya existe.";
		}
		verif();
	}
} 
else
{
	document.getElementById('datos').style.display = "";
}
}

function busqueda() {
var val;
err = "";
val = document.getElementById("nombre").value;
document.getElementById("nombre").value = MaysPrimera(val);
if(val == null || !isNaN(val) || val.length < 3 || /^\s+$/.test(val))
	err += "Se requiere el nombre de la categoría. \n";
else	
	predataserver('areas','Nombre',document.getElementById("nombre").value) //tabla, campo, dato
if(err.length > 0)
{
	alert("Verifique los siguientes errores: \n\n"+err+"\n");
	return false;
}
else
	return true;
}
window.onload = function() {
document.getElementById("area_r").onchange = function(){asig_non_are(this.selectedIndex);};
}
</script>
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" --><!-- InstanceEndEditable -->
</head>
<body>
<div id="datos" align="center">
 <h4>Consolidando datos...<br />
 <img src="../img/loader.gif" width="16" height="16" /></h4>
 </div>
<div align="left"><?php //echo breadcrumbs(); ?></div>
<div align="center" class="popup">
<!-- InstanceBeginEditable name="contenidos" -->
<?php
if(isset($_POST["id"], $_POST["nombre"], $_POST["guardar"], $_POST["area_r"]))
{
	require_once("../Connections/conect.inc.php");
	/*include("../script/cant.php");
	if(cant("areas","Nombre",$_POST["nombre"]) == 0)*/ //tabla, campo, dato
	{
		$sql = "update categorias set Nombre = '".$_POST["nombre"]."', Area = ".$_POST["area_r"].", Descripcion = '".addslashes($_POST["descrip"])."' where Id = ".$_POST["id"];
		//echo $sql;
		$exc = mysqli_query($conect, $sql);
		if($exc)
		{
			$_GET["nom"] = $_POST["nombre"];
			echo "<h4 align='center'>Registro actualizado</h4>";
			?><script language="javascript">
			parent.consulta.location.reload();
            document.location = "../fill.php"
            </script><?php
		}
		else
		{
			echo "<h4 align='center'>Error al actualizar el registro</h4>";
		}
	}
	/*else
	{
		echo "<h4 align='center'>El nombre del areas ya existe</h4>";
	}*/
	//mysqli_free_result($exc);
	unset($sql,$exc,$row);
	mysqli_close($conect);	
}
if(!isset($_SESSION['MM_Biblio_Autentic']))
{ 
include("../script/loggin.php");
?>
<center><strong>No posee privilegios para este m&oacute;dulo.<br /><br />
<a href="javascript:form();">Inicie sesi&oacute;n.</a></strong><br /><br /></center>
<?php
}
else
{
$rows["Nombre"] = $_GET["area"];
$rows["Area"] = $_GET["id"];
$rows["Descript"] = $_GET["descript"];
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="30%">&nbsp;</td>
    <td width="40%">&nbsp;</td>
    <td width="30%">&nbsp;</td>
  </tr>
  <tr>
    <td valign="top"><div align="right"><img src="../icon/areas.png" width="41" height="35" alt="icono" /></div></td>
    <td align="center">
    <fieldset>
    <legend align="center"><strong>Formulario para modificar categor&iacute;a</strong></legend>
    <div class="x_fieldset"><a href="javascript:void(0);" onclick="document.location = '../fill.php';" title="Cerrar">X</a></div>
    <form name ="formiden" method ="POST" action ="m_categoria.php" onsubmit="return busqueda();">
    <input name="id" id="id" type="hidden" value="<?php echo $_GET["bib"]; ?>" />
    <p align="left"><strong>Nombre de la categor&iacute;a:</strong></p>
    <div align="left"><input name="nombre" id="nombre" type="text" size="50" value="<?php echo $_GET["nom"]; ?>" /></div>
    <p align="left"><strong>Descripci&oacute;n de la categor&iacute;a:</strong></p>
    <div style="overflow:hidden; clear:both; height:110px; padding:5px;">
    <textarea name="descrip" id="descrip" class="txt_text" rows="6"><?php echo $_GET["descript"]; ?></textarea></div>
    <p align="left" style="clear:both;"><?php include("../script/areas.php"); ?></p>
    <p align="center"><input name="guardar" id="guardar" type="submit" value="Guardar" /></p>
    </form>
    <script>
		foco_in('nombre');
	</script>
    </fieldset>
    </td>
    <td><div id="no_send"></div></td>
  </tr>
</table>
<?php } ?>
<!-- InstanceEndEditable -->
<form name="chang_elemt" id="chang_elemt" target="contenido" method="post">
<!-- InstanceBeginEditable name="campos" -->

<!-- InstanceEndEditable -->
</form>
</div></body>

<script language="javascript">
verif();
</script>

<!-- InstanceEnd --></html>