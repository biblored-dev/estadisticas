<?php if(!isset($_SESSION)) { session_start(); } ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="google-signin-scope" content="profile email">
<meta name="google-signin-client_id" content="292751117875-7hc3ra4u3lha2fkmv44fpns2v2ko65mm.apps.googleusercontent.com">
<link href="css/css.css" rel="stylesheet" type="text/css" />
<title>.: Sistema general de estad&iacute;stica :.</title>
<script language="javascript" src="script/datosxml.js"></script>
<script language="javascript">
function verif() {
document.getElementById('datos').style.display='none';
}
function useHttpResp() {
if(http.readyState == 4)
{
	if(http.status == 200)
	{
		var timeValue = http.responseXML.getElementsByTagName("timenow")[0];
		if(timeValue.childNodes.length > 0)
		{
			document.getElementById('sesion').style.display = "none";
			verif();
			var regs = timeValue.childNodes[0].nodeValue;
			regs = regs.split(";");
			if(parseInt(regs[0]) > 0)
			{
				//window.parent.des_menu();
				window.parent.location = "index.php";
			}
			else
			{
				alert("No se ha podido autenticar la cuenta, \n informe al director de nodo o CST \n para verificar los permisos de acceso.")
				document.getElementById("users").innerHTML += "<h4>No se ha podido autenticar la cuenta, informe al director de nodo o CST para verificar los permisos de acceso.</h4>";
				document.getElementById('log_out').style.display = "";
			}
		}
	}
} 
else
{
	document.getElementById('datos').style.display = "";
}
}
</script>
</head>
<body>
<div id="datos" align="center">
<h4>Validando datos de usuario ...<br />
<img src="img/loader.gif" width="16" height="16" /></h4>
</div>
<div style="clear:both; margin-top:10px;" align="center" id="users"></div>
<div style="clear:both;" align="center" id="sesion">
<p>&nbsp;</p>
<p>&nbsp;</p>
<?php
if(isset($_POST["token"], $_POST["corr"], $_POST["nomm"]))
{ ?>
<script language="javascript">
function onSignIn(x,y) {
http.open("GET", "script/authxml.php?tab="+x+"&camp="+y, true); //False para que no continue el flujo hasta compeltar la petición ajax
http.onreadystatechange = useHttpResp;
return http.send(null);
}
onSignIn('<?php echo $_POST["corr"]; ?>','<?php echo $_POST["nomm"]; ?>')
document.getElementById("sesion").style.display = "none";
</script>
<?php } ?>
</div>
<div id="log_out" style="display:none; clear:both;" align="center">
<p>&nbsp;</p>
<h3>Cierre la sesión y vuelva a empezar.</h3>
<h3>
<a href="https://accounts.google.com/Logout" onclick="signOut();" target="_blank">Cerrar sesión</a>
</h3>
<script language="javascript">
function signOut() {
window.parent.location = "index.php";
}
</script>
</div>
</body>
</html>