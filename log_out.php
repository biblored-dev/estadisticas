<?php
include("script/scripts/session.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>.: Sistema general de estad&iacute;stica :.</title>
<link href="css/css.css" rel="stylesheet" type="text/css" />
<link rel="shortcut icon" href="img/favicon.ico" />
</head>
<body>
<?php
$_SESSION['MM_Bib_Nod'] = $_SESSION['MM_Bib_Id'] = $_SESSION['MM_Bib_Nom'] = $_SESSION['MM_Usr_Nom'] = $_SESSION['MM_Usr_Pri'] = $_SESSION['MM_Usr_Are'] = $_SESSION['MM_Biblio_Autentic'] = $_SESSION['MM_Usr_Cor'] = NULL;
unset($_SESSION['MM_Bib_Nod'], $_SESSION['MM_Bib_Id'], $_SESSION['MM_Bib_Nom'], $_SESSION['MM_Usr_Nom'], $_SESSION['MM_Usr_Pri'], $_SESSION['MM_Usr_Are'], $_SESSION['MM_Biblio_Autentic'], $_SESSION['MM_Usr_Cor']);
session_destroy();
//header("Location: ". "aut_v2.php");
?>
<script language="javascript">
window.parent.location = "index.php";
</script>
</body>
</html>