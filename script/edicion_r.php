<?php
$r = "";
if(isset($_GET["fecha_s"]))
{
	$f_r = explode("/",$_GET["fecha_s"]);
	$f1 = strtotime(date("Y")."-".date("m")."-".date("d"));
	$f2 = strtotime(date($f_r[1]."-".$f_r[0]."-".date('t', mktime(0, 0, 0, $f_r[0], 1, $f_r[1]))));
	$f3 = $f1 - $f2;
	$r = ($f3 / 86400);
	unset($f_r, $f1, $f2, $f3);
}
header('Content-Type: text/xml');
echo "<?xml version=\"1.0\"?><clock1><timenow>".$r."</timenow></clock1>";
?>