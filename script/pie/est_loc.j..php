/* $("#"+w).animate({ height: "400px" }, { queue: true, duration: 3000 }) */
function menu(w,x) {
if(x == 1)
	$("#"+w).show("slow");
else
	$("#"+w).hide("slow");
}
function add_div(w,x) {
var div = document.createElement('div');
div.id = w;
div.style = "min-width: 400px; height: 350px; margin: 0 auto";
document.getElementById(x).appendChild(div);
}
function espacios(w,x,y,z,a) {
var categorias = ["Salas de lectura", "Sala infantil", "Internet", "Multimedia", "Hemeroteca", "Sonoteca", "Videoteca", "Cubículos de lectura individual", "Sala de trabajo en grupo", "Ludoteca", "Sala de tareas", "Bebeteca"];
var campos = ["S1", "S2", "S3", "S4", "S5", "S6", "S7", "S8", "S9", "S10", "S11", "S12"];
if(!(document.getElementById("dv_" + campos[0])))
{
var table = document.getElementById(w);
for(i=0;i<categorias.length;i++)
{
	if(i%2 == 0)
    {
        var row = table.insertRow(0);
        row.bgcolor = "#FFFFFF";
        row.valign = "top";
    }
    var cell1 = row.insertCell(0);
    cell1.width = "50%";
    if(i < categorias.length)
    {
        if(!(document.getElementById("dv_" + campos[i])))
        {
            div = document.createElement('div');
            div.id = "dv_" + campos[i];
            div.innerHTML = "<strong>"+categorias[i]+"</strong>";
            cell1.appendChild(div);
        }
        datos(campos[i], x, "dv_" + campos[i], i, z);
    }       
}
}
else
	datos(w,x,y,z,'',a);
}
function atencion(w,x,y,z,a) {
var categorias = ["Encuentra la información que busca", "Recibe la ayuda que requiere", "El trato de los funcionarios es amable y cordial", "La información proporcionada es clara y precisa", "Encuentra sitio para lectura / consulta", "El horario se ajusta a sus necesidades", "Las condiciones ambientales son adecuadas"];
var campos = ["C1", "C2", "C3", "C4", "C5", "C6", "C7", "C8"];
if(!(document.getElementById("dv_" + campos[0])))
{
var table = document.getElementById(w);
for(i = 0; i < categorias.length; i ++)
{
    if(i%2 == 0)
    {
        var row = table.insertRow(0);
        row.bgcolor = "#FFFFFF";
        row.valign = "top";
    }    
    var cell1 = row.insertCell(0);
    cell1.width = "50%";
    if(i < categorias.length)
    {
        if(!(document.getElementById("dv_" + campos[i])))
        {
            div = document.createElement('div');
            div.id = "dv_" + campos[i];
            div.innerHTML = "<strong>"+categorias[i]+"</strong>";
            cell1.appendChild(div);
        }    
        datos(campos[i], x, "dv_" + campos[i], i, z, a);
    }
}
}
else
	datos(w,x,y,z,'',a);
}
function datos(w,x,y,z,a,b) {
    if(document.getElementById(w))
    {
		window.scrollTo(0, ($("#"+y).position().top));
        $("#"+w).toggle("slow");
    }
    else
    {    
        add_div(w,y);
        var options = {
            chart: {
                renderTo: w,
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false
            },
            title: {
                text: ''
            },
            tooltip: {
                formatter: function() {
                    return '<b>' + this.point.name + '</b>: ' + this.y+ ' Registros';
                }
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        color: '#000000',
                        connectorColor: '#000000',
                        formatter: function() {
                            return '<b>' + this.point.name + '</b>: ' + this.y;
                        }
                    },
                    showInLegend: true
                }
            },
            series: []
        };
    
        $.getJSON("master/pie/data/data-loc.php?dat="+x+"&add="+z+"&where="+a+"&mes="+b, function(json) {
            options.series = json;
            chart = new Highcharts.Chart(options);
        window.scrollTo(0, ($("#"+y).position().top)); 
        });
    }
}