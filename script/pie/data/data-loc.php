<?php
#Pie Chart
//$categories = array(1=>"Salas de lectura", 2=>"Sala infantil", 3=>"Internet", 4=>"Multimedia", 5=>"Hemeroteca", 6=>"Sonoteca", 7=>"Videoteca", 8=>"Cubículos de lectura individual", 9=>"Sala de trabajo en grupo", 10=>"Ludoteca", 11=>"Sala de tareas", 12=>"Bebeteca");
require '../../conn/connection.php';
$rows['type'] = 'pie';
$rows['name'] = 'Pelawat';
if(strlen($_GET['where']) > 4)
	$_GET['where'] .= " AND Fecha BETWEEN '".date('Y-'.$_GET['mes'].'-1')."' AND '".date('Y-'.$_GET['mes'].'-31')."'";
else
	$_GET['where'] .= " WHERE Fecha BETWEEN '".date('Y-'.$_GET['mes'].'-1')."' AND '".date('Y-'.$_GET['mes'].'-31')."'";
if($_GET['dat'] == 0)
{
	$result = mysql_query("SELECT Localidad, COUNT(*) AS Total FROM encuesta".$_GET['where']." GROUP BY Localidad");
	$dts  = array(0=>"Otra población", 1=>"Usaquén", 2=>"Chapinero", 3=>"Santa Fé", 4=>"San Cristobal", 5=>"Usme", 6=>"Tunjuelito", 7=>"Bosa", 8=>"Kennedy", 9=>"Fontibón",10=>"Engativá", 11=>"Suba", 12=>"Barrios Unidos", 13=>"Teusaquillo", 14=>"Mártires", 15=>"Antonio Nariño", 16=>"Puente Aranda", 17=>"Candelaria", 18=>"Rafael Uribe", 19=>"Ciudad Bolivar", 20=>"Sumapaz");
}
if($_GET['dat'] == 1)
{
	$result = mysql_query("SELECT Biblioteca, COUNT(*) AS Total FROM encuesta".$_GET['where']." GROUP BY Biblioteca");
	$dts = array(0=>"", 1=>"Virgilio Barco", 2=>"Las Ferias", 3=>"Biblioteca Del Deporte", 4=>"Carlos E. Restrepo", 5=>"La Peña", 6=>"La Victoria", 7=>"Puente Aranda", 8=>"Julio Mario Santo Domingo", 9=>"Suba",10=>"Usaquén - Servitá", 11=>"El Tintal", 12=>"Bosa", 13=>"La Giralda", 14=>"Lago Timiza", 15=>"Gabriel García Márquez", 16=>"Arborizadora Alta", 17=>"Perdomo Soledad Lamprea", 18=>"Rafael Uribe Uribe", 19=>"Venecia Pablo de Tarso");
}
if($_GET['dat'] == 2)
{
	$result = mysql_query("SELECT Sexo, COUNT(*) AS Total FROM encuesta".$_GET['where']." GROUP BY Sexo");
	$dts = array(0=>"",1=>"Femenino", 2=>"Masculino");
}
if($_GET['dat'] == 3)
{
	$result = mysql_query("SELECT Edad, COUNT(*) AS Total FROM encuesta".$_GET['where']." GROUP BY Edad");
	$dts = array(0=>"",1=>"Entre 0 y 5 años", 2=>"Entre 6 y 12 años", 3=>"Entre 13 y 18 años", 4=>"Entre 19 y 26 años", 5=>"Entre 27 y 44 años", 6=>"Entre 45 y 59 años", 7=>"Mayor de 60 años");
}
if($_GET['dat'] == 4)
{
	$result = mysql_query("SELECT Escolaridad, COUNT(*) AS Total FROM encuesta".$_GET['where']." GROUP BY Escolaridad");
	$dts = array(0=>"",1=>"Preescolar", 2=>"Primaria", 3=>"Secundaria", 4=>"Posgrado", 5=>"Técnico", 6=>"Universitario", 7=>"Ninguno");
}
if($_GET['dat'] == 5)
{
	$dts = array(1=>"Estudiante", 2=>"Profesor (a)", 3=>"Empleado (a)", 4=>"Desempleado (a)", 5=>"Independiente", 6=>"Hogar", 7=>"Pensionado/jubilado", 8=>"Otro");
	for($i=1;$i<=sizeof($dts);$i++)
	{
		$result = mysql_query("SELECT COUNT(Ocupacion) AS Total FROM encuesta where Ocupacion like '%".$i.",%'".$_GET['where'],$con);
		$r = mysql_fetch_array($result);
		$rows['data'][] = array($dts[$i], $r['Total']);	
	}
}
if($_GET['dat'] == 6)
{
	$result = mysql_query("SELECT Rasgos, COUNT(*) AS Total FROM encuesta".$_GET['where']." GROUP BY Rasgos");
	$dts = array(0=>"No respondió",1=>"Afrodescendiente", 2=>"Indígena", 3=>"Palenquero", 4=>"Rom", 5=>"Raizal", 6=>"Ninguno de los anteriores", 7=>"Otro");
}
if($_GET['dat'] == 7)
{
	$result = mysql_query("SELECT Grupo, COUNT(*) AS Total FROM encuesta".$_GET['where']." GROUP BY Grupo");
	$dts = array(0=>"No respondió",1=>"Desplazado", 2=>"Desmovilizado/Reinsertado/Reincorporado", 3=>"LGTBI", 4=>"Comunidades rurales y campesinas", 5=>"Víctimas", 6=>"Persona con discapacidad", 7=>"Habitante de Calle", 8=>"Persona en ejercicio de prostitución", 9=>"Otro");
}
if($_GET['dat'] == 8)
{
	if(strlen($_GET['where']) > 0)
	{
		$result = mysql_query("SELECT DISTINCT(Nodo) FROM encuesta".$_GET['where']);
		$r = mysql_fetch_array($result);
		$result = mysql_query("SELECT Biblioteca AS Nodo, COUNT(Id) AS Total FROM encuesta WHERE Nodo = ".$r['Nodo']." AND Fecha BETWEEN '".date('Y-'.$_GET['mes'].'-1')."' AND '".date('Y-'.$_GET['mes'].'-31')."' GROUP BY Biblioteca");
		$dts = array(0=>"", 1=>"Virgilio Barco", 2=>"Las Ferias", 3=>"Biblioteca Del Deporte", 4=>"Carlos E. Restrepo", 5=>"La Peña", 6=>"La Victoria", 7=>"Puente Aranda", 8=>"Julio Mario Santo Domingo", 9=>"Suba",10=>"Usaquén - Servitá", 11=>"El Tintal", 12=>"Bosa", 13=>"La Giralda", 14=>"Lago Timiza", 15=>"Gabriel García Márquez", 16=>"Arborizadora Alta", 17=>"Perdomo Soledad Lamprea", 18=>"Rafael Uribe Uribe", 19=>"Venecia Pablo de Tarso");
	}
	else
	{
		$result = mysql_query("SELECT Nodo, COUNT(*) AS Total FROM encuesta".$_GET['where']." GROUP BY Nodo");
		$dts = array(0=>"",1=>"Virgilio Barco", 2=>"Carlos E. Restrepo", 3=>"Julio Mario Santo Domingo", 4=>"El Tintal Manuel Zapata Olivella", 5=>"Gabriel García Márquez");
	}
}
if($_GET['dat'] == 9)
{
	$dts = array(1=>"Como apoyo a labores académicas", 2=>"Por trabajo", 3=>"Diversión y/o entretenimiento", 4=>"Lugar de encuentro", 5=>"Ninguno de los anteriores", 6=>"Otro");
	for($i=1;$i<=sizeof($dts);$i++)
	{
		$result = mysql_query("SELECT COUNT(Utiliza) AS Total FROM encuesta where Utiliza like '%".$i.",%'".$_GET['where'],$con);
		$r = mysql_fetch_array($result);
		$rows['data'][] = array($dts[$i], $r['Total']);	
	}
}
if($_GET['dat'] == 10)
{
	$result = mysql_query("SELECT Frecuencia, COUNT(*) AS Total FROM encuesta".$_GET['where']." GROUP BY Frecuencia");
	$dts = array(0=>"",1=>"Una vez a la semana", 2=>"Más de una vez a la semana", 3=>"Una vez al mes");
}
if($_GET['dat'] == 11)
{
	$result = mysql_query("SELECT Permanencia, COUNT(*) AS Total FROM encuesta".$_GET['where']." GROUP BY Permanencia");
	$dts = array(0=>"",1=>"Menos de una hora", 2=>"Más de una hora", 3=>"Más de dos horas", 4=>"Todo el día");
}
if($_GET['dat'] == 12)
{
	$dts = array(1=>"No conoce otra", 2=>"Considera que en esta lo atienden mejor", 3=>"Por su ubicación", 4=>"En ésta se encuentra la información que necesita", 5=>"Por el horario", 6=>"Las instalaciones le resultan agradables", 7=>"Por las actividades", 8=>"Por facilidades de acceso", 9=>"Otra");
	for($i=1;$i<=sizeof($dts);$i++)
	{
		$result = mysql_query("SELECT COUNT(Escogio) AS Total FROM encuesta where Escogio like '%".$i.",%'".$_GET['where'],$con);
		$r = mysql_fetch_array($result);
		$rows['data'][] = array($dts[$i], $r['Total']);	
	}	
}
if($_GET['dat'] == 13)
{
	$_GET['add'] = $_GET['add'] + 1;
	$result = mysql_query("SELECT S".$_GET['add'].", COUNT(*) AS Total FROM encuesta".$_GET['where']." GROUP BY S".$_GET['add']."");
	$dts = array(0=>"No existe, Ns/Nr", 1=>"Lo conoce", 2=>"Lo ha utilizado");
}
if($_GET['dat'] == 14)
{
	$_GET['add'] = $_GET['add'] + 1;
	$result = mysql_query("SELECT C".$_GET['add'].", COUNT(*) AS Total FROM encuesta".$_GET['where']." GROUP BY C".$_GET['add']."");
	$dts = array(0=>"",1=>"Siempre", 2=>"Casi siempre", 3=>"Algunas veces", 4=>"Nunca");
}
if($_GET['dat'] == 15)
{
	$dts = array(1=>"Talleres de escritura y lectura para jóvenes y adultos",2=>"Cine foros", 3=>"Talleres de lectura y escritura para niños", 4=>"Charlas y/o conferencias", 5=>"Talleres de escritura y/o para adultos mayores", 6=>"Exposiciones", 7=>"Actividades culturales");
	for($i=1;$i<=sizeof($dts);$i++)
	{
		$result = mysql_query("SELECT COUNT(Programas) AS Total FROM encuesta where Programas like '%".$i.",%'".$_GET['where'],$con);
		$r = mysql_fetch_array($result);
		$rows['data'][] = array($dts[$i], $r['Total']);	
	}	
}
if($_GET['dat'] == 16)
{
	$result = mysql_query("SELECT Afiliado, COUNT(*) AS Total FROM encuesta".$_GET['where']." GROUP BY Afiliado");
	$dts = array(0=>"",1=>"No", 2=>"Si");
}
if($_GET['dat'] == 17)
{
	$dts = array(1=>"Virgilio Barco", 2=>"Las Ferias", 3=>"Biblioteca Del Deporte", 4=>"Carlos E. Restrepo", 5=>"La Peña", 6=>"La Victoria", 7=>"Puente Aranda", 8=>"Julio Mario Santo Domingo", 9=>"Francisco José de Caldas", 10=>"Usaquén - Servitá", 11=>"El Tintal Manuel Zapata Olivella", 12=>"Bosa", 13=>"La Giralda", 14=>"Lago Timiza", 15=>"Gabriel García Márquez", 16=>"Arborizadora Alta", 17=>"Perdomo Soledad Lamprea", 18=>"Rafael Uribe Uribe", 19=>"Venecia Pablo de Tarso", 20=>"Otra (s)");
	for($i=1;$i<=sizeof($dts);$i++)
	{
		$result = mysql_query("SELECT COUNT(Bibliotecas) AS Total FROM encuesta where Bibliotecas like '%".$i.",%'".$_GET['where'],$con);
		$r = mysql_fetch_array($result);
		if($r['Total'] > 0)
			$rows['data'][] = array($dts[$i], $r['Total']);	
	}
}
if($_GET['dat'] == 18)
{
	
	$result = mysql_query("SELECT COUNT(servicios.Id) AS Total FROM encuesta, servicios where servicios.Encuesta = encuesta.Id".$_GET['where'],$con);
	$r = mysql_fetch_array($result);
	//$dts = array(0=>"Sin aportes",1=>"Con aportes");
	$rows['data'][] = array('Sin aportes', ($_GET['add'] - $r['Total']));
	$rows['data'][] = array('Con aportes', $r['Total']);
}
if($_GET['dat'] == 19)
{
	
	$result = mysql_query("SELECT COUNT(observaciones.Id) AS Total FROM encuesta, observaciones where observaciones.Encuesta = encuesta.Id".$_GET['where'],$con);
	$r = mysql_fetch_array($result);
	//$dts = array(0=>"Sin aportes",1=>"Con aportes");
	$rows['data'][] = array('Sin observaciones', ($_GET['add'] - $r['Total']));
	$rows['data'][] = array('Con observaciones', $r['Total']);
}

if($_GET['dat'] != 5 && $_GET['dat'] != 9 && $_GET['dat'] != 15 && $_GET['dat'] != 17 && $_GET['dat'] != 18 && $_GET['dat'] != 19)
{
	for($i=0;$i<mysql_num_rows($result);$i++)
	{
		$r = mysql_fetch_array($result);
		if($_GET['dat'] == 0)
			$rows['data'][] = array($dts[$r['Localidad']], $r['Total']);
		if($_GET['dat'] == 1)
			$rows['data'][] = array($dts[$r['Biblioteca']], $r['Total']);
		if($_GET['dat'] == 2)
			$rows['data'][] = array($dts[$r['Sexo']], $r['Total']);
		if($_GET['dat'] == 3)
			$rows['data'][] = array($dts[$r['Edad']], $r['Total']);
		if($_GET['dat'] == 4)
			$rows['data'][] = array($dts[$r['Escolaridad']], $r['Total']);	
		if($_GET['dat'] == 6)
			$rows['data'][] = array($dts[$r['Rasgos']], $r['Total']);	
		if($_GET['dat'] == 7)
			$rows['data'][] = array($dts[$r['Grupo']], $r['Total']);
		if($_GET['dat'] == 8)
			$rows['data'][] = array($dts[$r['Nodo']], $r['Total']);
		if($_GET['dat'] == 10)
			$rows['data'][] = array($dts[$r['Frecuencia']], $r['Total']);
		if($_GET['dat'] == 11)
			$rows['data'][] = array($dts[$r['Permanencia']], $r['Total']);
		if($_GET['dat'] == 13)
			$rows['data'][] = array($dts[$r['S'.$_GET['add']]], $r['Total']);
		if($_GET['dat'] == 14)
			$rows['data'][] = array($dts[$r['C'.$_GET['add']]], $r['Total']);
		if($_GET['dat'] == 16)
			$rows['data'][] = array($dts[$r['Afiliado']], $r['Total']);	
			
							
	}
}
$rslt = array();
array_push($rslt,$rows);
print json_encode($rslt, JSON_NUMERIC_CHECK);

mysql_close($con);
unset($dts, $i);


