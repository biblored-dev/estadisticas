﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="../css/css.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="scripts/jquery.min.js"></script>
<script type="text/javascript" src="menu_ocultar.js"></script>
<script type="text/javascript">
function verif() {
document.getElementById('datos').style.display='none';
}
function foco_in(z) {
document.getElementById(z).focus();
}
</script>
<title>.: Sistema general de estad&iacute;stica :.</title>
<link rel="shortcut icon" href="../img/favicon.ico" />
<script language="javascript" src="datosxml.js"></script>
<script language="javascript">
function predataserver(w) {
  document.getElementById("nom_nod").value = document.getElementById("nodo")[document.getElementById("nodo").selectedIndex].text;
  var sql = "select Id, Nombre from Biblioteca where Nodo = "+w;
  http.open("GET", "../script/datosxml_lII.php?d1=Id&d2=Nombre&dat="+sql, true); //False para que no continue el flujo hasta compeltar la petición ajax
  http.onreadystatechange = useHttpResp;
  return http.send(null);
}

function useHttpResp() {
if(http.readyState == 4)
{
	if(http.status == 200)
	{
		var salida = "";
	    var timeValue = http.responseXML.getElementsByTagName("timenow")[0];
	    var regs = timeValue.childNodes[0].nodeValue;
		regs = regs.split(";");
	    var listado = document.getElementById('biblo');
	    listado.length = regs.length;
	    listado.options[0].value = "";
	    listado.options[0].text = " Seleccione la biblioteca ";
	    listado.options.selectedIndex = 0;
		for(i = 0; i < (regs.length) - 1; i ++)
	    {
		   salida = regs[i].split(",,");
		   listado.options[i + 1].value = salida[0];
		   listado.options[i + 1].text = salida[1];
	    }
		verif();
	}
} 
else
{
	document.getElementById('datos').style.display = "";
}
}
function busqueda(){

document.getElementById("nom_bib").value = document.getElementById("biblo")[document.getElementById("biblo").selectedIndex].text;	
}
</script>
</head>
<body>
<div id="datos" align="center">
 <h4>Consolidando datos...<br />
 <img src="../img/loader.gif" width="16" height="16" /></h4>
 </div>
<div align="left"><?php //echo breadcrumbs(); ?></div>
<div align="center" class="popup">
<?php
if(isset($_POST["nodo"],$_POST["biblo"],$_POST["funcionario"]))
{
	if(!isset($_SESSION)) {
  	session_start();
	$_SESSION['MM_Biblio_Nod'] = $_POST["nodo"];
	$_SESSION['MM_Nom_Nod'] = $_POST["nom_nod"];
	$_SESSION['MM_Biblio_Bib'] = $_POST["biblo"];
	$_SESSION['MM_Nom_Bib'] = $_POST["nom_bib"];
	$_SESSION['MM_Biblio_Usr'] = $_POST["funcionario"];
	}
?>
<script language="javascript">parent.contenido.location = "../fill.php";</script>
<?php
}
else
{
	//http://www.uees.edu.sv/blogs/oscard/?p=145 
include("../Connections/conect.ldap.php");
echo "<br /><br />";//.$db;
//$ldaprdn = "CN=jpuentesb,CN=Users,DC=biblored,DC=local";
$ldaprdn = "CN=Administrator,CN=Users,DC=biblored,DC=local";
//$ldappass = "**Pekitas2016//";
$ldappass = "Ccom*DC2014";
$ldapbind = ldap_bind($db, $ldaprdn, $ldappass);
$ldapfind = "OU=FUNCIONARIOS,OU=BIBLIOTECAS,DC=biblored,DC=local";
if($ldapbind) {
	//echo "Conectado con el usuario";
	$result = ldap_search($db,$ldapfind, "(cn=*)") or die ("Error in search query: ".ldap_error($db));
	//$result = ldap_search($db,$ldapfind, "samAccountName='jpuentesb'") or die ("Error in search query: ".ldap_error($db));
    $data = ldap_get_entries($db, $result);
	print_r($data); 
	/*
	for ($i=0; $i<$data["count"]; $i++) {
            echo "dn is: ". $data[$i]["dn"] ."<br />";
            echo "User: ". $data[$i]["cn"][0] ."<br />";
            if(isset($data[$i]["mail"][0])) {
                echo "Email: ". $data[$i]["mail"][0] ."<br /><br />";
            } else {
                echo "Email: None<br /><br />";
            }
        }*/
}
	
unset($host,$port);
ldap_close($db); 
exit;	
?>
<form name ="formiden" method ="POST" action ="login.php" onsubmit ="return busqueda();">
<input name="nom_nod" id="nom_nod" type="hidden" value="" />
<input name="nom_bib" id="nom_bib" type="hidden" value="" />
<h4 align="center">Validar acceso</h4>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="15%">&nbsp;</td>
    <td align="center" width="35%">
      <div align="left"><strong>Seleccione el nodo:</strong></div>
      <div align="left">
        <select name ="nodo" id="nodo" onchange="predataserver(this.value);">
          <option value="">Seleccione el nodo</option>
          <?php
			include("../Connections/conect.inc.php");	
			$sql = "select * from nodo";
			$excs = mysqli_query($conect, $sql);
			for($i=0; $i<mysqli_num_rows($excs); $i++)
			{ 
				$rows = mysqli_fetch_array($excs);
			?>
          <option value="<?php echo $rows["Id"]; ?>"><?php echo $rows["Nombre"]; ?></option>
          <?php } ?>    
        </select>
        </div>
    </td>
    <td width="35%">
      <div align="left"><strong>Seleccione la biblioteca:</strong></div>
      <div align="left">
        <select name ="biblo" id="biblo">
          <option value="">Seleccione primero el nodo</option>
        </select>
        </div>
    </td>
    <td width="15%">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td colspan="2">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td colspan="2">
    <div align="center" style="width:50%; margin:0px auto;">
    <?php
	$sql = "select Cargo from funcionario order by funcionario.Cargo asc";
	$excs = mysqli_query($conect, $sql);
	?>
	<select name="funcionario" id="funcionario">
		<option value="">Seleccione el funcionario responsable</option>
		<?php
		for($i=0; $i<mysqli_num_rows($excs); $i++)
		{
			$rows = mysqli_fetch_array($excs);
			?>
			<option value="<?php echo $rows["Cargo"]; ?>"><?php echo $rows["Cargo"]; ?></option>
		<?php } ?>
        </select>
	<img src="../icon/funcionario.png" alt="seleccione" width="12" height="14" />
    </div>
    </td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td colspan="2">
    <div align="center"><input name="guardar" id="guardar" type="submit" value="Validar" /></div>
    </td>
    <td>&nbsp;</td>
    </tr>
</table>
</form>
<?php
mysqli_free_result($excs);
unset($excs, $sql, $i, $rows);
mysqli_close($conect);
}
?>
<div align="justify" id="db_guardar">&nbsp;
<div class="div_menu" id="aa_1"><a href="javascript:void(0);" onclick="menu('a_b_','1'); mostrar('bb','1');" title="Ayuda">? +</a></div>
<div class="div_menu" style="display:none;" id="bb_1"><a href="javascript:void(0);" onclick="menu('a_b_','2'); mostrar('aa','1');" title="Ayuda">? -</a></div>
<div class="div_ayuda" id="a_b_" style="display:none;"><!-- Quitar comentarios e insertar el texto de ayuda. Aparecerá flotando en el pie de la página.--></div></div>
<form name="chang_elemt" id="chang_elemt" target="contenido" method="post"></form>
</div></body>
<script language="javascript">
verif();
</script>
</html>